/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_read.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 13:56:15 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 13:58:51 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void
	print_argv(int ac, char **av)
{
	while (ac)
	{
		--ac;
		printf("index: %d, argv: %s\n", ac, av[ac]);
		fflush(stdout);
	}
}

int
	*ft_(int *ac, char **av)
{
	int		i;
	int		*fd;

	i = 0;
	if (*ac > 0)
	{
		fd = (int *)malloc(sizeof(int) * *ac);
		i = 0;
		while (i < *ac)
		{
			fd[i] = atoi(av[i + 1]);
			++i;
		}
	}
	else
	{
		*ac = 1;
		fd = (int *)malloc(sizeof(int));
		*fd = STDIN_FILENO;
	}
	return (fd);
}

int
	main(int ac, char **av)
{
	int		*fd;
	int		n;
	int		i;
	char	buff[1026];

	print_argv(ac, av);
	--ac;
	fd = ft_(&ac, av);
	n = 1;
	while (n > 0)
	{
		i = -1;
		while (++i < ac)
		{
			memset(buff, 0, sizeof(char) * 1025);
			printf("fd: %d, ", fd[i]);
			fflush(stdout);
			n = read(fd[i], buff, 1024);
			printf("%d: %s\n", n, buff);
			fflush(stdout);
		}
	}
	free(fd);
	return (0);
}
