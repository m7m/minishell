echo 'echo'

shtest "echo"
shtest "echo   "
shtest "echo -n"
shtest "echo -e"
shtest "echo -E"
shtest "echo -h"
shtest "echo -h o"
shtest "echo -nz"
shtest "echo -enz"
shtest "echo -zen"
shtest "echo -en 'a\tb'"
shtest "echo -ne 'a\t\nb'"
shtest "echo -nE 'a\t\nb'"
shtest "echo -eE 'a\t\nb'"
shtest "echo -Ee 'a\t\nb'"
shtest "echo -ee 'a\t\nb'"
shtest "echo -e -e 'a\t\nb'"
shtest "echo -E -e 'a\t\nb'"
shtest "echo -e -E 'a\t\nb'"

shtest "echo 1"
shtest "echo  1 "
shtest "echo -n 1"
shtest "echo -e 1"
shtest "echo -E 1"

shtest "echo 1 2"
shtest "echo  1 2 "
shtest "echo -n 1 2"
shtest "echo -e 1 2"
shtest "echo -E 1 2"

shtest "echo 1      2"
shtest "echo  1      2 "
shtest "echo -n 1      2"
shtest "echo -e 1      2"
shtest "echo -E 1      2"

shtest "echo 1			2"
shtest "echo  1			2 "
shtest "echo -n 1			2"
shtest "echo -e 1			2"
shtest "echo -E 1			2"

shtest "echo 1	 	 	 2"
shtest "echo  1	 	 	 2 "
shtest "echo -n 1	 	 	 2"
shtest "echo -e 1	 	 	 2"
shtest "echo -E 1	 	 	 2"


shtest "echo 1      2      "
shtest "echo -n 1      2      "
shtest "echo -e 1      2      "
shtest "echo -E 1      2      "

shtest "echo 1			2			3"
shtest "echo  1			2 3"
shtest "echo -n 1			2			3"
shtest "echo -e 1			2			3"
shtest "echo -E 1			2			3"

shtest "echo 1	 	 	 2	 	 	3 "
shtest "echo  1	 	 	 2 3 "
shtest "echo -n 1	 	 	 2	 	 	3 "
shtest "echo -e 1	 	 	 2	 	 	3 "
shtest "echo -E 1	 	 	 2	 	 	3 "

shtest "echo $HOME"
shtest "echo  $HOME "
shtest "echo -n $HOME"
shtest "echo -e $HOME"
shtest "echo -E $HOME"

shtest "echo \$HOME"
shtest "echo  \$HOME "
shtest "echo -n \$HOME"
shtest "echo -e \$HOME"
shtest "echo -E \$HOME"

shtest "echo '$HOME'"
shtest "echo  '$HOME' "
shtest "echo -n '$HOME'"
shtest "echo -e '$HOME'"
shtest "echo -E '$HOME'"

shtest "echo '\$HOME'"
shtest "echo  '\$HOME' "
shtest "echo -n '\$HOME'"
shtest "echo -e '\$HOME'"
shtest "echo -E '\$HOME'"

shtest 'echo "$HOME"'
shtest 'echo  "$HOME" '
shtest 'echo -n "$HOME"'
shtest 'echo -e "$HOME"'
shtest 'echo -E "$HOME"'

shtest 'echo "\$HOME"'
shtest 'echo  "\$HOME" '
shtest 'echo -n "\$HOME"'
shtest 'echo -e "\$HOME"'
shtest 'echo -E "\$HOME"'

# anti slash simple
{
	# anti slash simple
	{
		shtest 'echo \a'
		shtest 'echo    \a'
		shtest 'echo -n \a'
		shtest 'echo -e \a'
		shtest 'echo -E \a'

		shtest 'echo \b'
		shtest 'echo    \b'
		shtest 'echo -n \b'
		shtest 'echo -e \b'
		shtest 'echo -E \b'

		shtest 'echo \c'
		shtest 'echo    \c'
		shtest 'echo -n \c'
		shtest 'echo -e \c'
		shtest 'echo -E \c'

		shtest 'echo \z'
		shtest 'echo    \z'
		shtest 'echo -n \z'
		shtest 'echo -e \z'
		shtest 'echo -E \z'

		shtest 'echo \f'
		shtest 'echo    \f'
		shtest 'echo -n \f'
		shtest 'echo -e \f'
		shtest 'echo -E \f'

		shtest 'echo \n'
		shtest 'echo    \n'
		shtest 'echo -n \n'
		shtest 'echo -e \n'
		shtest 'echo -E \n'

		shtest 'echo \r'
		shtest 'echo    \r'
		shtest 'echo -n \r'
		shtest 'echo -e \r'
		shtest 'echo -E \r'

		shtest 'echo \t'
		shtest 'echo    \t'
		shtest 'echo -n \t'
		shtest 'echo -e \t'
		shtest 'echo -E \t'

		shtest 'echo \v'
		shtest 'echo    \v'
		shtest 'echo -n \v'
		shtest 'echo -e \v'
		shtest 'echo -E \v'

		shtest 'echo \\'
		shtest 'echo    \\'
		shtest 'echo -n \\'
		shtest 'echo -e \\'
		shtest 'echo -E \\'

		shtest 'echo \064\062'
		shtest 'echo    \064\062'
		shtest 'echo -n \064\062'
		shtest 'echo -e \064\062'
		shtest 'echo -E \064\062'

		shtest 'echo \'
		shtest 'echo    \'
		shtest 'echo -n \'
		shtest 'echo -e \'
		shtest 'echo -E \'
	}

	# x + anti slash
	{
		shtest 'echo g\a'
		shtest 'echo    g\a'
		shtest 'echo -n g\a'
		shtest 'echo -e g\a'
		shtest 'echo -E g\a'

		shtest 'echo g\b'
		shtest 'echo    g\b'
		shtest 'echo -n g\b'
		shtest 'echo -e g\b'
		shtest 'echo -E g\b'

		shtest 'echo g\c'
		shtest 'echo    g\c'
		shtest 'echo -n g\c'
		shtest 'echo -e g\c'
		shtest 'echo -E g\c'

		shtest 'echo g\z'
		shtest 'echo    g\z'
		shtest 'echo -n g\z'
		shtest 'echo -e g\z'
		shtest 'echo -E g\z'

		shtest 'echo g\f'
		shtest 'echo    g\f'
		shtest 'echo -n g\f'
		shtest 'echo -e g\f'
		shtest 'echo -E g\f'

		shtest 'echo g\n'
		shtest 'echo    g\n'
		shtest 'echo -n g\n'
		shtest 'echo -e g\n'
		shtest 'echo -E g\n'

		shtest 'echo g\r'
		shtest 'echo    g\r'
		shtest 'echo -n g\r'
		shtest 'echo -e g\r'
		shtest 'echo -E g\r'

		shtest 'echo g\t'
		shtest 'echo    g\t'
		shtest 'echo -n g\t'
		shtest 'echo -e g\t'
		shtest 'echo -E g\t'

		shtest 'echo g\v'
		shtest 'echo    g\v'
		shtest 'echo -n g\v'
		shtest 'echo -e g\v'
		shtest 'echo -E g\v'

		shtest 'echo g\\'
		shtest 'echo    g\\'
		shtest 'echo -n g\\'
		shtest 'echo -e g\\'
		shtest 'echo -E g\\'

		shtest 'echo g\064\062'
		shtest 'echo    g\064\062'
		shtest 'echo -n g\064\062'
		shtest 'echo -e g\064\062'
		shtest 'echo -E g\064\062'

		shtest 'echo g\'
		shtest 'echo    g\'
		shtest 'echo -n g\'
		shtest 'echo -e g\'
		shtest 'echo -E g\'
	}

	# anti slash + x
	{
		shtest 'echo \ad'
		shtest 'echo    \ad'
		shtest 'echo -n \ad'
		shtest 'echo -e \ad'
		shtest 'echo -E \ad'

		shtest 'echo \bd'
		shtest 'echo    \bd'
		shtest 'echo -n \bd'
		shtest 'echo -e \bd'
		shtest 'echo -E \bd'

		shtest 'echo \cd'
		shtest 'echo    \cd'
		shtest 'echo -n \cd'
		shtest 'echo -e \cd'
		shtest 'echo -E \cd'

		shtest 'echo \zd'
		shtest 'echo    \zd'
		shtest 'echo -n \zd'
		shtest 'echo -e \zd'
		shtest 'echo -E \zd'

		shtest 'echo \fd'
		shtest 'echo    \fd'
		shtest 'echo -n \fd'
		shtest 'echo -e \fd'
		shtest 'echo -E \fd'

		shtest 'echo \nd'
		shtest 'echo    \nd'
		shtest 'echo -n \nd'
		shtest 'echo -e \nd'
		shtest 'echo -E \nd'

		shtest 'echo \rd'
		shtest 'echo    \rd'
		shtest 'echo -n \rd'
		shtest 'echo -e \rd'
		shtest 'echo -E \rd'

		shtest 'echo \td'
		shtest 'echo    \td'
		shtest 'echo -n \td'
		shtest 'echo -e \td'
		shtest 'echo -E \td'

		shtest 'echo \vd'
		shtest 'echo    \vd'
		shtest 'echo -n \vd'
		shtest 'echo -e \vd'
		shtest 'echo -E \vd'

		shtest 'echo \\d'
		shtest 'echo    \\d'
		shtest 'echo -n \\d'
		shtest 'echo -e \\d'
		shtest 'echo -E \\d'

		shtest 'echo \064\062d'
		shtest 'echo    \064\062d'
		shtest 'echo -n \064\062d'
		shtest 'echo -e \064\062d'
		shtest 'echo -E \064\062d'

		shtest 'echo \d'
		shtest 'echo    \d'
		shtest 'echo -n \d'
		shtest 'echo -e \d'
		shtest 'echo -E \d'
	}

	# x + anti slash + x
	{
		shtest 'echo g\ad'
		shtest 'echo    g\ad'
		shtest 'echo -n g\ad'
		shtest 'echo -e g\ad'
		shtest 'echo -E g\ad'

		shtest 'echo g\bd'
		shtest 'echo    g\bd'
		shtest 'echo -n g\bd'
		shtest 'echo -e g\bd'
		shtest 'echo -E g\bd'

		shtest 'echo g\cd'
		shtest 'echo    g\cd'
		shtest 'echo -n g\cd'
		shtest 'echo -e g\cd'
		shtest 'echo -E g\cd'

		shtest 'echo g\zd'
		shtest 'echo    g\zd'
		shtest 'echo -n g\zd'
		shtest 'echo -e g\zd'
		shtest 'echo -E g\zd'

		shtest 'echo g\fd'
		shtest 'echo    g\fd'
		shtest 'echo -n g\fd'
		shtest 'echo -e g\fd'
		shtest 'echo -E g\fd'

		shtest 'echo g\nd'
		shtest 'echo    g\nd'
		shtest 'echo -n g\nd'
		shtest 'echo -e g\nd'
		shtest 'echo -E g\nd'

		shtest 'echo g\rd'
		shtest 'echo    g\rd'
		shtest 'echo -n g\rd'
		shtest 'echo -e g\rd'
		shtest 'echo -E g\rd'

		shtest 'echo g\td'
		shtest 'echo    g\td'
		shtest 'echo -n g\td'
		shtest 'echo -e g\td'
		shtest 'echo -E g\td'

		shtest 'echo g\vd'
		shtest 'echo    g\vd'
		shtest 'echo -n g\vd'
		shtest 'echo -e g\vd'
		shtest 'echo -E g\vd'

		shtest 'echo g\\d'
		shtest 'echo    g\\d'
		shtest 'echo -n g\\d'
		shtest 'echo -e g\\d'
		shtest 'echo -E g\\d'

		shtest 'echo g\064\062d'
		shtest 'echo    g\064\062d'
		shtest 'echo -n g\064\062d'
		shtest 'echo -e g\064\062d'
		shtest 'echo -E g\064\062d'

		shtest 'echo g\d'
		shtest 'echo    g\d'
		shtest 'echo -n g\d'
		shtest 'echo -e g\d'
		shtest 'echo -E g\d'
	}
}

# "anti slash"
{
	# anti slash simple
	{
		shtest 'echo "g\ad"'
		shtest 'echo    "g\ad"'
		shtest 'echo -n "g\ad"'
		shtest 'echo -e "g\ad"'
		shtest 'echo -E "g\ad"'

		shtest 'echo "g\bd"'
		shtest 'echo    "g\bd"'
		shtest 'echo -n "g\bd"'
		shtest 'echo -e "g\bd"'
		shtest 'echo -E "g\bd"'

		shtest 'echo "g\cd"'
		shtest 'echo    "g\cd"'
		shtest 'echo -n "g\cd"'
		shtest 'echo -e "g\cd"'
		shtest 'echo -E "g\cd"'

		shtest 'echo "g\zd"'
		shtest 'echo    "g\zd"'
		shtest 'echo -n "g\zd"'
		shtest 'echo -e "g\zd"'
		shtest 'echo -E "g\zd"'

		shtest 'echo "g\fd"'
		shtest 'echo    "g\fd"'
		shtest 'echo -n "g\fd"'
		shtest 'echo -e "g\fd"'
		shtest 'echo -E "g\fd"'

		shtest 'echo "g\nd"'
		shtest 'echo    "g\nd"'
		shtest 'echo -n "g\nd"'
		shtest 'echo -e "g\nd"'
		shtest 'echo -E "g\nd"'

		shtest 'echo "g\rd"'
		shtest 'echo    "g\rd"'
		shtest 'echo -n "g\rd"'
		shtest 'echo -e "g\rd"'
		shtest 'echo -E "g\rd"'

		shtest 'echo "g\td"'
		shtest 'echo    "g\td"'
		shtest 'echo -n "g\td"'
		shtest 'echo -e "g\td"'
		shtest 'echo -E "g\td"'

		shtest 'echo "g\vd"'
		shtest 'echo    "g\vd"'
		shtest 'echo -n "g\vd"'
		shtest 'echo -e "g\vd"'
		shtest 'echo -E "g\vd"'

		shtest 'echo "g\\d"'
		shtest 'echo    "g\\d"'
		shtest 'echo -n "g\\d"'
		shtest 'echo -e "g\\d"'
		shtest 'echo -E "g\\d"'

		shtest 'echo "g\064\062d"'
		shtest 'echo    "g\064\062d"'
		shtest 'echo -n "g\064\062d"'
		shtest 'echo -e "g\064\062d"'
		shtest 'echo -E "g\064\062d"'

		shtest 'echo "g\d"'
		shtest 'echo    "g\d"'
		shtest 'echo -n "g\d"'
		shtest 'echo -e "g\d"'
		shtest 'echo -E "g\d"'
	}

	# x + anti slash
	{
		shtest 'echo "gg\ad"'
		shtest 'echo    "gg\ad"'
		shtest 'echo -n "gg\ad"'
		shtest 'echo -e "gg\ad"'
		shtest 'echo -E "gg\ad"'

		shtest 'echo "gg\bd"'
		shtest 'echo    "gg\bd"'
		shtest 'echo -n "gg\bd"'
		shtest 'echo -e "gg\bd"'
		shtest 'echo -E "gg\bd"'

		shtest 'echo "gg\cd"'
		shtest 'echo    "gg\cd"'
		shtest 'echo -n "gg\cd"'
		shtest 'echo -e "gg\cd"'
		shtest 'echo -E "gg\cd"'

		shtest 'echo "gg\zd"'
		shtest 'echo    "gg\zd"'
		shtest 'echo -n "gg\zd"'
		shtest 'echo -e "gg\zd"'
		shtest 'echo -E "gg\zd"'

		shtest 'echo "gg\fd"'
		shtest 'echo    "gg\fd"'
		shtest 'echo -n "gg\fd"'
		shtest 'echo -e "gg\fd"'
		shtest 'echo -E "gg\fd"'

		shtest 'echo "gg\nd"'
		shtest 'echo    "gg\nd"'
		shtest 'echo -n "gg\nd"'
		shtest 'echo -e "gg\nd"'
		shtest 'echo -E "gg\nd"'

		shtest 'echo "gg\rd"'
		shtest 'echo    "gg\rd"'
		shtest 'echo -n "gg\rd"'
		shtest 'echo -e "gg\rd"'
		shtest 'echo -E "gg\rd"'

		shtest 'echo "gg\td"'
		shtest 'echo    "gg\td"'
		shtest 'echo -n "gg\td"'
		shtest 'echo -e "gg\td"'
		shtest 'echo -E "gg\td"'

		shtest 'echo "gg\vd"'
		shtest 'echo    "gg\vd"'
		shtest 'echo -n "gg\vd"'
		shtest 'echo -e "gg\vd"'
		shtest 'echo -E "gg\vd"'

		shtest 'echo "gg\\d"'
		shtest 'echo    "gg\\d"'
		shtest 'echo -n "gg\\d"'
		shtest 'echo -e "gg\\d"'
		shtest 'echo -E "gg\\d"'

		shtest 'echo "gg\064\062d"'
		shtest 'echo    "gg\064\062d"'
		shtest 'echo -n "gg\064\062d"'
		shtest 'echo -e "gg\064\062d"'
		shtest 'echo -E "gg\064\062d"'

		shtest 'echo "gg\d"'
		shtest 'echo    "gg\d"'
		shtest 'echo -n "gg\d"'
		shtest 'echo -e "gg\d"'
		shtest 'echo -E "gg\d"'
	}

	# anti slash + x
	{
		shtest 'echo "g\add"'
		shtest 'echo    "g\add"'
		shtest 'echo -n "g\add"'
		shtest 'echo -e "g\add"'
		shtest 'echo -E "g\add"'

		shtest 'echo "g\bdd"'
		shtest 'echo    "g\bdd"'
		shtest 'echo -n "g\bdd"'
		shtest 'echo -e "g\bdd"'
		shtest 'echo -E "g\bdd"'

		shtest 'echo "g\cdd"'
		shtest 'echo    "g\cdd"'
		shtest 'echo -n "g\cdd"'
		shtest 'echo -e "g\cdd"'
		shtest 'echo -E "g\cdd"'

		shtest 'echo "g\zdd"'
		shtest 'echo    "g\zdd"'
		shtest 'echo -n "g\zdd"'
		shtest 'echo -e "g\zdd"'
		shtest 'echo -E "g\zdd"'

		shtest 'echo "g\fdd"'
		shtest 'echo    "g\fdd"'
		shtest 'echo -n "g\fdd"'
		shtest 'echo -e "g\fdd"'
		shtest 'echo -E "g\fdd"'

		shtest 'echo "g\ndd"'
		shtest 'echo    "g\ndd"'
		shtest 'echo -n "g\ndd"'
		shtest 'echo -e "g\ndd"'
		shtest 'echo -E "g\ndd"'

		shtest 'echo "g\rdd"'
		shtest 'echo    "g\rdd"'
		shtest 'echo -n "g\rdd"'
		shtest 'echo -e "g\rdd"'
		shtest 'echo -E "g\rdd"'

		shtest 'echo "g\tdd"'
		shtest 'echo    "g\tdd"'
		shtest 'echo -n "g\tdd"'
		shtest 'echo -e "g\tdd"'
		shtest 'echo -E "g\tdd"'

		shtest 'echo "g\vdd"'
		shtest 'echo    "g\vdd"'
		shtest 'echo -n "g\vdd"'
		shtest 'echo -e "g\vdd"'
		shtest 'echo -E "g\vdd"'

		shtest 'echo "g\\dd"'
		shtest 'echo    "g\\dd"'
		shtest 'echo -n "g\\dd"'
		shtest 'echo -e "g\\dd"'
		shtest 'echo -E "g\\dd"'

		shtest 'echo "g\064\062dd"'
		shtest 'echo    "g\064\062dd"'
		shtest 'echo -n "g\064\062dd"'
		shtest 'echo -e "g\064\062dd"'
		shtest 'echo -E "g\064\062dd"'

		shtest 'echo "g\dd"'
		shtest 'echo    "g\dd"'
		shtest 'echo -n "g\dd"'
		shtest 'echo -e "g\dd"'
		shtest 'echo -E "g\dd"'
	}

	# x + anti slash + x
	{
		shtest 'echo "gg\add"'
		shtest 'echo    "gg\add"'
		shtest 'echo -n "gg\add"'
		shtest 'echo -e "gg\add"'
		shtest 'echo -E "gg\add"'

		shtest 'echo "gg\bdd"'
		shtest 'echo    "gg\bdd"'
		shtest 'echo -n "gg\bdd"'
		shtest 'echo -e "gg\bdd"'
		shtest 'echo -E "gg\bdd"'

		shtest 'echo "gg\cdd"'
		shtest 'echo    "gg\cdd"'
		shtest 'echo -n "gg\cdd"'
		shtest 'echo -e "gg\cdd"'
		shtest 'echo -E "gg\cdd"'

		shtest 'echo "gg\zdd"'
		shtest 'echo    "gg\zdd"'
		shtest 'echo -n "gg\zdd"'
		shtest 'echo -e "gg\zdd"'
		shtest 'echo -E "gg\zdd"'

		shtest 'echo "gg\fdd"'
		shtest 'echo    "gg\fdd"'
		shtest 'echo -n "gg\fdd"'
		shtest 'echo -e "gg\fdd"'
		shtest 'echo -E "gg\fdd"'

		shtest 'echo "gg\ndd"'
		shtest 'echo    "gg\ndd"'
		shtest 'echo -n "gg\ndd"'
		shtest 'echo -e "gg\ndd"'
		shtest 'echo -E "gg\ndd"'

		shtest 'echo "gg\rdd"'
		shtest 'echo    "gg\rdd"'
		shtest 'echo -n "gg\rdd"'
		shtest 'echo -e "gg\rdd"'
		shtest 'echo -E "gg\rdd"'

		shtest 'echo "gg\tdd"'
		shtest 'echo    "gg\tdd"'
		shtest 'echo -n "gg\tdd"'
		shtest 'echo -e "gg\tdd"'
		shtest 'echo -E "gg\tdd"'

		shtest 'echo "gg\vdd"'
		shtest 'echo    "gg\vdd"'
		shtest 'echo -n "gg\vdd"'
		shtest 'echo -e "gg\vdd"'
		shtest 'echo -E "gg\vdd"'

		shtest 'echo "gg\\dd"'
		shtest 'echo    "gg\\dd"'
		shtest 'echo -n "gg\\dd"'
		shtest 'echo -e "gg\\dd"'
		shtest 'echo -E "gg\\dd"'

		shtest 'echo "gg\064\062dd"'
		shtest 'echo    "gg\064\062dd"'
		shtest 'echo -n "gg\064\062dd"'
		shtest 'echo -e "gg\064\062dd"'
		shtest 'echo -E "gg\064\062dd"'

		shtest 'echo "gg\dd"'
		shtest 'echo    "gg\dd"'
		shtest 'echo -n "gg\dd"'
		shtest 'echo -e "gg\dd"'
		shtest 'echo -E "gg\dd"'
	}
}

# 'anti slash'
{
	# anti slash simple
	{
		shtest "echo 'g\ad'"
		shtest "echo    'g\ad'"
		shtest "echo -n 'g\ad'"
		shtest "echo -e 'g\ad'"
		shtest "echo -E 'g\ad'"

		shtest "echo 'g\bd'"
		shtest "echo    'g\bd'"
		shtest "echo -n 'g\bd'"
		shtest "echo -e 'g\bd'"
		shtest "echo -E 'g\bd'"

		shtest "echo 'g\cd'"
		shtest "echo    'g\cd'"
		shtest "echo -n 'g\cd'"
		shtest "echo -e 'g\cd'"
		shtest "echo -E 'g\cd'"

		shtest "echo 'g\zd'"
		shtest "echo    'g\zd'"
		shtest "echo -n 'g\zd'"
		shtest "echo -e 'g\zd'"
		shtest "echo -E 'g\zd'"

		shtest "echo 'g\fd'"
		shtest "echo    'g\fd'"
		shtest "echo -n 'g\fd'"
		shtest "echo -e 'g\fd'"
		shtest "echo -E 'g\fd'"

		shtest "echo 'g\nd'"
		shtest "echo    'g\nd'"
		shtest "echo -n 'g\nd'"
		shtest "echo -e 'g\nd'"
		shtest "echo -E 'g\nd'"

		shtest "echo 'g\rd'"
		shtest "echo    'g\rd'"
		shtest "echo -n 'g\rd'"
		shtest "echo -e 'g\rd'"
		shtest "echo -E 'g\rd'"

		shtest "echo 'g\td'"
		shtest "echo    'g\td'"
		shtest "echo -n 'g\td'"
		shtest "echo -e 'g\td'"
		shtest "echo -E 'g\td'"

		shtest "echo 'g\vd'"
		shtest "echo    'g\vd'"
		shtest "echo -n 'g\vd'"
		shtest "echo -e 'g\vd'"
		shtest "echo -E 'g\vd'"

		shtest "echo 'g\\d'"
		shtest "echo    'g\\d'"
		shtest "echo -n 'g\\d'"
		shtest "echo -e 'g\\d'"
		shtest "echo -E 'g\\d'"

		shtest "echo 'g\064\062d'"
		shtest "echo    'g\064\062d'"
		shtest "echo -n 'g\064\062d'"
		shtest "echo -e 'g\064\062d'"
		shtest "echo -E 'g\064\062d'"

		shtest "echo 'g\d'"
		shtest "echo    'g\d'"
		shtest "echo -n 'g\d'"
		shtest "echo -e 'g\d'"
		shtest "echo -E 'g\d'"
	}

	# x + anti slash
	{
		shtest "echo 'gg\ad'"
		shtest "echo    'gg\ad'"
		shtest "echo -n 'gg\ad'"
		shtest "echo -e 'gg\ad'"
		shtest "echo -E 'gg\ad'"

		shtest "echo 'gg\bd'"
		shtest "echo    'gg\bd'"
		shtest "echo -n 'gg\bd'"
		shtest "echo -e 'gg\bd'"
		shtest "echo -E 'gg\bd'"

		shtest "echo 'gg\cd'"
		shtest "echo    'gg\cd'"
		shtest "echo -n 'gg\cd'"
		shtest "echo -e 'gg\cd'"
		shtest "echo -E 'gg\cd'"

		shtest "echo 'gg\zd'"
		shtest "echo    'gg\zd'"
		shtest "echo -n 'gg\zd'"
		shtest "echo -e 'gg\zd'"
		shtest "echo -E 'gg\zd'"

		shtest "echo 'gg\fd'"
		shtest "echo    'gg\fd'"
		shtest "echo -n 'gg\fd'"
		shtest "echo -e 'gg\fd'"
		shtest "echo -E 'gg\fd'"

		shtest "echo 'gg\nd'"
		shtest "echo    'gg\nd'"
		shtest "echo -n 'gg\nd'"
		shtest "echo -e 'gg\nd'"
		shtest "echo -E 'gg\nd'"

		shtest "echo 'gg\rd'"
		shtest "echo    'gg\rd'"
		shtest "echo -n 'gg\rd'"
		shtest "echo -e 'gg\rd'"
		shtest "echo -E 'gg\rd'"

		shtest "echo 'gg\td'"
		shtest "echo    'gg\td'"
		shtest "echo -n 'gg\td'"
		shtest "echo -e 'gg\td'"
		shtest "echo -E 'gg\td'"

		shtest "echo 'gg\vd'"
		shtest "echo    'gg\vd'"
		shtest "echo -n 'gg\vd'"
		shtest "echo -e 'gg\vd'"
		shtest "echo -E 'gg\vd'"

		shtest "echo 'gg\\d'"
		shtest "echo    'gg\\d'"
		shtest "echo -n 'gg\\d'"
		shtest "echo -e 'gg\\d'"
		shtest "echo -E 'gg\\d'"

		shtest "echo 'gg\064\062d'"
		shtest "echo    'gg\064\062d'"
		shtest "echo -n 'gg\064\062d'"
		shtest "echo -e 'gg\064\062d'"
		shtest "echo -E 'gg\064\062d'"

		shtest "echo 'gg\d'"
		shtest "echo    'gg\d'"
		shtest "echo -n 'gg\d'"
		shtest "echo -e 'gg\d'"
		shtest "echo -E 'gg\d'"
	}

	# anti slash + x
	{
		shtest "echo 'g\add'"
		shtest "echo    'g\add'"
		shtest "echo -n 'g\add'"
		shtest "echo -e 'g\add'"
		shtest "echo -E 'g\add'"

		shtest "echo 'g\bdd'"
		shtest "echo    'g\bdd'"
		shtest "echo -n 'g\bdd'"
		shtest "echo -e 'g\bdd'"
		shtest "echo -E 'g\bdd'"

		shtest "echo 'g\cdd'"
		shtest "echo    'g\cdd'"
		shtest "echo -n 'g\cdd'"
		shtest "echo -e 'g\cdd'"
		shtest "echo -E 'g\cdd'"

		shtest "echo 'g\zdd'"
		shtest "echo    'g\zdd'"
		shtest "echo -n 'g\zdd'"
		shtest "echo -e 'g\zdd'"
		shtest "echo -E 'g\zdd'"

		shtest "echo 'g\fdd'"
		shtest "echo    'g\fdd'"
		shtest "echo -n 'g\fdd'"
		shtest "echo -e 'g\fdd'"
		shtest "echo -E 'g\fdd'"

		shtest "echo 'g\ndd'"
		shtest "echo    'g\ndd'"
		shtest "echo -n 'g\ndd'"
		shtest "echo -e 'g\ndd'"
		shtest "echo -E 'g\ndd'"

		shtest "echo 'g\rdd'"
		shtest "echo    'g\rdd'"
		shtest "echo -n 'g\rdd'"
		shtest "echo -e 'g\rdd'"
		shtest "echo -E 'g\rdd'"

		shtest "echo 'g\tdd'"
		shtest "echo    'g\tdd'"
		shtest "echo -n 'g\tdd'"
		shtest "echo -e 'g\tdd'"
		shtest "echo -E 'g\tdd'"

		shtest "echo 'g\vdd'"
		shtest "echo    'g\vdd'"
		shtest "echo -n 'g\vdd'"
		shtest "echo -e 'g\vdd'"
		shtest "echo -E 'g\vdd'"

		shtest "echo 'g\\dd'"
		shtest "echo    'g\\dd'"
		shtest "echo -n 'g\\dd'"
		shtest "echo -e 'g\\dd'"
		shtest "echo -E 'g\\dd'"

		shtest "echo 'g\064\062dd'"
		shtest "echo    'g\064\062dd'"
		shtest "echo -n 'g\064\062dd'"
		shtest "echo -e 'g\064\062dd'"
		shtest "echo -E 'g\064\062dd'"

		shtest "echo 'g\dd'"
		shtest "echo    'g\dd'"
		shtest "echo -n 'g\dd'"
		shtest "echo -e 'g\dd'"
		shtest "echo -E 'g\dd'"
	}

	# x + anti slash + x
	{
		shtest "echo 'gg\add'"
		shtest "echo    'gg\add'"
		shtest "echo -n 'gg\add'"
		shtest "echo -e 'gg\add'"
		shtest "echo -E 'gg\add'"

		shtest "echo 'gg\bdd'"
		shtest "echo    'gg\bdd'"
		shtest "echo -n 'gg\bdd'"
		shtest "echo -e 'gg\bdd'"
		shtest "echo -E 'gg\bdd'"

		shtest "echo 'gg\cdd'"
		shtest "echo    'gg\cdd'"
		shtest "echo -n 'gg\cdd'"
		shtest "echo -e 'gg\cdd'"
		shtest "echo -E 'gg\cdd'"

		shtest "echo 'gg\zdd'"
		shtest "echo    'gg\zdd'"
		shtest "echo -n 'gg\zdd'"
		shtest "echo -e 'gg\zdd'"
		shtest "echo -E 'gg\zdd'"

		shtest "echo 'gg\fdd'"
		shtest "echo    'gg\fdd'"
		shtest "echo -n 'gg\fdd'"
		shtest "echo -e 'gg\fdd'"
		shtest "echo -E 'gg\fdd'"

		shtest "echo 'gg\ndd'"
		shtest "echo    'gg\ndd'"
		shtest "echo -n 'gg\ndd'"
		shtest "echo -e 'gg\ndd'"
		shtest "echo -E 'gg\ndd'"

		shtest "echo 'gg\rdd'"
		shtest "echo    'gg\rdd'"
		shtest "echo -n 'gg\rdd'"
		shtest "echo -e 'gg\rdd'"
		shtest "echo -E 'gg\rdd'"

		shtest "echo 'gg\tdd'"
		shtest "echo    'gg\tdd'"
		shtest "echo -n 'gg\tdd'"
		shtest "echo -e 'gg\tdd'"
		shtest "echo -E 'gg\tdd'"

		shtest "echo 'gg\vdd'"
		shtest "echo    'gg\vdd'"
		shtest "echo -n 'gg\vdd'"
		shtest "echo -e 'gg\vdd'"
		shtest "echo -E 'gg\vdd'"

		shtest "echo 'gg\\dd'"
		shtest "echo    'gg\\dd'"
		shtest "echo -n 'gg\\dd'"
		shtest "echo -e 'gg\\dd'"
		shtest "echo -E 'gg\\dd'"

		shtest "echo 'gg\064\062dd'"
		shtest "echo    'gg\064\062dd'"
		shtest "echo -n 'gg\064\062dd'"
		shtest "echo -e 'gg\064\062dd'"
		shtest "echo -E 'gg\064\062dd'"

		shtest "echo 'gg\dd'"
		shtest "echo    'gg\dd'"
		shtest "echo -n 'gg\dd'"
		shtest "echo -e 'gg\dd'"
		shtest "echo -E 'gg\dd'"
	}
}
