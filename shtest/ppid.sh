#!/bin/sh

# utilise le ppid
ppid_gnulinux()
{
	opt="-o"

	OPT_OSX="pid,ppid,pgid,stat,command"
	OPT_GNU="pid,ppid,pgrp,stat,cmd"

	if [ $(uname -s) == "Darwin" ];then
		OPT1ARG="$OPT_OSX"
		OPT2ARG="$OPT_OSX --ppid" # option --ppid inexistant
	else
		OPT1ARG="$OPT_GNU"
		OPT2ARG="$OPT_GNU --ppid"
	fi

	ppid() {
		if [ -z "$2" ];then
			local i=0
		else
			opt="-ho"
			local i=$2
		fi
		ps $opt $OPT1ARG -p $1 \
			| awk -v i=$i '
BEGIN{
	if (i)
		t = "|" ;
	else
		t = "" ;
	for (j=0; j < i;j++) {
		t = "  "t ;
	}
}{
	print t$0
}'

		for pid in $(ps -ho $OPT2ARG $1 \
						 | awk -v ppid=$1 '($2 == ppid){print $1}')
		do
			ppid $pid $(($i+1))
		done
	}
	ppid $1
}

# utilise le tty
ppid_osx()
{
	ppid()
	{
		local i=$2
		local t=1

		ps -o pid,ppid,pgid,stat,command $1 | \
		if [ $i == 0 ];then
			cat
		else
			tail -n 1 
		fi | awk -v i=$i '
BEGIN{
	if (i)
		t = "|" ;
	else
		t = "" ;
	for (j=0; j < i;j++) {
		t = "  "t ;
	}
}{
	print t$0
}'

		tty=$(ps -o tty $1 | tail -n 1)
		for pid in $(ps -t $tty -o pid,ppid | awk -v ppid=$1 '{if (NR > 1) { if ($2 == ppid) { print $1 }}}')
		do
			ppid $pid $(($i+1))
		done
	}
	ppid $1 0
}

if [ "$(uname)" == "Darwin" ];then
	ppid_osx $1
else
	ppid_gnulinux $1
fi
