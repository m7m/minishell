#!/bin/bash

shtest "<<
"

shtest "<<f"
shtest "<< g" 
#"
shtest "<<" 
shtest "ls 3<<f 4<<f2 -d tt ff ."
shtest "<<&
"
shtest "<<&-
"
shtest "<<&"
shtest "<<&-"

# <<
{
	shtest "ls >f <<EOFF
truc
EOFF"
	shtest "ls >f <<EOFF
truc
"
	shtest "ls <<EOFF
"
	shtest "ls <<
"
	shtest "ls <<"
	shtest "cat <<EOFF  >f
truc
EOFF"
	shtest 'cat 2>f <<EOFF | cat -e                                                                                        
tr
EOFF'

	shtest "ls >f << EOFF
truc
EOFF"
	shtest "ls >f << EOFF
truc
"
	shtest "ls << EOFF"
	shtest "ls << EOFF "
	shtest "ls << "
	shtest "cat << EOFF  >f
truc
EOFF"
	shtest 'cat 2>f << EOFF | cat -e                                                                                        
tr
EOFF'
	shtest 'cat 2>f << 		    EOFF		    | cat -e                                                                                        
tr
EOFF'
	shtest 'cat 2>f<<EOFF|cat -e                                                                                        
tr
EOFF'
}

# <<-
{
	shtest "ls >f <<-EOFF
truc
EOFF"
	shtest "ls >f <<-EOFF
truc
"
	shtest "ls <<-EOFF"
	shtest "ls <<-EOFF
"
	shtest "ls <<-
"
	shtest "ls <<-"
	shtest "cat <<-EOFF  >f
truc
EOFF"
	shtest 'cat 2>f <<-EOFF | cat -e                                                                                        
tr
EOFF'

	shtest "ls >f <<- EOFF
truc
EOFF"
	shtest "ls >f <<- EOFF
truc
"
	shtest "ls <<- EOFF"
	shtest "cat <<- EOFF  >f
truc
EOFF"
	shtest 'cat 2>f <<- EOFF | cat -e                                                                                        
tr
EOFF'

	shtest 'cat 2>f <<- 		    EOFF		    | cat -e                                                                                        
tr
EOFF'
	shtest 'cat 2>f<<-EOFF |cat -e                                                                                        
tr
EOFF'


	shtest "ls >f << -EOFF
truc
EOFF"
	shtest "ls >f << -EOFF
truc
"
	shtest "ls << -EOFF 
"
	shtest "ls << 
"
	shtest "ls <<"
	shtest "cat << -EOFF  >f
truc
EOFF"
	shtest 'cat 2>f << -EOFF | cat -e                                                                                        
tr
EOFF'

	shtest 'cat 2>f << 		    -EOFF		    | cat -e                                                                                        
			tr
ads		t
EOFF'

	shtest 'cat -e <<-EOFF                                                                                        
			tr
ads		t
rr	
EOFF'
}


shtest './test_read 3 0 3<<f <<f2
j
f
k
f2
'

shtest './test_read 0 3 3<<f <<f2
j
f
k
f2
'

shtest './test_read 3 0 <<f 3<<f2
j
f
k
f2
'

shtest './test_read 0 3 <<f 3<<f2
j
f
k
f2
'

shtest './test_read 0 <<EOF
"$HOME
~
`ls -ld`
$(ls -ld)
end
EOF"
EOF
'

shtest './test_read 0 <<EOF
'\''$HOME
~
`ls -ld`
$(ls -ld)
end
EOF'\''
EOF
'


shtest './test_read 0 <<EOF
'\''$HOME
~
`ls -ld`
$(ls -ld)
()
)(
end
EOF'\''
EOF
'

shtest './test_read 0 <<EOF
0`ls "-ld`1
EOF
'
shtest './test_read 0 <<EOF
0$(ls "-ld)1
EOF
'

shtest 'echo ok;./test_read 0 <<EOF;echo ok
0`ls "-ld`1
EOF
'
shtest 'echo ok;./test_read 0 <<EOF;echo ok
0$(ls "-ld)1
EOF
'


shtest 'echo ok;./test_read 0 <<EOF;echo ok
0`ls "-ld`
EOF
'
shtest 'echo ok;./test_read 0 <<EOF;echo ok
`ls "-ld`1
EOF
'
shtest 'echo ok;./test_read 0 <<EOF;echo ok
`ls "-ld`
EOF
'


shtest 'echo ok;./test_read 0 <<EOF;echo ok
0$(ls "-ld)
EOF
'
shtest 'echo ok;./test_read 0 <<EOF;echo ok
$(ls "-ld)1
EOF
'
shtest 'echo ok;./test_read 0 <<EOF;echo ok
$(ls "-ld)
EOF
'


shtest 'echo ok;./test_read 0 <<EOF;echo ok
0`ls "-ld`1
EOF
'
shtest 'echo ok;./test_read 0 <<EOF;echo ok
0$(ls "-ld)1
EOF
'


shtest 'echo ok&&./test_read 0 <<EOF&&echo end$
0`ls "-ld`
EOF
'
shtest 'echo ok&&./test_read 0 <<EOF&&echo end$
`ls "-ld`1
EOF
'
shtest 'echo ok&&./test_read 0 <<EOF&&echo end$
`ls "-ld`
EOF
'


shtest 'echo ok&&./test_read 0 <<EOF&&echo end$
0$(ls "-ld)
EOF
'
shtest 'echo ok&&./test_read 0 <<EOF&&echo end$
$(ls "-ld)1
EOF
'
shtest 'echo ok&&./test_read 0 <<EOF&&echo end$
$(ls "-ld)
EOF
'

shtest 'cat -e <<`e`
e
`e`'

shtest 'cat -e <<<`ls /`'
shtest 'cat -e <<<a`ls /`'
shtest 'cat -e <<<`ls /`b'
shtest 'cat -e <<<a`ls /`b'
shtest 'cat -e <<< a`ls /`b'
#shtest 'cat -e <<< a `ls /` b'
shtest './test_read 5 10 5<<<a`ls -ld /`b'

shtest 'cat -e <<<`echo subshell`'
shtest 'cat -e <<<a`echo subshell`'
shtest 'cat -e <<<`echo subshell`b'
shtest 'cat -e <<<a`echo subshell`b'
shtest 'cat -e <<< a`echo subshell`b'
#shtest 'cat -e <<< a `echo subshell` b'
shtest './test_read 5 10 5<<<a`echo subshell`b'

shtest "cat << 	'EOF'  | ls
uu
EOF
"
shtest "cat << 'E1' ; echo <<E2
uu
E1
oo
E2
"
shtest 'cat <<E1
uu
E1
'
shtest 'cat <<`ls`
ee
`ls`
'
