shtest "./test_read -d tt . 2 < f"
shtest "./test_read -d tt . 1 < f"
shtest "./test_read -d tt 2 < 10000000"
shtest "2 < f"
shtest "2 < 1"
shtest " < f"
shtest " < "
shtest " < 
"

shtest "./test_read -d tt . 2< f"
shtest "./test_read -d tt . 1< f"
shtest "./test_read -d tt . 1< f 2< f2"
shtest "./test_read -d tt 1< f 2< f2"
shtest "./test_read 1< f 2< f2"
shtest "./test_read 1< f 2< f2 -d tt ff ."
shtest "./test_read 2< f 1< f2 -d tt ff ."
shtest "./test_read -d tt 2< 10000000"
shtest "./test_read -d tt 2< "
shtest "./test_read -d tt 2< 
"

shtest "2< f"
shtest "2< 1"
shtest "< f"
shtest "< "

shtest "./test_read -d tt . 2<f"
shtest "./test_read -d tt . 1<f"
shtest "./test_read -d tt . 1<f 2<f2"
shtest "./test_read -d tt 1<f 2<f2"
shtest "./test_read 1<f 2<f2"
shtest "./test_read 1<f 2<f2 -d tt ff ."
shtest "./test_read -d tt . 2<&1"
shtest "./test_read -d . 2<&1"
shtest "./test_read -d tt 2<&1"
shtest "./test_read -d tt 2<&10000000"
shtest "./test_read -d tt <&10000000"
shtest "./test_read -d tt 2<10000000"
shtest "./test_read -d tt 2<&"
shtest "./test_read -d tt 2<"
shtest "./test_read -d tt <&"
shtest "./test_read -d tt 20<&"

shtest "./test_read -d tt 2<&
"
shtest "./test_read -d tt 2<
"
shtest "./test_read -d tt <&
"
shtest "./test_read -d tt 20<&
"

shtest "2<&"
shtest "2<&
"
shtest "2<f"
shtest "f<&"
shtest "f<&
"
shtest "2<1"
shtest "<&"
shtest "<&
"
shtest "<f"
shtest "<"
shtest "<
"

shtest "./test_read -d tt .git 2<f 1<&2"
shtest "./test_read -d tt .git 1<&2 2<f"
shtest "./test_read -d tt .git 2<f 1<&2 <&-"
shtest "./test_read -d tt .git <&- 2<f 1<&2"
shtest "./test_read -d tt .git <&- 1<&2 2<f"

shtest "echo tt . 2<f"
shtest "echo tt . 1<f"
shtest "echo tt . 1<f 2<f2"
shtest "echo tt 1<f 2<f2"
shtest "echo 1<f 2<f2"
shtest "echo 1<f 2<f2 tt ff ."
shtest "echo tt . 2<&1"
shtest "echo . 2<&1"
shtest "echo tt 2<&1"
shtest "echo tt 2<&10000000"
shtest "echo tt <&10000000"
shtest "echo tt 2<10000000"

shtest "echo tt 2<&
"
shtest "echo tt 2<
"
shtest "echo tt <&
"
shtest "echo tt 20<&
"

shtest "echo tt 2<&"
shtest "echo tt 2<"
shtest "echo tt <&"
shtest "echo tt 20<&"

shtest "./test_read -d tt . 1 < f 2 < f2"
shtest "./test_read -d tt 1 < f 2 < f2"
shtest "./test_read 1 < f 2 < f2"
shtest "./test_read 1 < f 2 < f2 -d tt ff ."

shtest "./test_read <2>f"
shtest "./test_read <2> f"
shtest "./test_read <2 >f"
shtest "./test_read < 2>f"
shtest "./test_read <2>f "

shtest "./test_read < 2 >f"
shtest "./test_read < 2> f"
shtest "./test_read < 2>f "
shtest "./test_read <2 > f"
shtest "./test_read <2 >f "
shtest "./test_read <2> f "
shtest "echo ok>f; ./test_read <f>g"
shtest "echo ok>2; ./test_read <2>g"

shtest 'echo ok>f;./test_read 10 10<f'
shtest 'echo ok>f;./test_read 1 <f'
shtest 'echo ok>f;./test_read 1 1<f'
shtest 'echo ok>f;./test_read 2 2<f'
shtest 'echo ok:f>f;echo ok:g>g;./test_read 0 3 <f 3<g'
shtest 'echo ok:f>f;echo ok:g>g;./test_read 10 20 10<f 20<g'

shtest 'cat <&-'

shtest_fd3 'cat 0<&3'
shtest_fd3 'cat <&3'
shtest_fd3 'cat -e 0<&3'
shtest_fd3 'cat -e <&3'

return
# job crontrol
shtest "echo tt inc &<&-"
shtest "echo tt . &<f"
shtest "./test_read -d tt . &<f"
shtest "./test_read -d tt inc &<&-"
shtest "./test_read -d tt . &<f"
shtest "./test_read -d tt . &< f"
shtest "cat &< f"
shtest "./test_signal &< f"
