#!/bin/bash

PATH="$HOME/app/bin:$PATH"
# Bash version 4 attendu
echo $BASH_VERSION
if [ ${BASH_VERSION%.*.*} -ne 4 ];then
	if [ `uname` == "Darwin" ] \
		   && [ $(bash --version | head -n1 | sed 's/.* \([0-9]\)\.[0-9]\.[0-9].*/\1/') -eq 4 ]
	then
		bash $0 $*
		exit $?
	else
		echo "Erreur version bash >= 4.*.*"
		exit 1
	fi
fi

if [ -z "$1" ] || [ ! -e "$1" ];then
	echo "Nom du binaire du projet manquant en argument"
	exit 1
fi

UNAME=$(uname)
TMP="/tmp/shtest"

TEMOIN1=$TMP/test_bash
TEMOIN2=$TMP/test_sh42
BINP=$(realpath $1 || echo $1)
PROJECT=$(dirname $BINP)

DIFF="diff -a -y --suppress-common-lines"
LANG=C

if [ ! -x "$BINP" ];then
	echo "Erreur: $BINP"
	exit 1
fi

rm -rf -- "$TMP" 2>/dev/null
mkdir -p "$TMP" 2>/dev/null
cd $(dirname $0)

mkdir "$TEMOIN1" "$TEMOIN2" 2>&-
echo PROJECT = $PROJECT
echo binaire: BINP = $BINP 
echo $TEMOIN1
echo $TEMOIN2

# cp -rp -- "$PROJECT"/* "$TEMOIN1" 2>&-
# cp -rp -- "$PROJECT"/* "$TEMOIN2" 2>&-
rsync -a --delete-after "$PROJECT"/* "$TEMOIN1" 2>&-
rsync -a --delete-after "$PROJECT"/* "$TEMOIN2" 2>&-
gcc test_read.c -o $TEMOIN1/test_read
gcc test_argv.c -o $TEMOIN1/test_argv
gcc test_read.c -o $TEMOIN2/test_read
gcc test_argv.c -o $TEMOIN2/test_argv
sync

i=0

# $1 envoyer sur stdin
shtest()
{
	i=$(($i+1))
	echo -ne "\033[33m$i stdin = "

	printf "%.100s" "$1" | tr '\n\t' ','
	#printf "%s" "$1"
	echo -e "\033[0m"

	cd "$TEMOIN1"
	echo -e "ligne 1\nligne 2" >f
	rsync -a --delete-after $TEMOIN1/ $TEMOIN2/
	sync
	echo -n "$1" | bash >$TMP/bash 2>$TMP/bash_err

	cd "$TEMOIN2"
	echo -n "$1" | $BINP >$TMP/sh42 2>$TMP/sh42_err

	$DIFF $TMP/sh42 $TMP/bash
	$DIFF $TMP/sh42_err $TMP/bash_err
	$DIFF -q -r $TEMOIN1 $TEMOIN2
}

shtest_fd3()
{
	i=$(($i+1))
	echo -ne "\033[33m$i stdin = "

	#printf "%.20s" "$1" | tr '\n\t' ','
	printf "%s" "$1"
	echo -e "\033[0m"

	printf "ok\nok\n" >/tmp/f
	rsync -a --delete-after $TEMOIN1/ $TEMOIN2/
	sync
	exec 3</tmp/f
	cd "$TEMOIN1"
	echo -n "$1" | bash >$TMP/bash 2>$TMP/bash_err

	exec 3</tmp/f
	cd "$TEMOIN2"
	echo -n "$1" | $BINP >$TMP/sh42 2>$TMP/sh42_err

	$DIFF $TMP/sh42 $TMP/bash
	$DIFF $TMP/sh42_err $TMP/bash_err
	$DIFF -q -r $TEMOIN1 $TEMOIN2
}

# $2 = valeur attendu
shtestnobash()
{
	i=$(($i+1))
	echo -ne "\033[33m$i stdin = "

	#printf "%.20s" "$1" ·| tr '\n\t' ','
	printf "%s" "$1"
	
	echo -e "\033[0m"
	rm $TMP/sh42 $TMP/sh42_att 2>/dev/null
	
	cd "$TEMOIN2"
	echo -n "$1" | $BINP >$TMP/sh42 2>$TMP/sh42_err
	echo -ne "$2" > $TMP/sh42_att
	echo -ne "$3" > $TMP/sh42_att_err
	$DIFF $TMP/sh42 $TMP/sh42_att
	$DIFF $TMP/sh42_err $TMP/sh42_att_err
}

local_sleep()
{
	local TIMEOUT=$1

	while [ $TIMEOUT -gt 0 ];do
		sleep 10
		echo -n "."
		TIMEOUT=$(($TIMEOUT-10))
	done
}

# $1 = max time
# $2 = file
timeout()
{
	local TIMEOUT=$1
	local NAME=$2

	. $NAME.sh &> $NAME.diff.${UNAME,,}.tmp & PID=$!

	exec 2> /dev/null
	local_sleep $TIMEOUT & PID_SL=$!
	trap "kill $PID_SL ; kill $PID ; exit" SIGINT
	wait -n $PID_SL $PID
	if kill $PID ;then
		sleep 2;
		kill -1 $PID &&
			sleep 2; kill -9 $PID
		echo -e "\033[31mTimeout\033[0m"
		return 1
	else
		kill $PID_SL
	fi
	kill $PID_SL
	kill $PID
	return 0
}

shtest_diff()
{
	local NAME=$(basename $1 .sh)
	local TIMEOUT=$2

	i=0
	echo -n "$NAME: "
	timeout $TIMEOUT $NAME
	if [ $? -eq 0 ];then
		sleep 1
		$DIFF $NAME.diff.${UNAME,,} $NAME.diff.${UNAME,,}.tmp &>/dev/null
		if [ $? -ne 0 ];then
			echo -e "\033[31mko\033[0m"
		else
			echo -e "\033[32mok\033[0m"
		fi
	fi
}

# . minishell_builtins_cd.sh
# . minishell_builtins_echo.sh
# . minishell_builtins_env.sh

# . minishell_redirection_in.sh
# . minishell_redirection_out.sh
# . minishell_here_doc.sh

# . minishell_autres.sh
# . minishell_var.sh

# . minishell_if.sh
# . minishell_subshell.sh 
# . minishell_globbing.sh

# exit

shtest_diff minishell_builtins_cd.sh 30
shtest_diff minishell_builtins_echo.sh 420
shtest_diff minishell_builtins_env.sh 30

shtest_diff minishell_redirection_in.sh 60
shtest_diff minishell_redirection_out.sh 60
shtest_diff minishell_here_doc.sh 120

shtest_diff minishell_autres.sh 60
shtest_diff minishell_var.sh 60

shtest_diff minishell_if.sh 120
shtest_diff minishell_subshell.sh 60
shtest_diff minishell_globbing.sh 60

exit 0
