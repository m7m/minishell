
# varaible

shtest "tt=uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" 

shtestnobash "tt=uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=uu\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=uu\ntt=uu\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=uu\ntt=uu\n"

shtest "tt=uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=ii\ntt=ii\n"

shtestnobash "tt=uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=ii\ntt=ii\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=ii\ntt=ii\n"

shtestnobash "setenv tt uu
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=uu\ntt=uu\ntt=ii\ntt=ii\n"

shtest "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

shtestnobash "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

shtest "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unset tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

shtestnobash "
set | grep ^tt
env | grep ^tt
export tt=ii
set | grep ^tt
env | grep ^tt
unsetenv tt
set | grep ^tt
env | grep ^tt
" "tt=ii\ntt=ii\n"

####
# export -n
{
	shtest "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unset tt vv
set | sed -n 's/^tt=uu$/ko: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ko: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtestnobash "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unset tt vv
set | sed -n 's/^tt=uu$/ko: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ko: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtest "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ko: &/p'
export tt=ii vv=ss
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=ss$/ok: &/p'
env | sed -n 's/^vv=ss$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ss$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=$/ko: &/p'
env | sed -n 's/^tt=$/ko: &/p'
set | sed -n 's/^vv=$/ko: &/p'
env | sed -n 's/^vv=$/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=ii\nok: tt=ii\nok: vv=ss\nok: vv=ss\nok: tt=ii\nok: vv=ss\n"

	shtestnobash "tt=uu
vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=ii vv=bb
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=bb$/ok: &/p'
env | sed -n 's/^vv=bb$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=bb$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=bb$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: vv=ii\nok: tt=ii\nok: tt=ii\nok: vv=bb\nok: vv=bb\nok: tt=ii\nok: vv=bb\nok: tt=ii\nok: vv=bb\n"

	shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt=ii vv=nn
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=nn$/ok: &/p'
env | sed -n 's/^vv=nn$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt/ko: &/p'
set | sed -n 's/^vv=nn/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii
ok: tt=ii\nok: tt=ii\nok: vv=nn\nok: vv=nn
ok: tt=ii\nok: vv=nn\n"

	shtestnobash "setenv tt uu
setenv vv ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export tt=ii vv=oo
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=ii$/ok: &/p'
set | sed -n 's/^vv=oo$/ok: &/p'
env | sed -n 's/^vv=oo$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=oo$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt
set | sed -n 's/^tt=ii$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=oo$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii
ok: tt=ii\nok: tt=ii\nok: vv=oo\nok: vv=oo
ok: tt=ii\nok: vv=oo
ok: tt=ii\nok: vv=oo\n"

	shtest "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtest "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\n"

	shtest "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export -n tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
unset tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
"

	shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
export tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=uu$/ok: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=ii$/ok: &/p'
export -n tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
unsetenv tt vv
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: tt=uu\nok: vv=ii\nok: vv=ii\nok: tt=uu\nok: vv=ii\nok: tt=uu\nok: vv=ii\n"

	shtestnobash "
export -n tt vv
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=/ko: &/p'
env | sed -n 's/^vv=/ko: &/p'
"

	shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
export -n tt=uu vv=ii
set | sed -n 's/^tt=uu$/ok: &/p'
env | sed -n 's/^tt=/ko: &/p'
set | sed -n 's/^vv=ii$/ok: &/p'
env | sed -n 's/^vv=/ko: &/p'
" "ok: tt=uu\nok: vv=ii\n"

	shtestnobash "
set | sed -n 's/^tt=/ko: &/p'
env | sed -n 's/^tt=/ko: &/p'
export -n ttvv=oo
set | sed -n 's/^ttvv=oo/ok: &/p'
env | sed -n 's/^ttvv=/ko: &/p'
" "ok: ttvv=oo\n"

	shtestnobash "
set | sed -n 's/^ttvv=/ko: &/p'
env | sed -n 's/^ttvv=/ko: &/p'
export -ttvv=ii 2>/dev/null
export -n -ttvv=ii 2>/dev/null
set | sed -n 's/^ttvv=/ko: &/p'
env | sed -n 's/^ttvv=/ko: &/p'
"
}

shtest "echo $"
shtest "echo $HOME"
shtest "echo $HOM"
shtest "echo $HOME "
shtest "echo 1$HOME-2 "
shtest "echo \$HOME"
shtest 'echo "1-$HOME-2"'
shtest "echo '1-$HOME-2'"
shtest "t=file && ls -ld Make$t"
shtest "t='file .' && ls -ld Make$t"
shtest 't="file ." && ls -ld "Make$t"'

# =
shtest 'ls && yy=uu && set | grep yy'
shtest 'ls | yy=uu && set | grep yy'

# $
shtestnobash 'set gg "a e"
ls$gg-r' '42sh: line 2: `lsa`: command not found\n'
shtestnobash 'set gg "ae"
ls$gg-r' '42sh: line 2: `lsae-r`: command not found\n'
shtestnobash 'set gg " a e"
ls$gg-r' "ls: cannot access 'a': No such file or directory
ls: cannot access 'e-r': No such file or directory\n"
shtestnobash 'set gg " a"
ls$gg-r' "ls: cannot access 'a-r': No such file or directory\n"
shtestnobash 'set gg " a "
ls$gg-r' "ls: cannot access 'a': No such file or directory\n"
shtestnobash 'set gg "a "
ls$gg-r' '42sh: line 2: `lsa`: command not found\n'
shtestnobash 'set gg "a e "
ls$gg-r' '42sh: line 2: `lsa`: command not found\n'
shtestnobash 'set gg "'\''a e '\''"
ls$gg-r' '42sh: line 2: `lsa e -r`: command not found\n'
