
shtest "      "
shtest "				"
shtest "



"
shtest "	    		   	  "
shtest "	 
   		 
  	  "

# \
	#
shtest "l\
s -ld"
shtest "ls -d -la tt\ oo"
shtest "mkdir 'tt\ oo'"
shtest "ls -d -la tt\ oo"
shtest "ls -d -la 'tt\ oo'"
shtest "ls -d -la 'tt oo'"
shtest "ls -d -la \"tt oo\""
#echo -e '\''' "\""
shtest 'echo -e '\''\'\'''\'''\'' "\""'

# | && || ;
shtest "ls && echoo tt || ls .git | cat -e  && echo yy || ls ."
shtest "ls && echo tt || ls .git | cat -e  && echo yy || ls ."
shtest "echoo tt || ls | cat -e"
shtest "s && echo tt || cat f && echo yy"
shtest "echo tt | cat -e"
shtest "s | cat && echo tt || env"
shtest "ls ; ls .git"
shtest "ls ; ls truc"
shtest "ls truc ; ls .git"

# || && |
shtest 'lss | cat -e'
shtest 'ls | cat -e'
shtest 'lss | cat -e || echoo ii | cat -e'
shtest 'lss | cat -e && echoo ii | cat -e'
shtest 'lss | cat -e || echo ii | cat -e'
shtest 'lss | cat -e && echo ii | cat -e'
shtest 'ls | cat -e || echoo ii | cat -e'
shtest 'ls | cat -e && echoo ii | cat -e'
shtest 'ls | cat -e || echo ii | cat -e'
shtest 'ls | cat -e && echo ii | cat -e'
shtest 'lss | cat -e || echoo ii | cat -e && ls auteur | cat'
shtest 'lss | cat -e || echoo ii | cat -e || ls auteur | cat'
shtest 'ls | cat -e || echoo ii | cat -e && ls auteur | cat'
shtest 'ls | cat -e || echoo ii | cat -e || ls auteur | cat'
shtest 'lss | cat -e || echo ii | cat -e && ls auteur | cat'
shtest 'lss | cat -e || echo ii | cat -e || ls auteur | cat'
shtest 'ls | cat -e || echo ii | cat -e && ls auteur | cat'
shtest 'ls | cat -e || echo ii | cat -e || ls auteur | cat'
shtest 'lss | cat -e || echoo ii | cat -e && tttt auteur | cat'
shtest 'lss | cat -e || echoo ii | cat -e || tttt auteur | cat'
shtest "mkdir 'tt oo' && cd tt\ oo"

# |&
shtest "ls -ld . tt >f |& cat -e"
shtest "ls -ld . tt 2>f |& cat -e"
shtest "ls -ld . tt |& cat -e"

shtest 'echo in>in;cat <in 2>out | echo ; ls && cat -e in || echo ko && cat -e out'
shtest 'cat <in >out | echo || ls 2>err ; ecko ok'
shtest 'cat <`ls`'

shtest "echo \if"
shtest "echo \then"
shtest "echo \elif"
shtest "echo \else"
shtest "echo \fi"

shtest "echo \while"
shtest "echo \do"
shtest "echo \done"
shtest "echo \for"
shtest "echo \in"
shtest "echo \case"
shtest "echo \esac"

shtestnobash "" ""
shtestnobash "
" ""

shtest 'echo a"b"'\''c'\''d'
shtest 'echo "'\''"'

shtest 'echo '\'''\'''\'''
shtest 'echo '\''"'\'''
shtest 'echo '\''\'\'''
shtest 'echo "\\"'
shtest 'echo "\"'

shtest '/bin/echo a"b"'\''c'\''d'
shtest '/bin/echo "'\''"'
shtest '/bin/echo '\'''\'''\'''
shtest '/bin/echo '\''"'\'''
shtest '/bin/echo '\''\'\'''
shtest '/bin/echo "\\"'
shtest '/bin/echo "\"'

shtest '"ls" -ld'
shtest ''\''ls'\'' -ld'
shtest '"l"s -ld'
shtest ''\''l'\''s -ld'
shtest 'l"s" -ld'
shtest 'l'\''s'\'' -ld'

shtest '""'
shtest
