# &

shtest "ls &<< f -d tt ."

shtest "ls tt . & > f"
shtest "ls tt 2 > &"
shtest "ls tt 2 > "
shtest "ls tt  > &"
shtest "ls tt 20 > &"
shtest "2 > &"
shtest "f > &"
shtest " > &"

shtest "ls tt . 2> &1"
shtest "ls . 2> &1"
shtest "ls tt 2> &1"
shtest "ls tt 2> &10000000"
shtest "ls tt > &10000000"
shtest "ls tt 2> &"
shtest "ls tt > &"
shtest "ls tt 20> &"
shtest "2> &"
shtest "f> &"
shtest "> &"
shtest "ls tt . 2 > &1"
shtest "ls . 2 > &1"
shtest "ls tt 2 > &1"
shtest "ls tt 2 > &10000000"
shtest "ls tt  > &10000000"
shtest "cd ../.. && ls &
pwd"

shtest "cd ../.. && ls
&
pwd"
