shtest 'env echo $HOME'
shtest 'env -i echo $HOME'
shtest 'env /usr/bin/env echo $HOME'
shtest 'env -i /bin/bash -c "/usr/bin/env echo $HOME"'
shtest "env ./$(basename $BINP)"
shtest "env -i ./$(basename $BINP)"

# voir minishell_var.sh
return

shtestnobash "env -i setenv tt=oo
env"
shtestnobash "env setenv tt=oo
env"
# shtestnobash "env -i setenv tt oo
# env"
# shtestnobash "env setenv tt oo
# env"

shtestnobash "setenv tt=oo
env"
# shtestnobash "setenv tt oo
# env"

shtestnobash "setenv tt=oo
env
unsetenv tt
env
unsetenv tt
env"
# shtestnobash "setenv tt oo
# env
# unsetenv tt
# env
# unsetenv tt
# env"

shtestnobash "unsetenv
env
setenv
env
unsetenv tt hh
env"

shtestnobash "setenv tt=oo
setenv gg=hh
env
setenv
env
unsetenv tt hh
env"

shtestnobash "setenv tt=oo
setenv gg=hh
env
setenv
env
unsetenv tt
env
unsetenv hh
env"

shtestnobash "env
unsetenv
env"
