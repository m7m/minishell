# >
shtest "ls -d tt . 2 > f"
shtest "ls -d tt . 1 > f"
shtest "ls -d tt 2 > 10000000"
shtest "2 > f"
shtest "2 > 1"
shtest " > f"
shtest " > "
shtest " > 
"

shtest "ls -d tt . 2> f"
shtest "ls -d tt . 1> f"
shtest "ls -d tt . &> f"
shtest "ls -d tt . 1> f 2> f2"
shtest "ls -d tt 1> f 2> f2"
shtest "ls 1> -d f 2> f2"
shtest "ls 1> -d f 2> f2 tt ff ."
shtest "ls 2> -d f 1> f2 tt ff ."
shtest "ls -d tt 2> 10000000"
shtest "ls -d tt 2> "
shtest "ls -d tt 2> 
"

shtest "2> f"
shtest "2> 1"
shtest "> f"
shtest "> "
shtest "> 
"

shtest "ls -d tt . 2>f"
shtest "ls -d tt . 1>f"
shtest "ls -d tt . &>f"
shtest "ls -d tt . 1>f 2>f2"
shtest "ls -d tt 1>f 2>f2"
shtest "ls 1>f 2>f2"
shtest "ls 1>f 2>f2 -d tt ff ."
shtest "ls -d tt . 2>&1"
shtest "ls -d . 2>&1"
shtest "ls -d tt 2>&1"
shtest "ls -d tt 2>&10000000"
shtest "ls -d tt >&10000000"
shtest "ls -d tt 2>10000000"
shtest "ls -d tt 2>&"
shtest "ls -d tt 2>"
shtest "ls -d tt >&"
shtest "ls -d tt 20>&"
shtest "2>&"
shtest "2>f"
shtest "f>&"
shtest "2>1"
shtest ">&"
shtest ">f"
shtest ">"
shtest "ls -d tt inc &>&-"

shtest "ls -d tt 2>&
"
shtest "ls -d tt 2>
"
shtest "ls -d tt >&
"
shtest "ls -d tt 20>&
"
shtest "2>&
"
shtest "2>f
"
shtest "f>&
"
shtest "2>1
"
shtest ">&
"
shtest ">f
"
shtest ">
"
shtest "ls -d tt inc &>&-
"

shtest "ls -d tt .git 2>f 1>&2"
shtest "ls -d tt .git 1>&2 2>f"
shtest "ls -d tt .git 2>f 1>&2 >&-"
shtest "ls -d tt .git >&- 2>f 1>&2"
shtest "ls -d tt .git >&- 1>&2 2>f"
shtest "ls -d tt . &>f"
shtest "ls &>> -d f tt ."

shtest "echo tt . 2>f"
shtest "echo tt . 1>f"
shtest "echo tt . &>f"
shtest "echo tt . 1>f 2>f2"
shtest "echo tt 1>f 2>f2"
shtest "echo 1>f 2>f2"
shtest "echo 1>f 2>f2 tt ff ."
shtest "echo tt . 2>&1"
shtest "echo . 2>&1"
shtest "echo tt 2>&1"
shtest "echo tt 2>&10000000"
shtest "echo tt >&10000000"
shtest "echo tt 2>10000000"
shtest "echo tt 2>&"
shtest "echo tt 2>"
shtest "echo tt >&"
shtest "echo tt 20>&"
shtest "echo tt inc &>&-"

shtest "echo tt 2>&
"
shtest "echo tt 2>
"
shtest "echo tt >&
"
shtest "echo tt 20>&
"
shtest "echo tt inc &>&-
"

shtest ">>"
shtest ">>&"
shtest ">>&-"
shtest ">>
"
shtest ">>&
"
shtest ">>&-
"
shtest ">>f"
shtest ">> g"
shtest "ls 1>>f 2>>f2 -d tt ff ."

#####
## Comportement different
shtest "ls -d tt . 1 > f 2 > f2"
shtest "ls -d tt 1 > f 2 > f2"
shtest "ls 1 > f 2 > f2"
shtest "ls 1 > f 2 > f2 -d tt ff ."

shtest 'ls -ld | >f cat -e'
shtest 'ls -ld | 2>f cat -e'
shtest '>f 2> g ls -ld tt /'
