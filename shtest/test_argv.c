/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_argv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 13:48:00 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 13:58:32 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>

int		main(int ac, char **av)
{
	int	n;

	while (ac)
	{
		--ac;
		printf("index: %d, argv: %s\n", ac, av[ac]);
		fflush(stdout);
	}
	return (0);
}
