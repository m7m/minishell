rm -rf -- $TEMOIN1/test 2>/dev/null
mkdir $TEMOIN1/test 2>/dev/null
touch $TEMOIN1/test/{na01,ra01,sa01}

touch $TEMOIN1/{na01,ra01,sa01,'*','?'}

shtest 'echo [s]a01'
shtest 'echo [r]a01'
shtest 'echo [n]a01'

shtest 'echo ?a01'
touch $TEMOIN1/{sb01,nb01}
shtest 'echo s?01'
shtest 'echo [sn]?01'
shtest 'echo s[ab]?1'
shtest 'echo s[ab]0?'

shtest 'echo ?{s,b}*'
shtest 'echo ?{s,a,b}*'
shtest 'echo ?{a,b}*'
rm $TEMOIN1/{sb01,nb01}

shtest 'echo {n,r,s,a}'
shtest 'echo {n,r,s,a}*'
shtest 'echo {n,r,s,a}???'
shtest 'echo {n,r,s,a}o'
shtest 'echo a{1,2,3,4}b'

shtest 'echo [sr]*'
shtest 'echo [rs]*'
shtest 'echo [nb]*'
shtest 'echo [n]*'
shtest 'echo n*'

shtest 'cd test
ls *'
shtest 'cd test
ls * | cat -e'
shtest 'ls \**'
shtest 'ls -ld [si]*'

shtest 'echo ?*'
shtest 'echo *01'
shtest 'echo .*'
shtest 'echo *.'
shtest 'echo sa01*'
shtest 'echo sa01?'
shtest 'echo sa01[]'
shtest 'echo sa01\[\]'

shtest 'echo \*'
shtest 'echo \?'
shtest 'echo \{a\}'
shtest 'echo \{s\}'
shtest 'echo \[s\]'

shtest "cd $TEMOIN1/test;ls;echo [sr]*"

touch $TEMOIN1/'*abc'
shtest 'echo **'
shtest 'echo "*"*'
shtest 'echo "*"* 2 3'
shtest 'echo a[is]*'

shtest 'ls shtest/minishell{_globbing,}.sh'

shtest 'echo src/*'
shtest 'echo */*c'

rm -rf -- $TEMOIN1/test
rm $TEMOIN1/{na01,ra01,sa01,'*','?'}
