echo '$()'
shtest '$()'
shtest '$(ls)'
shtest 'echo $(echo ok)'
shtest 'echo $(echo ok)
'
shtest 'echo $(echo ok
)'
shtest 'echo $(echo 
ok
)'
shtest 'echo $(echo 
ok)'
shtest 'echo "$(echo 
ok
)"'
shtest 'echo '\''$(echo 
ok
)'\'''

shtest 'echo '\''$(echo ok'\'' )'
shtest 'echo "$( echo ok")'

shtest 'echo $(echo 2 | cat -e && echo 3)'
shtest 'echo a$(echo 2 | cat -e && echo 3)b'
shtest 'echo $(echo 2 | cat -e && echo 3)b'
shtest 'echo a$(echo 2 | cat -e && echo 3)'

shtest 'echo "$(echo 2 | cat -e && echo 3)"'
shtest 'echo "a$(echo 2 | cat -e && echo 3)b"'
shtest 'echo "$(echo 2 | cat -e && echo 3)b"'
shtest 'echo "a$(echo 2 | cat -e && echo 3)"'

shtest 'echo '\''$(echo 2 | cat -e && echo 3)'\'''
shtest 'echo '\''a$(echo 2 | cat -e && echo 3)b'\'''
shtest 'echo '\''$(echo 2 | cat -e && echo 3)b'\'''
shtest 'echo '\''a$(echo 2 | cat -e && echo 3)'\'''

shtest 'l$(echo "s -ld")'
shtest 'l$(echo "s -ld") ..'
shtest 'l$(echo -n "s -ld") ..'

# parseur de l'interieur vers l'exterieur
#shtest '"l$(echo \"s -ld\")"'
#shtest '"l$(echo \"s -ld\") .."'

shtest 'l$(echo '\''s -ld'\'')'
shtest 'l$(echo '\''s -ld'\'') ..'
shtest '"l$(echo '\''s -ld'\'')"'
shtest '"l$(echo '\''s -ld'\'') .."'

shtest 'echo l$(f)e'
shtest 'echo l$()e'
shtest 'echo '\''l$()e'\'''
shtest '"l$()s"'
shtest 'l$()s'

shtest './test_argv $(echo -e "1\n2")'
shtest './test_argv $(echo -e "1\n2")3'
shtest './test_argv $(echo -e "1\n2") 3'

shtest 't="$(echo 1 2)";echo $t'

echo '()'
shtest '()'
shtest '(ls)'
shtest '(echo 1 ; echo 2) | cat -e'
shtest '(echo 1 ; echo 2) | > f'
shtest '(echo 1 ; echo 2) >f'
shtest '(echo 1 ; echo -n 2) | cat -e'
shtest '(echo 1 ; echo -n 2) | >f'
shtest '(echo 1 ; echo -n 2) >f'
shtest 'echo (echo 1 ; echo 2) | cat -e'

echo '``'
shtest '``'
shtest '`ls`'
shtest 'echo `echo ok`'
shtest 'echo `echo ok`
'
shtest 'echo `echo ok
`'
shtest 'echo `echo 
ok
`'
shtest 'echo `echo 
ok`'
shtest 'echo "`echo 
ok
`"'
shtest 'echo '\''`echo 
ok
`'\'''

shtest 'echo '\''`echo ok'\'' `'
shtest 'echo "` echo ok"`'

shtest 'echo `echo 2 | cat -e && echo 3`'
shtest 'echo a`echo 2 | cat -e && echo 3`b'
shtest 'echo `echo 2 | cat -e && echo 3`b'
shtest 'echo a`echo 2 | cat -e && echo 3`'

shtest 'echo "`echo 2 | cat -e && echo 3`"'
shtest 'echo "a`echo 2 | cat -e && echo 3`b"'
shtest 'echo "`echo 2 | cat -e && echo 3`b"'
shtest 'echo "a`echo 2 | cat -e && echo 3`"'

shtest 'echo '\''`echo 2 | cat -e && echo 3`'\'''
shtest 'echo '\''a`echo 2 | cat -e && echo 3`b'\'''
shtest 'echo '\''`echo 2 | cat -e && echo 3`b'\'''
shtest 'echo '\''a`echo 2 | cat -e && echo 3`'\'''

shtest 'l`echo "s -ld"`'
shtest 'l`echo "s -ld"` ..'
shtest 'l`echo -n "s -ld"` ..'

shtest 'l`echo '\''s -ld'\''`'
shtest 'l`echo '\''s -ld'\''` ..'
shtest '"l`echo '\''s -ld'\''`"'
shtest '"l`echo '\''s -ld'\''` .."'

shtest 'echo l`f`e'
shtest 'echo l``e'
shtest 'echo '\''l``e'\'''
shtest '"l``s"'
shtest 'l``s'

shtest './test_argv `echo -e "1\n2"`'
shtest './test_argv `echo -e "1\n2"`3'
shtest './test_argv `echo -e "1\n2"` 3'

shtest 't="`echo 1 2`";echo $t'

# <<<
shtest 'echo $(echo o$(echo k))'
shtest 'echo a$(echo o$(echo k))b'
shtest 'echo $(echo o)$(echo k)'
shtest 'echo a$(echo o)$(echo k)b'
shtest 'echo a$(echo o)b$(echo k)c'

shtest 'echo `echo o`echo k``'
shtest 'echo a`echo o`echo k``b'
shtest 'echo `echo o``echo k`'
shtest 'echo a`echo o``echo k`b'
shtest 'echo a`echo o`b`echo k`c'


shtest 'cat <<<$(ls)'
shtest 'cat <<<a$(ls -ld)'
shtest 'cat <<<$(ls -ld)b'
shtest 'cat <<<a$(ls -ld)b'

shtest 'cat <<<`ls`'
shtest 'cat <<<a`ls -ld`'
shtest 'cat <<<`ls -ld`b'
shtest 'cat <<<a`ls -ld`b'

shtest '$(echo l)$(echo s)'
shtest '`echo l``echo s`'

shtest '(echo ok ; echo 2) $(echo 3)'
shtest 'l$(echo )s -ld /'
shtest '$(echo l)s -ld /'
shtest '$(echo )ls -ld /'
shtest '$(echo ls) -ld /'
shtest '$(echo l)$(echo s) >f'
shtest '$(echo l)$(echo s) | cat -e'
shtest '$(echo ls) | cat -e'
shtest 'l$(echoo -n)s'
shtest '$(echoo -n)'

shtest '`echo l``echo s``echo " -ld"`'
shtest '$(echo l)$(echo s)$(echo " -ld")'

shtest 'echo a"b$(echo cdefghjkl`echo tyuiop`0123)45"6'
shtest 'echo "$(echo \))"'
shtest 'echo $(echo \))'
shtest 'echo "\$(echo \))"'
shtest 'echo \$(echo \))'

shtest 'echo $((echo 1 | cat -e) | cat -e)'
shtest 'echo $(echo 0 (echo 1 | cat -e) | cat -e)'

shtest '(echo 1; ls -ld . tt 2>g2 ; jjj >g3 ) 2>f | cat -e'
shtest '(echo 1; ls -ld . tt >g2 ; jjj >g3 ) 2>f | cat -e'
shtest '(echo 1; ls -ld . tt >g2 ; jjj >g3 ) >f | cat -e'
shtest '(echo 1; ls -ld . tt >g2 ; jjj >g3 ) >f | cat -e'

shtest 'echo ok ; echo $(cat && c < ) ; echo ko'
shtest 'cat <<<`./test_read`'
shtest 'cat <<EOF
$(ls && cat <<E2
eeee
E2
)
EOF
'

shtest '(./test_read && echo ok) | cat -e'
shtest '(./test_read && echo ok) | >f'
shtest '(./test_read && echo ok) >f'
shtest 'echo "$(ls -ld\))"'
