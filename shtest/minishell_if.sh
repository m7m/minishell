echo 'if'

shtest "if"
shtest "then"
shtest "else"
shtest "elif"
shtest "fi"

shtest "echo if"
shtest "echo then"
shtest "echo else"
shtest "echo elif"
shtest "echo fi"

{
	
	# 1 : if then fi
	shtest 'if ls -ld . ;then echo 1 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;fi || echo ko'
	shtest 'if ls -ld . ;then nil ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;fi || echo ok'

	# 2 : shtest 'if then else fi'
	shtest 'if ls -ld . ;then echo 1 ;else echo 2 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;else echo 2 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;else echo 2 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;else echo 2 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;else echo 2 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;else echo 2 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;else echo 2 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;else echo 2 ;fi || echo ko'
	shtest 'if ls -ld . ;then nil ;else echo 2 ;fi'
	shtest 'if ls -ld . ;then nil ;else echo 2 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;else echo 2 ;fi && echo ok'
	shtest 'if ls -ld . ;then nil ;else echo 2 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;else nil ;fi'
	shtest 'if ls -ld e ;then echo 1 ;else nil ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;else nil ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;else nil ;fi || echo ok'
	shtest 'if ls -ld . ;then nil ;else nil ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;else nil ;fi && echo ok'
	shtest 'if ls -ld . ;then nil ;else nil ;fi || echo ko'
	shtest 'if ls -ld e ;then nil ;else nil ;fi ; echo ok'
	shtest 'if ls -ld e ;then nil ;else nil ;fi && echo ko'
	shtest 'if ls -ld e ;then nil ;else nil ;fi || echo ok'

	# 3 : shtest 'if then elif then fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi || echo ko'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;fi || echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;fi || echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld ee ;then echo 2 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld ee ;then echo 2 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld ee ;then echo 2 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld ee ;then echo 2 ;fi || echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;fi || echo ok'

	# 4 :  : shtest 'if then elif then else fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi || echo ko'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi || echo ko'

	#
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;else echo 3 ;fi || echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;else echo 3 ;fi || echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;else echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;else echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;else echo 3 ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;else echo 3 ;fi || echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else nil ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else nil ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else nil ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;else nil ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else nil ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else nil ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else nil ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld e ;then echo 2 ;else nil ;fi || echo ok'

	# 5 :  : shtest 'if then elif then elif then fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld . ;then echo 1 ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi && echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi || echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi || echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld .. ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi || echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld ../.. ;then echo 3 ;fi || echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi && echo ko'
	shtest 'if ls -ld . ;then nil ;elif ls -ld e ;then echo 2 ;elif ls -ld e ;then echo 3 ;fi || echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld ../.. ;then echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld ../.. ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld ../.. ;then echo 3 ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld ../.. ;then echo 3 ;fi || echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld e ;then echo 3 ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld e ;then echo 3 ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld e ;then echo 3 ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld .. ;then nil ;elif ls -ld e ;then echo 3 ;fi || echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then nil ;fi'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then nil ;fi ; echo ok'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then nil ;fi && echo ko'
	shtest 'if ls -ld e ;then echo 1 ;elif ls -ld ee ;then echo 2 ;elif ls -ld ../.. ;then nil ;fi || echo ok'

	# 5 ' ' -> ' \t\n'
}

{
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'


	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
.. 	
 	
;then 	
 	
nil 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
3 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
nil 	
 	
;fi'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
nil 	
 	
;fi 	
 	
; 	
 	
echo 	
 	
ok'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
nil 	
 	
;fi 	
 	
&& 	
 	
echo 	
 	
ko'
	shtest	'if 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
1 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
e 	
 	
;then 	
 	
echo 	
 	
2 	
 	
;elif 	
 	
ls 	
 	
-ld 	
 	
../.. 	
 	
;then 	
 	
nil 	
 	
;fi 	
 	
|| 	
 	
echo 	
 	
ok'

}

	shtest '
if 
	if ls -ld . ;then
 		echo ifif;
 	else
 		echo ififko;
 	fi
then
	echo 1
elif ls -ld .. ;then
	echo 2 ;
fi'

	shtest '
if ls -ld . ;then
	if echo ifif;then
		echo ifif2;
	fi
elif ls -ld .. ;then
	echo 2
fi'

	shtest '
if ls -ld . ;then
	echo 1 ;
elif
	if ls -ld ..;then
 		echo elifif
	fi
then
	echo 2
fi'


	shtest '
if 
	if ls -ld e ;then
 		echo ifif;
 	else
 		echo ififko;
 	fi
then
	echo 1
elif ls -ld .. ;then
	echo 2 ;
fi'

	shtest '
if ls -ld . ;then
	if ifif;then
		echo ifif;
	fi
elif ls -ld .. ;then
	echo 2
fi'

	shtest '
if ls -ld . ;then
	echo 1 ;
elif
	if ls -ld e;then
 		echo elifif
	fi
then
	echo 2
fi'

#difference de tabulation
	shtest '
if ls -ld e ;then
	echo 1 ;
elif
	if ls -ld ee;then
 		echo elifif
	fi
then
	echo 2
fi'
 shtest '
if ls -ld e ;then
 echo 1 ;
elif
 if ls -ld ee;then
   echo elifif
 fi
then
 echo 2
fi'

	shtest 'if ls -ld . ; then if ls -ld .. ;then echo ok;fi;echo ok2;fi'
	shtest 'if then fi;then fi;fi'
	shtest 'echo if then fi; echo then fi; echo fi'
