/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jobs.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 11:47:14 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 11:47:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef JOBS_H
# define JOBS_H

enum
{
	JOB_STOPPED = 0,
	JOB_RUNNING,
	JOB_TERMINATED,
	JOB_PENDING,
	JOB_PENDING_DELETE
};

#endif
