/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_name_prog.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/21 00:24:25 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/21 00:24:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_NAME_PROG_H
# define SH_NAME_PROG_H

# define NAME_PROG "42sh"
# define LEN_NAME_PROG 9

#endif
