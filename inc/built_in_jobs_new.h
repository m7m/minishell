/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   built_in_jobs_new.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/09 11:10:47 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/09 11:10:47 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILT_IN_JOBS_NEW_H
# define BUILT_IN_JOBS_NEW_H

struct s_builtins_job
*ft_jobs_newalloc(void);

#endif
