/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_hash.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 05:18:13 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 05:18:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_HASH_H
# define PR_HASH_H

# include <stddef.h>
# include "libft.h"
# include "ft_debug.h"
# include "t_hash.h"
# include "t_exit.h"

/*
** Fonction de gestion de table de hachage
*/

t_ul	ft_hash(char *elem, size_t len);
size_t	ft_hash_len(size_t len);
size_t	ft_hash_calc(size_t hdb_size, char *key, size_t key_len);

t_hdb	*ft_hnew_db(size_t nb_elem_base);
void	ft_hnew_table(t_hdb *hdb, t_htable *thtable, int data_static);
t_hash	**ft_hnew_thash(size_t len);

int		ft_hresize_th(t_hdb *hdb);
t_hash	*ft_hadd(t_htable *thtable, void *elem, size_t elem_len);
t_hash	*ft_hadd_nodup(t_htable *thtable, t_hash *nodup);
void	*ft_hadd_n(t_htable *thtable, void *key, size_t key_len, char *data);
void	ft_hadd_data(t_htable *thtable, t_hash *thash,
						void *data, size_t data_len);

void	ft_hreset_db(t_hdb *hdb);
void	ft_hdel_th(t_hash *thash, int data_static);
void	ft_hdel_db_table(t_hdb *hdb, int nums_table);
void	ft_hdel_db(t_hdb *hdb);
void	ft_hdel_table(t_htable *thtable);
void	ft_hdel_table_cont(t_htable *thtable);
int		ft_hdelkey_table(t_htable *thtable, void *key, size_t key_len);

t_hash	*ft_hdup_th(t_hash *thash);
void	ft_hdup_table(t_htable *thtablenew, t_htable *thtableold);

void	*ft_hupd(t_htable *thtable, t_hupdate *hupd);
void	ft_hupd_n(t_htable *thtable, void *key, size_t key_len, char *data);
void	*ft_hupd_isexist(t_htable *thtable, t_hupdate *hupd);
void	*ft_hupd_nosize(t_htable *thtable, char *key, char *data);
void	*ft_hupd_nodup(t_htable *thtable, t_hupdate *hupd);

size_t	ft_hchr(t_htable thtable, void *key, size_t key_len, void **data);
t_hash	*ft_hchr_th(t_htable thtable, void *key, size_t key_len);
t_hash	*ft_hchr_keyparent(t_hash *hnoeud,
						void *key, size_t key_len, int ntable);
t_hash	*ft_hchr_noeudkey(t_hash *noeud, void *key, size_t key_len, int ntable);
t_hash	**ft_hchr_noeud(t_htable thtable, void *key, size_t key_len);
t_hash	*ft_hchr_d(t_htable thtable, void *data, size_t data_len);

int		ft_hchk_deqd(t_hash *hd1, void *data, size_t data_len, int ntable);
int		ft_hchk_keqk(t_hash *hk1, void *key, size_t key_len, int ntable);

void	ft_hmove_th(t_htable *dst, t_htable *src, t_hash *thash);
int		ft_hmove_key(t_htable *dst, t_htable *src, void *key, size_t key_len);
int		ft_hmove_key_nosize(t_htable *dst, t_htable *src, char *key);
int		ft_hmoveadd_key_nosize(t_htable *dst, t_htable *src,
							char *key, char *data);

void	ft_tabtohash(char *tabstr[], t_htable *thtable);
void	ft_tabtohash_size(char *tabstr[], int itabstr, t_htable *thtable);
char	**ft_hashtotab(t_htable thtable);

void	ft_hashto_keyeqdata_write(t_htable thtable);

size_t	ft_hsearch_k(t_htab thtable, void *key, size_t key_len, t_ht ***tabh);
size_t	ft_hsearch_d(t_htab thtable, void *data, size_t data_len, t_ht ***tabh);

int		ft_search_in_h(const char *motif, char ***resul, t_htable *thtable,
					size_t (*fhsearch)(t_htable, void *, size_t, t_ht ***));

void	ft_htab_lie(t_htable *thtable, t_hash *thash, int adddel);
void	ft_htab_lie_upd(t_htable *thtable, size_t data_len);

#endif
