/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_surrogate.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 16:21:16 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 16:21:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SURROGATE_H
# define FT_SURROGATE_H

char	*ft_surrogate__dol(char **argv, char *arg, int notnull);
char	*ft_surrogate__tilde(char **argv);
char	*ft_surrogate__tilde2(char *arg, char **argv);

#endif
