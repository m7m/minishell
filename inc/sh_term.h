/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_term.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/22 12:42:56 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/22 12:42:56 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_TERM_H
# define SH_TERM_H

# include <termios.h>
# include <termcap.h>
# include <fcntl.h>
# include <sys/ioctl.h>

# include "l_sh.h"
# include "pr_xetlocal.h"

# include "pr_term.h"

struct	s_savterm
{
	struct termios	sterm;
	int				fd;
};

# ifndef BTERM_SIZE
#  define BTERM_SIZE 512
# endif

# ifndef TAB_SIZE
#  define TAB_SIZE 3
# endif

/*
** Gestion du buffeur pour les fonction tget*
*/

# if defined(__APPLE__)
#  define INIT_BUFF  char	buff[4097]
#  define INIT_PBUFF char	*pbuff
#  define VAL_BUFF ft_memset(buff, 0, 4096)
#  define VAL_PBUFF pbuff = buff
#  define BCAP &pbuff
#  define FT_INITCAP(c, x, kcap) c = ft_term_initcap__unix(kcap, x)
#  define FREE_CAP(c)
# else
#  define INIT_BUFF
#  define INIT_PBUFF
#  define VAL_BUFF
#  define VAL_PBUFF
#  define BCAP NULL
#  define FT_INITCAP(c, x, kcap) ft_term_initcap__(kcap, c = x)
#  define FREE_CAP(c) free(c)
# endif

# ifndef FT_TPUTS
#  define FT_TPUTS(str, affcnt) tputs(str, affcnt, ft_putwchar_t)
# endif

#endif
