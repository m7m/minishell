/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_tfd.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 18:06:01 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 18:06:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_TFD_H
# define T_TFD_H

/*
** file_flags: option du fichier pour open
*/

typedef struct		s_fd
{
	int			pipe[2];
	int			file_flags;
	int			opt;

	char		*str;

	struct s_fd	*next;
}					t_fd;

#endif
