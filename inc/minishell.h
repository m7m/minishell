/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/06 20:08:20 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/06 20:08:20 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# ifndef BUFF_SIZE
#  define BUFF_SIZE 4096
# endif
# ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200809L
# endif

# if BUFF_SIZE < 2
#  error BUFF_SIZE too small
# endif

# ifndef EOF
#  define EOF -1
# endif

# include "sh_name_prog.h"

# include <unistd.h>
# include <sys/wait.h>
# include <fcntl.h>

# include <sys/stat.h>
# include <dirent.h>
# include <limits.h>

# include <sys/ioctl.h>

# include "pr_signal.h"
# include "l_sh.h"
# include "pr_xetlocal.h"
# include "libft.h"
# include "flags.h"
# include "def_usage.h"
# include "pr_hash.h"
# include "t_struct.h"
# include "pr_lst.h"
# include "sh_macro.h"
# include "const_file.h"
# include "built_in.h"
# include "ft_updatekey.h"

/*
** Termios
*/

# include "pr_term.h"

# ifndef MAXNAMLEN
#  define MAXNAMLEN 4096
# endif

void	ft_init_hdb_tables(t_sh *sh, char **envp);
void	ft_gen_builtins_hash(t_htable *thtable);
void	ft_envp_min(t_htable *thtable);
void	ft_envp_key_shlvl(t_htable *thtable);
void	ft_local_min(t_htable *thtable);
void	ft_local_min__ifs(t_htable *hlocal);
void	ft_local_min__colline(t_htable *hlocal);
void	ft_gen_exec_hash(t_htable *hexec, t_htable *hcorrela, t_htable henvp);

int		ft_list_exec(int perm, char *path_henvp);
void	ft_correla_path(t_htable *hexec, t_htable *hcorrela, t_htable henvp);
void	ft_gen_correla_hash(t_htable *hcorrela);
void	*ft_exec_force_update(t_htable *hexec, t_htable henvp, char *cmd);

t_htab	*ft_getstatic_envp(t_htable *henvp);

int		ft_shgnl(int fd, char **line, struct s_sh *sh);
void	ft_prompt(t_sh *sh);
void	ft_print_prompt(void);
int		ft_gnl_in(int fd, char **line);
void	ft_prompt_parse(t_sh *sh, char *line, size_t line_len);

void	ft_isexec(t_sh *sh, t_argv *targv);
int		ft_isexec_bin(t_sh *sh, t_argv *targv);
int		ft_isshebang(t_sh *sh, t_argv *targv);

int		ft_parse_opt(int *retopt, char *c_opt);
int		ft_opt_tree(int opt, char c_opt);
char	*ft_parse_dotslash(int opt, char *dir, char *pwd);

int		ft_print_error(const char *cmd, int cmd_len,
				const char *msg_err, int msg_err_len);
void	ft_print_error_not_close(int flag);
void	ft_perr(const char *str, size_t str_len);
void	ft_print_error_n(const char *cmd, const char *msg_err);
int		ft_accesschk_perror(char *path, int perm);

int		ft_accesschk_dir(char *cmd, char *path);
int		ft_accesschk(const char *path);
int		ft_accesschk_path_x(char *path);

int		ft_linecomplete_flags(wchar_t *c_pline, long *flags);
int		ft_linecomplete_flags_c(char *c_pline, long *flags);

void	ft_shfork(t_sh *sh, t_argv *targv);
void	ft_forksplit(t_sh *sh, t_argv *targv);

int		ft_getopendir(const char *path, int opt,
					void (*f)(const char *, int, char *, int));
int		ft_perror_getcwd(char **pbuff);

char	**ft_getvar(char *var);
int		ft_isvar(char *v);
int		ft_lenvar(char *v);
char	*ft_replace_var(char **str);
char	*ft_replace_var_notnull(char **str);

int		ft_parse_dotslash__cutdirslash__link(
	char **pwd, size_t *pwd_len, char *dir);

#endif
