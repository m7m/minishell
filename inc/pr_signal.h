/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_signal.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/20 03:41:06 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/20 03:41:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_SIGNAL_H
# define PR_SIGNAL_H

# include "built_in_jobs.h"

void	ft_signal_trap(int sign);
int		ft_signal_trap_sigwinch(int b);
void	ft_sigaction_init(void);
void	ft_signal_reset(void);
void	ft_signal_reset_job(void);
void	ft_signal_reset_all(void);

#endif
