/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr__static_hlocal.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 09:32:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/22 09:32:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR__STATIC_HLOCAL_H
# define PR__STATIC_HLOCAL_H
# include "t_hash.h"

t_htable	*ft__static_hlocal(t_htable *hlocal);

#endif
