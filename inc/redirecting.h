/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirecting.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 11:34:47 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/03 11:34:47 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REDIRECTING_H
# define REDIRECTING_H

# include <libft.h>
# include "t_struct.h"

/*
** type =
** 0 : normal ou ';'// obsolete
** 1 : '|'// obsolete
** 2 : ||// obsolete
** 3 : &&// obsolete
** 4 : &// obsolete
** 5 : redirection
*/

# define SFD_HERE_DOC     B(5)
# define SFD_HERE_DOC_TAB B(6)
# define SFD_BUILT_IN     B(7)
# define SFD_HERE_LIN     B(8)

/*
** opt
*/
# define SFD_CREATE   B(10)
# define SFD_OPEN     B(11)
# define SFD_DUP      B(12)
# define SFD_CLOSE    B(13)
# define SFD_REDI_OUT B(14)
# define SFD_REDI_IN  B(15)
# define SFD_ERRTOOUT B(16)

#endif
