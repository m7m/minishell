/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_history.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:16:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:16:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_HISTORY_H
# define PR_HISTORY_H

# include <stddef.h>
# include "t_hash.h"

size_t	ft_gethistory(char *key, size_t key_len, char **data);
size_t	ft_gethistory_id(int key, char **data);
size_t	ft_gethistory_id_nodup(int key, char **data);
size_t	ft_gethistory_wcs(int key, wchar_t **data);
size_t	ft_gethistory_prev(wchar_t **data);
size_t	ft_gethistory_next(wchar_t **data);
size_t	ft_gethistory_chrprev(char *motif, int startindexsearch);
size_t	ft_gethistory_chrprev_cmp(char *motif, int startindexsearch);
int		ft_history_save_current_ligne(wchar_t *line);
int		ft_history_add(char *data, int reset_id_struct);
void	ft_history_add_byid(int id, char *data);
void	ft_history_read(void);
void	ft_history_write(void);
void	ft_history_reset_line(void);
void	ft_delhistory(char *key, t_htable *thhis);
void	ft_delhistory_id(int id, t_htable *thhis);
void	ft_history_limit(t_htable *thhis);
int		ft_history_isexisttolast(char *data, t_htable thhis);

/*
** !
*/

int		ft_history_parser(char **line);

void	ft_history_parser__last_arg(
	char **line, char **pline);
void	ft_history_parser__first_arg(
	char **line, char **pline);
int		ft_history_parser__chrword(
	int start,
	char **line, char **pline,
	size_t (*fhistchr)(char *, int));
int		ft_history_parser__digit(
	int rev, char **line, char **pline);
void	ft_history_parser__last_cmd(
	int index, size_t size, char **line, char **pline);
int		ft_history_parser__substitu(char **line);

#endif
