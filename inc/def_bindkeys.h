/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   def_bindkeys.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:26:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:26:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEF_BINDKEYS_H
# define DEF_BINDKEYS_H

# define BACKSPACE	"\x7F"
# define CARRI_RET	"\x0D"
# define DEL		"\E[3~"

# define HOME		"\E[H"
# define END		"\E[F"

/*
** autocompletion
*/
# define HORIZ_TAB	"\x09"
# define NEWLINE	"\x0A"

# define CTRL_A		"\x01"
# define CTRL_C		"\x03"
# define CTRL_D		"\x04"
# define CTRL_E		"\x05"
# define CTRL_K		"\x0B"
# define CTRL_L		"\x0C"

/*
** reverse-i-search
*/
# define CTRL_R		"\x12"
# define CTRL_U		"\x15"
# define CTRL_V		"\x16"
# define CTRL_W		"\x17"
# define CTRL_Y		"\x19"

# define ALT_BACKSP	"\E" BACKSPACE
# define ALT_D		"\Ed"

/*
** lower
*/
# define ALT_L		"\El"

/*
** upper
*/
# define ALT_U		"\Eu"

# define ARROW_U	"\E[A"
# define ARROW_D	"\E[B"
# define ARROW_R	"\E[C"
# define ARROW_L	"\E[D"
# define M_ARROW_R	"\E[1;3C"
# define M_ARROW_L	"\E[1;3D"
# define C_ARROW_U	"\E[1;5A"
# define C_ARROW_D	"\E[1;5B"
# define C_ARROW_R	"\E[1;5C"
# define C_ARROW_L	"\E[1;5D"
# define META_B		"\Eb"
# define META_F		"\Ef"

#endif
