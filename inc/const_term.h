/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   const_term.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:12:21 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:12:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONST_TERM_H
# define CONST_TERM_H

# define CONST_CLIPBOARD "_clipboard"
# define CONST_QUOTE "_quote"
# define CONST_SQUOTE "_squote"
# define CONST_GRAVE "_grave"

# define CONST_DEF_HISTFILE "~/sh42_history"
# define CONST_HISTFILE "HISTFILE"
# define CONST_DEF_HISTSIZE "500"
# define CONST_HISTSIZE "HISTSIZE"
# define CONST_SAVEHIST "SAVEHIST"

# define REVERSE_I_SEARCH "(reverse-i-search)`"

#endif
