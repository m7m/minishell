/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr__static_tjobs.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 05:21:28 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/18 05:21:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR__STATIC_TJOBS_H
# define PR__STATIC_TJOBS_H

struct s_builtins_job	*ft__static_tjobs(struct s_builtins_job **origtjobs);

#endif
