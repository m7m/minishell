/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_xetenvp.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:15:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:15:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_XETENVP_H
# define PR_XETENVP_H

size_t	ft_getenvp(char *key, char **data);
size_t	ft_getnenvp(size_t nkey, char *key, char **data);
void	ft_setenvp(char *key, char *data);

#endif
