/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_xetlocal.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:15:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:15:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_XETLOCAL_H
# define PR_XETLOCAL_H

# include <stddef.h>

size_t	ft_getlocal(char *key, char **data);
size_t	ft_getnlocal(size_t nkey, char *key, char **data);
int		ft_getlocalint(char *key);
void	ft_setlocal(char *key, char *data);
void	ft_setnlocal(size_t nkey, char *key, char *data);

#endif
