/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_builtins.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 17:33:40 by mmichel           #+#    #+#             */
/*   Updated: 2017/05/04 10:48:08 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_BUILTINS_H
# define T_BUILTINS_H

# include <sys/stat.h>
# include <sys/types.h>

# define NB_OPT_READ 8
# define NB_OPT_UNARY 23
# define NB_OPT_BINARY 20
# define OPT_UNARY "bcdefghknprstuwxzLOGS"

# define B_READ_SIZE 1

enum
{
	BUILTIN_TRUE,
	BUILTIN_FALSE,
	BUILTIN_FAILURE
};

typedef struct		s_builtins_job
{
	pid_t			pc;
	pid_t			grp;
	int				status;

	int				sig_status;

	char			**argv;
}					t_builtins_job;

typedef struct		s_builtins_jobs
{
	pid_t			last_pc;
	int				last_index;
	int				max_index;

	int				*sh_opt;

	t_builtins_job	*tjobs;
}					t_builtins_jobs;

typedef struct		s_builtins_cd
{
	char			*newpwd;
	size_t			newpwd_len;
	char			*pwd;
	size_t			pwd_len;
	int				opt;
}					t_builtins_cd;

/*
**	Builtins Test
*/

typedef struct		s_opt_unary
{
	char			*opt;
	int				(*f)(const char*);
}					t_opt_unary;

typedef struct		s_opt_binary
{
	char			*opt;
	int				(*f)(int op, char **argv);
}					t_opt_binary;

/*
**	Builtins Read
*/

typedef struct		s_read
{
	int				r;
	int				fd;
	int				bign;
	long			time;
	int				reply;
	int				silent;
	int				max_char;

	char			*sep;
	char			delim;
	char			*array;
	char			*prompt;
}					t_read;

typedef struct		s_read_data
{
	int				ac;
	char			prev;
	char			*tmp;
	int				count;
	int				escape;
	char			**av;
	int				start;
	char			*line;
	int				error;

	char			buff[B_READ_SIZE + 1];
}					t_read_data;

typedef struct		s_opt_read
{
	char			opt;
	int				(*f)(t_read *read, char **argv, int *i, int j);
}					t_opt_read;

#endif
