/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:46:26 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/28 05:52:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>

# ifndef BUFF_SIZE
#  define BUFF_SIZE 1024
# endif
# ifndef MAXFD
#  define MAXFD 2048
# endif

# ifndef EOF
#  define EOF -1
# endif

int		get_next_line(int const fd, char **line);
int		get_next_linenl(int const fd, char **line);

char	*ft_strcpy(char *dst, const char *src);
char	*ft_strjoin(char const *s1, char const *s2);
char	*ft_strdup(const char *s1);
char	*ft_strsub(char const *s, unsigned int start, size_t len);
char	*ft_strchr(const char *s, int c);
char	*ft_strnew(size_t size);
void	*ft_memset(void *b, int c, size_t len);

#endif
