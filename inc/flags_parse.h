/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags_parse.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 23:42:11 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 20:29:02 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_PARSE_H
# define FLAGS_PARSE_H

enum
{
	FLP_NONE = 1,
	FLP_RESTART = 2,
	FLP_MQUOT,
	FLP_SQUOT,
	FLP_ANTSL,
	FLP_DQUOT,
	FLP_HERE,
	FLP_HERE_TAB,
	FLP_HERELINE,

	FLP_DOL_PARENTH,
	FLP_DOL_PARENTH_OPEN,
	FLP_DOL_PARENTH_CLOS,

	FLP_PARENTH,
	FLP_DQUOT_PARENTH,

	FLP_DOL_ACCOLA,
	FLP_DOL_ACCOLA_OPEN,
	FLP_DOL_ACCOLA_CLOS,
	FLP_ACCOLA,
	FLP_ACCOLA_OPEN,
	FLP_ACCOLA_CLOS,

	FLP_AND,
	FLP_OR,
	FLP_PIPEER,
	FLP_PIPE,

	FLP_OUTTO,
	FLP_INTO,

	FLP_TILDE,
	FLP_DOL,
	FLP_EQUALE,
	FLP_DOTEXC,
	FLP_CROSI,

	FLP_SPACE,
	FLP_TABUL,
	FLP_NEWLI,
	FLP_SEMIC,
	FLP_FG,

	FLP_IF,
	FLP_THEN,
	FLP_ELIF,
	FLP_ELSE,
	FLP_FI,
	FLP_SHELLSCRIPT,

	FLP_WHILE,
	FLP_DO,
	FLP_DONE,
	FLP_FOR,
	FLP_IN,
	FLP_GLOBBING,

	FLP_END_FL
};

#endif
