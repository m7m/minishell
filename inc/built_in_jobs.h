/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   built_in_jobs.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 11:47:01 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 11:47:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILT_IN_JOBS_H
# define BUILT_IN_JOBS_H

# ifndef JOBS_MAX
#  define JOBS_MAX 1024
# endif

# include "jobs.h"
# include "t_argv.h"

void	ft_builtins_fg(
	struct s_sh *sh, struct s_argv *targv);

int		ft_builtins_fg__restart_wait(
	struct s_builtins_job *tjobs, struct s_sh *sh);

void	ft_builtins_bg(
	struct s_sh *sh, struct s_argv *targv);

void	ft_builtins_jobs(
	struct s_sh *sh, struct s_argv *targv);

void	ft_builtins_jobs_bg(
	int status, pid_t pc, struct s_builtins_job **tjobs, struct s_argv *targv);

void	ft_builtins_jobs_mod(
	int status, pid_t pc, struct s_builtins_jobs *tljobs);

void	ft_builtins_jobs_add(
	int status, pid_t pc, struct s_builtins_jobs *tljobs, char **argv);

int		ft_jobs_count_status(
	int status, struct s_builtins_jobs *tljobs);

void	ft_jobs_stat_actu(pid_t pc, int status, int sig_value);
void	ft_jobs_stat(t_builtins_jobs *tljobs);
int		ft_jobs_count(t_builtins_job *tjobs);

void	ft_jobs_free_static(void);
void	ft_jobs_reset_static(void);
void	ft_jobs_reset(void);

#endif
