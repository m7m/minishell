/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_history.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/23 05:13:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/23 05:13:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_HISTORY_H
# define T_HISTORY_H

# include "t_hash.h"

/*
** id_key = représente la ligne courante lors du premier mouvement
** id_key_h = repésente la position dans l'historique
*/

typedef struct	s_history
{
	int			id_key;
	size_t		id_key_h;
	t_htable	*thhistory;
}				t_history;

t_history		g_thistory;

#endif
