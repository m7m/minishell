/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_signal.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/20 21:05:01 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/20 21:05:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_SIGNAL_H
# define SH_SIGNAL_H

# include <signal.h>
# include "pr_signal.h"

# ifndef SIGWINCH
#  define SIGWINCH 28
# endif

# define _GNU_SOURCE

#endif
