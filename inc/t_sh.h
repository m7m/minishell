/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_sh.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 17:32:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 17:32:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_SH_H
# define T_SH_H

# include "pr_hash.h"
# include <termios.h>
# include "t_builtins.h"
# include "../src/etat/t_etat.h"
# include "t_argv.h"
# include "pr__static_sh.h"

typedef struct s_sh		t_sh;

struct					s_free
{
	struct s_tree	*t;
	char			*line;
	char			*line_lastparse;
	char			*line_redirecting;
	wchar_t			**term_line_buff;
	struct s_argv	*targv_here_doc_etat;
};

struct					s_sh
{
	int						opt;
	int						nb_line;
	int						fd;

	t_htable				henvp;
	t_htable				hbuiltins;
	t_htable				hexec;
	t_htable				hcorrela;
	t_htable				hlocal;
	t_htable				hhistory;

	struct termios			sterm;
	struct s_etat			tet[255];

	struct s_builtins_jobs	tljobs;

	struct s_argv			*targv;

	struct s_free			*sf;
};

#endif
