/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_term.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 14:09:40 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/23 14:09:40 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_TERM_H
# define PR_TERM_H

# include "t_struct.h"
# include "ft_debug.h"
# include "built_in_jobs.h"

void	ft_term(t_sh *sh);
void	ft_term_reset(void);
void	ft_term_initcap(t_scap *tc);
void	ft_term_init(struct termios sterm, int fd);
void	ft_term_restore(void);
void	ft_term_read(t_sh *sh, t_scap tc);

int		ft_term_line(int fd, t_scap tc, char **line, t_sh *sh);
int		ft_istermcap(char **tmp, t_term_line *tline);
int		ft_istermcap_test(int enable_fonc, t_termcaps *tcaps);
char	*ft__tl_bcl_read(int fd);
int		ft__tl_bcl_nread(wchar_t **buff, t_term_line *tline);
void	ft__tl_bcl_nread_quot(t_term_line *tline);
int		ft_term_line__nread__hered(wchar_t n, wchar_t **buff,
								t_term_line *tline);
void	ft__tl_bcl_nread_join(int n, wchar_t **buff, t_term_line *tline);

void	ft_term_line_align_refre(t_termcaps *tcaps);
void	ft_term_line_align(int x, t_scap caps);

void	ft_term_line_refresh(int n, t_termcaps *tcaps, wchar_t *line);
void	ft_term_line_calc_ppos_x(int co, int *prompt_len, char **pprompt);
int		ft_term_line_calc_nquote(wchar_t *line);

int		ft_termcaps(int enable_fonc, t_termcaps *tcaps);
void	ft_termcaps__init_twstt(struct s_wsttermcaps twstt[]);
void	ft_termcaps__init_twstt_nof(struct s_wsttermcaps_nof twstt[]);
int		ft_termcaps_pos(int fd, int *pos_x, int *pos_y);
void	ft_termcaps_strpos(int x, int y, char *s);
int		ft_termcaps_co(void);
int		ft_termcaps_li(void);

int		ft_termcaps_rel_y_max(t_termcaps *tcaps);
int		ft_termcaps_rel_y(t_termcaps *tcaps);
int		ft_termcaps_rel_y3(int prompt_len, int len_x, int co, wchar_t *line);
int		ft_termcaps_rel_y4(t_termcaps *tcaps);
int		ft_termcaps_rel_x(t_termcaps *tcaps);

void	ft_termcaps_clr_n(int abs_x, char s[]);
void	ft_termcaps_clr_line(int abs_y, int del_y, char *cap_al);
void	ft_termcaps_clr_all(t_termcaps *tcaps);

void	ft_termcaps_setpos(int x, int y, int abs_y);
void	ft_termcaps_setclipboard(int code_func,
								int start, int end, wchar_t *line);
void	ft_termcaps_rigth_wcs(t_termcaps *tcaps, wchar_t *(*f)(wchar_t *));
void	ft_termcaps_left_wcs(t_termcaps *tcaps, wchar_t *(*f)(wchar_t *));
wchar_t	*ft_wcsnospace_left(wchar_t *s);

int		ft_putwchar_t(int c);
void	ft_term_print(wchar_t *s, t_scap cap, int prompt_len, int y_max);
void	ft_term_print_size(int print_prompt, int li, t_termcaps tcaps);

wchar_t	*ft_wcsnospace_leftv2(wchar_t *s);
wchar_t	*ft_wcsnospace_leftv3(wchar_t *s);
int		ft_search_bin(const char *motif, char ***result);
int		ft_search(int perm, const char *motif, char ***result);
int		ft_search_in_h(const char *motif, char ***resul,
					t_htable *thtable,
					size_t (*fhsearch)(t_htable, void *, size_t, t_ht ***));
void	ft_term_print_pad(size_t itab, char **tab, t_scap cap);

void	ft_term_linetoetat(int n, wchar_t **buff, t_term_line *tline);

/*
** Bindkey
*/

int		ft_termcaps__backspace(t_termcaps *tcaps);
int		ft_termcaps__del(t_termcaps *tcaps);

int		ft_termcaps__ctrl_arrow_u(t_termcaps *tcaps);
int		ft_termcaps__ctrl_arrow_d(t_termcaps *tcaps);
int		ft_termcaps__arrow_cl(t_termcaps *tcaps);
int		ft_termcaps__arrow_cr(t_termcaps *tcaps);

int		ft_termcaps__arrow_l(t_termcaps *tcaps);
int		ft_termcaps__arrow_r(t_termcaps *tcaps);
int		ft_termcaps__arrow_u(t_termcaps *tcaps);
int		ft_termcaps__arrow_d(t_termcaps *tcaps);

int		ft_termcaps__ctrl_a(t_termcaps *tcaps);
int		ft_termcaps__ctrl_c(t_termcaps *tcaps);
int		ft_termcaps__ctrl_d(t_termcaps *tcaps);
int		ft_termcaps__ctrl_e(t_termcaps *tcaps);
int		ft_termcaps__ctrl_k(t_termcaps *tcaps);
void	ft_termcaps__ctrl_k_del(t_termcaps *tcaps, int abs_x, int abs_y);
int		ft_termcaps__ctrl_l(t_termcaps *tcaps);
int		ft_termcaps__ctrl_r(t_termcaps *tcaps);
int		ft_termcaps__ctrl_u(t_termcaps *tcaps);
int		ft_termcaps__ctrl_y(t_termcaps *tcaps);
int		ft_termcaps__ctrl_v(t_termcaps *tcaps);
int		ft_termcaps__ctrl_w(t_termcaps *tcaps);

int		ft_termcaps__alt_backspace(t_termcaps *tcaps);
int		ft_termcaps__alt_d(t_termcaps *tcaps);
int		ft_termcaps__alt_l(t_termcaps *tcaps);
int		ft_termcaps__alt_u(t_termcaps *tcaps);

int		ft_termcaps__keypad_osx(t_termcaps *tcaps);

int		ft_termcaps__horiz_tab(t_termcaps *tcaps);
char	*ft_termcaps__horiz_tab__antsp(int b, char *motif, char *tabmotif);

#endif
