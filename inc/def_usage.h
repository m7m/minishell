/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   def_usage.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 11:19:04 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/11 11:19:04 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEF_USAGE_H
# define DEF_USAGE_H

# define HELP_CD "cd [-L|-P] [dir]"
# define HELP_BG  "bg jobs_spec"
# define HELP_ECHO  "echo [-neE] ..."
# define HELP_ENV "env [-i] [name=value]... [utility [argument...]]"
# define HELP_ENVBIN  "envbin"
# define HELP_EXIT "exit [n]"
# define HELP_EXPORT "export [-n] [namevar[=valuevar]] ..."
# define HELP_FALSE "false"
# define HELP_FG  "fg jobs_spec"
# define HELP_HISTORY  "history [-cdrw]"
# define HELP_JOBS  "jobs [-lprs]"
# define HELP_PWD "pwd [-L|-P]"
# define HELP_SET "set [name value]"
# define HELP_SETENV "setenv [name value]"
# define HELP_TRUE "true"
# define HELP_UNSET "unset [-a] name ..."
# define HELP_UNSETENV "unsetenv [-a] name ..."

# define S(x) " " HELP_##x "\n"

# define HELP_B S(BG)
# define HELP_C S(CD)
# define HELP_E S(ECHO) S(ENV) S(ENVBIN) S(EXIT)
# define HELP_F S(FALSE) S(FG)
# define HELP_H S(HISTORY)
# define HELP_J S(JOBS)
# define HELP_P S(PWD)
# define HELP_S S(SETENV)
# define HELP_T S(TRUE)
# define HELP_U S(UNSET) S(UNSETENV)

# define HELP_B_H HELP_B HELP_C HELP_E HELP_F HELP_H
# define HELP_J_U  HELP_J HELP_P HELP_S HELP_T HELP_U
# define HELP HELP_B_H HELP_J_U

/*
** Built-ins
*/

# define NAME_PROG_ERR NAME_PROG": "

# define CD_NAME "cd"
# define CD_USAGEL0 "Usage: " HELP_CD "\n"
# define CD_USAGEL1 "-L : Change the logical current working directory.\n"
# define CD_USAGEL2 "-P : Change the physical current working directory."
# define CD_USAGES CD_USAGEL0 CD_USAGEL1 CD_USAGEL2
# define CD_ERR_OLD "OLDPWD not set"
# define CD_ERR_HOM "HOME not set"
# define CD_ERR_NA "unknown"

# define SETENV_NAME "setenv"
# define SETENV_ERR_MANY "Too many argments."
# define SETENV_ERR_ALNUM "Variable name must contain [a-zA-Z_] characters."
# define SETENV_ERR_DIGIT "Variable name do not begin with a digit."

# define ENV_NAME "env"
# define ENV_ERR_MANY "Too many argments.\n"
# define ENV_ERR_ALNUM "Variable name must contain [a-zA-Z_] characters.\n"
# define ENV_ERR_DIGIT "Variable name do not begin with a digit.\n"

# define PWD_NAME "pwd"
# define PWD_USAGEL0 "Usage: " HELP_PWD "\n"
# define PWD_USAGEL1 "-L : Display the logical current working directory.\n"
# define PWD_USAGEL2 "-P : Display the physical current working directory."
# define PWD_USAGES PWD_USAGEL0 PWD_USAGEL1 PWD_USAGEL2

# define UNSETENV_ERR_FEW "Too few argments."

# define MSG_ERR_GETCWD MSG_ERR3("getcwd", "cannot access directories")

#endif
