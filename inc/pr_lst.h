/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_lst.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 06:05:31 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/19 06:05:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_LST_H
# define PR_LST_H

# include "t_list.h"
# include "libft.h"

t_list	*ft_lstnew(void);
t_list	*ft_lstadd(t_list **plist, t_list *new);
t_list	*ft_lstnewadd(t_list **plist);
void	ft_lstfree(t_list *lstlist);
t_list	*ft_lstsort_list(t_list *lst, int (*cmp)(t_list *, t_list *));
t_list	*ft_lstadd_sortf(t_list *lst, t_list *new,
						int (*cmp)(t_list *, t_list *));

#endif
