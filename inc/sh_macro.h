/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_macro.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 06:56:57 by mmichel           #+#    #+#             */
/*   Updated: 2017/05/04 11:28:31 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_MACRO_H
# define SH_MACRO_H

# include "sh_name_prog.h"

# define GBH00(n, s) thash = ft_hadd(thtable, #n, s)
# define GBH01(n) thash->data = (void *)ft_builtins_##n
# define GBH02 thash->data_len = 1
# define GEN_BUILTINS_H(n, s) GBH00(n, s);GBH01(n);GBH02

# define NB_CORRELA 1
# define GEN_CORRELA_H(n, s) GEN_BUILTINS_H(n, s)

# define ERR4_FMT "%1P%s: %s: %s: %s\n"
# define STDERRFD STDERR_FILENO
# define MSG_ERR4(c, p, m) ft_printf(ERR4_FMT, STDERRFD, NAME_PROG, c, p, m)

# define ERR3_FMT "%1P%s: `%s': %s\n"
# define MSG_ERR3(c, p) ft_printf(ERR3_FMT, STDERRFD, NAME_PROG, c, p)

# define ERR3_FMTINT "%1P%s: %d: %s\n"
# define MSG_ERR3INT(i, p) ft_printf(ERR3_FMTINT, STDERRFD, NAME_PROG, i, p)

# define ERR2_FMT "%1P%s: %s\n"
# define MSG_ERR2(c) ft_printf(ERR2_FMT, STDERRFD, NAME_PROG, c)
# define MSG_ERR1(name, msg) ft_printf(ERR2_FMT, STDERRFD, name, msg)

# define ERR3INTER_FMT "%1P%s: `%s': error internaly: %s\n"
# define ERR3INTER_ARG ERR3INTER_FMT, STDERRFD, NAME_PROG, __FUNCTION__
# define MSG_ERRINTER(m) ft_printf(ERR3INTER_ARG, m)

# define ERRNBLINE_FMTINT "%1P%s: line %d: `%.80s`: %s\n", STDERRFD
# define MSG_ERRNBLINE(n, c, p) ft_printf(ERRNBLINE_FMTINT, NAME_PROG, n, c, p)

#endif
