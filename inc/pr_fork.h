/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_fork.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/18 06:46:22 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/18 06:46:22 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_FORK_H
# define PR_FORK_H

# include <unistd.h>
# include "minishell.h"
# include "redirecting.h"
# include "tfd.h"
# include "targv.h"

int		ft_perror_dup(int fd, const char *msg);
int		ft_forksplit__current(int pc, t_sh *sh, t_argv *targv);
void	ft_fork__child(t_sh *sh, t_argv *targv);

int		ft_fork__child_open(t_fd *t, t_argv *targv, t_sh *sh);
int		ft_fork__child_open_xto_open(
	t_fd *t, int fd_read, int fd_write, t_sh *sh);
int		ft_fork__child_open_here(t_fd *t, t_argv *targv, t_sh *sh);

int		ft_fork__child_close(t_argv *targv);

void	ft_fork__child_exit_fd(int status, int fderr, char *msg, t_argv *targv);
void	ft_fork__child_exit(int status, char *msg, t_argv *targv);

int		ft_exec_errcode(int status, t_argv *targv);
void	ft_forksplit__print_ret(t_htable *hlocal, int status);
void	ft_fork__child_bg(t_sh *sh, t_argv *targv);

void	ft_fork_group(pid_t pc, t_sh *sh);

#endif
