/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_struct.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 11:34:03 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/01 11:21:58 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_STRUCT_H
# define T_STRUCT_H

# include <stddef.h>
# include "t_argv.h"
# include "t_tfd.h"
# include "t_builtins.h"
# include "t_sh.h"

typedef struct		s_shebang
{
	char	*pathbin;
	char	**cmd;
	size_t	cmd_len;
}					t_shebang;

/*
** im: insert mode (désactivé)
** sf: scrolle down
** sr: scrolle up
** sc: save pos
** rc: restore pos
** nd: move right
** al: add clear line
** dl: delete line
** cd: clear line one cursor...
*/

typedef struct		s_scap
{
	char	*im;
	char	*sf;
	char	*sr;
	char	*sc;
	char	*rc;
	char	*nd;
	char	*al;
	char	*dl;
	char	*cd;
}					t_scap;

/*
** pos_x: position dans la ligne edition
** *nquote: point sur tline.nquote
*/

typedef struct		s_termcaps
{
	int		ppos_x;
	int		pos_x;
	int		*nquote;

	int		abs_x;
	int		abs_y;

	int		old_bindkeys;
	int		co;

	wchar_t	**line;
	char	*home;
	char	*c;
	t_scap	cap;
}					t_termcaps;

/*
** ctput = bind key
*/

struct				s_wsttermcaps
{
	int		c_len;
	char	*c;
	char	*ctput;
	int		(*f)(t_termcaps *);
};

struct				s_wsttermcaps_nof
{
	int		f;
	int		c_len;
	char	*c;
	char	*ctput;
};

typedef struct		s_term_line
{
	long		flags;
	int			fd;
	int			len;

	int			nquote;
	wchar_t		*line;
	t_termcaps	tcaps;
}					t_term_line;

typedef struct		s_term_line_re
{
	int			n;
	int			x;
	int			y;
	int			abs_y;
	int			co_old;
	int			li;

	int			line_len;
	int			prompt_len;
	char		*prompt;
}					t_term_line_re;

struct				s_term_print_pad
{
	size_t	nb_col;
	size_t	len_max;
	size_t	y;

	size_t	itab;
	char	**tab;
};

struct				s_term_ctrl_r
{
	int		ppos_x;
	int		ichr;

	char	last_search[4096];
	char	motif[4096];
	char	buff[1026];
};

typedef struct		s_linetoargv_var
{
	int		del_index[2];

	char	**tab_var;
	char	*argv_reste;

	char	*arg;
	char	*argv;
}					t_linetoargv_var;

typedef struct		s_linetoargv_fl
{
	int		here_len;
	int		sp;
	char	*here;
	char	*argend;
	char	*argstart;
}					t_linetoargv_fl;

struct				s_term_print
{
	int		len;
	int		prompt_len;
	int		co;
	int		ny;
	wchar_t	*nl;
	wchar_t	*s;

};

#endif
