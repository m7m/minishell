/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tfd.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 12:27:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/03 12:27:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TFD_H
# define TFD_H

# include "t_tfd.h"
# include "pr_lst.h"
# include "redirecting.h"

t_fd	*ft_tfd_newadd(t_fd **node);
void	ft_tfd_mvend(t_fd **node);
void	ft_tfd_free(t_fd *node);
t_fd	*ft_tfd_newaddsort(t_fd **node);
void	ft_tfd_sort(t_fd **node);
t_fd	*ft_tfd_chrfdread(t_fd **node, int fd);
t_fd	*ft_tfd_chrfdread_notalloc(t_fd *node, int fd);
t_fd	*ft_tfd_chrfdwrite(t_fd **node, int fd);
t_fd	*ft_tfd_chrfdwrite_notalloc(t_fd *node, int fd);

t_fd	*ft_tfd_newaddfdw(t_fd **node, int fd);
t_fd	*ft_tfd_newaddfdr(t_fd **node, int fd);

#endif
