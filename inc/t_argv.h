/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_argv.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 17:30:16 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 17:30:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_ARGV_H
# define T_ARGV_H

# include "t_sh.h"

struct s_sh;

/*
** int				redirecting;// remplis/utiliser par <>&|;
** int				num_blk; voir ft_redirecting__semicolon_fg.c ancien fonction
** int				argc;// faiblement utile
** char				**argv;// execve
** char				**envp;// execve
** char				*pathbin;// execve
** void				(*builtins)(t_sh *, struct s_argv *);// voir ft_isbuiltins
** struct s_fd		*tfd;// remplis par < > << >> <<<
** struct s_argv	*next; // obsolete
*/

typedef struct		s_argv
{
	int				argc;
	char			**argv;

	char			**envp;
	char			*pathbin;
	void			(*builtins)(struct s_sh *, struct s_argv *);

	struct s_fd		*tfd;
}					t_argv;

#endif
