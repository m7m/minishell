/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr__static.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:49:21 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:49:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR__STATIC_H
# define PR__STATIC_H

# include "t_hash.h"
# include "pr__static_hlocal.h"

t_htable	*ft__static_hexec(t_htable *thtable);
t_htable	*ft__static_henvp(t_htable *henvp);
t_htable	*ft__static_hbuiltins(t_htable *hbuiltins);

#endif
