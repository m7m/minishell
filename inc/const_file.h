/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   const_file.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 15:15:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/18 15:15:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONST_FILE_H
# define CONST_FILE_H

# include <dirent.h>
# include <sys/stat.h>

# if defined(NAME_MAX) && !defined(FILENAME_MAX)
#  define FILENAME_MAX NAME_MAX
# else
#  if !defined(FILENAME_MAX)
#   if defined(__DARWIN_MAXNAMLEN)
#    define FILENAME_MAX __DARWIN_MAXNAMLEN
#   else
#    define FILENAME_MAX 1024
#   endif
#  endif
# endif

# define GET_FILE B(0)
# define GET_DOTFILE B(1)
# define GET_DIR B(2)
# define GET_DOTDIR B(3)
# define GET_DOTDOT B(4)
# define GET_PERM_R	B(8)
# define GET_PERM_W B(9)
# define GET_PERM_X B(10)
# define GET_PERM_RO B(11)
# define GET_PERM_WO B(12)
# define GET_PERM_XO B(13)
# define GET_PERM_RX (GET_PERM_R | GET_PERM_X)
# define GET_PERM_ALL (GET_PERM_R | GET_PERM_W | GET_PERM_X)
# define GET_PERM_IGN B(14)

#endif
