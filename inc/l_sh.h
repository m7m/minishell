/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   l_sh.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:17:24 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/12 16:16:11 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef L_SH_H
# define L_SH_H

# include <stddef.h>
# include <unistd.h>

/*
** Lib
*/

char	**ft_taballocadd(char **tab, char *add);
char	**ft_taballocins(char **tab, char *add);
char	**ft_taballocadd_size(char **tab, size_t len, char *add);
char	**ft_taballocjoint(char **dsttab, size_t dsttablen,
						char **srctab, size_t srctablen);
char	**ft_tabsort_add(char *tab[], char *add);
char	**ft_tabsort_add_igndbl(char *tab[], char *add);

char	**ft_tabresize(char **tab, int plus_len);
void	ft_tabdel(char **tab, int index);

char	**ft_parse_argopt(int narg, char **argv, int *retopt,
						int (*fopt_tree)(int, char));
void	ft_strdel_c(char *str, char c);
int		ft_tablen(char **tab);
size_t	ft_tabsize(char **tab);
char	**ft_tabdup(char **tab);

void	ft_perror_signal(int sign);
char	**ft_strsplit_multi(char const *s, char *cmulti, int nmulti);
char	**ft_strsplit_multiandend(char const *s, char *cmulti,
								int nmulti, char *cend);
char	*ft_strjoin_inser(char const *s1, char const *s2, int ins);
size_t	ft_stoa_s(size_t n, char *str);
char	*ft_strword(char *s);
char	*ft_strrword(char *s);

int		ft_strpcmp(const char *s1, const char *s2);

void	ft_strins(char *dst, char *ins, size_t pos);
int		ft_putwchar_t(int c);
void	ft_strreplace(char *s, int c, int replace);
char	*ft_strreplaces(const char *s, const char *motif, const char *replace);
char	*ft_strnreplaces(const char *s, const char *motif, const char *replace,
						size_t n);

char	*ft_strrchrnull(const char *s, int c);
void	**ft_malloc_array(size_t ty_l, size_t nb_l, size_t ty_c, size_t ta_col);

char	*ft_dirname(char *path);
void	ft_sleep(int time);

void	ft_putnwcs(wchar_t *wcs, int n);

int		ft_isint(char *s);
char	*ft_pass_space(char *s);
char	*ft_pass_space_rev(char *start, char *s);
char	*ft_pass_spacenewline(char *s);
char	*ft_pass_spacenewline_rev(char *start, char *s);

int		ft_islong(char *s);
char	*ft_strjoinf(char **s1, char const *s2);

char	*ft_strjoin_free12(char **s1, char **s2);
char	*ft_strjoin_free2(const char *s1, char **s2);
char	*ft_strjoin_free1(char **s1, const char *s2);

int		ft_readblock(int fd, char **line);

void	*ft_mv(void **dst, void **src);
char	*ft_mvstr(char **dst, char **src);

ssize_t	ft_readlink(const char *pathname, char **buff);

#endif
