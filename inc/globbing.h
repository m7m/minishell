/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 18:14:32 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/05 19:47:59 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GLOBBING_H
# define GLOBBING_H

unsigned char	globbing_parse_if(char *arg);
unsigned char	globbing_parse_ifs(char **args);

char			*globbing_bracket_end(char *arg);

enum	e_glob_type
{
	NORMALE,
	WILDCAR,
	INTEROG,
	BRACKET
};

enum	e_solve_t
{
	FILE,
	DIR
};

typedef struct	s_glob
{
	char			*str;
	unsigned char	type;

	char			pad[7];
	struct s_glob	*next;
}				t_glob;

typedef struct	s_globbing
{
	t_glob		**globs;
	char		*dir;
}				t_globbing;

t_globbing		globbing_parse_arg(char *arg);

void			fill_glob_normal(t_glob *glob, char **arg);
void			fill_glob_wildcard(t_glob *glob, char **arg);
void			fill_glob_interog(t_glob *glob, char **arg);
void			fill_glob_bracket(t_glob *glob, char **arg, char *ptr);

char			**globbing_solve(t_globbing glob);

unsigned char	globbing_matches(t_glob *glob, char *str);
unsigned char	next_glob(t_glob *glob, char *str);
unsigned char	globbing_matches_normale(t_glob *glob, char *str);
unsigned char	globbing_matches_wildcar(t_glob *glob, char *str);
unsigned char	globbing_matches_interog(t_glob *glob, char *str);
unsigned char	globbing_matches_bracket(t_glob *glob, char *str);

char			*glob_link_dir(char *dir, char *p);

#endif
