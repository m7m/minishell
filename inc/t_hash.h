/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_hash.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 05:20:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 05:20:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_HASH_H
# define T_HASH_H

# include "stddef.h"

typedef struct				s_hash
{
	void					*key;
	size_t					key_len;
	void					*data;
	size_t					data_len;
	int						num_table;
	struct s_hash			*next;
}							t_hash;

typedef struct				s_hdb
{
	size_t					nb_elem;
	size_t					size;
	struct s_hash			**thash;

	int						nb_collission;
	int						nb_resize;

	int						nums_table;
	int						data_static;
}							t_hdb;

/*
** Attention a thtable_lie:
** une boucle peut se produire si:
**  t1 -> t2 -> t3 -> t2
** pas de boucle dans se cas:
**  t1 -> t2 -> t3 -> t1
** thtable_lie permet d'avoir un masque avec un ou '|' binaire sur num_table
** ajout/suppression :
** d'un element dans une table lie modifie 'nb_elem' de chaque table lie
*/

typedef struct				s_htable
{
	size_t					key_lenmax;
	size_t					data_lenmax;
	size_t					nb_elem;

	int						data_static;
	int						num_table;

	struct s_htable			*thtable_lie;

	t_hdb					*hdb;
}							t_htable;

typedef t_htable			t_htab;

typedef struct				s_hupdate
{
	void					*key;
	size_t					key_len;
	void					*data;
	size_t					data_len;
}							t_hupdate;

typedef struct s_hupdate	t_hnodup;
typedef struct s_hupdate	t_ht;

#endif
