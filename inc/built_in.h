/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   built_in.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/21 00:14:41 by mmichel           #+#    #+#             */
/*   Updated: 2017/05/08 14:55:11 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILT_IN_H
# define BUILT_IN_H

# include "t_sh.h"
# include "t_argv.h"
# include "built_in_jobs.h"

/*
** Ptr builtins
*/

int		ft_isbuiltins(t_sh *sh, t_argv **targv);
void	ft_builtins_setenv(t_sh *sh, t_argv *targv);
void	ft_builtins_unset(t_sh *sh, t_argv *targv);
void	ft_builtins_unsetenv(t_sh *sh, t_argv *targv);
void	ft_builtins_env(t_sh *sh, t_argv *targv);
void	ft_builtins_exit(t_sh *sh, t_argv *targv);
void	ft_builtins_pwd(t_sh *sh, t_argv *targv);
void	ft_builtins_help(t_sh *sh, t_argv *targv);
void	ft_builtins_envbin(t_sh *sh, t_argv *targv);
void	ft_builtins_echo(t_sh *sh, t_argv *targv);
void	ft_builtins_set(t_sh *sh, t_argv *targv);
void	ft_builtins_export(t_sh *sh, t_argv *targv);

void	ft_builtins_history(t_sh *sh, t_argv *targv);

void	ft_builtins_cd(t_sh *sh, t_argv *targv);
void	ft_builtins_cd__error(char *path, int errcode, t_htable *hlocal);
int		ft_builtins_cd__opt_tree(int opt, char c_opt);
void	ft_builtins_cd____upd(t_htable *henvp, t_builtins_cd tcd);

/*
**		BUILTINS TEST
*/
int		ft_checkopt_unary(const char *s);
int		ft_one_arg(int *pos, char **argv);
int		ft_exec_unary(int *pos, char **argv);
int		ft_exec_binary(int *pos, char **argv);
int		ft_builtins_test_error(const char *msg);
int		ft_builtins_test__parser(int *pos, int argc, char **argv);

void	ft_builtins_test(t_sh *sh, t_argv *targv);
void	ft_builtins_test__init_opt_binary(t_opt_binary *tbt);
void	ft_builtins_test__init_opt_unary(t_opt_unary *tbt);

/*
**		TEST_OPT_UNARY
*/

int		ft_builtins_test__opt_unary_b(const char *file);
int		ft_builtins_test__opt_unary_c(const char *file);
int		ft_builtins_test__opt_unary_d(const char *file);
int		ft_builtins_test__opt_unary_e(const char *file);
int		ft_builtins_test__opt_unary_f(const char *file);

int		ft_builtins_test__opt_unary_g(const char *file);
int		ft_builtins_test__opt_unary_h(const char *file);
int		ft_builtins_test__opt_unary_k(const char *file);
int		ft_builtins_test__opt_unary_n(const char *file);
int		ft_builtins_test__opt_unary_p(const char *file);

int		ft_builtins_test__opt_unary_r(const char *file);
int		ft_builtins_test__opt_unary_s(const char *file);
int		ft_builtins_test__opt_unary_t(const char *file);
int		ft_builtins_test__opt_unary_u(const char *file);
int		ft_builtins_test__opt_unary_w(const char *file);

int		ft_builtins_test__opt_unary_x(const char *file);
int		ft_builtins_test__opt_unary_z(const char *file);
int		ft_builtins_test__opt_unary_bigl(const char *file);
int		ft_builtins_test__opt_unary_bigo(const char *file);
int		ft_builtins_test__opt_unary_bigg(const char *file);
int		ft_builtins_test__opt_unary_bigs(const char *file);

/*
**	TEST_OPT_BINARY
*/

int		ft_builtins_test__opt_binary_nt(int op, char **argv);
int		ft_builtins_test__opt_binary_ot(int op, char **argv);
int		ft_builtins_test__opt_binary_ef(int op, char **argv);
int		ft_builtins_test__opt_binary_eq(int op, char **argv);

int		ft_builtins_test__opt_binary_ne(int op, char **argv);
int		ft_builtins_test__opt_binary_gt(int op, char **argv);
int		ft_builtins_test__opt_binary_ge(int op, char **argv);
int		ft_builtins_test__opt_binary_lt(int op, char **argv);
int		ft_builtins_test__opt_binary_le(int op, char **argv);

int		ft_builtins_test__opt_binary_seq(int op, char **argv);
int		ft_builtins_test__opt_binary_nseq(int op, char **argv);

/*
**	READ
*/

void	ft_readinit_sig();
void	ft_readreset_sig();
void	ft_run_timeout(long sec);
int		ft_builtins_read(t_sh *sh, t_argv *targv);
int		ft_entry_read(t_read *tread, int i, int argc, char **argv);
void	ft_init_read_buff(t_read_data *rd, int ac, char **av, int start);

void	ft_init_sep(t_read *read);
void	ft_init_tread(t_read *read);
void	ft_init_opt(t_opt_read *opt);
int		ft_read_usage(char *opt, char *err);
int		ft_check_valid_identifiers(char *var);
void	ft_buff_to_local(int reply, char **av, int i, char *tmp);
int		ft_init_read(t_read *tread, int i, int argc, char **arg);
int		ft_read_buff(t_read *tread, int i, int ac, char **av);
int		ft_read_1b1(t_read *tread, int i, int argc, char **argv);
int		ft_check_var(int start, char **av, int *error, int verbose);
int		ft_parser_opt(t_opt_read *opt, t_read *read, char **argv, int *i);

int		ft_builtins_read__opt_a(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_d(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_n(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_p(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_r(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_s(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_t(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_u(t_read *read, char **argv, int *i, int j);
int		ft_builtins_read__opt_bign(t_read *read, char **argv, int *i, int j);

#endif
