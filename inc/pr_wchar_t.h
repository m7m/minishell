/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pr_wchar_t.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:25:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:25:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PR_WCHAR_T_H
# define PR_WCHAR_T_H

# include <wchar.h>
# include <stddef.h>

size_t	ft_wcslen(wchar_t const *s);

size_t	ft_wcstombs(char *dest, wchar_t *src, size_t n);
char	*ft_wcstombs_alloc(wchar_t *wcs);
wchar_t	*ft_mbstowcs_alloc(char *s);
size_t	ft_mbstowcs(wchar_t *wcs, char *src, size_t n);

size_t	ft_mbslen(char *str);
char	*ft_mbsnlen(char *str, size_t n);

wchar_t	*ft_wcsjoin_inser(wchar_t const *s1, wchar_t const *s2, int ins);
wchar_t	*ft_wcsjoin(wchar_t const *s1, wchar_t const *s2);
wchar_t	*ft_wcsdup(const wchar_t *s1);
wchar_t	*ft_wcssub(wchar_t const *s, unsigned int start, size_t len);

wchar_t	*ft_wcswcs(const wchar_t *s1, const wchar_t *s2);
wchar_t	*ft_wcschr(const wchar_t *s, int c);
wchar_t	*ft_wcsrchr(const wchar_t *s, int c);
wchar_t	*ft_wcschrnull(const wchar_t *s, int c);
wchar_t	*ft_wcsnwcs(const wchar_t *s1, const wchar_t *s2, size_t n);
int		ft_wcscmp(const wchar_t *s1, const wchar_t *s2);

wchar_t	*ft_wcsword(wchar_t *s);
wchar_t	*ft_wcsrword(wchar_t *s);

wchar_t	*ft_wcscpy(wchar_t *dst, const wchar_t *src);

size_t	ft_isutf8(wchar_t wc);

int		ft_iswalnum(wint_t wc);

int		ft_wcsncount(const wchar_t *s, const wchar_t motif, int max);
int		ft_wcscount(const wchar_t *s, const wchar_t motif);
void	ft_wcsreplace(wchar_t *s, int c, int replace);
wchar_t	*ft_wcsreplaces(const wchar_t *s,
						const wchar_t *motif,
						const wchar_t *replace);
wchar_t	*ft_wcsnreplaces(const wchar_t *s,
						const wchar_t *motif,
						const wchar_t *replace,
						size_t n);

#endif
