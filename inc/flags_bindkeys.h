/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags_bindkeys.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:14:04 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:14:04 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_BINDKEYS_H
# define FLAGS_BINDKEYS_H

/*
** flags kill:
** FL_ALT_BACKSP
** FL_ALT_D
** FL_CTRL_K
** FL_CTRL_U
** FL_CTRL_W
*/

enum
{
	FL_CTRL_C = -2,
	FL_NONE = 1,
	FL_BACKSPACE,
	FL_DEL,
	FL_HORIZ_TAB,
	FL_NEWLINE,
	FL_CARRI_RET,

	FL_CTRL_A,
	FL_CTRL_D,
	FL_CTRL_E,
	FL_CTRL_K,
	FL_CTRL_L,
	FL_CTRL_R,
	FL_CTRL_U,
	FL_CTRL_V,
	FL_CTRL_W,
	FL_CTRL_Y,

	FL_ALT_BACKSP,
	FL_ALT_D,
	FL_ALT_L,
	FL_ALT_U,

	FL_ARROW_U,
	FL_ARROW_D,
	FL_ARROW_R,
	FL_ARROW_L,
	FL_M_ARROW_R,
	FL_M_ARROW_L,
	FL_C_ARROW_U,
	FL_C_ARROW_D,
	FL_C_ARROW_R,
	FL_C_ARROW_L,
	FL_META_B,
	FL_META_F,

	FL_END_FL
};

#endif
