/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_updatekey.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 11:46:47 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 11:46:47 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_UPDATEKEY_H
# define FT_UPDATEKEY_H

void	ft_updatekey_ret(t_htable *hlocal, int ret);
void	ft_updatekey__(t_htable *henvp, char *cmd);

#endif
