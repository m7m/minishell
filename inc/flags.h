/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 05:18:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 05:18:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_H
# define FLAGS_H

# include <libft.h>

# define FL_END B(0)

/*
** Flags parse cmd
** '\\'
** "
** '
** `
** $(
** $[
** ${
*/

# define FL_SLA B(0)
# define FL_GUD B(1)
# define FL_GUS B(2)
# define FL_AGC B(3)
# define FL_DAO B(4)
# define FL_DCO B(5)
# define FL_DPO B(6)

# define FL_NEWLINE_2 			LB(0)
# define FL_ESPELUE 			LB(1)
# define FL_SEMICOL 			LB(2)
# define FL_OR      			LB(3)
# define FL_AND     			LB(4)

# define FL_PIPEERR 			LB(5)
# define FL_PIPE    			LB(6)

# define FL_HERED   			LB(7)
# define FL_HEREDT  			LB(8)
# define FL_HEREL   			LB(9)

# define FL_REDIREC_OUT			LB(10)
# define FL_REDIREC_IN 			LB(11)
# define FL_REDIREC 			(FL_REDIREC_IN | FL_REDIREC_OUT)

# define FL_SQUOT   			LB(12)
# define FL_DQUOT   			LB(13)
# define FL_GRAVE   			LB(14)
# define FL_CROCH   			LB(15)
# define FL_ACCOL   			LB(16)
# define FL_PAREN   			LB(17)
# define FL_DOLLA   			LB(18)
# define FL_EQUAL   			LB(19)
# define FL_ANTSL   			LB(20)
# define FL_ANT_C   			LB(21)
# define FL_QUOT				(FL_SQUOT | FL_DQUOT)
# define FL_PAR					(FL_ACCOL | FL_CROCH | FL_PAREN)
# define FL_ALL					(FL_QUOT | FL_GRAVE | FL_PAR)

# define FL_ERR     			-1
# define FL_DOTEX   			-1

# define FL_DELTAB  			LB(24)

# define FL_DOL_ACCOL			LB(25)
# define FL_DOL_PARENTH			LB(26)

# define FL_IF			LB(27)
# define FL_THEN		LB(28)
# define FL_ELIF		LB(29)
# define FL_ELSE		LB(30)
# define FL_FI			LB(31)
# define FL_FOR 		LB(32)
# define FL_IN  		LB(33)
# define FL_WHILE		LB(34)
# define FL_DO  	 	LB(35)
# define FL_DONE 		LB(36)
# define FL_CASE 		LB(37)
# define FL_ESAC 		LB(38)

# define FL_PAREN_CLOSE 		LB(39)
# define FL_CROISI				LB(41)

# define ETAT_NB_FLAGS			sizeof(long)

# define FL_DEL_SPACE (FL_REDIREC | FL_AND | FL_OR | FL_PIPE | FL_ANTSL)

# define FL_SHELLSCRIPT_IF FL_IF | FL_THEN | FL_ELIF | FL_ELSE
# define FL_SHELLSCRIPT_WF FL_FOR | FL_WHILE | FL_DO | FL_IN
# define FL_SHELLSCRIPT (FL_SHELLSCRIPT_IF | FL_SHELLSCRIPT_WF)

# define ISETAT_SP_IGN(flags) (flags & (FL_HERED | FL_REDIREC))
# define ISETAT_IGN(flags) (flags & (FL_HERED | FL_REDIREC))

# define CHECKCLOSE_IGN_0 FL_DELTAB | FL_SHELLSCRIPT
# define CHECKCLOSE_IGN_1 FL_DOL_ACCOL
# define CHECKCLOSE_IGN CHECKCLOSE_IGN_0 | CHECKCLOSE_IGN_1
# define ISETAT_CHECKCLOSE_IGN(flags) (flags & ~(CHECKCLOSE_IGN))

# define CHK_PARENTH_CLOS_01 FL_PAREN | FL_DOL_PARENTH
# define CHK_PARENTH_CLOS CHK_PARENTH_CLOS_01 | FL_DQUOT_DOLPAREN | FL_DQUOT
# define ISETAT_CHK_PARENTH_CLOS(flags) (flags & ~(CHK_PARENTH_CLOS))

/*
** builtins: opt cd
*/

# define FL_BUI_CD_END  B(0)
# define FL_BUI_CD_L    B(1)
# define FL_BUI_CD_P    B(2)
# define FL_BUI_CD__    B(3)
# define FL_BUI_CD_H    B(4)

/*
** builtins: opt echo
*/

# define FL_BUI_EC_END  B(0)
# define FL_BUI_EC_NNEW B(2)
# define FL_BUI_EC_BAC  B(3)
# define FL_BUI_EC_NBAC B(4)
# define FL_BUI_EC_SP   B(5)

/*
** builtins: opt jobs
*/

# define JOBS_N B(1)
# define JOBS_P B(2)
# define JOBS_R B(3)
# define JOBS_S B(4)
# define JOBS_X B(5)
# define JOBS_L (B(0) | JOBS_R | JOBS_S)

/*
** Erreur code
*/

# define ERR_NSFOD 127
# define ERR_PERM  126
# define ERR_ISDIR 126

/*
** Options sh
*/

# define SH_TTY           B(0)
# define SH_JOBS          B(1)
# define SH_DEBUG         B(2)
# define SH_SUBSH         B(3)
# define SH_REPARSE       B(4)

# define IS_SHTTY(sh) (sh->opt & SH_TTY)
# define IS_SHJOBS(sh) (sh->opt & SH_JOBS)
# define IS_SHDB(sh) (sh->opt & SH_DEBUG)
# define IS_SUBSH(sh) (sh->opt & SH_SUBSH)

#endif
