/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   targv.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 12:26:26 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/03 12:26:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TARGV_H
# define TARGV_H

# include "t_argv.h"
# include "tfd.h"

void	ft_targv_freeone(t_argv **targv);
void	ft_targv_free(t_argv *targv);
t_argv	*ft_targv_newadd(t_argv **node_targv);

#endif
