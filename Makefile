#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/18 06:53:03 by mmichel           #+#    #+#              #
#    Updated: 2017/04/05 19:37:21 by tfontani         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME := 42sh

PREFIX := .
INC := inc libft
LIBS := libft
SRC := src

VPATH = $(SRC):$(INC):$(patsubst %,:%,$(LIBS))

SRCLIBC = $(shell find $(SRC) -type f | grep ".c$$" \
	| grep -vE "(/\.|/\#)" )

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	ARCH := .osx.o
	ECHO := echo
else
	ARCH := .o
	ECHO := echo -e
endif
OBJ = $(SRCLIBC:.c=$(ARCH))

ifndef DEBUG_LVL
	DEBUG_LVL = 5
endif

CC := gcc
LDLIBS := $(patsubst %, -L%,$(LIBS))
LDFLAGS := $(subst lib, -l,$(LIBS))
CFLAGS := $(patsubst %, -I%,$(INC)) -Wall -Werror -Wextra
CFLAGS_DEFAULT := $(patsubst %, -I%,$(INC)) -Wall -Werror -Wextra
LOGFILE := debug.log
CFLAGS_DB := -g -DDEBUG_LVL=$(DEBUG_LVL)

EXTRACLEAN = $(shell find $(SRC) -type f | grep -E "(.gcov|.gcno|.gcda)$$")
EXTRACLEAN += $(shell find . -maxdepth 1 -type f \
	| grep -E "(.gcov|.gcno|.gcda)$$")

ifdef S
ifeq ($(UNAME),Darwin)
	CFLAGS += -fsanitize=address -g
	CFLAGS_DEFAULT += -fsanitize=address -g
else
	CFLAGS += -fsanitize=address -fsanitize=leak
	CFLAGS_DEFAULT += -fsanitize=address -fsanitize=leak
endif
endif

all: $(NAME)

$(NAME): $(LDFLAGS) $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDLIBS) $(LDFLAGS) -ltermcap

-lft: libft

libft: submodule
	$(MAKE) $(GNUMAKEFLAGS) -j 4 -C $@

.PHONY: submodule
submodule:
	@git submodule update --init

clean:
ifneq ("$(wildcard $(LIBS)/Makefile)", "")
	for sub in $(LIBS); do \
		$(MAKE) $(GNUMAKEFLAGS) -s -C $$sub $@ ;\
	done
endif
	$(RM) -- $(OBJ)

fclean:
ifneq ("$(wildcard $(LIBS)/Makefile)", "")
	@for sub in $(LIBS); do \
		$(MAKE) $(GNUMAKEFLAGS) -s -C $$sub $@ ;\
	done
endif
	$(RM) -- $(NAME) $(OBJ)
ifneq (,$(findstring .g, $(EXTRACLEAN)))
	$(RM) -- $(EXTRACLEAN)
	$(RM) -r -- html
endif

fclean_notlib:
	$(RM) -- $(NAME) $(OBJ)
ifneq (,$(findstring .g, $(EXTRACLEAN)))
	$(RM) -- $(EXTRACLEAN)
	$(RM) -r -- html
endif

re: fclean all

#include Makefile_inibedebug.mk

%$(ARCH): %.c
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: logfile
logfile:
	@echo LOGFILE: $(LOGFILE)
	@$(shell echo "" > $(LOGFILE))
	@touch  $(LOGFILE)

.PHONY: check-syntax
#check-syntax: CFLAGS += -pedantic -fsyntax-only
check-syntax: CFLAGS += -fsyntax-only
check-syntax: CFLAGS += -O2 -Wchar-subscripts -Wcomment -Wformat=2 -Wimplicit-int
check-syntax: CFLAGS += -Werror-implicit-function-declaration -Wmain -Wparentheses
check-syntax: CFLAGS += -Wsequence-point -Wreturn-type -Wswitch -Wtrigraphs -Wunused
check-syntax: CFLAGS += -Wuninitialized -Wunknown-pragmas -Wfloat-equal -Wundef
check-syntax: CFLAGS += -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings
check-syntax: CFLAGS += -Wconversion -Wsign-compare -Waggregate-return -Wstrict-prototypes
check-syntax: CFLAGS += -Wmissing-prototypes -Wmissing-declarations -Wmissing-noreturn
check-syntax: CFLAGS += -Wformat -Wmissing-format-attribute -Wno-deprecated-declarations
check-syntax: CFLAGS += -Wpacked -Wredundant-decls -Wnested-externs -Winline -Wlong-long
check-syntax: CFLAGS += -Wunreachable-code
check-syntax: $(LDFLAGS) $(OBJ)

.PHONY: db
db: CFLAGS_DEFAULT += -g
db: CFLAGS += $(CFLAGS_DB)
db: LIBFT_CFLAGS = -g
# db: CFLAGS += -ansi -O -Wwrite-strings -Wstrict-prototypes
# db: CFLAGS += -Wuninitialized -Wunreachable-code
db: $(NAME) logfile
	env PATH=/bin $(PWD)/$(NAME) $(OPT_TEST)

.PHONY: mem
mem: CFLAGS_DEFAULT += -g
mem: CFLAGS += -g
mem: CFLAGS_DB += -g
mem: $(NAME) | logfile
	@$(ECHO) Execute valgrind $(NAME) $(OPT_TEST)
	valgrind --tool=memcheck --leak-check=full --show-reachable=yes \
	--num-callers=20 --track-fds=yes --track-origins=yes \
	--read-var-info=yes --leak-resolution=high \
	--trace-children=yes \
	--suppressions=valgrind/tgetent_suppression_osx.supp \
	--suppressions=valgrind/tgetent_suppression_osx_fork.supp \
	--suppressions=valgrind/tgetent_suppression_linux.supp \
	$(PWD)/$(NAME) $(OPT_TEST)

.PHONY: memdb
memdb: CFLAGS += $(CFLAGS_DB)
memdb: mem

.PHONY: gcovdb
gcovdb: CFLAGS_DEFAULT += -g
gcovdb: CFLAGS += $(CFLAGS_DB)
gcovdb: gcov

.PHONY: gcov
gcov: CFLAGS_DEFAULT += -fprofile-arcs -ftest-coverage
gcov: CFLAGS += -fprofile-arcs -ftest-coverage
gcov: re $(NAME) $(OBJ)
	@$(ECHO) Genere gcov.log
	./$(NAME)
	gcov $(OBJ) -f > gcov.log
	-@mkdir html
	@$(ECHO) chmod -R o=rwX $(SRC)
	lcov -c -d . -o html/lcov.txt
	@$(ECHO) chmod -R o=rX $(SRC)
	gcov $(OBJ) -f > gcov.log
	@$(ECHO) Genere index.html in html
	genhtml html/lcov.txt -o html
	@$(ECHO) uri : file://`pwd`/html/index.html

.PHONY: gprof
gprof: CFLAGS_DEFAULT += -pg
gprof: CFLAGS += -pg
gprof: re test
	gprof --display-unused-functions -b $(BINTEST) $(OPT_TEST)

.DEFAULT_GOAL := all
.PHONY: all re clean fclean
