
# src/lib
src/lib/ft_readblock$(ARCH) : src/lib/ft_readblock.c
	$(CC) -c $(CFLAGS) $< -o $@


# signal
src/ft_signa%$(ARCH) : src/ft_signa%.c
	$(CC) -c $(CFLAGS) $< -o $@

# parser
src/parser/%$(ARCH) : src/parser/%.c
	$(CC) -c $(CFLAGS) $< -o $@

# term
src/term/ft_term_linetoetat$(ARCH) : src/term/ft_term_linetoetat.c
	$(CC) -c $(CFLAGS) $< -o $@
# src/term/ft_term_line__nread$(ARCH) : src/term/ft_term_line__nread.c
# 	$(CC) -c $(CFLAGS) $< -o $@
# src/term/ft_term_line__nread_quot$(ARCH) : src/term/ft_term_line__nread_quot.c
# 	$(CC) -c $(CFLAGS) $< -o $@
src/term/ft_term_line__nread_joint$(ARCH) : src/term/ft_term_line__nread_joint.c
	$(CC) -c $(CFLAGS) $< -o $@

# jobs
src/ft_builtins_%$(ARCH) : src/ft_builtins_%.c
	$(CC) -c $(CFLAGS) $< -o $@
src/ft_jobs%$(ARCH) : src/ft_jobs%.c
	$(CC) -c $(CFLAGS) $< -o $@

# src/term/%$(ARCH) : src/term/%.c
# 	$(CC) -c $(CFLAGS) $< -o $@

# fork
src/ft_fork%$(ARCH) : src/ft_fork%.c
	$(CC) -c $(CFLAGS) $< -o $@

# updatekey
src/ft_updatekey_$(ARCH) : src/ft_updatekey_.c
	$(CC) -c $(CFLAGS) $< -o $@

# etat
src/etat/%$(ARCH) : src/etat/%.c
	$(CC) -c $(CFLAGS) $< -o $@


src/ft_getlocal$(ARCH) : src/ft_getlocal.c
	$(CC) -c $(CFLAGS) $< -o $@

# inibe

src/%$(ARCH) : src/%.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

src/ft_setlocal$(ARCH) : src/ft_setlocal.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

# term

src/term/ft_term_initcap$(ARCH) : src/term/ft_term_initcap.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps__init_twstt$(ARCH) : src/term/ft_termcaps__init_twstt.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps__init_twstt_nof$(ARCH) : src/term/ft_termcaps__init_twstt_nof.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

# termcap
src/term/ft_termcaps_li$(ARCH) : src/term/ft_termcaps_li.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps_co$(ARCH) : src/term/ft_termcaps_co.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps_pos$(ARCH) : src/term/ft_termcaps_pos.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps_rel_$(ARCH) : src/term/ft_termcaps_rel_.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps_setpos$(ARCH) : src/term/ft_termcaps_setpos.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

src/term/ft_termcaps_clr_n$(ARCH) : src/term/ft_termcaps_clr_n.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/term/ft_termcaps_setclipboard$(ARCH) : src/term/ft_termcaps_setclipboard.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

# hash
src/lib/hashtab/%$(ARCH) : src/lib/hashtab/%.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

# wchar_t
src/lib/wchar_t/%$(ARCH) : src/lib/wchar_t/%.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

# history
src/ft_builtins_history$(ARCH) : src/ft_builtins_history.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/ft_gethistory$(ARCH) : src/ft_gethistory.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/ft_gethistory_search$(ARCH) : src/ft_gethistory_search.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/ft_xethistory$(ARCH) : src/ft_xethistory.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/ft_xethistory_read$(ARCH) : src/ft_xethistory_read.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
src/ft_xethistory_write$(ARCH) : src/ft_xethistory_write.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@

# opendir

src/lib/ft_getopendir$(ARCH) : src/lib/ft_getopendir.c
	$(CC) -c $(CFLAGS_DEFAULT) $< -o $@
