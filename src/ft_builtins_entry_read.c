/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_entry_read.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/01 12:12:24 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 10:55:38 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define ISATTY 1
#include "minishell.h"

static int
	ft_getterm_fd(int fd, struct termios *t)
{
	if (isatty(fd))
	{
		if (tcgetattr(fd, t) < 0)
		{
			ft_putendl_fd("ERROR : termios getattr", 2);
			return (-1);
		}
		return (ISATTY);
	}
	return (!ISATTY);
}

static int
	ft_prepare_term(t_read *tread, struct termios *t)
{
	if (tread->silent)
		t->c_lflag &= ~(ECHO);
	if (tread->max_char || tread->delim || tread->r)
		t->c_lflag &= ~(ICANON);
	t->c_lflag |= ISIG;
	t->c_cc[VMIN] = 1;
	t->c_cc[VTIME] = 100;
	if (tcsetattr(tread->fd, TCSANOW, t) < 0)
		return (BUILTIN_FAILURE);
	return (0);
}

static int
	ft_do_read_1b1(t_read *tread)
{
	if (!tread->silent && tread->max_char == -1 &&
		tread->delim == '\n' && !tread->r &&
		tread->bign == -1)
		return (0);
	return (1);
}

int
	ft_entry_read(t_read *tread, int i, int argc, char **argv)
{
	struct termios	t;
	int				ret;
	struct termios	save_t;

	if (ft_check_var(i, argv, NULL, 1) > 0)
		return (BUILTIN_FALSE);
	if ((ret = ft_getterm_fd(tread->fd, &t)) == -1)
		return (BUILTIN_FAILURE);
	save_t = t;
	(tread->time > 0) ? ft_run_timeout(tread->time) : ' ';
	if (ft_do_read_1b1(tread))
	{
		if (ret == ISATTY)
		{
			if ((ret = ft_prepare_term(tread, &t)))
				return (BUILTIN_FAILURE);
			ret = ft_read_1b1(tread, i, argc, argv);
			tcsetattr(tread->fd, TCSANOW, &save_t);
		}
		else
			ret = ft_read_1b1(tread, i, argc, argv);
	}
	else
		ret = ft_read_buff(tread, i, argc, argv);
	return (ret);
}
