/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_dotslash.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:52:03 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:52:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"
#include "libft.h"
#include "../inc/l_sh.h"
#include <sys/stat.h>

/*
** Retourne la fussion pwd + dir
** met la nouvelle taille dans pwd_len
*/

#include "../inc/l_sh.h"

static char
	*ft_parse_dotslash__double_dot(
		char *dir, char **pwd, size_t *pwd_len)
{
	char		*cut;

	DEBUG;
	if (*pwd_len > 1)
	{
		cut = ft_strrchr(*pwd, '/');
		{
			*pwd_len = (cut - *pwd);
			if (!*pwd_len)
				*pwd_len = 1;
			pwd[0][*pwd_len] = 0;
		}
		DEBUGSTR(*pwd);
	}
	if (dir[2])
		dir += 3;
	else
		dir += 2;
	return (dir);
}

static char
	*ft_parse_dotslash__cutdirslash(
		int opt, char *dir, char **pwd, size_t *pwd_len)
{
	char	*cut;

	cut = ft_strchrnull(dir, '/');
	DEBUGNBR(*pwd_len);
	if (pwd[0][*pwd_len - 1] != '/')
		pwd[0][(*pwd_len)++] = '/';
	ft_memcpy(pwd[0] + *pwd_len, dir, cut - dir);
	DEBUG;
	*pwd_len = *pwd_len + (cut - dir);
	pwd[0][*pwd_len] = 0;
	DEBUGSTR(pwd[0]);
	if (opt && ft_parse_dotslash__cutdirslash__link(pwd, pwd_len, dir) == -1)
		return (NULL);
	DEBUGSTR(*pwd);
	cut = ft_strchrnull(dir, '/');
	if (*cut)
		dir = cut + 1;
	else
		dir = cut;
	DEBUGSTR(dir);
	if (ft_accesschk_perror(pwd[0], X_OK))
		return (NULL);
	return (dir);
}

static char
	*ft_parse_dotslash__cut(int opt, char *dir, char **pwd, size_t *pwd_len)
{
	char	*cut;

	DEBUG;
	while (*dir == '/' && dir[1] == '/')
		++dir;
	DEBUGSTR(dir);
	if (*dir == '.' && dir[1] == '.' && (dir[2] == '/' || dir[2] == 0))
		dir = ft_parse_dotslash__double_dot(dir, pwd, pwd_len);
	else if (*dir == '.' && (dir[1] == '/' || dir[1] == 0))
	{
		DEBUGSTR(dir);
		if (*(++dir))
			++dir;
	}
	else if ((cut = ft_strchrnull(dir, '/')) != dir)
		dir = ft_parse_dotslash__cutdirslash(opt, dir, pwd, pwd_len);
	else
		++dir;
	return (dir);
}

/*
** Supprime le superflu pour retourner la destion voulu
** opt = 1 verifie lien symbolique
** opt = 0 ignore lien symbolique
*/

char
	*ft_parse_dotslash(int opt, char *dir, char *pwd)
{
	size_t	pwd_len;
	char	*ppwd;
	char	**pppwd;

	DEBUGSTR(dir);
	pwd_len = ft_strlen(pwd);
	DEBUGSTR(pwd);
	DEBUGNBR(pwd_len);
	ppwd = ft_memdup_resize(
		pwd, pwd_len + 1, ft_strlen(dir) + sizeof(char) * 2);
	ft_memchk_exit(ppwd);
	ppwd[pwd_len] = 0;
	DEBUG;
	pppwd = &ppwd;
	if (*dir == '/')
		++dir;
	while (dir && *dir)
		dir = ft_parse_dotslash__cut(opt, dir, pppwd, &pwd_len);
	DEBUGSTR(*pppwd);
	if (dir)
		return (*pppwd);
	free(*pppwd);
	return (NULL);
}
