/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_pwd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:57:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:57:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_builtins_pwd_opt_p(void)
{
	char	*data;

	DEBUG;
	if (ft_perror_getcwd((char **)&data))
		return ;
	ft_putstr(data);
	free(data);
	ft_putchar('\n');
}

void
	ft_builtins_pwd(t_sh *sh, t_argv *targv)
{
	void	*data;
	size_t	idata;

	DEBUGSTR(*targv->argv);
	ft_updatekey_ret(&sh->hlocal, 0);
	if (targv->argc > 1 && targv->argv[1][0] == '-' && targv->argv[1][1] != 'L')
	{
		if (targv->argv[1][1] == 'P' && !targv->argv[1][2])
			ft_builtins_pwd_opt_p();
		else
			MSG_ERR1("pwd", PWD_USAGES);
	}
	else if ((idata = ft_hchr(sh->hlocal, "PWD", 3, &data)))
	{
		write(STDOUT_FILENO, data, ft_strlen((char *)data));
		ft_putchar('\n');
	}
	else
	{
		MSG_ERR_GETCWD;
		ft_updatekey_ret(&sh->hlocal, 1);
	}
}
