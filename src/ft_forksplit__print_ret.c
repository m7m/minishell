/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_forksplit__print_ret.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 22:14:03 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/31 22:14:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_forksplit__print_ret(t_htable *hlocal, int status)
{
	if (status)
	{
		if (status < 256)
		{
			ft_perror_signal(status);
			status += 128;
		}
		else
			status >>= 8;
	}
	DEBUGNBR(status);
	ft_updatekey_ret(hlocal, status);
}
