/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isexec_bin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/22 14:18:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/22 14:18:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "redirecting.h"

static void
	ft___errprint(int errcode, char *cmd, char *msg, t_sh *sh)
{
	MSG_ERRNBLINE(sh->nb_line, cmd, msg);
	ft_updatekey_ret(&sh->hlocal, errcode);
}

static int
	ft_isexec_bin__err(t_sh *sh, char *cmd, char *data)
{
	int			errcode;
	struct stat	data_stat;

	if (!data || !*cmd)
		ft___errprint(-1, cmd, "command not found", sh);
	else if ((errcode = ft_accesschk_path_x((char *)data)))
	{
		DEBUGNBR(errcode);
		if (errcode == -1)
			ft___errprint(ERR_NSFOD, cmd, "No such file or directory", sh);
		else if (errcode & X_OK)
			ft___errprint(ERR_PERM, cmd, "Permssion denied", sh);
	}
	else if (!stat(data, &data_stat) && S_ISDIR(data_stat.st_mode))
		ft___errprint(ERR_ISDIR, cmd, "is a directory", sh);
	else
		return (0);
	return (-1);
}

int
	ft_isexec_bin(t_sh *sh, t_argv *targv)
{
	void		**data;

	DEBUGSTR(targv->pathbin);
	data = (void **)&targv->pathbin;
	DEBUGSTR(*targv->argv);
	if (ft_strchr(*targv->argv, '/'))
		*data = (void *)*targv->argv;
	else if (!ft_hchr(sh->hexec, *targv->argv, ft_strlen(*targv->argv), data))
		*data = ft_exec_force_update(&sh->hexec, sh->henvp, *targv->argv);
	DEBUGSTR((char *)*data);
	if (ft_isexec_bin__err(sh, *targv->argv, targv->pathbin)
		|| ft_isshebang(sh, targv))
		return (-1);
	return (0);
}
