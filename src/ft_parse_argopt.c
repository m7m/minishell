/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_argopt.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:38:31 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:38:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int
	ft_parse_argopt__opt(int *retopt, char *c_opt, int (*fopt_tree)(int, char))
{
	int	opt;

	opt = *retopt;
	DEBUGSTR(c_opt);
	DEBUGNBR(opt);
	if (opt & FL_END)
		return (1);
	else if (*c_opt == '-' && *(c_opt + 1) == '-' && *(c_opt + 2) == 0)
	{
		DEBUG;
		*retopt |= FL_END;
		return (0);
	}
	if (*c_opt == '-' && *(c_opt + 1))
	{
		while (*(++c_opt))
		{
			if ((opt = fopt_tree(opt, *c_opt)) == -1)
				return (1);
		}
		DEBUGNBR(opt);
		*retopt = opt;
		return (0);
	}
	return (1);
}

char
	**ft_parse_argopt(int narg, char **argv, int *retopt,
					int (*fopt_tree)(int, char))
{
	int		i;
	int		j;
	char	**arg;

	i = -1;
	j = 0;
	arg = NULL;
	DEBUGNBR(narg);
	if (narg)
	{
		if (!(arg = (char **)malloc(sizeof(char *) * (narg + 1))))
			ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
		arg[narg] = NULL;
		while (++i < narg)
		{
			if (ft_parse_argopt__opt(retopt, argv[i], fopt_tree))
			{
				DEBUGSTR(argv[i]);
				arg[j] = argv[i];
				++j;
			}
		}
		arg[j] = NULL;
	}
	return (arg);
}
