/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal_trap_sigwinch.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/24 11:54:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/24 11:54:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int
	ft_signal_trap_sigwinch(int b)
{
	static int	flags = 0;
	int			ret;

	ret = flags;
	if (b)
		flags = b;
	else
		flags = 0;
	return (ret);
}
