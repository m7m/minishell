/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setlocal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 09:13:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/22 09:13:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_hash.h"
#include "pr_wchar_t.h"
#include "pr__static_hlocal.h"

void
	ft_setlocal(char *key, char *data)
{
	t_htable	*hlocal;

	DEBUG;
	hlocal = ft__static_hlocal(NULL);
	if ((data = ft_hupd_nosize(hlocal, key, data)))
		free(data);
}

void
	ft_setnlocal(size_t nkey, char *key, char *data)
{
	t_htable	*hlocal;

	DEBUG;
	hlocal = ft__static_hlocal(NULL);
	ft_hupd_n(hlocal, (void *)key, nkey, data);
}
