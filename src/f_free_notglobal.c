/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_free_notglobal.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 18:51:30 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 18:51:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stddef.h>
#define MAXFREE 255
#include <libft.h>

#include "parser/f_tree.h"

#include "../inc/targv.h"
#include "../inc/pr_fork.h"

struct s_free
	*f_free_notglobal__static(void)
{
	static struct s_free	ptrfree[1];

	return (ptrfree);
}

void
	f_free_notglobal_cal(void)
{
	struct s_free	*ptrfree;

	DEBUG;
	ptrfree = f_free_notglobal__static();
	DEBUGPTR(ptrfree->t);
	if (ptrfree->t)
		f_tree_freeparent(ptrfree->t);
	ptrfree->t = NULL;
	ft_strdel(&ptrfree->line);
	if (ptrfree->term_line_buff && *ptrfree->term_line_buff)
	{
		free(*ptrfree->term_line_buff);
		*ptrfree->term_line_buff = NULL;
		ptrfree->term_line_buff = NULL;
	}
	if (ptrfree->line_lastparse)
		ft_strdel(&ptrfree->line_lastparse);
	if (ptrfree->targv_here_doc_etat)
	{
		ft_fork__child_close(ptrfree->targv_here_doc_etat);
		ft_targv_freeone(&ptrfree->targv_here_doc_etat);
	}
	if (ptrfree->line_redirecting)
		ft_strdel(&ptrfree->line_redirecting);
}
