/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__parser.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 13:04:02 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:08:47 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test_error(const char *msg)
{
	MSG_ERR3("test", msg);
	return (BUILTIN_FAILURE);
}

static int
	ft_parser__(int *pos, int argc, char **argv, int neg)
{
	int		ret;

	while (*pos < argc && !ft_strcmp(argv[*pos], "!"))
	{
		(*pos)++;
		neg = !neg;
	}
	if (*pos >= argc)
		return (ft_builtins_test_error("argument expected"));
	if ((argc - *pos) >= 3 && (ret = ft_exec_binary(pos, argv)) != -1)
		return (neg ^ ret);
	if (argv[*pos][0] == '-' && argv[*pos][1] && !argv[*pos][2])
	{
		if (ft_checkopt_unary(argv[*pos]))
		{
			ret = ft_exec_unary(pos, argv);
			if (argc - (*pos + 1) < 0)
				return (ft_builtins_test_error("argument expected"));
			(*pos)++;
			return (neg ^ ret);
		}
		MSG_ERR4("test", argv[*pos], "unary operator expected");
		return (BUILTIN_FAILURE);
	}
	return (neg ^ ft_one_arg(pos, argv));
}

static int
	ft_and__(int *pos, int argc, char **argv)
{
	int		ret;
	int		tmp;

	ret = 1;
	tmp = ret;
	while (*pos < argc)
	{
		tmp = ft_parser__(pos, argc, argv, 0);
		if (tmp != BUILTIN_FAILURE)
			ret = ret & !tmp;
		else
			ret = BUILTIN_FAILURE;
		if (ret == BUILTIN_FAILURE ||
			!argv[*pos] ||
			ft_strcmp(argv[*pos], "-a") != 0)
			return (ret);
		(*pos)++;
	}
	return (ret);
}

int
	ft_builtins_test__parser(int *pos, int argc, char **argv)
{
	int		ret;
	int		tmp;

	ret = 0;
	while (*pos < argc)
	{
		tmp = ft_and__(pos, argc, argv);
		ret = (tmp != BUILTIN_FAILURE) ? ret | tmp : BUILTIN_FAILURE;
		if (ret == BUILTIN_FAILURE ||
			!argv[*pos] ||
			ft_strcmp(argv[*pos], "-o") != 0)
			break ;
		(*pos)++;
	}
	if (argv[*pos - 1][0] == '-' &&
		(argv[*pos - 1][1] == 'a' ||
		argv[*pos - 1][1] == 'o'))
		return (ft_builtins_test_error("argument expected"));
	return (ret);
}
