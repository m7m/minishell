/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xethistory.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:54:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:54:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_history.h"
#include "pr_history.h"
#include "pr_wchar_t.h"

/*
** Supprime la ligne sauvegarder
*/

void
	ft_history_reset_line(void)
{
	int		id;
	char	skey[255];

	if (g_thistory.id_key)
	{
		id = ft_itoa_s(g_thistory.id_key, skey);
		ft_hdelkey_table(g_thistory.thhistory, skey, id);
		g_thistory.id_key = 0;
	}
	g_thistory.id_key_h = 0;
}

void
	ft_history_add_byid(int id, char *data)
{
	char		skey[255];

	DEBUG;
	ft_itoa_s(id, skey);
	if ((data = ft_hupd_nosize(g_thistory.thhistory, skey, data)))
		free(data);
}

/*
** Retourne la key(id)
** data n'est pas dupliqué donc il ne doit pas être free
*/

int
	ft_history_add(char *data, int reset_id_struct)
{
	int			id;
	char		skey[255];
	t_hupdate	hupd;

	DEBUGSTR(data);
	if (reset_id_struct)
		ft_history_reset_line();
	id = ft_history_isexisttolast(data, *g_thistory.thhistory);
	if (id < 1)
		return (-1);
	hupd.key = skey;
	hupd.key_len = ft_itoa_s(id, skey);
	hupd.data = (void *)data;
	if (data)
		hupd.data_len = ft_strlen(data) + 1;
	else
		hupd.data_len = 0;
	if (ft_hupd(g_thistory.thhistory, &hupd))
		free(hupd.data);
	return (id);
}

/*
** Ne sauvegarde que si id_key est null
** si line == NULL alors supprime id_key
*/

int
	ft_history_save_current_ligne(wchar_t *line)
{
	char	*mbs_line;

	DEBUG;
	if (g_thistory.id_key)
		return (g_thistory.id_key);
	g_thistory.id_key = g_thistory.thhistory->nb_elem + 1;
	g_thistory.id_key_h = 0;
	DEBUGNBR(g_thistory.id_key);
	;
	mbs_line = NULL;
	if (line)
		ft_memchk_exit(mbs_line = ft_wcstombs_alloc(line));
	ft_history_add_byid(g_thistory.id_key, mbs_line);
	if (mbs_line)
		free(mbs_line);
	DEBUG;
	return (0);
}
