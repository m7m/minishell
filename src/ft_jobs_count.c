/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_jobs_count.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/24 08:51:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/24 08:51:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/t_builtins.h"
#include "../inc/jobs.h"
#include <libft.h>
#include <signal.h>
#include "../inc/built_in_jobs.h"

int
	ft_jobs_count(t_builtins_job *tjobs)
{
	int	i;
	int	n;

	DEBUGPTR(tjobs);
	if (!tjobs)
		return (0);
	i = 0;
	n = 0;
	while (i < JOBS_MAX)
	{
		if (tjobs[i].pc > 0)
			++n;
		++i;
	}
	DEBUGNBR(n);
	return (n);
}

pid_t
	ft_jobs_count_status(int status, t_builtins_jobs *tljobs)
{
	int				i;
	t_builtins_job	*tjobs;

	DEBUGPTR(tljobs);
	tjobs = tljobs->tjobs;
	if (!tjobs)
		return (0);
	i = tljobs->max_index + 1;
	while (i)
	{
		--i;
		if (tjobs[i].pc > 0 && tjobs[i].status == status)
		{
			DEBUGNBR(i);
			DEBUGNBR(tjobs[i].pc);
			DEBUGNBR(tjobs[i].grp);
			return (tjobs[i].pc);
		}
	}
	DEBUG;
	return (0);
}
