/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:37:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:37:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef DEBUG_LVL

/*
** Pas d'allocation lors d'une interception d'un signal
*/

# if DEBUG_LVL != 2 && DEBUG_LVL != 5
#  undef DEBUG_LVL
#  define DEBUG_LVL 2
# endif
#endif

#ifndef _POSIX_C_SOURCE
# define _POSIX_C_SOURCE 200809L
#endif

#include "minishell.h"
#include "sh_signal.h"

void
	ft_sigaction_trap(int sign, siginfo_t *sinfo, void *v)
{
	(void)v;
	DEBUGNBR(sign);
	if (sinfo)
	{
		DEBUGNBR(sinfo->si_signo);
		DEBUGNBR(sinfo->si_pid);
		DEBUGNBR(sinfo->si_status);
		DEBUGNBR(*(int *)&sinfo->si_value);
		DEBUGNBR(WIFEXITED(sinfo->si_status));
		DEBUGNBR(WIFSIGNALED(sinfo->si_status));
		DEBUGNBR(WIFSTOPPED(sinfo->si_status));
	}
	if (sign == SIGCHLD)
	{
		if (sinfo->si_pid)
			ft_jobs_stat_actu(sinfo->si_pid, sinfo->si_status,
							sinfo->si_value.sival_int);
	}
	else if (sign == SIGSEGV)
		ft_exit(SIGSEGV, "Terminated: SIGSEGV\n");
	else if (sign == SIGUSR1)
		ft_exit(SIGUSR1, "Terminated: SIGUSR1\n");
	else if (sign == SIGWINCH)
		ft_signal_trap_sigwinch(1);
	DEBUG;
}

void
	ft_sigaction_init(void)
{
	struct sigaction act;

	DEBUG;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	act.sa_handler = SIG_IGN;
	act.sa_flags = SA_RESTART;
	if (sigaction(SIGTSTP, &act, 0) == -1 || sigaction(SIGTTIN, &act, 0) == -1
	|| sigaction(SIGTTOU, &act, 0) == -1 || sigaction(SIGQUIT, &act, 0) == -1
	|| sigaction(SIGHUP, &act, 0) == -1 || sigaction(SIGTERM, &act, 0) == -1
	|| sigaction(SIGINT, &act, 0) == -1 || sigaction(SIGALRM, &act, 0) == -1)
		ft_exit(EXIT_FAILURE, "sigaction failure\n");
	;
	act.sa_handler = 0;
	act.sa_flags = SA_RESTART | SA_SIGINFO;
	act.sa_sigaction = ft_sigaction_trap;
	if (sigaction(SIGWINCH, &act, 0) == -1 || sigaction(SIGUSR1, &act, 0) == -1)
		ft_exit(EXIT_FAILURE, "sigaction failure\n");
	COM("'");
	act.sa_flags = SA_RESETHAND | SA_SIGINFO;
	/* if (sigaction(SIGSEGV, &act, 0) == -1) */
	/* 	ft_exit(EXIT_FAILURE, "sigaction failure\n"); */
	act.sa_flags = SA_RESTART | SA_SIGINFO | SA_NOCLDSTOP;
	if (sigaction(SIGCHLD, &act, 0) == -1)
		ft_exit(EXIT_FAILURE, "sigaction failure\n");
}

void
	ft_signal_trap(int sign)
{
	DEBUGNBR(sign);
	if (sign == SIGINT)
	{
		DEBUG;
		signal(SIGINT, SIG_IGN);
		kill(0, SIGINT);
		signal(SIGINT, SIG_DFL);
	}
	else if (sign == SIGWINCH)
		ft_signal_trap_sigwinch(1);
}
