/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shfork.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/18 06:38:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/18 06:38:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "redirecting.h"

void
	ft_shfork(t_sh *sh, t_argv *targv)
{
	DEBUGPTR(targv);
	if (targv->builtins && !targv->tfd)
	{
		DEBUGSTR("no fork built-in");
		targv->builtins(sh, targv);
		DEBUGSTR("end built-in");
	}
	else
		ft_forksplit(sh, targv);
}
