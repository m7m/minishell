/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_binary_3.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:45:58 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:00:11 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test__opt_binary_seq(int op, char **argv)
{
	return (!(!ft_strcmp(argv[op - 1], argv[op + 1])));
}

int
	ft_builtins_test__opt_binary_nseq(int op, char **argv)
{
	return (!(ft_strcmp(argv[op - 1], argv[op + 1])));
}
