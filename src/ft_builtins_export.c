/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_export.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 04:24:20 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/06 04:24:20 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "targv.h"

static int
	ft_builtins___err(char *key)
{
	char	*pkey;

	if (ft_isdigit(*key))
	{
		MSG_ERR3("export", SETENV_ERR_DIGIT);
		return (1);
	}
	pkey = key;
	while (*key)
		if (ft_isalnum(*key) || *key == '_')
			++key;
		else
		{
			DEBUGSTR(key);
			DEBUGSTR(pkey);
			MSG_ERR3("export 0", SETENV_ERR_ALNUM);
			return (1);
		}
	if (pkey == key)
	{
		MSG_ERR3("export 1", SETENV_ERR_ALNUM);
		return (1);
	}
	return (0);
}

static void
	ft_builtins_export__opt(t_htable *dst, t_htable *src, char **argv)
{
	char	*key;
	char	*data;

	while (*argv)
	{
		key = *argv;
		if ((data = ft_strchr(key, '=')))
		{
			*data = 0;
			++data;
		}
		if (ft_builtins___err(key))
			return ;
		DEBUGSTR(key);
		DEBUGSTR(data);
		ft_hmove_key_nosize(dst, src, key);
		if (data)
			if ((data = (char *)ft_hupd_nosize(dst, key, data)))
				free(data);
		++argv;
	}
}

void
	ft_builtins_export(t_sh *sh, t_argv *targv)
{
	DEBUGSTR(*targv->argv);
	DEBUGPTR(sh);
	if (targv->argc == 1)
		ft_hashto_keyeqdata_write(sh->henvp);
	else if (!ft_strcmp("-n", targv->argv[1]))
	{
		if (targv->argc > 2)
			ft_builtins_export__opt(&sh->hlocal, &sh->henvp, targv->argv + 2);
		else
			ft_hashto_keyeqdata_write(sh->henvp);
	}
	else
		ft_builtins_export__opt(&sh->henvp, &sh->hlocal, targv->argv + 1);
	ft_updatekey_ret(&sh->hlocal, 0);
}
