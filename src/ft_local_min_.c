/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_local_min_.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:42:32 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:42:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_local_min__ifs(t_htable *hlocal)
{
	void	*data;

	if (!(data = ft_hupd_nosize(hlocal, "IFS", " \t\n")))
		free(data);
}
