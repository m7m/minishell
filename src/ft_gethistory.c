/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gethistory.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/23 05:18:06 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/23 05:18:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_history.h"
#include "pr_history.h"
#include "pr_wchar_t.h"

/*
** Ne pas oublier ft_hchr renvoies la taille avec \0
*/

size_t
	ft_gethistory(char *key, size_t key_len, char **data)
{
	size_t	data_len;
	char	*tmp;

	data_len = ft_hchr(*g_thistory.thhistory, key, key_len, (void **)&tmp);
	if (data_len > 1)
	{
		*data = ft_memdup(tmp, data_len);
		ft_memchk_exit(*data);
		--data_len;
	}
	return (data_len);
}

size_t
	ft_gethistory_id(int key, char **data)
{
	int		key_len;
	char	skey[255];

	key_len = ft_itoa_s(key, skey);
	return (ft_gethistory(skey, key_len, data));
}

size_t
	ft_gethistory_wcs(int key, wchar_t **data)
{
	int		key_len;
	size_t	data_len;
	char	*tmp;
	char	skey[255];

	key_len = ft_itoa_s(key, skey);
	data_len = ft_hchr(*g_thistory.thhistory, skey, key_len, (void **)&tmp);
	if (data_len > 0)
	{
		*data = ft_mbstowcs_alloc(tmp);
		DEBUGWCS(*data);
		ft_memchk_exit(*data);
		data_len = ft_wcslen(*data);
	}
	return (data_len);
}

size_t
	ft_gethistory_prev(wchar_t **data)
{
	DEBUGNBR(g_thistory.thhistory->nb_elem);
	DEBUGNBR(g_thistory.id_key_h);
	if (!g_thistory.id_key_h)
		g_thistory.id_key_h = g_thistory.thhistory->nb_elem;
	if (g_thistory.id_key_h > 1)
		--g_thistory.id_key_h;
	else
		g_thistory.id_key_h = 1;
	DEBUGNBR(g_thistory.id_key);
	return (ft_gethistory_wcs(g_thistory.id_key_h, data));
}

size_t
	ft_gethistory_next(wchar_t **data)
{
	size_t	ret;

	DEBUG;
	if (!g_thistory.id_key_h)
		g_thistory.id_key_h = g_thistory.thhistory->nb_elem;
	if (g_thistory.id_key_h < g_thistory.thhistory->nb_elem)
		++g_thistory.id_key_h;
	ret = ft_gethistory_wcs(g_thistory.id_key_h, data);
	DEBUGNBR(g_thistory.id_key);
	if (g_thistory.id_key_h == g_thistory.thhistory->nb_elem)
		ft_history_reset_line();
	return (ret);
}
