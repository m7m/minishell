/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_jobs_bg.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 05:04:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/23 05:04:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include "t_struct.h"
#include "l_sh.h"
#include "libft.h"
#include "minishell.h"
#include "../inc/jobs.h"

static void
	ft_builtins_jobs_bg__print(int index, int pc, int *sh_opt)
{
	if (*sh_opt & SH_TTY)
	{
		ft_putstr("Job [");
		ft_putnbr(index);
		ft_putstr("]: pid ");
		ft_putnbr(pc);
		ft_putstr("\n");
	}
}

/*
** Sauvegarde pid et un index associe
*/

static int
	ft_builtins_jobs_add__index(
		pid_t pc, t_builtins_jobs *tljobs)
{
	int	i;

	if (!tljobs->max_index
		|| tljobs->tjobs[tljobs->max_index].pc != 0)
		i = ++tljobs->max_index;
	else
	{
		if (!tljobs->max_index)
			++tljobs->max_index;
		i = tljobs->max_index;
	}
	DEBUGNBR(i);
	if (i >= JOBS_MAX)
	{
		MSG_ERR4("jobs", i, "superior to JOBS_MAX");
		kill(pc, SIGINT);
		--tljobs->max_index;
		return (-1);
	}
	return (i);
}

void
	ft_builtins_jobs_add(
		int status, pid_t pc, t_builtins_jobs *tljobs, char **argv)
{
	int				i;
	t_builtins_job	*ppc;

	DEBUGPTR(tljobs);
	DEBUGPTR(tljobs->tjobs);
	DEBUGNBR(pc);
	ppc = tljobs->tjobs;
	if ((i = ft_builtins_jobs_add__index(pc, tljobs)) == -1)
		return ;
	DEBUGNBR(ppc[i].pc);
	ppc[i].pc = pc;
	ppc[i].status = status;
	ppc[i].grp = getpgid(pc);
	DEBUGSTR(*argv);
	if (!ppc[i].argv && argv && *argv)
	{
		ppc[i].argv = ft_tabdup(argv);
		ft_memchk_exit(ppc[i].argv);
		DEBUGSTR(ppc[i].argv[0]);
	}
	if (status != JOB_PENDING)
		ft_builtins_jobs_bg__print(i, (int)pc, tljobs->sh_opt);
}

void
	ft_builtins_jobs_mod(
		int status, pid_t pc, t_builtins_jobs *tljobs)
{
	int				i;
	t_builtins_job	*tjobs;

	DEBUGPTR(tljobs);
	DEBUGNBR(pc);
	tjobs = tljobs->tjobs;
	i = tljobs->max_index;
	DEBUG;
	while (i && tjobs[i].pc != pc)
		--i;
	if (!i)
	{
		MSG_ERR4("error internaly", "jobs control", "poor implementation");
		return ;
	}
	DEBUGNBR(i);
	DEBUGNBR(tjobs[i].status);
	tjobs[i].pc = pc;
	tjobs[i].status = status;
	DEBUGSTR(tjobs[i].argv[0]);
	ft_builtins_jobs_bg__print(i, (int)pc, tljobs->sh_opt);
}
