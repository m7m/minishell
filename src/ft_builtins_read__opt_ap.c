/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read__opt_ap.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 17:30:31 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 10:58:05 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_read__opt_a(t_read *read, char **argv, int *i, int j)
{
	if (argv[*i][j + 1])
		read->array = &(argv[*i][j + 1]);
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-a", "option requires an argument"));
		read->array = argv[++(*i)];
	}
	return (0);
}

int
	ft_builtins_read__opt_d(t_read *read, char **argv, int *i, int j)
{
	if (argv[*i][j + 1])
		read->delim = argv[*i][j + 1];
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-d", "option requires an argument"));
		read->delim = argv[++(*i)][0];
	}
	return (0);
}

int
	ft_builtins_read__opt_n(t_read *read, char **argv, int *i, int j)
{
	char	*tmp;
	int		max_char;

	if (argv[*i][j + 1])
		tmp = &(argv[*i][j + 1]);
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-n", "option requires an argument"));
		tmp = argv[++(*i)];
	}
	if (!ft_isint(tmp))
	{
		MSG_ERR4("read", tmp, "invalid number");
		return (BUILTIN_FALSE);
	}
	max_char = ft_atoi(tmp);
	if (max_char < 0)
	{
		MSG_ERR4("read", tmp, "invalid number");
		return (BUILTIN_FALSE);
	}
	read->bign = -1;
	read->max_char = max_char;
	return (0);
}

int
	ft_builtins_read__opt_bign(t_read *read, char **argv, int *i, int j)
{
	char	*tmp;
	int		max_char;

	if (argv[*i][j + 1])
		tmp = &(argv[*i][j + 1]);
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-n", "option requires an argument"));
		tmp = argv[++(*i)];
	}
	if (!ft_isint(tmp))
	{
		MSG_ERR4("read", tmp, "invalid number");
		return (BUILTIN_FALSE);
	}
	max_char = ft_atoi(tmp);
	if (max_char < 0)
	{
		MSG_ERR4("read", tmp, "invalid number");
		return (BUILTIN_FALSE);
	}
	read->max_char = -1;
	read->bign = max_char;
	return (0);
}

int
	ft_builtins_read__opt_p(t_read *read, char **argv, int *i, int j)
{
	if (argv[*i][j + 1])
		read->prompt = &(argv[*i][j + 1]);
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-p", "option requires an argument"));
		read->prompt = argv[++(*i)];
	}
	return (0);
}
