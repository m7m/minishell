/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_correla_.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/16 12:01:22 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/16 12:01:22 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "const_file.h"

static void
	ft_correla_path__upd(t_htable *hcorrela, t_hash *thnewpath)
{
	t_hupdate	hupd;

	DEBUG;
	hupd.key = "PATH";
	hupd.key_len = SC(4);
	hupd.data = thnewpath->data;
	hupd.data_len = thnewpath->data_len;
	DEBUG;
	if (ft_hupd(hcorrela, &hupd))
		free(hupd.data);
}

void
	ft_correla_path(t_htable *hexec, t_htable *hcorrela, t_htable henvp)
{
	void		*oldpath;
	size_t		oldpath_len;
	t_hash		*thnewpath;
	char		path[4096 + 1];

	DEBUG;
	thnewpath = ft_hchr_th(henvp, "PATH", SC(4));
	oldpath = NULL;
	oldpath_len = ft_hchr(*hcorrela, "PATH", SC(4), (void *)&oldpath);
	DEBUG;
	if (!thnewpath)
		ft_hdel_table_cont(hexec);
	if (!ft_hchk_deqd(thnewpath, oldpath, oldpath_len, henvp.num_table))
		return ;
	if (thnewpath->data_len > 4096)
	{
		MSG_ERR4("Error internaly", "thnewpath->data_len", "too large.");
		return ;
	}
	DEBUGSTR("mise a jour requis");
	ft_hdel_table_cont(hexec);
	ft_memcpy(path, thnewpath->data, thnewpath->data_len);
	path[thnewpath->data_len] = 0;
	ft_hresize_th(hexec->hdb);
	ft_correla_path__upd(hcorrela, thnewpath);
}
