/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_opt_tree.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 06:18:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/01 06:18:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void
	ft_print_usage(char c)
{
	ft_putstr_fd(NAME_PROG, STDERR_FILENO);
	ft_putstr_fd(": illegal option -- ", STDERR_FILENO);
	ft_putchar_fd(c, STDERR_FILENO);
	ft_putstr_fd("\nusage: ", STDERR_FILENO);
	write(STDERR_FILENO, NAME_PROG, LEN_NAME_PROG);
	ft_putstr_fd(" [OPTION] [file ...]\n", STDERR_FILENO);
	ft_exit(EXIT_FAILURE, NULL);
}

int
	ft_opt_tree(int opt, char c_opt)
{
	DEBUGC(c_opt);
	ft_print_usage(c_opt);
	return (opt);
}
