/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_unary_xbigs.c                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/01 11:10:49 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:10:52 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test__opt_unary_x(const char *file)
{
	return (access(file, X_OK) == -1);
}

int
	ft_builtins_test__opt_unary_z(const char *str)
{
	return (str[0] != '\0');
}

int
	ft_builtins_test__opt_unary_bigo(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && geteuid() == st.st_uid));
}

int
	ft_builtins_test__opt_unary_bigg(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && getegid() == st.st_gid));
}

int
	ft_builtins_test__opt_unary_bigs(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && S_ISSOCK(st.st_mode)));
}
