/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_fg__.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 06:23:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/11 06:23:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr_fork.h"
#include "sh_signal.h"

static void
	ft_builtins_fg__restart_wait_tty(int pc)
{
	tcsetpgrp(STDIN_FILENO, pc);
}

static int
	ft_builtins_fg__restart_wait__end(
		int status, int k_stat,
		t_builtins_job *tjobs, t_sh *sh)
{
	if (!WIFSTOPPED(status) || k_stat)
	{
		DEBUG;
		free(tjobs->argv);
		tjobs->argv = NULL;
		tjobs->pc = 0;
		tjobs->status = 0;
		return (0);
	}
	else if (WIFSTOPPED(status))
		ft_builtins_jobs_mod(JOB_STOPPED, tjobs->pc, &sh->tljobs);
	return (1);
}

int
	ft_builtins_fg__restart_wait(t_builtins_job *tjobs, t_sh *sh)
{
	int		status;
	int		k_stat;
	pid_t	pgrp;

	DEBUGNBR(tjobs->pc);
	status = 0;
	if (IS_SHTTY(sh))
		ft_builtins_fg__restart_wait_tty(tjobs->pc);
	pgrp = getpgid(tjobs->pc);
	DEBUGNBR(pgrp);
	if (pgrp == -1)
		k_stat = kill(tjobs->pc, SIGCONT);
	else
		k_stat = kill(-pgrp, SIGCONT);
	if (!k_stat)
		waitpid(tjobs->pc, &status, WUNTRACED);
	if (IS_SHTTY(sh))
		ft_builtins_fg__restart_wait_tty(getpid());
	DEBUGNBR(k_stat);
	ft_forksplit__print_ret(&sh->hlocal, status);
	DEBUGNBR(status);
	return (ft_builtins_fg__restart_wait__end(status, k_stat, tjobs, sh));
}
