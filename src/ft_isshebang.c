/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isshebang.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/22 00:53:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/22 00:53:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void
	ft_shebang_err(t_htable *hlocal, char *path, char *pathinterp, int errcode)
{
	char	*str;

	DEBUG;
	if (errcode == 2)
	{
		pathinterp = NAME_PROG;
		str = "is not interpreter (not yet)";
	}
	else if (errcode == 3)
		str = "interpreter is not posix";
	else if (errcode == 5)
		str = "too small size file";
	else if (errcode == ~F_OK)
		str = "bad interpreter: No such file or directory";
	else if (errcode & X_OK)
		str = "bad interpreter: Permssion denied";
	else
		str = "Error unknown (size file ?)";
	MSG_ERR4(path, pathinterp, str);
	ft_updatekey_ret(hlocal, errcode + 1);
}

static void
	ft_shebang__rpl__jointshebangarg(t_argv *targv,
									char lineshebangopt[])
{
	size_t	opt_len;
	char	**opt;
	char	**cmd;

	DEBUG;
	opt = ft_strsplit(lineshebangopt, ' ');
	ft_memchk_exit(opt);
	opt_len = ft_tablen(opt);
	;
	free(*targv->argv);
	ft_memchk_exit(*targv->argv = ft_strdup(targv->pathbin));
	cmd = ft_taballocjoint(opt, opt_len, targv->argv, targv->argc);
	free(opt);
	free(targv->argv);
	ft_memchk_exit(cmd);
	targv->argc = ft_tablen(cmd);
	targv->argv = cmd;
	targv->pathbin = *cmd;
}

static void
	ft_shebang__rpl(t_argv *targv, char lineshebangopt[])
{
	DEBUGSTR(" is shebang ");
	DEBUGSTR(lineshebangopt);
	ft_shebang__rpl__jointshebangarg(targv, lineshebangopt);
}

static int
	ft_shebangchk(t_argv *targv)
{
	int		fd;
	char	*buff;
	char	pbuff[512 + 2];
	int		nbread;

	DEBUGSTR(targv->pathbin);
	buff = pbuff;
	if ((fd = open(targv->pathbin, O_RDONLY)) < 0)
		return (-1);
	nbread = read(fd, buff, 512);
	close(fd);
	if (!nbread)
		return (5);
	if (nbread < 3)
		return (0);
	if (*buff != 0x23 || buff[1] != 0x21)
		return (0);
	buff[nbread] = 0;
	if (!(buff = ft_strchr(pbuff + 2, '\n'))
		|| buff == pbuff + 2)
		return (2);
	*buff = 0;
	ft_shebang__rpl(targv, pbuff + 2);
	return (1);
}

int
	ft_isshebang(t_sh *sh, t_argv *targv)
{
	int		e;
	char	*sav_pathbin;

	DEBUGSTR(*targv->argv);
	DEBUGSTR(targv->pathbin);
	sav_pathbin = targv->pathbin;
	if ((e = ft_accesschk(targv->pathbin) & R_OK)
		|| !(e = ft_shebangchk(targv)))
		return (0);
	else if (e != 1 || (e = ft_accesschk_path_x(targv->pathbin)))
	{
		ft_shebang_err(&sh->hlocal, sav_pathbin, targv->pathbin, e);
		if (e != ~F_OK && e != X_OK)
			return (-1);
		return (-1);
	}
	return (0);
}
