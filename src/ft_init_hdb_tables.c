/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_hdb_tables.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 22:48:00 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 22:48:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr__static.h"
#include "const_term.h"
#include "pr_history.h"
#include "t_history.h"
#include "../inc/ft_surrogate.h"

t_htable
	*ft__static_hbuiltins(t_htable *hbuiltins)
{
	static t_htable	*static_hbuiltins = 0;

	if (hbuiltins)
		static_hbuiltins = hbuiltins;
	return (static_hbuiltins);
}

static void
	ft_init_histfile(
		int parse_tilde,
		const char *name_var, const char *default_var)
{
	char	*tmp0;

	if (!ft_getlocal((char *)name_var, NULL))
	{
		ft_memchk_exit(tmp0 = ft_strdup((char *)default_var));
		if (parse_tilde)
			ft_surrogate__tilde(&tmp0);
		ft_setlocal((char *)name_var, tmp0);
		free(tmp0);
	}
}

static void
	ft_init_hdb_tables__local(t_hdb *thdb, t_htable *thlocal)
{
	struct stat	stat_stdin;

	DEBUG;
	ft_local_min(thlocal);
	ft_hresize_th(thdb);
	ft__static_hlocal(thlocal);
	if (!fstat(STDIN_FILENO, &stat_stdin) && S_ISCHR(stat_stdin.st_mode))
	{
		ft_setlocal(CONST_QUOTE, "\"> ");
		ft_setlocal(CONST_SQUOTE, "'> ");
		ft_setlocal(CONST_GRAVE, "`> ");
		ft_init_histfile(1, CONST_HISTFILE, CONST_DEF_HISTFILE);
		ft_init_histfile(0, CONST_HISTSIZE, CONST_DEF_HISTSIZE);
	}
}

static void
	ft_init_hdb_tables__history(t_sh *sh, t_hdb *thdb, t_htable *thhistory)
{
	ft_hnew_table(thdb, thhistory, 1);
	g_thistory.id_key = 0;
	g_thistory.id_key_h = 0;
	g_thistory.thhistory = thhistory;
	if (IS_SHTTY(sh))
	{
		DEBUGSTR("Charge historique");
		ft_history_read();
		ft_atexit(ft_history_write);
	}
}

void
	ft_init_hdb_tables(t_sh *sh, char **envp)
{
	t_hdb	*thdb;

	DEBUG;
	thdb = ft_hnew_db(256);
	ft_hnew_table(thdb, &sh->henvp, 1);
	ft__static_henvp(&sh->henvp);
	ft_hnew_table(thdb, &sh->hlocal, 1);
	sh->henvp.num_table |= sh->hlocal.num_table;
	sh->hlocal.thtable_lie = &sh->henvp;
	sh->henvp.thtable_lie = &sh->hlocal;
	ft_tabtohash(envp, &sh->henvp);
	ft_envp_min(&sh->henvp);
	ft_hnew_table(thdb, &sh->hbuiltins, 0);
	DEBUGNBR(sh->hbuiltins.num_table);
	ft_gen_builtins_hash(&sh->hbuiltins);
	ft__static_hbuiltins(&sh->hbuiltins);
	DEBUGNBR(sh->hbuiltins.num_table);
	;
	ft_hnew_table(thdb, &sh->hcorrela, 1);
	DEBUGNBR(sh->hcorrela.num_table);
	ft_hnew_table(thdb, &sh->hexec, 1);
	ft__static_hexec(&sh->hexec);
	DEBUGNBR(sh->hexec.num_table);
	ft_init_hdb_tables__local(thdb, &sh->hlocal);
	ft_init_hdb_tables__history(sh, thdb, &sh->hhistory);
}
