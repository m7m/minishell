/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_envp_key_shlvl.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 04:47:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 04:47:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_envp_key_shlvl(t_htable *thtable)
{
	void		*data;
	char		voidtochar[255];
	t_hupdate	hupd;

	hupd.key = (void *)"SHLVL";
	hupd.key_len = SC(5);
	if ((hupd.data_len = ft_hchr(*thtable, hupd.key, hupd.key_len, &data)))
	{
		ft_memcpy(voidtochar, data, hupd.data_len);
		voidtochar[hupd.data_len] = 0;
		hupd.data_len = ft_stoa_s((size_t)ft_atoi(voidtochar) + 1, voidtochar);
		hupd.data = voidtochar;
	}
	else
	{
		hupd.data = (void *)"1";
		hupd.data_len = SC(1);
	}
	DEBUGSTR(hupd.data);
	if (ft_hupd(thtable, &hupd))
		free(hupd.data);
}
