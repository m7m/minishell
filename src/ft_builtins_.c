/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 08:07:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/14 08:07:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "targv.h"

void
	ft_builtins_exit(t_sh *sh, t_argv *targv)
{
	int	status;

	(void)sh;
	DEBUGSTR(*targv->argv);
	DEBUGPTR(sh);
	if (targv->argc > 1)
		status = ft_atoi(*targv->argv);
	else
		status = EXIT_SUCCESS;
	ft_targv_free(targv);
	DEBUG;
	if (IS_SHTTY(sh))
		ft_exit(status, "bye :(\n");
	else
		ft_exit(status, "");
}

void
	ft_builtins_help(t_sh *sh, t_argv *targv)
{
	(void)sh;
	(void)targv;
	DEBUGSTR(*targv->argv);
	DEBUGPTR(sh);
	ft_putstr_fd(HELP, STDOUT_FILENO);
	ft_updatekey_ret(&sh->hlocal, 0);
}
