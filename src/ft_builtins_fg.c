/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_fg.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/21 00:06:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/21 00:06:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr_fork.h"
#include "sh_signal.h"
#include "../inc/built_in_jobs.h"

static void
	ft_builtins_fg__restart_print(int i, t_builtins_job tjobs)
{
	char	**argv;

	DEBUG;
	ft_printf("Job: [%d] %d\tcmd: ", i, tjobs.pc);
	argv = tjobs.argv;
	DEBUG;
	while (*argv)
	{
		ft_printf(" %s", *argv);
		++argv;
	}
	ft_printf("\n");
	ft_fflush(STDOUT_FILENO);
}

static int
	ft_builtins_fg__restart(int index, t_sh *sh)
{
	int				i;
	t_builtins_job	*tjobs;

	i = 0;
	tjobs = sh->tljobs.tjobs;
	while (i < JOBS_MAX && i < index)
		++i;
	DEBUGNBR(i);
	if (i < JOBS_MAX && i == index && tjobs[i].pc > 0 && tjobs[i].status != 3)
	{
		DEBUGNBR(tjobs[i].pc);
		ft_builtins_fg__restart_print(i, tjobs[i]);
		if (ft_builtins_fg__restart_wait(tjobs + i, sh))
		{
			ft_putstr("Job [");
			ft_putnbr(i);
			ft_putstr("]: pid ");
			ft_putnbr(tjobs[i].pc);
			ft_putstr("\n");
		}
		return (1);
	}
	return (0);
}

static int
	ft_builtins_fg__find_index(t_builtins_job *tjobs, t_argv *targv)
{
	int	index;
	int	indexend;

	index = -1;
	indexend = 0;
	if (targv->argv[1])
	{
		indexend = ft_atoi(targv->argv[1]);
		if ((size_t)ft_intlen(index) == ft_strlen(targv->argv[1]))
			index = indexend;
	}
	else
		while (indexend < JOBS_MAX && tjobs[indexend].pc > -1)
		{
			if (tjobs[indexend].pc > 0)
				index = indexend;
			++indexend;
		}
	return (index);
}

void
	ft_builtins_fg(t_sh *sh, t_argv *targv)
{
	int	index;

	DEBUG;
	if (sh->tljobs.tjobs)
	{
		index = ft_builtins_fg__find_index(sh->tljobs.tjobs, targv);
		DEBUGNBR(index);
		if (index != -1 && ft_builtins_fg__restart(index, sh))
			return ;
	}
	if (targv->argv[1])
		MSG_ERR4("fg", targv->argv[1], "no such job");
	else
		MSG_ERR3("fg", "no such job");
}
