/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:46:00 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/28 18:24:38 by youhnia          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** Renvois la ligne lu sur le fd
** Une ligne est compris du debut jusqu'a un \n (0x0a) ou EOF
** retourne 1 tant qu'il y a quelque chose de retourne
*/

/*
** Nettoie le buffeur static
** tout le buffeur si le fd negatif
** le buffeur fd si fd positif
*/

/*
** if (((fd < 0 || !line) && gnl_clrstatbuf(fd, buf)) || fd > MAXFD
** static int     gnl_clrstatbuf(int fd, char *buf[MAXFD + 2])
** {
** 	if (fd < 0)
** 	{
** 		fd = -1;
** 		while (++fd < MAXFD + 1)
** 			if (buf[fd])
** 			{
** 				free(buf[fd]);
** 				buf[fd] = NULL;
** 			}
** 	}
** 	else if (fd < MAXFD + 2 && buf[fd])
** 	{
** 		free(buf[fd]);
** 		buf[fd] = NULL;
** 	}
** 	return (1);
** }
*/

static int	gnl_ret(int const fd, char **line, char **buf, int n)
{
	char		*tmp1;

	if (!(tmp1 = ft_strchr(buf[fd], '\n')) && n == BUFF_SIZE)
		return (get_next_line(fd, line));
	else if (tmp1)
	{
		*line = ft_strsub(buf[fd], 0, (unsigned int)(tmp1 - buf[fd]));
		ft_strcpy(buf[fd], buf[fd] + (tmp1 - buf[fd]) + 1);
		tmp1 = NULL;
	}
	else if (*buf[fd])
	{
		*line = buf[fd];
		buf[fd] = NULL;
	}
	else
	{
		free(buf[fd]);
		buf[fd] = NULL;
		return (0);
	}
	return (1);
}

/*
** Si fd negatif appel gnl_clrstatbuf
** Si line nul appel gnl_clrstatbuf
*/

int			get_next_line(int const fd, char **line)
{
	static char *buf[MAXFD] = {0};
	char		tmp1[BUFF_SIZE + 2];
	char		*tmp2;
	int			n;

	ft_memset(tmp1, 0, BUFF_SIZE + 2);
	if (!line || fd < 0 || fd >= MAXFD
	|| (n = read(fd, tmp1, BUFF_SIZE)) < 0
	|| (n && !buf[fd] && !(tmp2 = ft_strdup(tmp1)))
	|| (n && buf[fd] && !(tmp2 = ft_strjoin(buf[fd], tmp1))))
		return (-1);
	if (n)
	{
		free(buf[fd]);
		buf[fd] = tmp2;
	}
	if (!buf[fd])
		return (0);
	return (gnl_ret(fd, line, buf, n));
}
