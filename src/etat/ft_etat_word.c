/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_word.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/23 02:02:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/23 02:02:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "etat.h"
#include "../../inc/l_sh.h"

/*
** Deux allocation sur le retour si aucune erreur
** 1er: tlta->line
** 2eme: tlta->pline
** si erreur aucune allocation
** Stock sur 'line' le 'word'
** Retourne un pointeur qui pointe
**  sur la chaine d'entre situé
**  sur la fin du 'word'
**  ceci permettant un calcule differenciel
**   sur le 'line' d'entrée et celui de sortie
*/

char
	*ft_etat_word(char **line, struct s_sh *sh)
{
	t_linetoargv	tlta;

	DEBUGSTR(*line);
	ft_memset(&tlta, 0, sizeof(tlta));
	tlta.line = ft_pass_space(*line);
	tlta.line = ft_strdup(tlta.line);
	ft_memchk_exit(tlta.line);
	tlta.pline = tlta.line;
	tlta.sh = sh;
	if (ft_etat_mini(&tlta)
		|| !tlta.pline || tlta.old_c == -1)
	{
		DEBUGNBR(tlta.old_c);
		DEBUGSTR(tlta.line);
		DEBUGSTR(tlta.pline);
		free(tlta.line);
		return (NULL);
	}
	DEBUGSTR(tlta.line);
	DEBUGSTR(tlta.pline);
	*line = tlta.pline;
	return (tlta.line);
}
