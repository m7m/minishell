/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_init.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 21:18:01 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/04 21:18:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_mini.h"
#include "chkclose/ft_etat_chkclose.h"

int
	ft_etat_mini__space(t_linetoargv *tlta)
{
	char	*arg;

	DEBUGNBR(tlta->flags);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	if (tlta->flags & ~FL_HERED && *tlta->pline)
		return (FLP_NONE);
	arg = NULL;
	if (tlta->pline > tlta->line
		|| tlta->old_c == FLP_SQUOT
		|| tlta->old_c == FLP_DQUOT
		|| tlta->old_c == FLP_MQUOT)
	{
		arg = ft_strsub(tlta->line, 0, tlta->pline - tlta->line);
		ft_memchk_exit(arg);
		DEBUGSTR(arg);
	}
	while (*tlta->pline && (*tlta->pline == ' ' || *tlta->pline == '\t'))
		++tlta->pline;
	ft_strcpy(tlta->line, tlta->pline);
	tlta->pline = arg;
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	tlta->old_c = FLP_SPACE;
	return (0);
}

int
	ft_etat_mini__redirect(t_linetoargv *tlta)
{
	DEBUGNBR(tlta->flags);
	if (tlta->flags)
		return (FLP_NONE);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	ft_etat_mini__space(tlta);
	return (0);
}

static t_etat
	*ft_etat_mini_init__add__redi(t_etat *et)
{
	ft_etat_add(++et, "&>>", ft_etat_mini__space);
	ft_etat_add(++et, "&>", ft_etat_mini__space);
	ft_etat_add(++et, ">&-", ft_etat_mini__redirect);
	ft_etat_add(++et, ">&", ft_etat_mini__redirect);
	ft_etat_add(++et, ">>", ft_etat_mini__redirect);
	ft_etat_add(++et, ">", ft_etat_mini__redirect);
	ft_etat_add(++et, "<<<", ft_etat_mini__redirect);
	ft_etat_add(++et, "<<-", ft_etat_mini__redirect);
	ft_etat_add(++et, "<<", ft_etat_mini__redirect);
	ft_etat_add(++et, "<&-", ft_etat_mini__redirect);
	ft_etat_add(++et, "<&", ft_etat_mini__redirect);
	ft_etat_add(++et, "<", ft_etat_mini__redirect);
	return (et);
}

static void
	ft_etat_mini_init__add(t_etat *et)
{
	ft_etat_add(et, "'", ft_etat__squot);
	ft_etat_add(++et, "\\", ft_etat__antsl);
	ft_etat_add(++et, "\"", ft_etat__dquot);
	ft_etat_add(++et, "`", ft_etat_mini__mquot);
	et = ft_etat_mini_init__add__redi(et);
	ft_etat_add(++et, ";", ft_etat_mini__space);
	ft_etat_add(++et, "\n", ft_etat_mini__space);
	ft_etat_add(++et, "|", ft_etat_mini__space);
	ft_etat_add(++et, "&", ft_etat_mini__space);
	ft_etat_add(++et, " ", ft_etat_mini__space);
	ft_etat_add(++et, "\t", ft_etat_mini__space);
	ft_etat_add(++et, "#", ft_etat_mini__space);
	ft_etat_add(++et, ")", ft_etat_chkclose__parenth_close);
	ft_etat_add(++et, "$(", ft_etat_chkclose__dolparen);
	ft_etat_add(++et, "(", ft_etat_mini__parenth);
	ft_etat_add(++et, "", ft_etat_mini__space);
	ft_etat_add(++et, 0, 0);
}

/*
** ' ' ou \t ou \0 ligne terminer
** le resulta se trouve sur 'pline'
*/

/*
** Fonction utiliser par les redirections
*/

t_etat
	*ft_etat_mini_init(void)
{
	static t_etat	et[127] = {{0, 0, 0}};

	DEBUG;
	if (!et->c)
		ft_etat_mini_init__add(et);
	DEBUG;
	return (et);
}
