/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_redirecting_init.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 12:58:44 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 12:58:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"
#include "../../../inc/sh_macro.h"
#include "../../../inc/l_sh.h"

/*
** sur le if:
** traitement des erreurs
** si FL_DOL_PARENTH envoie a l'execution avec msg EOF not termined
*/

int
	ft_etat_heredoc__end(t_linetoargv *tlta)
{
	DEBUG;
	tlta->flags &= ~(FL_HEREDT);
	if (tlta->flags)
	{
		return (-1);
	}
	return (0);
}

int
	ft_etat_heredoc__tab(t_linetoargv *tlta)
{
	char	*s;

	DEBUGNBR(tlta->flags);
	if (tlta->flags & FL_HEREDT)
	{
		DEBUG;
		if ((tlta->pline > tlta->line && *(tlta->pline - 1) == '\n')
			|| tlta->pline == tlta->line)
		{
			DEBUG;
			s = tlta->pline + 1;
			while (*s == '\t')
				++s;
			ft_strcpy(tlta->pline, s);
			return (FLP_HERE_TAB);
		}
	}
	return (FLP_NONE);
}

static void
	ft_etat_redirecting_init__add(t_etat *et)
{
	ft_etat_add(et++, "`", ft_etat_heredoc__mquot);
	ft_etat_add(et++, "\t", ft_etat_heredoc__tab);
	ft_etat_add(et++, "$(", ft_etat_heredoc__dolparenth);
	ft_etat_add(et++, ")", ft_etat_heredoc__dolparenth_close);
	ft_etat_add(et++, "\\", ft_etat_heredoc__antsl);
	ft_etat_add(et++, "$", ft_etat_heredoc__dol);
	ft_etat_add(et++, "", ft_etat_heredoc__end);
	ft_etat_add(et++, 0, 0);
}

/*
** Fonction utiliser par les redirections
*/

t_etat
	*ft_etat_redirecting_init(void)
{
	static t_etat	et[127] = {{0, 0, 0}};

	DEBUG;
	if (!et->c)
		ft_etat_redirecting_init__add(et);
	DEBUG;
	return (et);
}
