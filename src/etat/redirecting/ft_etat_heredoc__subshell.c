/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_heredoc__subshell.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 01:25:34 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 01:25:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"

#include "../../parser/f_tree.h"
#include "../../parser/tree/f_ftree_sub.h"
#include "../../parser/tree/f_tree_exec.h"

#include "../../../inc/jobs.h"
#include "../../../inc/pr_fork.h"

#include "../../f_free_notglobal.h"

static void
	ft_etat_heredoc__subshell__sub(int p[2], char *s, t_linetoargv *tlta)
{
	t_argv	*targv_sav;
	t_tree	*t;

	targv_sav = tlta->sh->targv;
	tlta->sh->sf->targv_here_doc_etat = targv_sav;
	tlta->sh->targv = NULL;
	;
	ft_memchk_exit(t = f_tree_newadd(0, 0));
	t->line = ft_strsub(tlta->pline, 0, s - (tlta->pline));
	DEBUGSTR(t->line);
	ft_memchk_exit(t->line);
	ft_strcpy(tlta->pline, s);
	DEBUGSTR(tlta->pline);
	COM("'");
	tlta->sh->sf->line_redirecting = tlta->line;
	f_ftree__sub_tfd(p, tlta->sh);
	tlta->sh->opt |= SH_REPARSE;
	/* f_free_notglobal_cal(); */
	tlta->sh->sf->t = t;
	tlta->sh->tljobs.last_pc = f_ftree_subshell(JOB_PENDING, p, t, tlta->sh);
	tlta->sh->sf->line_redirecting = NULL;
	f_tree_freeone(t);
	tlta->sh->sf->t = NULL;
	tlta->sh->opt &= ~SH_REPARSE;
	tlta->sh->targv = targv_sav;
}

static void
	ft_etat_heredoc__subshell__join(int len, char *line, t_linetoargv *tlta)
{
	char	*tmp;

	f_ftree__sub_endline(len, line);
	DEBUGSTR(line);
	if (line)
	{
		tmp = ft_strjoin_inser(tlta->line, line, tlta->pline - tlta->line);
		free(tlta->line);
		free(line);
		ft_memchk_exit(tmp);
		tlta->pline = tmp + (tlta->pline - tlta->line);
		tlta->line = tmp;
	}
}

void
	ft_etat_heredoc__subshell(char *s, t_linetoargv *tlta)
{
	int		len;
	char	*line;
	int		p[2];

	DEBUGSTR(s);
	DEBUGSTR(tlta->pline);
	if (pipe(p))
	{
		ft_updatekey_ret(&tlta->sh->hlocal, 2);
		MSG_ERR3("pipe()", "internaly error");
		return ;
	}
	ft_etat_heredoc__subshell__sub(p, s, tlta);
	line = NULL;
	len = f_ftree__sub_read(p[0], &line, &tlta->sh->tljobs);
	close(p[0]);
	close(p[1]);
	if (len == -1)
	{
		free(line);
		MSG_ERRINTER("f_ftree__sub_read return -1");
	}
	else
		ft_etat_heredoc__subshell__join(len, line, tlta);
	tlta->sh->sf->targv_here_doc_etat = NULL;
}
