/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat__sp_t.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 21:21:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/04 21:21:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../etat.h"
#include "flags.h"
#include "l_sh.h"
#include "../../../inc/t_sh.h"

int
	ft_etat__space(t_linetoargv *tlta)
{
	char	*arg;

	DEBUGNBR(tlta->flags);
	if (tlta->flags)
		return (FLP_NONE);
	if (tlta->pline > tlta->line
		|| tlta->old_c == FLP_SQUOT
		|| tlta->old_c == FLP_DQUOT
		|| tlta->old_c == FLP_MQUOT)
	{
		arg = ft_strsub(tlta->line, 0, tlta->pline - tlta->line);
		ft_memchk_exit(arg);
		++tlta->sh->targv->argc;
		tlta->sh->targv->argv = ft_taballocadd(tlta->sh->targv->argv, arg);
	}
	while (*tlta->pline && (*tlta->pline == ' ' || *tlta->pline == '\t'))
		++tlta->pline;
	ft_strcpy(tlta->line, tlta->pline);
	tlta->pline = tlta->line;
	DEBUGSTR(tlta->line);
	return (FLP_SPACE);
}

int
	ft_etat__tab(t_linetoargv *tlta)
{
	int	ret;

	DEBUG;
	if (tlta->flags & FL_DELTAB
		&& tlta->line < tlta->pline && *(tlta->pline - 1) == '\n')
	{
		DEBUG;
		ft_strcpy(tlta->pline, tlta->pline + 1);
		return (FLP_TABUL);
	}
	else
		ret = ft_etat__space(tlta);
	if (ret == FLP_SPACE)
		return (FLP_TABUL);
	return (FLP_NONE);
}
