/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_heredoc__antsl.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 02:29:34 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 02:29:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"

int
	ft_etat_heredoc__antsl(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	if (!ft_strchr("$\\`", tlta->pline[1]))
		return (FLP_NONE);
	ft_strcpy(tlta->pline, tlta->pline + 1);
	if (*tlta->pline)
		++tlta->pline;
	DEBUGSTR(tlta->line);
	return (FLP_ANTSL);
}
