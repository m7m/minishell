/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_heredoc__dol.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 02:28:31 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 02:28:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"
#include "../../../inc/ft_surrogate.h"

int
	ft_etat_heredoc__dol(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->line);
	if (tlta->flags & ~FL_HEREDT)
		return (FLP_NONE);
	tlta->pline = ft_surrogate__dol(&tlta->line, tlta->pline, 0);
	DEBUGSTR(tlta->line);
	return (FLP_DOL);
}
