/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   etat_redirecting.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 12:57:23 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 12:57:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ETAT_REDIRECTING_H
# define ETAT_REDIRECTING_H

long	ft_etat_redirecting(t_linetoargv *tlta);

#endif
