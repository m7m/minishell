/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat__tilde.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 11:35:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/03 11:35:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "../../../inc/ft_surrogate.h"
#include "../etat.h"

int
	ft_etat__tilde(t_linetoargv *tlta)
{
	if (ISETAT_IGN(tlta->flags & ~FL_EQUAL))
		return (FLP_NONE);
	tlta->pline = ft_surrogate__tilde2(tlta->pline, &tlta->line);
	return (FLP_TILDE);
}
