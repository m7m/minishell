/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_redirecting.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 01:53:00 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 01:53:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ETAT_REDIRECTING_H
# define FT_ETAT_REDIRECTING_H

# include "../etat.h"

struct s_etat	*ft_etat_redirecting_init(void);

void			ft_etat_heredoc__subshell(char *s, t_linetoargv *tlta);

int				ft_etat_heredoc__dolparenth(t_linetoargv *tlta);
int				ft_etat_heredoc__dolparenth_close(t_linetoargv *tlta);
int				ft_etat_heredoc__mquot(t_linetoargv *tlta);
int				ft_etat_heredoc__dol(t_linetoargv *tlta);
int				ft_etat_heredoc__antsl(t_linetoargv *tlta);

#endif
