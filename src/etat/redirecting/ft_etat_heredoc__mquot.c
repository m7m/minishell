/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_heredoc__mquot.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 01:56:24 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 01:56:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"
#include "../../../inc/l_sh.h"
#include "../../../inc/sh_macro.h"

int
	ft_etat_heredoc__mquot(t_linetoargv *tlta)
{
	char	*s;
	char	*pass;

	DEBUG;
	s = ft_strchr(tlta->pline + 1, '`');
	if (!s)
	{
		MSG_ERR3(tlta->pline, "\"`\" not close");
		return (-1);
	}
	if (tlta->flags & FL_DOL_PARENTH)
	{
		tlta->pline = s + 1;
		return (FLP_NONE);
	}
	pass = ft_pass_space(tlta->pline + 1);
	ft_strcpy(s, s + 1);
	ft_strcpy(tlta->pline, tlta->pline + 1);
	if (pass == s)
		return (FLP_MQUOT);
	s--;
	DEBUGSTR(s);
	ft_etat_heredoc__subshell(s, tlta);
	return (FLP_MQUOT);
}
