/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat__crosi.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 17:11:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/15 17:11:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../etat.h"
#include "flags.h"
#include "minishell.h"

int
	ft_etat__crosi(t_sh *sh, t_linetoargv *tlta)
{
	char	*nextline;

	(void)sh;
	if (ISETAT_IGN(tlta->flags)
		|| (tlta->pline > tlta->line
			&& !ft_strchr(" \n\t", *(tlta->pline - 1))))
		return (FLP_NONE);
	nextline = ft_strchr(tlta->pline, '\n');
	if (nextline)
		ft_strcpy(tlta->pline, nextline);
	else
		*tlta->pline = 0;
	return (FLP_CROSI);
}
