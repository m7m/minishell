/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_heredoc__dolparenth.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 01:52:01 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 01:52:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"

static char
	*ft_etat_heredoc__dolparenth__start(char *start)
{
	static char	*s = NULL;
	char		*tmp;

	tmp = s;
	s = start;
	return (tmp);
}

int
	ft_etat_heredoc__dolparenth(t_linetoargv *tlta)
{
	DEBUG;
	tlta->flags |= FL_DOL_PARENTH;
	if (!tlta->count[FLP_DOL_PARENTH])
		ft_etat_heredoc__dolparenth__start(tlta->pline);
	tlta->count[FLP_DOL_PARENTH]++;
	tlta->pline += 2;
	return (FLP_DOL_PARENTH);
}

int
	ft_etat_heredoc__dolparenth_close(t_linetoargv *tlta)
{
	char	*s;
	char	*tmp;

	DEBUGNBRUL(tlta->flags);
	if (!(tlta->flags & FL_DOL_PARENTH))
		return (FLP_NONE);
	--tlta->count[FLP_DOL_PARENTH];
	DEBUGNBR(tlta->count[FLP_DOL_PARENTH]);
	if (!tlta->count[FLP_DOL_PARENTH])
	{
		tmp = ft_etat_heredoc__dolparenth__start(0);
		if (!tmp)
			return (-1);
		tlta->flags &= ~FL_DOL_PARENTH;
		ft_strcpy(tmp, tmp + 2);
		tlta->pline -= 2;
		s = tlta->pline;
		ft_strcpy(s, s + 1);
		tlta->pline = tmp;
		ft_etat_heredoc__subshell(s, tlta);
		tlta->flags &= ~FL_DOL_PARENTH;
	}
	else
		tlta->pline++;
	return (FLP_DOL_PARENTH);
}
