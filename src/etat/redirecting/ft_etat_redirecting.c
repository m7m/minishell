/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_redirecting.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 12:56:45 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 12:56:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_redirecting.h"

long
	ft_etat_redirecting(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->line);
	return (ft_etat(ft_etat_redirecting_init, tlta));
}
