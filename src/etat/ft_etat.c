/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 21:17:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/04 21:17:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "targv.h"
#include "etat.h"

void
	ft_etat_add(t_etat *et, char *c, int (*f)(t_linetoargv *))
{
	if (!c)
		et->c_len = 0;
	else if (*c)
		et->c_len = ft_strlen(c);
	else
		et->c_len = 1;
	et->c = c;
	et->f = f;
}

long
	ft_etat(t_etat *(*fetat_init)(void),
			t_linetoargv *tlta)
{
	t_etat	*tet;

	DEBUGSTR(tlta->line);
	tet = fetat_init();
	if (ft_etat_exec(tlta, tet))
	{
		DEBUGSTR("erreur detecte");
		return (-1);
	}
	DEBUGNBR(tlta->flags);
	return ((long)tlta->flags);
}

long
	ft_etat_mini(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->line);
	return (ft_etat(ft_etat_mini_init, tlta));
}
