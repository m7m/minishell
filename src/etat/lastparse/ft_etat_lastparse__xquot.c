/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__xquot.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 06:39:42 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 06:39:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_lastparse.h"

int
	ft_etat_lastparse__squot(t_linetoargv *tlta)
{
	DEBUG;
	if (tlta->flags & ~(FL_SQUOT | FL_DOL_PARENTH | FL_PAREN))
		return (FLP_NONE);
	if (tlta->flags & FL_SQUOT)
		tlta->flags &= ~FL_SQUOT;
	else
		tlta->flags |= FL_SQUOT;
	if (tlta->flags & (FL_DOL_PARENTH | FL_PAREN))
		++tlta->pline;
	else
		ft_strcpy(tlta->pline, tlta->pline + 1);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	return (FLP_SQUOT);
}

int
	ft_etat_lastparse__dquot(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	if (tlta->flags & ~(FL_DQUOT | FL_DOL_PARENTH | FL_PAREN))
		return (FLP_NONE);
	if (tlta->flags & FL_DQUOT)
		tlta->flags &= ~FL_DQUOT;
	else
		tlta->flags |= FL_DQUOT;
	if (tlta->flags & (FL_DOL_PARENTH | FL_PAREN))
		++tlta->pline;
	else
		ft_strcpy(tlta->pline, tlta->pline + 1);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	return (FLP_DQUOT);
}
