/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__parenth.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 17:33:01 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/20 17:33:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_lastparse.h"
#include "../../../inc/t_sh.h"
#include "../../../inc/l_sh.h"

int
	ft_etat_lastparse__parenth_close(t_linetoargv *tlta)
{
	DEBUG;
	if (!(tlta->flags & (FL_DOL_PARENTH | FL_PAREN)))
		return (FLP_NONE);
	DEBUGSTR(tlta->pline);
	tlta->flags |= FL_PAREN_CLOSE;
	return (-1);
}

int
	ft_etat_lastparse_recurs(
		t_linetoargv *tlta, t_linetoargv *tlta_rec, long flags)
{
	ft_memset(tlta_rec, 0, sizeof(t_linetoargv));
	tlta_rec->line = tlta->line;
	tlta_rec->pline = tlta->pline;
	tlta_rec->flags = flags;
	tlta_rec->sh = tlta->sh;
	return (ft_etat_lastparse(tlta_rec));
}

static int
	ft_etat_lastparse__parenth__end(
		t_linetoargv *tlta, t_linetoargv tlta_rec)
{
	char			*start;
	char			*end;

	end = tlta_rec.pline;
	tlta->pline = tlta_rec.line + ((tlta->pline - tlta->line) - 1);
	tlta->line = tlta_rec.line;
	ft_strcpy(end, end + 1);
	ft_strcpy(tlta->pline, tlta->pline + 1);
	start = tlta->pline;
	end -= 1;
	DEBUGSTR(start);
	DEBUGSTR(end);
	if (ft_etat_subshell_tree2(start, end, tlta))
		return (-1);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	DEBUGC(*tlta->pline);
	if (*ft_pass_spacenewline(tlta->pline))
		return (-1);
	return (0);
}

int
	ft_etat_lastparse__parenth(t_linetoargv *tlta)
{
	t_linetoargv	tlta_rec;

	DEBUG;
	if ((tlta->sh->targv->argc && tlta->flags & ~(FL_PAREN | FL_DOL_PARENTH))
		|| tlta->flags & ~(FL_PAREN | FL_DOL_PARENTH))
		return (FLP_NONE);
	++tlta->pline;
	if (ft_etat_lastparse_recurs(tlta, &tlta_rec, FL_PAREN) != -1
		|| tlta_rec.flags != (FL_PAREN_CLOSE | FL_PAREN))
	{
		DEBUGNBRUL(tlta_rec.flags);
		tlta->line = tlta_rec.line;
		tlta->pline = tlta_rec.pline;
		tlta->flags = tlta_rec.flags;
		return (-1);
	}
	DEBUG;
	if (tlta->flags & FL_DOL_PARENTH)
	{
		tlta->line = tlta_rec.line;
		tlta->pline = tlta_rec.pline + 1;
		return (FLP_PARENTH);
	}
	return (ft_etat_lastparse__parenth__end(tlta, tlta_rec));
}

int
	ft_etat_lastparse__dol_parenth(t_linetoargv *tlta)
{
	char			*start;
	char			*end;
	t_linetoargv	tlta_rec;

	if (tlta->flags & ~(FL_DQUOT | FL_DOL_PARENTH | FL_PAREN))
		return (FLP_NONE);
	tlta->pline += 2;
	if (ft_etat_lastparse_recurs(tlta, &tlta_rec, FL_DOL_PARENTH) != -1
		|| tlta_rec.flags != (FL_PAREN_CLOSE | FL_DOL_PARENTH))
	{
		tlta->line = tlta_rec.line;
		tlta->pline = tlta_rec.pline;
		tlta->flags = tlta_rec.flags;
		return (-1);
	}
	end = tlta_rec.pline;
	tlta->pline = tlta_rec.line + ((tlta->pline - tlta->line) - 2);
	tlta->line = tlta_rec.line;
	ft_strcpy(end, end + 1);
	ft_strcpy(tlta->pline, tlta->pline + 2);
	start = tlta->pline;
	end -= 2;
	if (ft_etat_subshell_tree(start, end, tlta))
		return (-1);
	return (FLP_DOL_PARENTH);
}
