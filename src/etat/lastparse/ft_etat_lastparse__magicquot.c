/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__magicquot.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 15:46:42 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/20 15:46:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_lastparse.h"

int
	ft_etat_lastparse__magicquot(t_linetoargv *tlta)
{
	char	*start;
	char	*end;

	DEBUG;
	if (tlta->flags & ~(FL_DQUOT | FL_DOL_PARENTH | FL_PAREN))
		return (FLP_NONE);
	end = ft_strchr(tlta->pline + 1, '`');
	if (!end)
		return (-1);
	if (tlta->flags & (FL_DOL_PARENTH | FL_PAREN))
	{
		tlta->pline = end + 1;
		return (FLP_MQUOT);
	}
	ft_strcpy(end, end + 1);
	ft_strcpy(tlta->pline, tlta->pline + 1);
	start = tlta->pline;
	--end;
	DEBUGSTR(start);
	DEBUGSTR(end);
	if (ft_etat_subshell_tree(start, end, tlta))
		return (-1);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	return (FLP_MQUOT);
}
