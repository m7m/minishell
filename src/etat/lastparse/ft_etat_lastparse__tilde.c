/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__tilde.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 06:41:39 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 06:41:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_lastparse.h"
#include "../../../inc/ft_surrogate.h"

int
	ft_etat_lastparse__tilde(t_linetoargv *tlta)
{
	DEBUG;
	if (tlta->flags)
		return (FLP_NONE);
	tlta->pline = ft_surrogate__tilde2(tlta->pline, &tlta->line);
	return (FLP_TILDE);
}
