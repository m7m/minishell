/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__equal.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 21:05:17 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/14 21:05:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../etat.h"
#include "../../../inc/t_sh.h"

void
	f_add_argv(char *s, t_linetoargv *tlta)
{
	++tlta->sh->targv->argc;
	tlta->sh->targv->argv = ft_taballocadd(tlta->sh->targv->argv, s);
}

int
	ft_etat_lastparse__equale(t_linetoargv *tlta)
{
	int		len;
	char	*arg;

	DEBUG;
	len = tlta->pline - tlta->line;
	if (tlta->flags
		|| tlta->sh->targv->argc
		|| !len)
		return (FLP_NONE);
	DEBUGSTR(tlta->line);
	arg = ft_strdup("set");
	ft_memchk_exit(arg);
	f_add_argv(arg, tlta);
	;
	arg = ft_strsub(tlta->line, 0, len);
	ft_memchk_exit(arg);
	f_add_argv(arg, tlta);
	;
	ft_strcpy(tlta->line, tlta->pline + 1);
	tlta->pline = tlta->line;
	DEBUGSTR(tlta->line);
	return (FLP_EQUALE);
}
