/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_bracket_impl.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/26 17:19:06 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/30 14:24:41 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"
#include "../../etat.h"
#include "../../../../libft/libft.h"
#include "libftfon.h"

static char		*get_next_word(char **arg)
{
	char	*s;

	if ((s = ft_strnew(0)))
		while (**arg != '}' && **arg != ',' && **arg)
		{
			if (**arg != '\\')
				ft_strpush(&s, **arg);
			else if ((*arg)[1])
				ft_strpush(&s, *++*arg);
			++*arg;
		}
	if (**arg == ',')
		++*arg;
	return (s);
}

static char		**g_b_i(char *arg, char **be, char **af, size_t beg)
{
	char	**b;
	char	*p;

	p = arg;
	arg[beg] = '\0';
	if (!(*be = ft_strdup(arg))
	|| !(b = (char**)ft_parrnew()))
		return ((char**)0);
	arg += beg + 1;
	while (*arg != '}' && *arg)
		ft_parrpush((void***)&b, get_next_word(&arg));
	*af = ft_strdup(arg + (*arg == '}'));
	if (!*arg)
	{
		ft_parrfree((void**)b);
		b = (char**)0;
	}
	else if (arg[-1] == ',')
		ft_parrpush((void***)&b, ft_strnew(0));
	free(p);
	return (b);
}

static void		replace_line(t_linetoargv *lta, char *be, char *af, char **b)
{
	char	*after;
	char	*p;

	if (!(after = ft_strchr(lta->pline, ' ')))
		after = lta->pline + ft_strlen(lta->pline);
	after = ft_strdup(after);
	free(lta->line);
	lta->line = ft_strnew(0);
	while (*b)
	{
		p = ft_strjoin(be, *b);
		ft_freeswap((void**)&p, ft_strjoin(p, af));
		ft_freeswap((void**)&lta->line, (*lta->line) ? ft_strcjoin(lta->line,
			p, ' ') : ft_strjoin(lta->line, p));
		free(p);
		free(*b);
		++b;
	}
	ft_freeswap((void**)&lta->line, ft_strjoin(lta->line, after));
	free(after);
	lta->pline = lta->line;
}

int				ft_etat_lastparse_globbing_bracket(t_linetoargv *lta)
{
	char	**b;
	char	*be;
	char	*af;

	if (lta->flags & (FL_SQUOT | FL_DQUOT | FL_GRAVE))
		return (FLP_NONE);
	if (!(b = g_b_i(globbing_get_arg(lta), &be, &af, lta->pline - lta->line))
	|| !b[0] || !b[1])
	{
		if (b)
			ft_parrfree((void**)b);
		free(be);
		free(af);
		return (FLP_NONE);
	}
	replace_line(lta, be, af, b);
	free(be);
	free(af);
	free(b);
	return (FLP_GLOBBING);
}
