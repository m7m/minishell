/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_check_matches.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 14:24:33 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/30 14:24:34 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "../../../../libft/libft.h"
#include "libftfon.h"

static void		glob_check_back(char **matches, char *p)
{
	char	*tmp;
	char	c;

	c = *p;
	*p = '\0';
	if ((tmp = ft_strnew(0)))
	{
		ft_strpush(&tmp, c);
		if (ft_freeswap((void**)&tmp, ft_strcjoin(*matches, tmp, '\\'))
		&& ft_freeswap((void**)&tmp, ft_strjoin(tmp, p + 1)))
			ft_freeswap((void**)matches, tmp);
	}
}

void			glob_check_matches_glob(char **matches)
{
	char	*p;

	while (*matches)
	{
		p = *matches;
		while (*p && *p != '*' && *p != '[' && *p != '?' && *p != '{')
		{
			if (*p == '\\' && p[1])
				++p;
			++p;
		}
		if (*p)
			glob_check_back(matches, p);
		else
			++matches;
	}
}
