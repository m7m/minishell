/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_solve.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/04 15:37:26 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 16:00:25 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "../../../../libft/libft.h"
#include "libftfon.h"

#include <stdlib.h>

static void		process_dir(t_glob *glob, char **dirs)
{
	while (*dirs)
	{
		if (!globbing_matches(glob, *dirs))
		{
			free(*dirs);
			ft_parrrem((void**)dirs);
		}
		else
			++dirs;
	}
}

static void		solve_last_dir(t_glob *glob, char **dc, char *dir,
								char ***matches)
{
	while (*dc)
	{
		if (globbing_matches(glob, *dc))
		{
			if (*dir == '.' && !dir[1])
				ft_parrpush((void***)matches, ft_strdup(*dc));
			else
				ft_parrpush((void***)matches, glob_link_dir(dir, *dc));
		}
		++dc;
	}
}

static char		**globbing_solve_files(t_glob *glob, char **matching_dirs)
{
	char	**dc;
	char	**matches;
	char	**dirs;

	dirs = matching_dirs;
	matches = (char**)ft_parrnew();
	while (*matching_dirs)
	{
		if ((dc = ft_gdc(*matching_dirs)))
		{
			solve_last_dir(glob, dc, *matching_dirs, &matches);
			ft_parrfree((void**)dc);
		}
		++matching_dirs;
	}
	ft_parrfree((void**)dirs);
	ft_strarrsort(matches);
	return (matches);
}

static void		globbing_solve_dirs(t_glob **globs, char *dir,
										char ***matching_dirs)
{
	char	**dirs;
	char	**p;

	if ((dirs = ft_gdd(dir)))
	{
		if (*dirs)
			process_dir(*globs, dirs);
		if (*dirs)
		{
			p = dirs;
			if (globs[2])
				while (*p)
					globbing_solve_dirs(globs + 1, (*dir == '.' && !dir[1]) ?
						ft_strdup(*p++)
						: glob_link_dir(dir, *p++), matching_dirs);
			else
				while (*p)
					ft_parrpush((void***)matching_dirs, (*dir == '.' && !dir[1])
						? ft_strdup(*p++) : glob_link_dir(dir, *p++));
		}
		ft_parrfree((void**)dirs);
	}
	free(dir);
}

char			**globbing_solve(t_globbing glob)
{
	char	**matching_dirs;

	matching_dirs = (char**)ft_parrnew();
	if (glob.globs[1])
		globbing_solve_dirs(glob.globs, ft_strdup(glob.dir), &matching_dirs);
	else
		ft_parrpush((void***)&matching_dirs, ft_strdup(glob.dir));
	return (globbing_solve_files(glob.globs[ft_parrlen((void**)glob.globs) - 1],
		matching_dirs));
}
