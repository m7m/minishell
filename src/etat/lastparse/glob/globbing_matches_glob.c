/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_matches_glob.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 19:01:39 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/26 16:33:31 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "../../../../libft/libft.h"
#include "libftfon.h"

unsigned char	globbing_matches_normale(t_glob *glob, char *str)
{
	return (ft_strncmp(glob->str, str, ft_strlen(glob->str))) ?
	0 : next_glob(glob->next, str + ft_strlen(glob->str));
}

unsigned char	globbing_matches_wildcar(t_glob *glob, char *str)
{
	char	*ptr;

	ptr = str + ft_strlen(str) + 1;
	while (str != ptr && !next_glob(glob->next, str))
		++str;
	return (str != ptr);
}

unsigned char	globbing_matches_interog(t_glob *glob, char *str)
{
	return (*str) ? next_glob(glob->next, str + 1) : 0;
}

unsigned char	globbing_matches_bracket(t_glob *glob, char *str)
{
	return (*str && ft_strchr(glob->str, *str)) ?
	next_glob(glob->next, str + 1) : 0;
}
