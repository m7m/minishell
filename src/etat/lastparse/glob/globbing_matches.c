/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_matches.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/04 15:42:10 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/26 16:33:57 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

unsigned char	next_glob(t_glob *glob, char *str)
{
	if (!glob)
		return (!*str);
	if (glob->type == NORMALE)
		return (globbing_matches_normale(glob, str));
	if (glob->type == WILDCAR)
		return (globbing_matches_wildcar(glob, str));
	if (glob->type == INTEROG)
		return (globbing_matches_interog(glob, str));
	if (glob->type == BRACKET)
		return (globbing_matches_bracket(glob, str));
	return (0);
}

unsigned char	globbing_matches(t_glob *glob, char *str)
{
	if (*str == '.' && (glob->type != NORMALE || *glob->str != '.'))
		return (0);
	return (next_glob(glob, str));
}
