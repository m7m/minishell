/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_parse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 18:00:57 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 16:12:58 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "libftfon.h"

static t_glob	*parse_next_glob(char **arg)
{
	t_glob	*glob;
	t_glob	*glob_beg;
	char	*ptr;

	glob = (t_glob*)0;
	glob_beg = ft_m(sizeof(t_glob));
	while (**arg && **arg != '/')
	{
		glob = (!glob) ? glob_beg :
			(glob->next = ft_m(sizeof(t_glob)));
		if (**arg == '*')
			fill_glob_wildcard(glob, arg);
		else if (**arg == '[' && (ptr = globbing_bracket_end(*arg)))
			fill_glob_bracket(glob, arg, ptr);
		else if (**arg == '?')
			fill_glob_interog(glob, arg);
		else
			fill_glob_normal(glob, arg);
	}
	if (**arg)
		*arg = ft_strpassc(*arg, '/');
	glob->next = (t_glob*)0;
	return (glob_beg);
}

static char		*set_starting_dir_beg(char **arg)
{
	char	*dir;
	char	*p;
	char	c;

	if (**arg != '/' && (**arg != '.' || (*arg)[1] != '/'))
		return (ft_strdup("."));
	p = ft_strpassc(*arg + (**arg == '.'), '/');
	c = *p;
	*p = '\0';
	dir = ft_strdup(*arg);
	*p = c;
	*arg = p;
	return (dir);
}

static char		*parse_starting_dir(char **arg)
{
	char	*dir;
	char	*ptr;

	dir = set_starting_dir_beg(arg);
	while ((ptr = ft_strchr(*arg, '/')))
	{
		*ptr = '\0';
		if (globbing_parse_if(*arg, 0))
		{
			*ptr = '/';
			break ;
		}
		dir = ft_freeswap((void**)&dir,
			(*dir != '/' || *ft_strpassc(dir, '/')) ?
			ft_strcjoin(dir, *arg, '/') : ft_strjoin(dir, *arg));
		*arg = ptr + 1;
	}
	return (dir);
}

t_globbing		globbing_parse_arg(char *arg, size_t diff)
{
	t_globbing	glob;
	char		c;
	char		*p;

	p = arg;
	glob.globs = (t_glob**)ft_parrnew();
	glob.dir = parse_starting_dir(&arg);
	diff += p - arg;
	if (diff)
	{
		ft_parrpush((void***)&glob.globs, ft_m(sizeof(t_glob)));
		glob.globs[0]->type = NORMALE;
		c = arg[diff];
		arg[diff] = '\0';
		glob.globs[0]->str = ft_strdup(arg);
		arg[diff] = c;
		arg += diff;
		glob.globs[0]->next = parse_next_glob(&arg);
	}
	while (*arg)
		ft_parrpush((void***)&glob.globs, parse_next_glob(&arg));
	return (glob);
}
