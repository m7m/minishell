/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_bracket.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 17:06:24 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/01 13:35:26 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "../../../../libft/libft.h"
#include "libftfon.h"

char	*globbing_bracket_end(char *arg)
{
	if (arg[1] == ']')
		++arg;
	while (*++arg != ']' && *arg)
		;
	return (*arg) ? arg : (char*)0;
}

char	*glob_link_dir(char *dir, char *p)
{
	if (*dir == '/' && !*ft_strpassc(dir, '/'))
		return (ft_strjoin(dir, p));
	if (*dir == '.' && dir[1] == '/' && !*ft_strpassc(dir + 2, '/'))
		return (ft_strjoin(dir, p));
	return (ft_strcjoin(dir, p, '/'));
}
