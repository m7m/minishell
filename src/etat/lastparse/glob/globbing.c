/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 18:18:01 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 16:01:04 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "../ft_etat_lastparse.h"
#include "../../../../libft/libft.h"
#include "libftfon.h"

#include <stdlib.h>

static void	free_globs(t_glob **globs)
{
	t_glob			*glob;
	unsigned int	i;

	i = 0;
	while (globs[i])
	{
		while ((glob = globs[i]))
		{
			if (glob->str)
				free(glob->str);
			globs[i] = glob->next;
			free(glob);
		}
		++i;
	}
	free(globs);
}

static void	glob_replace_line(char **matches, t_linetoargv *lta)
{
	char	*p;
	char	*after;

	glob_check_matches_glob(matches);
	if (!(after = ft_strchr(lta->pline, ' ')))
		after = lta->pline + ft_strlen(lta->pline);
	if (!(after = ft_strdup(after))
	|| !ft_freeswap((void**)&lta->line, ft_strdup(*matches)))
		return ;
	while (*++matches)
	{
		if ((p = ft_strjoin("\"", *matches))
		&& ft_freeswap((void**)&p, ft_strjoin(p, "\"")))
		{
			ft_freeswap((void**)&lta->line, ft_strcjoin(lta->line, p, ' '));
			free(p);
		}
	}
	ft_freeswap((void**)&lta->line, ft_strjoin(lta->line, after));
	free(after);
	lta->pline = lta->line;
}

static int	perform_globbing(char *arg, t_linetoargv *lta)
{
	char		**matches;
	t_globbing	glob;

	glob = globbing_parse_arg(arg, lta->pline - lta->line);
	matches = globbing_solve(glob);
	free(glob.dir);
	free_globs(glob.globs);
	if (*matches)
	{
		free(arg);
		glob_replace_line(matches, lta);
		ft_parrfree((void**)matches);
		return (FLP_GLOBBING);
	}
	free(matches);
	ft_putstr_fd("no matches found: ", 2);
	ft_putstr_fd(arg, 2);
	ft_putchar_fd('\n', 2);
	free(arg);
	return (-1);
}

char		*globbing_get_arg(t_linetoargv *lta)
{
	char	*p;
	char	*arg;
	char	c;

	p = lta->pline;
	while (*p && *p != ' ')
		++p;
	c = *p;
	*p = '\0';
	arg = ft_strdup(lta->line);
	*p = c;
	return (arg);
}

int			ft_etat_lastparse_globbing(t_linetoargv *lta)
{
	char	*arg;

	if (lta->flags & (FL_SQUOT | FL_DQUOT | FL_GRAVE))
		return (FLP_NONE);
	if (ft_strchr(lta->pline, '{'))
	{
		arg = lta->pline;
		lta->pline = ft_strchr(lta->pline, '{');
		if (ft_etat_lastparse_globbing_bracket(lta) == FLP_GLOBBING)
			return (FLP_GLOBBING);
		lta->pline = arg;
	}
	if (!(arg = globbing_get_arg(lta)))
		return (FLP_NONE);
	if (!globbing_parse_if(arg + (lta->pline - lta->line),
	lta->pline - lta->line))
	{
		free(arg);
		return (FLP_NONE);
	}
	return (perform_globbing(arg, lta));
}
