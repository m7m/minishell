/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_parse_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 16:51:35 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/01 13:45:17 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

unsigned char	globbing_parse_if(char *arg, size_t n)
{
	if (*arg == '['
	&& (arg[1] == ' ' || arg[1] == '\t' || arg[1] == '\n')
	&& n && (arg[-1] == ' ' || arg[-1] == '\t' || arg[-1] == '\n'))
		return (0);
	while (*arg)
	{
		if (*arg == '*' || *arg == '?' ||
		(*arg == '[' && globbing_bracket_end(arg)))
			return (1);
		else if (*arg == '\\' && arg[1])
			++arg;
		++arg;
	}
	return (0);
}
