/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftfon.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/26 17:19:14 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/26 17:19:17 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTFON_H
# define LIBFTFON_H

# include <libft.h>

void			*ft_freeswap(void **ptr, void *swap);
void			*ft_m(unsigned int byte_nb);

char			**ft_gdc(char *path);
char			**ft_gdd(char *path);

char			*ft_strcjoin(const char *s1, const char *s2, char c);
void			ft_strrdc(char *str);
char			*ft_strpassc(char *str, char c);

void			ft_strpush(char **str, char c);
void			ft_strrem(char *str);

void			ft_strarrsort(char **arr);

void			**ft_parrnew(void);
void			ft_parrfree(void **tab);
void			ft_parrelmfree(void **tab);
void			ft_parrpush(void ***tab, void *elem);
void			ft_parrrem(void **tab);
void			ft_parrprem(void **tab, void *elem);
unsigned int	ft_parrlen(void **array);
void			**ft_parrdup(void **array);
void			**ft_parrndup(void **array, unsigned int n);
void			**ft_parrjoin(void **a1, void **a2);
void			ft_parrcat(void ***a1, void **a2);

#endif
