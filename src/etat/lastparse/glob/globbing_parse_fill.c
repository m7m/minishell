/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   globbing_parse_fill.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 18:23:32 by tfontani          #+#    #+#             */
/*   Updated: 2017/04/05 20:11:46 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "globbing.h"

#include "../../../../libft/libft.h"
#include "libftfon.h"

void	fill_glob_normal(t_glob *glob, char **arg)
{
	char	*ptr;

	glob->str = ft_strnew(0);
	ptr = *arg;
	while (*ptr && *ptr != '*' && *ptr != '?'
	&& (*ptr != '[' || !globbing_bracket_end(ptr)) && *ptr != '/')
	{
		if (*ptr == '\\' && ptr[1])
			++ptr;
		ft_strpush(&glob->str, *ptr);
		++ptr;
	}
	glob->type = NORMALE;
	*arg = ptr;
}

void	fill_glob_wildcard(t_glob *glob, char **arg)
{
	glob->type = WILDCAR;
	glob->str = (char*)0;
	*arg = ft_strpassc(*arg, '*');
}

void	fill_glob_interog(t_glob *glob, char **arg)
{
	glob->type = INTEROG;
	glob->str = (char*)0;
	++*arg;
}

void	fill_glob_bracket(t_glob *glob, char **arg, char *ptr)
{
	*ptr = '\0';
	glob->str = ft_strdup(*arg + 1);
	glob->type = BRACKET;
	*arg = ptr + 1;
	*ptr = ']';
	ft_strrdc(glob->str);
}
