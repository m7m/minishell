/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 20:09:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 20:09:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../etat.h"
#include "ft_etat_lastparse.h"

int
	ft_etat_lastparse(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->line);
	if (ft_etat(ft_etat_lastparse_init, tlta) == -1)
	{
		DEBUGSTR("erreur detecte");
		return (-1);
	}
	DEBUG;
	return (0);
}
