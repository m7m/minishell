/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__antsl.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 06:41:00 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 06:41:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_lastparse.h"

int
	ft_etat_lastparse__antsl(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	DEBUGNBRUL(FL_DQUOT & tlta->flags);
	DEBUGNBRUL(tlta->flags);
	if (tlta->flags & ~(FL_DQUOT | FL_DOL_PARENTH))
		return (FLP_NONE);
	if ((tlta->flags & FL_DQUOT && ft_strchr("\\\"$", tlta->pline[1]))
		|| (!tlta->flags))
	{
		ft_strcpy(tlta->pline, tlta->pline + 1);
		if (*tlta->pline)
			++tlta->pline;
		return (FLP_ANTSL);
	}
	tlta->pline += 2;
	DEBUGSTR(tlta->line);
	return (FLP_ANTSL);
}
