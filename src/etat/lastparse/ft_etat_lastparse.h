/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 20:12:58 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 19:47:24 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ETAT_LASTPARSE_H
# define FT_ETAT_LASTPARSE_H

# include "../etat.h"

int
ft_etat_lastparse(struct s_linetoargv *tlta);
struct s_etat
*ft_etat_lastparse_init(void);

int
ft_etat_lastparse__dol(struct s_linetoargv *tlta);

int
ft_etat_lastparse__squot(t_linetoargv *tlta);
int
ft_etat_lastparse__dquot(t_linetoargv *tlta);
int
ft_etat_lastparse__antsl(t_linetoargv *tlta);
int
ft_etat_lastparse__tilde(t_linetoargv *tlta);
int
ft_etat_lastparse__equale(t_linetoargv *tlta);

int
ft_etat_subshell_tree(
	char *start,
	char *end,
	t_linetoargv *tlta);

int
ft_etat_subshell_tree2(
	char *start,
	char *end,
	t_linetoargv *tlta);

int
ft_etat_lastparse__magicquot(t_linetoargv *tlta);

int
ft_etat_lastparse__parenth(t_linetoargv *tlta);
int
ft_etat_lastparse__parenth_close(t_linetoargv *tlta);
int
ft_etat_lastparse__dol_parenth(t_linetoargv *tlta);
int
ft_etat_lastparse_globbing(t_linetoargv *tlta);
int
ft_etat_lastparse_globbing_bracket(t_linetoargv *tlta);

#endif
