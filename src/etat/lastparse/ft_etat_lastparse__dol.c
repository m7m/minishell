/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse__dol.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 15:03:35 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 15:03:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../etat.h"
#include "../../../inc/ft_surrogate.h"
#include "minishell.h"

int
	ft_etat_lastparse__dol(t_linetoargv *tlta)
{
	int	len;

	DEBUG;
	if (tlta->flags & (FL_SQUOT | FL_HERED))
		return (FLP_NONE);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	len = tlta->pline - tlta->line;
	if (ft_lenvar(tlta->pline + 1))
		ft_surrogate__dol(&tlta->line, tlta->pline, 0);
	else
		++len;
	tlta->pline = tlta->line + len;
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	return (FLP_DOL);
}
