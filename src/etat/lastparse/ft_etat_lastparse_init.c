/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_lastparse_init.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 19:50:39 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 19:51:11 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/t_sh.h"
#include "ft_etat_lastparse.h"
#include "../../../inc/sh_macro.h"

int
	ft_etat_lastparse__space(t_linetoargv *tlta)
{
	char	*arg;

	DEBUG;
	if (tlta->flags)
		return (FLP_NONE);
	if (tlta->pline > tlta->line
		|| tlta->old_c == FLP_SQUOT
		|| tlta->old_c == FLP_DQUOT
		|| tlta->old_c == FLP_MQUOT)
	{
		DEBUG;
		arg = ft_strsub(tlta->line, 0, tlta->pline - tlta->line);
		ft_memchk_exit(arg);
		++tlta->sh->targv->argc;
		tlta->sh->targv->argv = ft_taballocadd(tlta->sh->targv->argv, arg);
	}
	while (*tlta->pline && ft_strchr(" \t\n", *tlta->pline))
		++tlta->pline;
	ft_strcpy(tlta->line, tlta->pline);
	tlta->pline = tlta->line;
	DEBUGSTR(tlta->line);
	return (FLP_SPACE);
}

int
	ft_etat_lastparse__tab(t_linetoargv *tlta)
{
	int	ret;

	DEBUG;
	ret = ft_etat_lastparse__space(tlta);
	if (ret == FLP_SPACE)
		return (FLP_TABUL);
	return (FLP_NONE);
}

int
	ft_etat_lastparse__end(t_linetoargv *tlta)
{
	DEBUG;
	if (tlta->flags)
	{
		DEBUGNBRUL(FL_DQUOT & tlta->flags);
		DEBUGNBRUL(FL_SQUOT & tlta->flags);
		DEBUGNBRUL(FL_ANTSL & tlta->flags);
		MSG_ERR3INT(tlta->flags, "error lastparse");
		return (-1);
	}
	ft_etat_lastparse__space(tlta);
	return (0);
}

void
	ft_etat_lastparse_init__add(t_etat *et)
{
	DEBUG;
	ft_etat_add(et, " ", ft_etat_lastparse__space);
	ft_etat_add(++et, "\t", ft_etat_lastparse__space);
	ft_etat_add(++et, "\n", ft_etat_lastparse__space);
	ft_etat_add(++et, "\"", ft_etat_lastparse__dquot);
	ft_etat_add(++et, "'", ft_etat_lastparse__squot);
	ft_etat_add(++et, "\\", ft_etat_lastparse__antsl);
	ft_etat_add(++et, "~", ft_etat_lastparse__tilde);
	ft_etat_add(++et, "$(", ft_etat_lastparse__dol_parenth);
	ft_etat_add(++et, ")", ft_etat_lastparse__parenth_close);
	ft_etat_add(++et, "(", ft_etat_lastparse__parenth);
	ft_etat_add(++et, "`", ft_etat_lastparse__magicquot);
	ft_etat_add(++et, "$", ft_etat_lastparse__dol);
	ft_etat_add(++et, "=", ft_etat_lastparse__equale);
	ft_etat_add(++et, "[", ft_etat_lastparse_globbing);
	ft_etat_add(++et, "*", ft_etat_lastparse_globbing);
	ft_etat_add(++et, "?", ft_etat_lastparse_globbing);
	ft_etat_add(++et, "{", ft_etat_lastparse_globbing_bracket);
	ft_etat_add(++et, "", ft_etat_lastparse__end);
	ft_etat_add(++et, 0, 0);
}

t_etat
	*ft_etat_lastparse_init(void)
{
	static t_etat	et[127] = {{0, 0, 0}};

	DEBUG;
	if (!et->c)
		ft_etat_lastparse_init__add(et);
	DEBUG;
	return (et);
}
