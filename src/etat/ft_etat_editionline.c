/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_editionline.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/24 06:34:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/24 06:34:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "etat.h"
#include "chkclose/ft_etat_chkclose.h"
#include "../../inc/minishell.h"

long
	ft_etat_editionline_tlta(
		char *line, char *pline,
		t_linetoargv *tlta, t_sh *sh)
{
	tlta->line = line;
	tlta->pline = pline;
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	tlta->sh = sh;
	if (ft_etat(ft_etat_chkclose_init, tlta)
		&& !tlta->flags)
	{
		DEBUGSTR(tlta->pline);
		DEBUGNBRUL(tlta->flags);
		DEBUGSTR("erreur de syntax");
		return (-1);
	}
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	DEBUGNBRUL(tlta->flags);
	return (tlta->flags);
}

/*
** si -1 et flags == 0 erreur syntaxique
** si -1 et flags != 0 ligne non finie
*/

/*
** $? == 2
*/

long
	ft_etat_editionline(char *line, t_sh *sh)
{
	t_linetoargv	tlta;

	DEBUGSTR(line);
	ft_memset(&tlta, 0, sizeof(tlta));
	return (ft_etat_editionline_tlta(line, line, &tlta, sh));
}

long
	ft_etat_chkclose(char *line, t_sh *sh)
{
	int				nb_line;
	long			ret;
	t_linetoargv	tlta;

	DEBUGSTR(line);
	ft_memset(&tlta, 0, sizeof(tlta));
	nb_line = sh->nb_line;
	ret = ft_etat_editionline_tlta(line, line, &tlta, sh);
	if (ret > 0)
		ret &= ~FL_HERED;
	if (ret)
	{
		ft_updatekey_ret(&sh->hlocal, 2);
		MSG_ERRNBLINE(sh->nb_line, tlta.pline, "syntax error");
	}
	else
		sh->nb_line = nb_line;
	return (ret);
}

long
	ft_etat_chkclose_nointeractive(char *line, t_sh *sh)
{
	int				nb_line;
	long			ret;
	t_linetoargv	tlta;

	DEBUGSTR(line);
	ft_memset(&tlta, 0, sizeof(tlta));
	nb_line = sh->nb_line;
	ret = ft_etat_editionline_tlta(line, line, &tlta, sh);
	if (ret == FL_ANTSL)
		*tlta.pline = 0;
	sh->nb_line = nb_line;
	return (ret);
}
