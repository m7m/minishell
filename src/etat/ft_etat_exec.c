/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_exec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/24 06:34:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/24 06:34:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "etat.h"

int
	ft_etat_exec(t_linetoargv *tlta, t_etat *tet)
{
	int	ret;
	int	i;

	ret = 1;
	i = 0;
	while (ret > 0)
	{
		if (!tet[i].c)
		{
			i = 0;
			++tlta->pline;
		}
		tlta->old_c = ret;
		if (!ft_memcmp(tet[i].c, tlta->pline, tet[i].c_len))
		{
			if ((ret = tet[i].f(tlta)) > FLP_NONE)
				i = -1;
		}
		++i;
	}
	DEBUGNBR(ret);
	return (ret);
}
