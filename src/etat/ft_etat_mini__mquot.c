/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_mini__mquot.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 18:53:18 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/11 18:53:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_mini.h"

int
	ft_etat_mini__mquot(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	if (tlta->flags & ~(FL_DQUOT | FL_GRAVE))
		return (FLP_NONE);
	if (!(tlta->pline = ft_strchr(tlta->pline + 1, '`')))
	{
		tlta->flags &= FL_GRAVE;
		return (-1);
	}
	tlta->pline++;
	return (FLP_MQUOT);
}
