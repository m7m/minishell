/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 21:25:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/04 21:25:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "targv.h"
#include "etat.h"
#include "minishell.h"

int
	ft_etat__antsl(t_linetoargv *tlta)
{
	DEBUG;
	if (!(ISETAT_IGN(tlta->flags))
		|| (ISETAT_IGN(tlta->flags) & FL_DQUOT
			&& (tlta->pline[1] == '"'
				|| tlta->pline[1] == '$')))
	{
		++tlta->pline;
		if (*tlta->pline)
			++tlta->pline;
		return (FLP_ANTSL);
	}
	return (FLP_NONE);
}
