/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_mini__parenth.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 20:29:56 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/17 20:29:56 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_mini.h"

int
	ft_etat_mini__parenth(t_linetoargv *tlta)
{
	DEBUG;
	if (tlta->flags & (FL_GRAVE | FL_DQUOT | FL_SQUOT))
		return (FLP_NONE);
	return (-1);
}
