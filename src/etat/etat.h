/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   etat.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 23:52:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/02 23:52:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ETAT_H
# define ETAT_H

# include "t_etat.h"
# include "../../inc/flags_parse.h"
# include "../../inc/flags.h"
# include <libft.h>

long	ft_etat(t_etat *(*fetat_init)(void), t_linetoargv *tlta);
int		ft_etat_exec(t_linetoargv *tlta, t_etat *tet);
void	ft_etat_add(t_etat *et, char *c, int (*f)(t_linetoargv *));

long	ft_etat_mini(t_linetoargv	*tlta);
t_etat	*ft_etat_mini_init(void);

char	*ft_etat_word(char **line, struct s_sh *sh);
long	ft_etat_editionline(char *line, struct s_sh *sh);
long	ft_etat_editionline_tlta(
	char *line, char *pline, t_linetoargv *tlta, struct s_sh *sh);

#endif
