/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_etat.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 17:28:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 17:28:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_ETAT_H
# define T_ETAT_H

# include <stddef.h>
# include "../../inc/flags_parse.h"

/*
** flags : flags.h
** old_c : flags_parser.h
*/

typedef struct			s_linetoargv
{
	long			flags;
	int				count[FLP_END_FL];
	int				old_c;

	char			*line;
	char			*pline;

	struct s_sh		*sh;
}						t_linetoargv;

typedef struct			s_etat
{
	size_t			c_len;
	char			*c;
	int				(*f)(struct s_linetoargv *);
}						t_etat;

#endif
