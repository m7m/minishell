/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat__xquot.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 21:23:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/04 21:23:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "etat.h"

static int
	ft_etat__quot(int flag, t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	if (!(tlta->flags & (flag | FL_HERED)))
	{
		DEBUG;
		tlta->flags |= flag;
		tlta->pline++;
	}
	else if (tlta->flags & flag)
	{
		tlta->flags &= ~flag;
		tlta->pline++;
	}
	else
		return (FLP_NONE);
	DEBUGSTR(tlta->pline);
	DEBUGNBR(tlta->flags);
	return (0);
}

int
	ft_etat__squot(t_linetoargv *tlta)
{
	int	flag;

	flag = FL_SQUOT;
	DEBUG;
	if (tlta->flags & ~flag)
		return (FLP_NONE);
	if (!(tlta->flags & (flag | FL_HERED)))
	{
		DEBUG;
		tlta->flags |= flag;
		ft_strcpy(tlta->pline, tlta->pline + 1);
	}
	else if (tlta->flags & flag)
	{
		tlta->flags &= ~flag;
		ft_strcpy(tlta->pline, tlta->pline + 1);
	}
	else
		return (FLP_NONE);
	return (FLP_SQUOT);
}

int
	ft_etat__dquot(t_linetoargv *tlta)
{
	int	flag;

	flag = FL_DQUOT;
	DEBUG;
	if (tlta->flags & ~FL_DQUOT)
		return (FLP_NONE);
	if (!(tlta->flags & (flag | FL_HERED | FL_ANTSL)))
	{
		DEBUG;
		tlta->flags |= FL_DQUOT;
		ft_strcpy(tlta->pline, tlta->pline + 1);
	}
	else if (tlta->flags & flag)
	{
		tlta->flags &= ~flag;
		ft_strcpy(tlta->pline, tlta->pline + 1);
	}
	else
		return (FLP_NONE);
	return (FLP_DQUOT);
}

int
	ft_etat__mquot(t_linetoargv *tlta)
{
	DEBUG;
	if (tlta->flags & ~(FL_DQUOT | FL_GRAVE))
		return (FLP_NONE);
	if (ft_etat__quot(FL_GRAVE, tlta) == FLP_NONE)
		return (FLP_NONE);
	return (FLP_MQUOT);
}
