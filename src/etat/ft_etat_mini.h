/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_minii.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 06:34:30 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 06:34:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ETAT_MINI_H
# define FT_ETAT_MINI_H

# include "etat.h"

int		ft_etat__antsl(struct s_linetoargv *tlta);
int		ft_etat__mquot(struct s_linetoargv *tlta);
int		ft_etat__dquot(struct s_linetoargv *tlta);
int		ft_etat__squot(struct s_linetoargv *tlta);
int		ft_etat__crosi(struct s_linetoargv *tlta);

int		ft_etat_mini__parenth(struct s_linetoargv *tlta);
int		ft_etat_mini__mquot(t_linetoargv *tlta);

#endif
