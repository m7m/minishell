/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__red_heredoc.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:32:17 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:32:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

static long
	ft____red_heredoc__chreof_foundnotend(
		char *eof, char *eof_start, char *eof_end, t_linetoargv *tlta)
{
	long	len;
	char	*s;

	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	len = eof_start - tlta->line;
	s = ft_strdup(tlta->line);
	ft_memchk_exit(s);
	eof_end = ft_strchrnull(s + len, '\n');
	*eof_end = 0;
	DEBUGSTR(s);
	tlta->flags &= ~FL_HERED;
	DEBUGNBRUL(tlta->flags);
	len = ft_etat_chkclose__recursion_flags(s, s + len, tlta);
	DEBUGNBRUL(tlta->flags);
	free(s);
	DEBUGNBRUL(len);
	free(eof);
	if (!len)
	{
		tlta->flags |= FL_HERED;
		return (0);
	}
	return (len);
}

static long
	ft____red_heredoc__chreof_foundend(
		char *eof, char *eof_start, char *eof_end, t_linetoargv *tlta)
{
	long	len;
	char	*s;

	len = eof_start - tlta->line;
	s = ft_strdup(tlta->line);
	ft_memchk_exit(s);
	DEBUGSTR(s);
	DEBUGSTR(s + len);
	DEBUGSTR(s + ((eof_end + ft_strlen(eof)) - tlta->line));
	ft_strcpy(
		s + len, s + ((eof_end + ft_strlen(eof)) - tlta->line));
	DEBUGSTR(s);
	DEBUGSTR(s + len);
	tlta->flags &= ~FL_HERED;
	DEBUGNBRUL(tlta->flags);
	len = ft_etat_chkclose__recursion_flags(
		s, s + (tlta->pline - tlta->line), tlta);
	free(s);
	DEBUGNBRUL(len);
	free(eof);
	return (len);
}

static int
	ft____red_heredoc__chreof_found(
		char *eof, char *eof_start, char *eof_end, t_linetoargv *tlta)
{
	long	len;

	DEBUGSTR(eof_start);
	DEBUGSTR(eof_end);
	DEBUGSTR(tlta->pline);
	if (eof_end)
		len = ft____red_heredoc__chreof_foundend(eof, eof_start, eof_end, tlta);
	else
		len = ft____red_heredoc__chreof_foundnotend(
			eof, eof_start, eof_end, tlta);
	if (len > 0 || len == -1)
		return (-1);
	return (0);
}

static int
	ft____red_heredoc__chreof(
		char *line, t_linetoargv *tlta)
{
	int		len;
	char	*eof;
	char	*eof_start;
	char	*eof_end;
	char	*tmp;

	DEBUGSTR(tlta->pline);
	eof = line;
	if ((eof_start = ft_strchr(tlta->pline, '\n')))
	{
		len = ft_strlen(eof);
		eof = (char *)malloc(SC(len + 3));
		ft_memchk_exit(eof);
		ft_strcpy(eof + 1, line);
		eof[0] = '\n';
		eof[++len] = 0;
		tmp = eof_start;
		while ((eof_end = ft_strstr(tmp, eof))
			&& eof_end[len] != 0 && eof_end[len] != '\n')
			tmp = eof_end + 1;
		return (ft____red_heredoc__chreof_found(
					eof, eof_start, eof_end, tlta));
	}
	tlta->flags |= FL_HERED;
	return (FLP_HERE);
}

int
	ft_etat_chkclose__red_heredoc(t_linetoargv *tlta)
{
	int		ret;
	char	*line;
	char	*line_modifer;

	DEBUGNBR(tlta->flags);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	DEBUGSTR(tlta->line);
	tlta->pline += 2;
	if (*tlta->pline == '-')
		++tlta->pline;
	line = tlta->pline;
	DEBUGSTR(line);
	if (!*line
		|| !(line_modifer = ft_etat_word(&line, tlta->sh)))
		return (-1);
	DEBUGSTR(line_modifer);
	tlta->pline += ft_strlen(tlta->pline) - ft_strlen(line_modifer);
	free(line_modifer);
	DEBUGSTR(line);
	DEBUGNBR(*line);
	tlta->flags |= FL_HERED;
	ret = ft____red_heredoc__chreof(line, tlta);
	free(line);
	return (ret);
}
