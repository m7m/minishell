/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__while.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 20:37:53 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/14 20:37:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__while(t_linetoargv *tlta)
{
	DEBUG;
	return (ft_etat_chkclose__sh_commun_firts(FL_WHILE, FL_DONE, 5, tlta));
}

int
	ft_etat_chkclose__do(t_linetoargv *tlta)
{
	DEBUG;
	return (ft_etat_chkclose__sh_commun(FL_WHILE, FL_DO, 2, tlta));
}

int
	ft_etat_chkclose__done(t_linetoargv *tlta)
{
	int	ret;

	DEBUG;
	ret = ft_etat_chkclose__sh_commun(FL_DO, FL_DONE, 4, tlta);
	DEBUGNBR(ret);
	DEBUGNBR(FLP_SHELLSCRIPT == ret);
	if (ret == FLP_SHELLSCRIPT)
		return (0);
	DEBUG;
	return (ret);
}
