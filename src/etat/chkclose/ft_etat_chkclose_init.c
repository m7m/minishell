/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose_init.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:22:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:22:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

/*
** feature: destection edition de ligne et coloration des erreurs
** retourne -1 si erreur,
** la valeur de tlta->flags permet de savoir sur quel flag
**  si 0 alors erreur de syntax
** tlta->line est vaut un const donc ne doit pas bouger ni subir un strcpy
**  cela est du a une verifiaction qui se fait de gauche a droit
**  et de droite a gauche, voir ft_etat_chkclose__tokenprim
** !! la line ne doit en aucun cas changer de longeur !!
*/

static t_etat
	*ft_etat_chkclose_init__add_red(t_etat *et)
{
	ft_etat_add(++et, ">&-", ft_etat_chkclose__none);
	ft_etat_add(++et, "<&-", ft_etat_chkclose__none);
	ft_etat_add(++et, ">&", ft_etat_chkclose__red_digit);
	ft_etat_add(++et, "<&", ft_etat_chkclose__red_digit);
	ft_etat_add(++et, "<<<", ft_etat_chkclose__red_hereline);
	ft_etat_add(++et, "<<-", ft_etat_chkclose__red_heredoc);
	ft_etat_add(++et, "<<", ft_etat_chkclose__red_heredoc);
	ft_etat_add(++et, "&>>", ft_etat_chkclose__red_stderrappend);
	ft_etat_add(++et, "&>", ft_etat_chkclose__red_stderr);
	ft_etat_add(++et, ">>", ft_etat_chkclose__red_stdappend);
	ft_etat_add(++et, ">", ft_etat_chkclose__red_stdout);
	ft_etat_add(++et, "<", ft_etat_chkclose__red_stdin);
	return (et);
}

static t_etat
	*ft_etat_chkclose_init__add_shellscript(t_etat *et)
{
	ft_etat_add(++et, "if", ft_etat_chkclose__if);
	ft_etat_add(++et, "then", ft_etat_chkclose__then);
	ft_etat_add(++et, "elif", ft_etat_chkclose__elif);
	ft_etat_add(++et, "else", ft_etat_chkclose__else);
	ft_etat_add(++et, "fi", ft_etat_chkclose__fi);
	ft_etat_add(++et, "while", ft_etat_chkclose__while);
	ft_etat_add(++et, "do", ft_etat_chkclose__do);
	ft_etat_add(++et, "for", ft_etat_chkclose__for);
	ft_etat_add(++et, "in", ft_etat_chkclose__in);
	ft_etat_add(++et, "done", ft_etat_chkclose__done);
	ft_etat_add(++et, "case", ft_etat_chkclose__done);
	ft_etat_add(++et, "esac", ft_etat_chkclose__done);
	ft_etat_add(++et, ";;", ft_etat_chkclose__done);
	return (et);
}

static void
	ft_etat_chkclose_init__add(t_etat *et)
{
	ft_etat_add(et, "`", ft_etat_chkclose__mquot);
	ft_etat_add(++et, "'", ft_etat_chkclose__squot);
	ft_etat_add(++et, "\"", ft_etat_chkclose__dquot);
	ft_etat_add(++et, "\\", ft_etat_chkclose__antsl);
	ft_etat_add(++et, "#", ft_etat_chkclose__crosi);
	/* ft_etat_add(++et, "}", ft_etat_chkclose__accol_close); */
	/* ft_etat_add(++et, "${", ft_etat_chkclose__dolaccol); */
	/* ft_etat_add(++et, "{", ft_etat_chkclose__accol_block); */
	ft_etat_add(++et, ")", ft_etat_chkclose__parenth_close);
	ft_etat_add(++et, "$(", ft_etat_chkclose__dolparen);
	ft_etat_add(++et, "(", ft_etat_chkclose__parenth);
	et = ft_etat_chkclose_init__add_red(et);
	ft_etat_add(++et, "&&", ft_etat_chkclose__and);
	ft_etat_add(++et, "||", ft_etat_chkclose__or);
	ft_etat_add(++et, "|&", ft_etat_chkclose__pipeerr);
	ft_etat_add(++et, "|", ft_etat_chkclose__pipe);
	et = ft_etat_chkclose_init__add_shellscript(et);
	ft_etat_add(++et, "\n", ft_etat_chkclose__newline);
	ft_etat_add(++et, "&", ft_etat_chkclose__tokenprim);
	ft_etat_add(++et, ";", ft_etat_chkclose__tokenprim);
	ft_etat_add(++et, "", ft_etat_chkclose__end);
	ft_etat_add(++et, 0, 0);
}

t_etat
	*ft_etat_chkclose_init(void)
{
	static t_etat	tet[127] = {{0, 0, 0}};

	DEBUG;
	if (!tet->c)
		ft_etat_chkclose_init__add(tet);
	return (tet);
}
