/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__none.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:40:06 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:40:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__none(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	while (*tlta->pline && *tlta->pline != '-')
		++tlta->pline;
	++tlta->pline;
	return (FLP_INTO + FLP_OUTTO);
}
