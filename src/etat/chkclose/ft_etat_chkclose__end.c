/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__end.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:40:26 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:40:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__end(t_linetoargv *tlta)
{
	DEBUG;
	(void)tlta;
	return (0);
}
