/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:24:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:24:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ETAT_CHKCLOSE_H
# define FT_ETAT_CHKCLOSE_H

# include "../etat.h"
# include "../../../inc/l_sh.h"

long	ft_etat_chkclose(char *line, struct s_sh *sh);
t_etat	*ft_etat_chkclose_init(void);

long	ft_etat_chkclose_nointeractive(char *line, struct s_sh *sh);

int		ft_etat_chkclose__end(t_linetoargv *tlta);
int		ft_etat_chkclose__recursion(t_linetoargv *tlta);
long	ft_etat_chkclose__recursion_flags(
	char *line, char *pline, t_linetoargv *tlta);
int		ft_etat_chkclose__recursion_lineflags(
	long flags, t_linetoargv *tlta);

int		ft_etat_chkclose__commun(
	int size, long flag, int ret, t_linetoargv *tlta);

int		ft_etat_chkclose__mquot(t_linetoargv *tlta);
int		ft_etat_chkclose__squot(t_linetoargv *tlta);
int		ft_etat_chkclose__dquot(t_linetoargv *tlta);
int		ft_etat_chkclose__antsl(t_linetoargv *tlta);
int		ft_etat_chkclose__crosi(t_linetoargv *tlta);

int		ft_etat_chkclose__red_stdin(t_linetoargv *tlta);
int		ft_etat_chkclose__red_stdout(t_linetoargv *tlta);
int		ft_etat_chkclose__red_stdappend(t_linetoargv *tlta);
int		ft_etat_chkclose__red_stderr(t_linetoargv *tlta);
int		ft_etat_chkclose__red_stderrappend(t_linetoargv *tlta);
int		ft_etat_chkclose__red_hereline(t_linetoargv *tlta);

int		ft_etat_chkclose__red_commun(
	int size, long flag, int ret, t_linetoargv *tlta);
int		ft_etat_chkclose__red_digit(t_linetoargv *tlta);
int		ft_etat_chkclose__red_heredoc(t_linetoargv *tlta);

int		ft_etat_chkclose__none(t_linetoargv *tlta);

int		ft_etat_chkclose__and(t_linetoargv *tlta);
int		ft_etat_chkclose__or(t_linetoargv *tlta);
int		ft_etat_chkclose__pipeerr(t_linetoargv *tlta);
int		ft_etat_chkclose__pipe(t_linetoargv *tlta);

int		ft_etat_chkclose__newline(t_linetoargv *tlta);
int		ft_etat_chkclose__tokenprim(t_linetoargv *tlta);
int		ft_etat_chkclose__tokenprim__prev(char *s, t_linetoargv *tlta);

int		ft_etat_chkclose__sh_commun(
	long flags_src, long flags_add, int size, t_linetoargv *tlta);

int		ft_etat_chkclose__if(t_linetoargv *tlta);
int		ft_etat_chkclose__then(t_linetoargv *tlta);
int		ft_etat_chkclose__elif(t_linetoargv *tlta);
int		ft_etat_chkclose__else(t_linetoargv *tlta);
int		ft_etat_chkclose__fi(t_linetoargv *tlta);

int		ft_etat_chkclose__for(t_linetoargv *tlta);
int		ft_etat_chkclose__in(t_linetoargv *tlta);
int		ft_etat_chkclose__done(t_linetoargv *tlta);
int		ft_etat_chkclose__do(t_linetoargv *tlta);
int		ft_etat_chkclose__while(t_linetoargv *tlta);

int		ft_etat_chkclose__parenth_close(t_linetoargv *tlta);
int		ft_etat_chkclose__dolparen(t_linetoargv *tlta);
int		ft_etat_chkclose__parenth(t_linetoargv *tlta);

int		ft_etat_chkclose__tokenprim__prev2(char *line, t_linetoargv *tlta);

int		ft_etat_chkclose__sh_commun_firts(
	long flags_src,
	long flags_end,
	int size,
	t_linetoargv *tlta);
int		ft_etat_chkclose__sh_commun_firts__none(
	char *line, t_linetoargv *tlta);

#endif
