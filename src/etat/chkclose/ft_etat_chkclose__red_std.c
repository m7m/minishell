/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__red_std.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 07:06:46 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 07:06:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__red_stderrappend(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__red_commun(3, FL_REDIREC_OUT, FLP_OUTTO, tlta));
}

int
	ft_etat_chkclose__red_stderr(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__red_commun(2, FL_REDIREC_OUT, FLP_OUTTO, tlta));
}

int
	ft_etat_chkclose__red_stdappend(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__red_commun(2, FL_REDIREC_OUT, FLP_OUTTO, tlta));
}

int
	ft_etat_chkclose__red_stdout(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__red_commun(1, FL_REDIREC_OUT, FLP_OUTTO, tlta));
}

int
	ft_etat_chkclose__red_stdin(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__red_commun(1, FL_REDIREC_IN, FLP_INTO, tlta));
}
