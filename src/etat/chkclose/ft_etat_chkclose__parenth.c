/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__parenth.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 06:38:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/27 06:38:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

static int
	ft_etat_chkclose__parenth__recurs(t_linetoargv *tlta)
{
	int		ret;
	long	flags;

	flags = tlta->flags;
	ret = ft_etat_chkclose__recursion_lineflags(0, tlta);
	DEBUGSTR(tlta->pline);
	DEBUGC(*tlta->pline);
	if (*tlta->pline != ')' && ret == -1)
		return (-1);
	DEBUG;
	if (*tlta->pline == ')')
	{
		tlta->pline++;
		tlta->flags = flags;
		return (0);
	}
	DEBUGSTR("erreur remonté");
	if (*tlta->pline)
	{
		DEBUGSTR("erreur syntax");
		tlta->flags = 0;
	}
	else
		tlta->flags |= FL_PAREN;
	return (-1);
}

static int
	ft_etat_chkclose__parenth__ifredire(char *s)
{
	while (*s && ft_isdigit(*s))
		++s;
	if (*s == '<' || *s == '>')
		return (1);
	return (0);
}

int
	ft_etat_chkclose__parenth(t_linetoargv *tlta)
{
	char	*s;

	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	s = ft_pass_space_rev(tlta->line, tlta->pline - 1);
	if (s != tlta->pline - 1 && !ft_strchr("\n|;><&", *s))
		return (-1);
	s = ++tlta->pline;
	while (*s && ft_strchr("\n \t", *s))
		++s;
	if (*s == ')')
		return (-1);
	if (ft_etat_chkclose__parenth__recurs(tlta))
		return (-1);
	s = ft_pass_spacenewline(tlta->pline);
	DEBUG;
	if (!*s
		|| ft_strchr("|;&", *s)
		|| ft_etat_chkclose__parenth__ifredire(s))
		return (FLP_PARENTH);
	DEBUG;
	return (-1);
}

int
	ft_etat_chkclose__dolparen(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags & ~(FL_DQUOT)))
		return (FLP_NONE);
	tlta->pline += 2;
	if (ft_etat_chkclose__parenth__recurs(tlta))
		return (-1);
	DEBUG;
	return (FLP_DOL_PARENTH);
}

int
	ft_etat_chkclose__parenth_close(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	DEBUGSTR(tlta->pline);
	return (-1);
}
