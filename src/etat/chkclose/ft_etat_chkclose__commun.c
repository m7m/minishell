/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__commun.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 11:56:34 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 11:56:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

static int
	ft_etat_chkclose__commun__chk(int size, long flag, t_linetoargv *tlta)
{
	char	*s;

	s = ft_pass_spacenewline(tlta->pline + size);
	if (!*s)
	{
		DEBUG;
		tlta->flags |= flag;
		return (-1);
	}
	DEBUGC(*s);
	if (ft_strchr("&;\n", *s))
		return (-1);
	return (0);
}

int
	ft_etat_chkclose__commun(int size, long flag, int ret, t_linetoargv *tlta)
{
	char	*s;

	DEBUGSTR(tlta->pline);
	DEBUGNBRUL(tlta->flags);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	if (ft_etat_chkclose__commun__chk(size, flag, tlta))
		return (-1);
	s = tlta->pline - 1;
	DEBUGSTR(s);
	while (s > tlta->line && (*s == ' ' || *s == '\t'))
		--s;
	DEBUGSTR(s);
	DEBUGSTR(tlta->line);
	if (s >= tlta->line && ft_strchr("&|<>;\n", *s))
		return (-1);
	DEBUG;
	s = ft_pass_space(tlta->line);
	if (s == tlta->pline)
		return (-1);
	DEBUGSTR(tlta->pline);
	tlta->pline += size;
	return (ret);
}
