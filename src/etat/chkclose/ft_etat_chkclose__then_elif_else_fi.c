/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__then_elif_else_fi.c              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/09 13:27:49 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/09 13:27:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__then(t_linetoargv *tlta)
{
	DEBUG;
	return (ft_etat_chkclose__sh_commun(FL_IF | FL_ELIF, FL_THEN, 4, tlta));
}

int
	ft_etat_chkclose__elif(t_linetoargv *tlta)
{
	DEBUG;
	DEBUGNBRUL(FL_IF & tlta->flags);
	DEBUGNBRUL(FL_THEN & tlta->flags);
	DEBUGNBRUL(FL_ELIF & tlta->flags);
	DEBUGNBRUL(FL_ELSE & tlta->flags);
	return (ft_etat_chkclose__sh_commun(FL_THEN, FL_ELIF, 4, tlta));
}

int
	ft_etat_chkclose__else(t_linetoargv *tlta)
{
	DEBUG;
	return (ft_etat_chkclose__sh_commun(FL_THEN, FL_ELSE, 4, tlta));
}

/*
** Fin de la recursion provoqué par if
*/

int
	ft_etat_chkclose__fi(t_linetoargv *tlta)
{
	int	ret;

	DEBUG;
	ret = ft_etat_chkclose__sh_commun(FL_THEN | FL_ELSE, FL_FI, 2, tlta);
	DEBUGNBR(ret);
	DEBUGNBR(FLP_SHELLSCRIPT == ret);
	if (ret == FLP_SHELLSCRIPT)
		return (0);
	DEBUG;
	return (ret);
}
