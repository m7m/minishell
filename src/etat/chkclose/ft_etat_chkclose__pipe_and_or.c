/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__pipe_and_or.h                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:29:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:29:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__pipe(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__commun(1, FL_PIPE, FLP_PIPE, tlta));
}

int
	ft_etat_chkclose__and(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__commun(2, FL_AND, FLP_AND, tlta));
}

int
	ft_etat_chkclose__or(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__commun(2, FL_OR, FLP_OR, tlta));
}

int
	ft_etat_chkclose__pipeerr(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__commun(2, FL_PIPEERR, FLP_PIPEER, tlta));
}
