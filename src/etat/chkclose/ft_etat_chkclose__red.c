/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__red.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:31:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:31:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

static int
	ft_etat_chkclose__commun2__chk(int size, long flag, t_linetoargv *tlta)
{
	char	*s;

	s = ft_pass_spacenewline(tlta->pline + size);
	if (!*s)
	{
		DEBUG;
		tlta->flags |= flag;
		return (-1);
	}
	DEBUGC(*s);
	if (ft_strchr("&<>;\n()", *s))
		return (-1);
	return (0);
}

static int
	ft_etat_chkclose__commun2(int size, long flag, int ret, t_linetoargv *tlta)
{
	char	*s;

	DEBUGSTR(tlta->pline);
	DEBUGNBRUL(tlta->flags);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
	{
		DEBUGNBR(FLP_NONE);
		return (FLP_NONE);
	}
	if (ft_etat_chkclose__commun2__chk(size, flag, tlta))
		return (-1);
	s = tlta->pline - 1;
	DEBUGSTR(s);
	while (s > tlta->line && (*s == ' ' || *s == '\t'))
		--s;
	DEBUGSTR(s);
	DEBUGSTR(tlta->line);
	if (s >= tlta->line
		&& ft_strchr("<>\n", *s))
		return (-1);
	DEBUGSTR(tlta->pline);
	tlta->pline += size;
	return (ret);
}

int
	ft_etat_chkclose__red_commun(
		int size, long flag, int ret, t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	ret = ft_etat_chkclose__commun2(size, flag, ret, tlta);
	if (ret == -1)
	{
		DEBUGNBR(tlta->flags);
		tlta->flags = 0;
		return (-1);
	}
	return (ret);
}

int
	ft_etat_chkclose__red_hereline(t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	return (ft_etat_chkclose__red_commun(3, FL_HEREL, FLP_HERELINE, tlta));
}

/*
** >&file -> (convertie) -> >file
*/

int
	ft_etat_chkclose__red_digit(t_linetoargv *tlta)
{
	char	*s;
	char	*ps;

	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	s = ft_pass_space(tlta->pline + 2);
	DEBUGSTR(s);
	if (!*s
		|| ft_strchr("&<>;\n", *s)
		|| !(ps = ft_etat_word(&s, tlta->sh))
		|| !s)
		return (-1);
	;
	DEBUGSTR(s);
	free(s);
	free(ps);
	tlta->pline += 2;
	return (FLP_INTO + FLP_OUTTO);
}
