/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__antsl.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:34:08 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:34:08 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__antsl(t_linetoargv *tlta)
{
	DEBUGNBR(tlta->flags & FL_GRAVE);
	DEBUGC(tlta->pline[1]);
	if (!(ISETAT_CHECKCLOSE_IGN(tlta->flags))
		|| (tlta->flags & FL_DQUOT && ft_strchr("\\\"", tlta->pline[1]))
		|| tlta->flags & (FL_PAREN | FL_DOL_PARENTH)
		|| (tlta->flags & FL_GRAVE && tlta->pline[1] == '`'))
	{
		DEBUGC(*tlta->pline);
		DEBUGC(tlta->pline[1]);
		if (!tlta->pline[1] ||
			(tlta->pline[1] == '\n' && !tlta->pline[2]))
		{
			tlta->flags = FL_ANTSL;
			return (-1);
		}
		++tlta->pline;
		if (*tlta->pline)
			++tlta->pline;
		return (FLP_ANTSL);
	}
	return (FLP_NONE);
}
