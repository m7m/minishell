/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__recursion.h                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:27:16 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:27:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

/*
** Le retour de ft_etat_editionline_tlta n'est pas vérifié
*/

int
	ft_etat_chkclose__recursion(t_linetoargv *tlta)
{
	t_linetoargv	tlta_rec;

	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	ft_memset(&tlta_rec, 0, sizeof(tlta));
	ft_etat_editionline_tlta(
		tlta->line, tlta->pline, &tlta_rec, tlta->sh);
	DEBUGNBR(tlta_rec.flags);
	DEBUGSTR(tlta_rec.line);
	DEBUGSTR(tlta_rec.pline);
	tlta->line = tlta_rec.line;
	tlta->pline = tlta_rec.pline;
	return (FLP_RESTART);
}

int
	ft_etat_chkclose__recursion_lineflags(
		long flags, t_linetoargv *tlta)
{
	long			ret;
	t_linetoargv	tlta_rec;

	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	ft_memset(&tlta_rec, 0, sizeof(tlta));
	tlta_rec.flags = flags;
	ret = ft_etat_editionline_tlta(
		tlta->line, tlta->pline, &tlta_rec, tlta->sh);
	DEBUGNBRUL(ret);
	DEBUGNBR(tlta_rec.flags);
	DEBUGSTR(tlta_rec.line);
	DEBUGSTR(tlta_rec.pline);
	tlta->line = tlta_rec.line;
	tlta->pline = tlta_rec.pline;
	tlta->flags = tlta_rec.flags;
	if (ret == -1)
		return (-1);
	return (0);
}

/*
** line n'est plus free
*/

long
	ft_etat_chkclose__recursion_flags(
		char *line, char *pline, t_linetoargv *tlta)
{
	long			ret;
	t_linetoargv	tlta_rec;

	ft_memset(&tlta_rec, 0, sizeof(tlta));
	tlta_rec.flags = tlta->flags;
	DEBUGNBR(tlta_rec.flags);
	DEBUGSTR(tlta->line);
	DEBUGSTR(tlta->pline);
	ret = ft_etat_editionline_tlta(
		line, pline, &tlta_rec, tlta->sh);
	DEBUGSTR(line);
	DEBUGNBR(ret);
	DEBUGNBR(tlta_rec.flags);
	DEBUGSTR(tlta_rec.line);
	DEBUGSTR(tlta_rec.pline);
	tlta->flags = tlta_rec.flags;
	return (ret);
}
