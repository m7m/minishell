/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__if.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:23:13 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:23:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__if(t_linetoargv *tlta)
{
	return (ft_etat_chkclose__sh_commun_firts(FL_IF, FL_FI, 2, tlta));
}
