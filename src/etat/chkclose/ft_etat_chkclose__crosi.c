/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__crosi.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:34:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:34:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__crosi(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags)
		|| (tlta->pline > tlta->line
			&& !ft_strchr(" \n\t", *(tlta->pline - 1))))
		return (FLP_NONE);
	tlta->pline = ft_strchrnull(tlta->pline, '\n');
	if (*tlta->pline)
		++tlta->pline;
	return (FLP_CROSI);
}
