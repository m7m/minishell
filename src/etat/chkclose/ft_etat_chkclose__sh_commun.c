/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__sh_commun.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:23:03 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:23:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

/*
** Recherche du ; ou \n si non trouve alors erreur
*/

static int
	ft_etat____commun__checkprev(t_linetoargv *tlta)
{
	char	*s;

	DEBUG;
	s = ft_pass_space_rev(tlta->line, tlta->pline - 1);
	if (ft_strchr(";\n", *s))
		return (0);
	DEBUGSTR(s);
	return (1);
}

int
	ft_etat_chkclose__sh_commun(
		long flags_src,
		long flags_add,
		int size,
		t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	DEBUGNBRUL(tlta->flags);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	DEBUG;
	if (tlta->pline[size] && !ft_strchr(" \t\n&;", tlta->pline[size]))
		return (FLP_NONE);
	DEBUGNBRUL(tlta->flags);
	if (tlta->line == tlta->pline)
		return (-1);
	DEBUG;
	if (ft_etat____commun__checkprev(tlta))
	{
		if (tlta->flags & (flags_src))
			return (-1);
		return (FLP_NONE);
	}
	DEBUG;
	if (!(tlta->flags & (flags_src)))
		return (-1);
	tlta->flags = flags_add | (tlta->flags & ~(flags_src));
	tlta->pline += size;
	DEBUGSTR(tlta->pline);
	return (FLP_SHELLSCRIPT);
}
