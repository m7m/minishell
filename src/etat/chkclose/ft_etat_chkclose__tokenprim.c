/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__tokenprim.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:32:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:32:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

/*
** ?(if|then|else)
** Verifie si il y a un token de type shellscript sur la gauche
** exemple: -> 'if ;' == erreur
*/

int
	ft_etat_chkclose__tokenprim__prev(char *s, t_linetoargv *tlta)
{
	int	len;

	len = s - tlta->line;
	DEBUGNBR(len);
	if (len >= 1)
	{
		DEBUGSTR(tlta->line);
		DEBUGSTR(s);
		DEBUGSTR(s - 1);
		if (!ft_strncmp("if", s - 1, 2)
			&& (len == 1 || ft_strchr("\t ;&\n", *(s - 2))))
			return (-1);
		if (len >= 3)
		{
			DEBUGSTR(s - 3);
			if (((tlta->flags & FL_THEN && !ft_strncmp("then", s - 3, 4))
				|| (tlta->flags & FL_ELSE && !ft_strncmp("else", s - 3, 4)))
			&& (len == 3 || ft_strchr("\t ;&\n", *(s - 4))))
				return (-1);
		}
	}
	return (0);
}

int
	ft_etat_chkclose__tokenprim(t_linetoargv *tlta)
{
	char	*s;

	DEBUGSTR(tlta->pline);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags))
		return (FLP_NONE);
	s = tlta->pline - 1;
	DEBUG;
	if (s < tlta->line)
	{
		tlta->flags = 0;
		return (-1);
	}
	s = ft_pass_spacenewline_rev(tlta->line, s);
	DEBUGSTR(s);
	if (ft_strchr("&<>; \t", *s)
		|| ft_etat_chkclose__tokenprim__prev(s, tlta))
	{
		DEBUGSTR("erreur multi token");
		tlta->flags = 0;
		return (-1);
	}
	++tlta->pline;
	return (FLP_FG + FLP_SEMIC);
}
