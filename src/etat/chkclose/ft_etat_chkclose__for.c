/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__for.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 20:39:03 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/14 20:39:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

int
	ft_etat_chkclose__for(t_linetoargv *tlta)
{
	(void)tlta;
	return (FLP_NONE);
}

int
	ft_etat_chkclose__in(t_linetoargv *tlta)
{
	(void)tlta;
	return (FLP_NONE);
}
