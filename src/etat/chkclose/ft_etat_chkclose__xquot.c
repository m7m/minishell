/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__xquot.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:23:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:23:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

static int
	ft_etat_chkclose__quot(int flag, t_linetoargv *tlta)
{
	DEBUGSTR(tlta->pline);
	if (!(tlta->flags & (flag | FL_HERED)))
		tlta->flags |= flag;
	else if (tlta->flags & flag)
		tlta->flags &= ~flag;
	else
		return (FLP_NONE);
	++tlta->pline;
	DEBUGSTR(tlta->pline);
	DEBUGNBRUL(tlta->flags);
	return (0);
}

int
	ft_etat_chkclose__squot(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags & ~FL_SQUOT))
		return (FLP_NONE);
	if (ft_etat_chkclose__quot(FL_SQUOT, tlta) == FLP_NONE)
		return (FLP_NONE);
	return (FLP_SQUOT);
}

int
	ft_etat_chkclose__dquot(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags & ~(FL_DQUOT)))
		return (FLP_NONE);
	DEBUGNBRUL(FL_DOL_PARENTH & tlta->flags);
	DEBUGNBRUL(FL_DQUOT & tlta->flags);
	if (!(tlta->flags & (FL_DQUOT | FL_ANTSL)))
		tlta->flags |= FL_DQUOT;
	else if (tlta->flags & FL_DQUOT)
		tlta->flags &= ~FL_DQUOT;
	else
		return (FLP_NONE);
	++tlta->pline;
	return (FLP_DQUOT);
}

int
	ft_etat_chkclose__mquot(t_linetoargv *tlta)
{
	DEBUG;
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags & ~(FL_DQUOT | FL_GRAVE)))
		return (FLP_NONE);
	if (ft_etat_chkclose__quot(FL_GRAVE, tlta) == FLP_NONE)
		return (FLP_NONE);
	return (FLP_MQUOT);
}
