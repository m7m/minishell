/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__newline.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 05:33:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 05:33:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"
#include "../../../inc/t_sh.h"

/*
** Change si necessaire '\n' -> ';' ou ' '
*/

static void
	ft_etat_chkclose__newline__nltosp_(t_linetoargv *tlta)
{
	int		len;
	char	*s;

	s = ft_pass_spacenewline_rev(tlta->line, tlta->pline);
	len = s - tlta->line;
	DEBUGNBR(len);
	if (len >= 1)
	{
		if ((tlta->flags & FL_IF && !ft_strncmp("if", s - 1, 2))
			|| (len >= 3
			&& ((tlta->flags & FL_THEN && !ft_strncmp("then", s - 3, 4))
				|| (tlta->flags & FL_ELIF && !ft_strncmp("elif", s - 3, 4))
				|| (tlta->flags & FL_ELSE && !ft_strncmp("else", s - 3, 4)))))
			*tlta->pline = ' ';
		else if (*s != ';')
			*tlta->pline = ';';
	}
	++tlta->pline;
}

int
	ft_etat_chkclose__newline(t_linetoargv *tlta)
{
	DEBUG;
	++(tlta->sh->nb_line);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags)
		|| !(tlta->flags & (FL_IF | FL_ELIF | FL_ELSE | FL_THEN)))
		return (FLP_NONE);
	DEBUGNBR(tlta->flags & FL_HERED);
	ft_etat_chkclose__newline__nltosp_(tlta);
	return (FLP_NEWLI);
}
