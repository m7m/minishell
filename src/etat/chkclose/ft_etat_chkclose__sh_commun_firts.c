/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etat_chkclose__sh_commun_firts.c                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 17:51:44 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/12 17:51:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etat_chkclose.h"

/*
** 0 == not none
*/

static int
	ft_etat_chkclose__sh_commun_firts__none2(
		char *line, t_linetoargv *tlta)
{
	char	*s;

	if (tlta->line == line - 1)
		return (0);
	s = ft_pass_space_rev(tlta->line, line - 1);
	if (ft_strchr(";\n", *s))
		return (1);
	return (0);
}

static int
	ft_etat_chkclose__sh_commun_firts__none__recurs(
		int len, char *s, t_linetoargv *tlta)
{
	DEBUGNBR(len);
	if ((len == 1
	|| (ft_strchr(" \t\n;&", *(s - 2)) && ft_strchr(" \t\n", s[1])))
		&& !ft_strncmp("if", s - 1, 2))
		return (ft_etat_chkclose__sh_commun_firts__none(s, tlta));
	if (len > 4
		&& ft_strchr(" \t\n;&", *(s - 4))
		&& ft_strchr(" \t\n`", s[1]))
		if (!ft_strncmp("then", s - 3, 4)
			|| !ft_strncmp("elif", s - 3, 4)
			|| !ft_strncmp("else", s - 3, 4))
			return (ft_etat_chkclose__sh_commun_firts__none2(s - 2, tlta));
	return (1);
}

int
	ft_etat_chkclose__sh_commun_firts__none(
		char *line, t_linetoargv *tlta)
{
	int		len;
	char	*s;

	DEBUGSTR(tlta->line);
	DEBUGSTR(line);
	if (tlta->line >= line - 1)
		return (0);
	if (!ft_strchr(" \n\t;&", *(line - 1)))
		return (1);
	s = ft_pass_space_rev(tlta->line, line - 1);
	if (ft_strchr("&;\n|", *s))
		return (0);
	len = s - tlta->line;
	DEBUGSTR(s);
	if (len > 0)
		return (ft_etat_chkclose__sh_commun_firts__none__recurs(len, s, tlta));
	DEBUG;
	return (1);
}

int
	ft_etat_chkclose__sh_commun_firts(
		long flags_src,
		long flags_end,
		int size,
		t_linetoargv *tlta)
{
	long	flags;

	DEBUGSTR(tlta->pline);
	if (ISETAT_CHECKCLOSE_IGN(tlta->flags)
		|| !ft_strchr(" \t\n", tlta->pline[size])
		|| ft_etat_chkclose__sh_commun_firts__none(tlta->pline, tlta))
		return (FLP_NONE);
	DEBUG;
	tlta->pline += 2;
	flags = tlta->flags;
	if (ft_etat_chkclose__recursion_lineflags(flags_src, tlta))
		return (-1);
	DEBUG;
	if (!(tlta->flags & flags_end))
	{
		DEBUGSTR("erreur remonté");
		if (*tlta->pline)
		{
			DEBUGSTR("erreur syntax");
			tlta->flags = 0;
		}
		return (-1);
	}
	tlta->flags = flags;
	return (FLP_SHELLSCRIPT);
}
