/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_replace_var.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 14:12:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/26 14:12:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "../inc/ft_surrogate.h"

/*
** Retourne un pointeur sur la fin de la derniere substitution
*/

char
	*ft_replace_var(char **str)
{
	char	*pstr;
	char	*substitu;

	DEBUG;
	pstr = *str;
	if (**str == '~')
	{
		pstr = ft_surrogate__tilde(str);
		substitu = pstr;
	}
	else
		substitu = NULL;
	while (*pstr)
	{
		if (*pstr == '$')
		{
			pstr = ft_surrogate__dol(str, pstr, 0);
			if (**str == '~')
				pstr = ft_surrogate__tilde(str);
			substitu = pstr;
		}
		else
			++pstr;
	}
	return (substitu);
}

char
	*ft_replace_var_notnull(char **str)
{
	char	*pstr;
	char	*substitu;

	DEBUG;
	pstr = *str;
	if (**str == '~')
	{
		pstr = ft_surrogate__tilde(str);
		substitu = pstr;
	}
	else
		substitu = NULL;
	while (*pstr)
	{
		if (*pstr == '$')
		{
			pstr = ft_surrogate__dol(str, pstr, 1);
			substitu = pstr;
		}
		else
			++pstr;
	}
	return (substitu);
}
