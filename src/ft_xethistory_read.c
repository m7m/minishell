/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xethistory_read.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/26 00:54:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/26 00:54:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "const_term.h"
#include "pr_history.h"
#include "libft.h"
#include "get_next_line.h"
#include "pr_xetlocal.h"
#include <sys/stat.h>
#include <fcntl.h>

static char
	*ft_history_read__join(char *line, char *linenl)
{
	char	*tmp;

	DEBUGSTR(linenl);
	DEBUGSTR(line);
	ft_memchk_exit(tmp = ft_strjoin(linenl, line));
	free(linenl);
	free(line);
	return (tmp);
}

static char
	*ft_history_read__multinl(char *line, char *linenl)
{
	int		len;

	len = ft_strlen(line) - 1;
	DEBUGSTR(line);
	if (line[len] == '\\')
	{
		line[len] = '\n';
		if (!linenl)
			linenl = line;
		else
			linenl = ft_history_read__join(line, linenl);
		line = NULL;
	}
	else if (linenl)
	{
		line = ft_history_read__join(line, linenl);
		linenl = NULL;
	}
	if (line)
		ft_history_add(line, 0);
	free(line);
	return (linenl);
}

void
	ft_history_read(void)
{
	int			fd;
	struct stat	ststat;
	char		*linenl;
	char		*line;

	if (ft_getlocal(CONST_HISTFILE, &line)
		&& !stat(line, &ststat) && (fd = open(line, O_RDONLY)) > -1)
	{
		line = NULL;
		linenl = NULL;
		while (get_next_line(fd, &line) == 1)
		{
			DEBUG;
			if (*line)
				linenl = ft_history_read__multinl(line, linenl);
			else
				free(line);
		}
		close(fd);
		if (linenl)
		{
			ft_history_add(linenl, 0);
			free(linenl);
		}
	}
}
