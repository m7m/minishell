/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork__child.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/18 00:24:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/18 00:24:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"

static void
	ft_fork__child_intern(t_sh *sh, t_argv *targv)
{
	if (targv->argc)
		targv->builtins(sh, targv);
	ft_fork__child_exit(EXIT_SUCCESS, 0, targv);
}

void
	ft_fork__child(t_sh *sh, t_argv *targv)
{
	int	ret;

	DEBUGSTR("je suis child");
	DEBUGSTR(targv->pathbin);
	if ((ret = ft_fork__child_open(targv->tfd, targv, sh)) == 1)
		ft_fork__child_exit(EXIT_SUCCESS, 0, targv);
	else if (ret)
		ft_fork__child_exit(EXIT_FAILURE, 0, targv);
	else if (targv->builtins || !targv->argc)
		ft_fork__child_intern(sh, targv);
	else
	{
		DEBUGSTR(targv->pathbin);
		DEBUGSTR("execve");
		execve(targv->pathbin, targv->argv, targv->envp);
	}
	ft_fork__child_close(targv);
	DEBUGSTR("child close");
	ft_fork__child_exit(EXIT_FAILURE, "Bad executable", targv);
}
