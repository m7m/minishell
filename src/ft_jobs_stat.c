/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_jobs_stat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 10:08:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/23 10:08:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

#ifdef DEBUG_LVL

/*
** Pas d'allocation lors d'une interception d'un signal
*/

# if DEBUG_LVL != 2
#  undef DEBUG_LVL
#  define DEBUG_LVL 2
# endif
#endif

#include "g_jobs.h"
#include <signal.h>
#include <sys/wait.h>
#include "../inc/jobs.h"
#include "../inc/l_sh.h"
#include "../inc/flags.h"
#include "../inc/built_in_jobs.h"

/*
** Actualise les processus en font de tache
*/

static void
	ft_jobs_stat__print(int i, t_builtins_job tjobs, int *sh_opt)
{
	char	**argv;

	DEBUG;
	if (*sh_opt & SH_TTY)
	{
		ft_printf("Job: [%d] %d\tDone\tcmd: ", i, tjobs.pc);
		argv = tjobs.argv;
		DEBUG;
		if (argv)
			while (*argv)
			{
				ft_printf(" %s", *argv);
				++argv;
			}
		ft_printf("\n");
		ft_fflush(STDOUT_FILENO);
		DEBUG;
	}
}

static void
	ft_jobs_stat__upd(int i, t_builtins_jobs *tljobs)
{
	DEBUGNBR(tljobs->tjobs[i].pc);
	if (tljobs->tjobs[i].status == JOB_TERMINATED
		|| tljobs->tjobs[i].status == JOB_PENDING_DELETE)
	{
		DEBUGNBR(tljobs->tjobs[i].status == JOB_TERMINATED);
		if (tljobs->tjobs[i].status == JOB_TERMINATED)
			ft_jobs_stat__print(i, tljobs->tjobs[i], tljobs->sh_opt);
		free(tljobs->tjobs[i].argv);
		tljobs->tjobs[i].argv = NULL;
		tljobs->tjobs[i].pc = 0;
		tljobs->tjobs[i].status = 0;
	}
}

void
	ft_jobs_stat(t_builtins_jobs *tljobs)
{
	int	i;

	DEBUG;
	if (tljobs->tjobs)
	{
		g_tljobs = tljobs;
		i = 0;
		DEBUGPTR(tljobs->tjobs);
		while (i < tljobs->max_index && i < JOBS_MAX)
		{
			++i;
			if (tljobs->tjobs[i].pc > 0)
			{
				ft_jobs_stat_actu(tljobs->tjobs[i].pc, -1, -1);
				ft_jobs_stat__upd(i, tljobs);
			}
		}
		if (i < JOBS_MAX && i == tljobs->max_index)
		{
			while (tljobs->max_index && !tljobs->tjobs[tljobs->max_index].pc)
				--tljobs->max_index;
		}
	}
	DEBUG;
}

static void
	ft_jobs_stat_actu__check(t_builtins_job *tjobs)
{
	int	s;
	int	w;

	s = 0;
	DEBUGNBR(tjobs->pc);
	w = waitpid(tjobs->pc, &s, WNOHANG | WUNTRACED);
	DEBUGNBR(w);
	DEBUGNBR(s);
	DEBUGNBR(WIFEXITED(s));
	DEBUGNBR(WIFSIGNALED(s));
	if (w == -1
	|| (w && (WIFEXITED(s) || (WIFSIGNALED(s) && !WIFSTOPPED(s)))))
	{
		DEBUG;
		if (tjobs->status == JOB_PENDING)
		{
			DEBUG;
			tjobs->status = JOB_PENDING_DELETE;
		}
		else
		{
			DEBUG;
			tjobs->status = JOB_TERMINATED;
		}
	}
}

void
	ft_jobs_stat_actu(pid_t pc, int status, int sig_value)
{
	int				i;
	t_builtins_job	*tjobs;

	DEBUGNBR(pc);
	if (!g_tljobs || !g_tljobs->tjobs || !pc)
		return ;
	tjobs = g_tljobs->tjobs;
	DEBUGNBR(status);
	i = 0;
	while (i < JOBS_MAX && tjobs[i].pc != pc)
		++i;
	if (i < JOBS_MAX && tjobs[i].pc > -1)
	{
		DEBUGNBR(tjobs[i].pc);
		if (tjobs[i].status != JOB_TERMINATED
			&& tjobs[i].status != JOB_PENDING_DELETE)
			ft_jobs_stat_actu__check(tjobs + i);
		tjobs[i].sig_status = status;
		if (sig_value)
			tjobs[i].sig_status = sig_value + 128;
	}
	DEBUGNBR(i);
}
