/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork__child_open.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/18 06:56:31 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/18 06:56:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"

static int
	ft_fork__child_open_xto_dup(t_fd *t, int fd_read, int fd_write, t_sh *sh)
{
	int	fd;

	DEBUGNBR(t->pipe[fd_read]);
	DEBUGNBR(t->pipe[fd_write]);
	if (t->opt & SFD_ERRTOOUT)
	{
		if (ft_fork__child_open_xto_open(t, fd_read, fd_write, sh))
			return (-1);
		t->pipe[fd_read] = STDERR_FILENO;
		DEBUGNBR(t->pipe[fd_read]);
		DEBUGNBR(t->pipe[fd_write]);
	}
	if ((fd = dup(t->pipe[fd_write])) == -1)
		return (ft_perror_dup(t->pipe[fd_write], "Bad file descriptor"));
	if (dup2(fd, t->pipe[fd_read]) == -1)
		return (ft_perror_dup(fd, "dup2() file descriptor"));
	close(fd);
	return (0);
}

static int
	ft_fork__child_open_xto(t_fd *t, int fd_read, int fd_write, t_sh *sh)
{
	DEBUGNBR(t->pipe[0]);
	DEBUGNBR(t->pipe[1]);
	if (t->opt & SFD_OPEN)
		return (ft_fork__child_open_xto_open(t, fd_read, fd_write, sh));
	else if (t->opt & (SFD_DUP | SFD_ERRTOOUT))
		return (ft_fork__child_open_xto_dup(t, fd_read, fd_write, sh));
	else if (t->opt & SFD_CLOSE)
	{
		DEBUGNBR(t->pipe[fd_read]);
		close(t->pipe[fd_read]);
	}
	return (0);
}

int
	ft_fork__child_open(t_fd *t, t_argv *targv, t_sh *sh)
{
	int		ret;

	if (!t)
		return (0);
	if (ft_fork__child_open(t->next, targv, sh))
		return (-1);
	if (t->opt & SFD_REDI_OUT)
		ret = ft_fork__child_open_xto(t, 0, 1, sh);
	else if (t->opt & SFD_REDI_IN)
		ret = ft_fork__child_open_xto(t, 1, 0, sh);
	else if (t->opt & (SFD_HERE_DOC | SFD_HERE_DOC_TAB | SFD_HERE_LIN))
		ret = ft_fork__child_open_here(t, targv, sh);
	else
	{
		DEBUGSTR("test: opt de tfd invalide\n");
		return (-1);
	}
	return (ret);
}
