/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read__optinit.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 10:55:57 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/08 14:26:12 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "../inc/const_local.h"

int
	ft_read_usage(char *opt, char *err)
{
	MSG_ERR4("read", opt, err);
	ft_putstr_fd("read: usage: read [-rs] [-d delim] ", 2);
	ft_putstr_fd("[-n nchars] [-N nchars] [-p prompt] ", 2);
	ft_putendl_fd("[-t timeout] [-u fd] [name ...]", 2);
	return (BUILTIN_FAILURE);
}

static void
	ft_add_opt(t_opt_read *opt, char name,
	int (*f)(t_read *read, char **argv, int *i, int j))
{
	opt->f = f;
	opt->opt = name;
}

void
	ft_init_tread(t_read *read)
{
	ft_bzero(read, sizeof(t_read));
	read->time = -1;
	read->delim = '\n';
	read->bign = -1;
	read->max_char = -1;
	read->fd = STDIN_FILENO;
}

void
	ft_init_sep(t_read *read)
{
	char	*sep;

	sep = NULL;
	if (ft_getlocal("IFS", &sep) > 0)
		read->sep = ft_strdup(sep);
	else
		read->sep = ft_strdup(IFS);
}

void
	ft_init_opt(t_opt_read *opt)
{
	ft_add_opt(opt++, 'd', ft_builtins_read__opt_d);
	ft_add_opt(opt++, 'n', ft_builtins_read__opt_n);
	ft_add_opt(opt++, 'p', ft_builtins_read__opt_p);
	ft_add_opt(opt++, 'r', ft_builtins_read__opt_r);
	ft_add_opt(opt++, 's', ft_builtins_read__opt_s);
	ft_add_opt(opt++, 't', ft_builtins_read__opt_t);
	ft_add_opt(opt++, 'u', ft_builtins_read__opt_u);
	ft_add_opt(opt++, 'N', ft_builtins_read__opt_bign);
}
