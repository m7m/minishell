/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_targv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 11:52:56 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/03 11:52:56 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "t_struct.h"
#include "../inc/tfd.h"
#include "../inc/l_sh.h"

t_argv
	*ft_targv_newadd(t_argv **node_targv)
{
	t_argv	*new;

	if (!(new = (t_argv *)ft_memalloc(sizeof(t_argv)))
		|| !(new->argv = (char **)malloc(sizeof(char *) * 1)))
	{
		free(new);
		return (NULL);
	}
	*new->argv = 0;
	if (*node_targv)
		ft_exit(EXIT_FAILURE, "ft_targv_newadd: targv: n'est plus une liste\n");
	*node_targv = new;
	return (new);
}

void
	ft_targv_freeone(t_argv **targv)
{
	t_argv	*node;

	if (*targv)
	{
		node = *targv;
		ft_tfd_free(node->tfd);
		ft_tabfree_size(&node->argv, node->argc);
		free(node->envp);
		free(node);
		*targv = 0;
	}
}

void
	ft_targv_free(t_argv *targv)
{
	ft_targv_freeone(&targv);
}
