/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xetenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:45:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:45:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_hash.h"
#include "pr_wchar_t.h"

t_htable
	*ft__static_henvp(t_htable *henvp)
{
	static t_htable	*static_henvp = 0;

	if (henvp)
		static_henvp = henvp;
	return (static_henvp);
}

/*
** Chercher key dans henvp
** Si *data = NULL = len = 0 = non trouvé
** La taille n'inclus pas le \0
*/

size_t
	ft_getenvp(char *key, char **data)
{
	size_t		len;
	t_htable	henvp;

	DEBUG;
	henvp = *ft__static_henvp(NULL);
	len = ft_hchr(henvp, key, ft_strlen(key), (void **)data);
	if (len && data && !data[0][len - 1])
		--len;
	return (len);
}

size_t
	ft_getnenvp(size_t nkey, char *key, char **data)
{
	size_t		len;
	t_htable	henvp;

	DEBUGNBR(nkey);
	DEBUGSTR(key);
	henvp = *ft__static_henvp(NULL);
	len = ft_hchr(henvp, key, nkey, (void **)data);
	DEBUGNBR(len);
	DEBUGSTR(*data);
	if (len)
		len = ft_mbslen(*data);
	return (len);
}

void
	ft_setenvp(char *key, char *data)
{
	t_htable	*henvp;

	DEBUG;
	henvp = ft__static_henvp(NULL);
	if ((data = ft_hupd_nosize(henvp, key, data)))
		free(data);
}
