/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read__opt_ru.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 20:20:08 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/11 10:18:46 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_read__opt_r(t_read *read, char **argv, int *i, int j)
{
	read->r = 1;
	if (argv[*i][j + 1])
		return (-1);
	return (0);
}

int
	ft_builtins_read__opt_s(t_read *read, char **argv, int *i, int j)
{
	read->silent = 1;
	if (argv[*i][j + 1])
		return (-1);
	return (0);
}

int
	ft_builtins_read__opt_t(t_read *read, char **argv, int *i, int j)
{
	long	sec;
	char	*tmp;

	if (argv[*i][j + 1])
		tmp = &(argv[*i][j + 1]);
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-t", "option requires an argument"));
		tmp = argv[++(*i)];
	}
	if (!ft_islong(tmp))
	{
		MSG_ERR4("read", tmp, "invalid timeout specification");
		return (BUILTIN_FALSE);
	}
	sec = ft_atol(tmp);
	if (sec < 0)
	{
		MSG_ERR4("read", tmp, "invalid timeout specification");
		return (BUILTIN_FALSE);
	}
	read->time = (sec > 99999999) ? 99999999 : sec;
	return (0);
}

int
	ft_builtins_read__opt_u(t_read *read, char **argv, int *i, int j)
{
	int		fd;
	char	*tmp;

	if (argv[*i][j + 1])
		tmp = &(argv[*i][j + 1]);
	else
	{
		if (!argv[*i + 1])
			return (ft_read_usage("-u", "option requires an argument"));
		tmp = argv[++(*i)];
	}
	if (!ft_isint(tmp))
	{
		MSG_ERR4("read", tmp, "invalid file descriptor specification");
		return (BUILTIN_FALSE);
	}
	fd = ft_atoi(tmp);
	if (fcntl(fd, F_GETFL) == -1)
	{
		MSG_ERR4("read", tmp, "invalid file descriptor: Bad file descriptor");
		return (BUILTIN_FALSE);
	}
	read->fd = fd;
	return (0);
}
