/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isvar.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 17:52:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/27 17:52:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Verifie si 'v' represente un nom de variable valide
** debutant par $
** si valide retourne 1 sinon 0
*/

int
	ft_isvar(char *v)
{
	int	notvar;

	DEBUG;
	notvar = (*v == '$');
	while (notvar && v[notvar])
	{
		if (!ft_isalnum(v[notvar])
			&& v[notvar] != '-' && v[notvar] != '_')
			notvar = 0;
		else
			++notvar;
	}
	return (notvar);
}
