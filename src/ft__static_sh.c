/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft__static_sh.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 00:47:58 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/15 00:47:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

struct s_sh
	*ft__static_sh(struct s_sh *sh)
{
	static struct s_sh	*st_sh = NULL;

	if (sh)
		st_sh = sh;
	return (st_sh);
}
