/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_rigth_str.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:33:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:33:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Envoie tcaps->line a f
**  est positionne le curseur en rapport avec la valeur retourné de f
** f doit renvoyer un pointeur sur tcaps->line
*/

void
	ft_termcaps_rigth_wcs(t_termcaps *tcaps, wchar_t *(*f)(wchar_t *))
{
	int		diff_x;
	int		diff_y;
	int		relat_x;
	wchar_t	*r;

	relat_x = tcaps->pos_x + *tcaps->nquote;
	r = f((*tcaps->line) + relat_x);
	diff_x = r - ((*tcaps->line) + relat_x);
	diff_y = ft_termcaps_rel_y4(tcaps);
	tcaps->pos_x += diff_x;
	tcaps->abs_x = ft_termcaps_rel_x(tcaps);
	DEBUGNBR(tcaps->abs_x);
	tcaps->abs_y += ft_termcaps_rel_y4(tcaps) - diff_y;
	DEBUGNBR(tcaps->abs_y);
	DEBUGNBR(tcaps->abs_x);
	ft_termcaps_setpos(tcaps->abs_x, tcaps->abs_y, tcaps->abs_y);
}
