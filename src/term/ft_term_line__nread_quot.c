/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line__nread_quot.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 03:45:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/02 03:45:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "minishell.h"
#include "const_term.h"

static void
	ft__tl_bcl_nread_quot_if_s(char *s, t_term_line *tline)
{
	tline->tcaps.home = s;
	tline->tcaps.ppos_x = ft_strlen(tline->tcaps.home);
}

static void
	ft__tl_bcl_nread_quot_if__2(t_term_line *tline)
{
	if (tline->flags & FL_DOL_PARENTH)
		ft__tl_bcl_nread_quot_if_s("$( > ", tline);
	else if (tline->flags & FL_ACCOL)
		ft__tl_bcl_nread_quot_if_s("{ > ", tline);
	else if (tline->flags == FL_IF)
		ft__tl_bcl_nread_quot_if_s("if > ", tline);
	else if (tline->flags == FL_ELSE)
		ft__tl_bcl_nread_quot_if_s("else > ", tline);
	else if (tline->flags == FL_ELIF)
		ft__tl_bcl_nread_quot_if_s("elif > ", tline);
	else if (tline->flags == FL_THEN)
		ft__tl_bcl_nread_quot_if_s("then > ", tline);
	else
		ft__tl_bcl_nread_quot_if_s("> ", tline);
}

static void
	ft__tl_bcl_nread_quot_if(t_term_line *tline)
{
	if (tline->flags & FL_DQUOT)
		ft__tl_bcl_nread_quot_if_s("\" > ", tline);
	else if (tline->flags & FL_SQUOT)
		ft__tl_bcl_nread_quot_if_s("' > ", tline);
	else if (tline->flags & FL_GRAVE)
		ft__tl_bcl_nread_quot_if_s("` > ", tline);
	else if (tline->flags & FL_OR)
		ft__tl_bcl_nread_quot_if_s("|| > ", tline);
	else if (tline->flags & FL_AND)
		ft__tl_bcl_nread_quot_if_s("&& > ", tline);
	else if (tline->flags & FL_PIPE)
		ft__tl_bcl_nread_quot_if_s("| > ", tline);
	else if (tline->flags & FL_REDIREC)
		ft__tl_bcl_nread_quot_if_s("<> > ", tline);
	else if (tline->flags & FL_HERED)
		ft__tl_bcl_nread_quot_if_s("<< > ", tline);
	else if (tline->flags & FL_ANTSL)
		ft__tl_bcl_nread_quot_if_s("\\\\n > ", tline);
	else if (tline->flags & FL_PAREN)
		ft__tl_bcl_nread_quot_if_s("( > ", tline);
	else
		ft__tl_bcl_nread_quot_if__2(tline);
}

static void
	ft__tl_bcl_nread_quot_calcnquot(t_term_line *tline)
{
	if (tline->flags & FL_DEL_SPACE)
	{
		DEBUGWCS(tline->line);
		if (tline->flags & FL_ANTSL)
		{
			--tline->len;
			DEBUGWC(tline->line[tline->len]);
			tline->line[tline->len] = 0;
			tline->nquote = tline->len;
			DEBUGNBR(tline->len);
		}
		else if (tline->line[tline->len] == L'\n')
		{
			tline->line[tline->len] = L' ';
			tline->nquote = tline->len + 1;
		}
		DEBUGWCS(tline->line);
	}
	else
		tline->nquote = tline->len + 1;
}

void
	ft__tl_bcl_nread_quot(t_term_line *tline)
{
	int	abs_y;

	DEBUGNBR(tline->flags);
	tline->tcaps.home = NULL;
	ft__tl_bcl_nread_quot_if(tline);
	ft__tl_bcl_nread_quot_calcnquot(tline);
	DEBUG;
	tline->tcaps.pos_x = 0;
	if (ft_termcaps_pos(STDIN_FILENO, NULL, &abs_y))
		return ;
	ft_termcaps_setpos(1, abs_y, 0);
	ft_putstr(tline->tcaps.home);
	DEBUG;
}
