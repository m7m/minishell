/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__init_twstt_nof.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 22:29:13 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/29 22:29:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"

static void
	ft__twstt_nof(struct s_wsttermcaps_nof *twstt,
			char *c, char *ctput, int f)
{
	twstt->c = c;
	DEBUGSTR(c);
	twstt->c_len = ft_strlen(c);
	twstt->ctput = ctput;
	twstt->f = f;
}

static struct s_wsttermcaps_nof
	*ft___init_twstt_nof_arrow(struct s_wsttermcaps_nof twstt[])
{
	ft__twstt_nof(++twstt, ARROW_U, 0, FL_ARROW_U);
	ft__twstt_nof(++twstt, ARROW_D, 0, FL_ARROW_D);
	ft__twstt_nof(++twstt, ARROW_R, 0, FL_ARROW_R);
	ft__twstt_nof(++twstt, ARROW_L, 0, FL_ARROW_L);
	ft__twstt_nof(++twstt, M_ARROW_R, 0, FL_C_ARROW_R);
	ft__twstt_nof(++twstt, M_ARROW_L, 0, FL_C_ARROW_L);
	ft__twstt_nof(++twstt, C_ARROW_U, 0, FL_C_ARROW_U);
	ft__twstt_nof(++twstt, C_ARROW_D, 0, FL_C_ARROW_D);
	ft__twstt_nof(++twstt, C_ARROW_R, 0, FL_C_ARROW_R);
	ft__twstt_nof(++twstt, C_ARROW_L, 0, FL_C_ARROW_L);
	return (twstt);
}

void
	ft_termcaps__init_twstt_nof(struct s_wsttermcaps_nof twstt[])
{
	ft__twstt_nof(twstt, BACKSPACE, 0, FL_BACKSPACE);
	ft__twstt_nof(++twstt, DEL, 0, FL_DEL);
	ft__twstt_nof(++twstt, END, 0, FL_CTRL_E);
	ft__twstt_nof(++twstt, HORIZ_TAB, 0, FL_HORIZ_TAB);
	ft__twstt_nof(++twstt, CTRL_A, 0, FL_CTRL_A);
	ft__twstt_nof(++twstt, CTRL_C, 0, FL_CTRL_C);
	ft__twstt_nof(++twstt, CTRL_D, 0, FL_CTRL_D);
	ft__twstt_nof(++twstt, CTRL_E, 0, FL_CTRL_E);
	ft__twstt_nof(++twstt, CTRL_K, 0, FL_CTRL_K);
	ft__twstt_nof(++twstt, CTRL_L, 0, FL_CTRL_L);
	ft__twstt_nof(++twstt, CTRL_R, 0, FL_CTRL_R);
	ft__twstt_nof(++twstt, CTRL_U, 0, FL_CTRL_U);
	ft__twstt_nof(++twstt, CTRL_V, 0, FL_CTRL_V);
	ft__twstt_nof(++twstt, CTRL_W, 0, FL_CTRL_W);
	ft__twstt_nof(++twstt, CTRL_Y, 0, FL_CTRL_Y);
	twstt = ft___init_twstt_nof_arrow(twstt);
	ft__twstt_nof(++twstt, META_B, 0, FL_C_ARROW_L);
	ft__twstt_nof(++twstt, META_F, 0, FL_C_ARROW_R);
	ft__twstt_nof(++twstt, ALT_BACKSP, 0, FL_ALT_BACKSP);
	ft__twstt_nof(++twstt, ALT_D, 0, FL_ALT_D);
	ft__twstt_nof(++twstt, ALT_L, 0, FL_ALT_L);
	ft__twstt_nof(++twstt, ALT_U, 0, FL_ALT_U);
	ft__twstt_nof(++twstt, 0, 0, 0);
}
