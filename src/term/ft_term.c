/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/22 11:14:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/22 11:14:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "sh_term.h"

static struct s_savterm
	ft__static_term(struct s_savterm ssavterm, int stock)
{
	static struct s_savterm	static_savterm;

	if (stock)
	{
		static_savterm.fd = ssavterm.fd;
		static_savterm.sterm = ssavterm.sterm;
	}
	return (static_savterm);
}

void
	ft_term_restore(void)
{
	struct s_savterm	ssavterm;

	DEBUG;
	ssavterm.fd = STDIN_FILENO;
	ssavterm = ft__static_term(ssavterm, STDIN_FILENO);
	tcsetattr(ssavterm.fd, TCSAFLUSH, &ssavterm.sterm);
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
}

static int
	ft_term__fd(int fd)
{
	pid_t	p;

	p = getpid();
	DEBUGNBR(p);
	setpgid(p, p);
	tcsetpgrp(STDIN_FILENO, p);
	DEBUGNBR(isatty(fd));
	if (!isatty(fd))
	{
		DEBUGSTR("open new STDIN_FILENO");
		if ((fd = open("/dev/tty", O_RDWR)) < 0)
			ft_exit(EXIT_FAILURE, "Error: Term open failed\n");
		if (!isatty(fd))
			ft_exit(EXIT_FAILURE, "Error: new STDIN_FILENO not tty\n");
		if (dup2(fd, STDIN_FILENO) == -1)
			ft_exit(EXIT_FAILURE, "Error: duplicate filedescriptor\n");
		close(fd);
	}
	return (STDIN_FILENO);
}

void
	ft_term_reset(void)
{
	struct s_savterm	ssavterm;
	int					fd;

	DEBUG;
	fd = ft_term__fd(STDIN_FILENO);
	DEBUG;
	ssavterm.fd = 0;
	ssavterm = ft__static_term(ssavterm, 0);
	ft_term_init(ssavterm.sterm, fd);
}

/*
** open fd  | O_NDELAY | O_NONBLOCK | O_NOCTTY
*/

void
	ft_term(t_sh *sh)
{
	struct termios		sterm;
	struct s_savterm	ssavterm;
	int					fd;

	DEBUG;
	fd = ft_term__fd(STDIN_FILENO);
	DEBUG;
	if ((tcgetattr(fd, &sterm) < 0))
		ft_exit(EXIT_FAILURE, "Error: Not getattr tty\n");
	ssavterm.fd = fd;
	ssavterm.sterm = sterm;
	ft__static_term(ssavterm, 1);
	ft_atexit(ft_term_restore);
	sh->fd = fd;
	sh->sterm = sterm;
	ft_term_init(sterm, fd);
}
