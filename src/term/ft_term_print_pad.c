/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_print_pad.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:29:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:29:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "t_struct.h"
#include "sh_term.h"

/*
** Version basic
*/

static size_t
	ft__lenmax(char **tab)
{
	size_t	len_max;
	size_t	len;

	len_max = 0;
	while (*tab)
	{
		len = ft_strlen(*tab);
		if (len > len_max)
			len_max = len;
		++tab;
	}
	return (len_max);
}

static void
	ft_term_print_pad__w2(size_t i, char *pbuff, struct s_term_print_pad *ttpp)
{
	size_t	len;

	DEBUG;
	while (i < ttpp->itab)
	{
		DEBUGNBR(ttpp->itab);
		DEBUGNBR(i);
		DEBUGSTR(ttpp->tab[i]);
		len = ft_strlen(ttpp->tab[i]);
		ft_memcpy(pbuff, ttpp->tab[i], len);
		DEBUG;
		pbuff += ttpp->len_max;
		i += (ttpp->itab + (ttpp->itab % ttpp->nb_col)) / ttpp->nb_col + 1;
		DEBUGNBR(i);
		++ttpp->y;
	}
	if (ttpp->len_max >= len)
		ft_strcpy(pbuff - (ttpp->len_max - len), "\n");
}

static void
	ft_term_print_pad__w(size_t co, char *buff, struct s_term_print_pad ttpp)
{
	size_t	nline;

	DEBUG;
	nline = 0;
	while (ttpp.y < ttpp.itab)
	{
		DEBUGSTR(*ttpp.tab);
		ft_memset(buff, ' ', co);
		ft_term_print_pad__w2(nline, buff, &ttpp);
		DEBUGSTR(buff);
		ft_putstr(buff);
		++nline;
	}
}

void
	ft_term_print_pad(size_t itab, char **tab, t_scap cap)
{
	size_t					co;
	struct s_term_print_pad	ttpp;
	char					*buff;

	DEBUGNBR(itab);
	ttpp.len_max = ft__lenmax(tab) + 2;
	co = ft_termcaps_co();
	DEBUGNBR(ttpp.len_max);
	ttpp.nb_col = (co) / (ttpp.len_max);
	DEBUGNBR((ttpp.len_max) * itab);
	DEBUGNBR(ttpp.nb_col);
	ft_memchk_exit(buff = (char *)malloc(SC(co + 2)));
	buff[co] = 0;
	DEBUG;
	ttpp.itab = itab;
	ttpp.tab = tab;
	ttpp.y = 0;
	FT_TPUTS(cap.sf, 1);
	ft_term_print_pad__w(co, buff, ttpp);
	DEBUGSTR(buff);
	free(buff);
}
