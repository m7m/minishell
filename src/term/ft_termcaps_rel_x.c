/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_rel_x.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 12:13:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/15 12:13:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "pr_wchar_t.h"
#include <stddef.h>

static int
	ft_termcaps_rel_x__f2(int x, int t, int co)
{
	DEBUGNBR(t);
	t *= TAB_SIZE;
	x += t;
	DEBUGNBR(x);
	if (!(x %= co))
		x = co;
	DEBUGNBR(x);
	return (x);
}

static int
	ft_termcaps_rel_x__f1(wchar_t *line, wchar_t *end_line, wchar_t *pend_line,
						t_termcaps *tcaps)
{
	int	t;
	int	x;

	if (line)
		while (pend_line > line && *(--pend_line) != L'\n')
			;
	if (line == pend_line)
	{
		x = tcaps->ppos_x + tcaps->pos_x + 1;
		t = 0;
		if (line)
			t = ft_wcsncount(pend_line, '\t', tcaps->pos_x);
	}
	else
	{
		x = (end_line - pend_line);
		DEBUGNBR(x);
		t = ft_wcsncount(pend_line, '\t', x);
	}
	return (ft_termcaps_rel_x__f2(x, t, tcaps->co));
}

/*
** Retourne la valeur relative de x par rapport à la ligne
*/

int
	ft_termcaps_rel_x(t_termcaps *tcaps)
{
	wchar_t	*line;
	wchar_t	*end_line;
	wchar_t	*pend_line;

	DEBUGNBR(tcaps->pos_x);
	line = *tcaps->line + *tcaps->nquote;
	end_line = *tcaps->line + *tcaps->nquote + tcaps->pos_x;
	pend_line = end_line;
	return (ft_termcaps_rel_x__f1(line, end_line, pend_line, tcaps));
}
