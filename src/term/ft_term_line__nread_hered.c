/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line__nread_hered.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 02:55:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/02 02:55:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "pr_wchar_t.h"
#include "minishell.h"

static int
	ft____hered_err(wchar_t *start, int n,
					wchar_t **buff, t_term_line *tline)
{
	DEBUG;
	tline->flags = 0;
	if (*start == '<')
		return (0);
	DEBUG;
	ft__tl_bcl_nread_join(n, buff, tline);
	ft_termcaps__ctrl_e(&tline->tcaps);
	ft_term_line_refresh(n, &tline->tcaps, tline->line + tline->nquote);
	FT_TPUTS(tline->tcaps.cap.sf, 1);
	MSG_ERR3("<<", "bad caractere after");
	tline->len = 0;
	return (-1);
}

static int
	ft_term_line__nread__hered_eof(wchar_t **start)
{
	wchar_t	*pstart;

	pstart = *start;
	if (*pstart == L'-')
		++pstart;
	while (*pstart == L' ' || *pstart == L'\t')
		++pstart;
	*start = pstart;
	if (ft_iswalnum(*pstart))
		return (0);
	return (1);
}

int
	ft_term_line__nread__hered(int n, wchar_t **buff, t_term_line *tline)
{
	int		i;
	int		istart;
	wchar_t	*start;
	wchar_t	*motif;

	if (!tline->line || !(start = ft_wcswcs(tline->line, L"<<")))
		return (0);
	start += 2;
	if (ft_term_line__nread__hered_eof(&start))
		return (ft____hered_err(start, n, buff, tline));
	istart = 0;
	while (start[istart] && !ft_wcschr(L" \t\n", start[istart]))
		++istart;
	i = ft_wcslen(start + istart) - 1;
	if (i < istart)
		return (0);
	ft_memchk_exit(motif = (wchar_t *)ft_memalloc(SW(istart + 2)));
	ft_memcpy(motif + 1, start, SW(istart));
	*motif = L'\n';
	if (!ft_memcmp(start + i, motif, SW(istart + 1)))
		tline->flags = 0;
	free(motif);
	return (0);
}
