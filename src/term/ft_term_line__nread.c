/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line__nread.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/23 06:05:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/23 06:05:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "minishell.h"
#include "sh_term.h"
#include "pr_wchar_t.h"

/*
** boucle sur chaque caractère du read sans les termcaps
*/

static int
	ft__tl_bcl_nread__flagnl(wchar_t **buff, t_term_line *tline)
{
	DEBUG;
	FT_TPUTS(tline->tcaps.cap.sf, 1);
	DEBUGWC(tline->line[tline->nquote]);
	ft__tl_bcl_nread_join(1, buff, tline);
	ft__tl_bcl_nread_quot(tline);
	return (-1);
}

static void
	ft__tl_bcl_nread__notflagnl(int n, wchar_t **buff, t_term_line *tline)
{
	DEBUG;
	ft_term_line_refresh(n, &tline->tcaps, tline->line + tline->nquote);
	ft_wcscpy(*buff, *buff + 1);
	FT_TPUTS(tline->tcaps.cap.sf, 1);
}

static int
	ft__tl_bcl_nread__if(int *n, wchar_t **buff, t_term_line *tline)
{
	DEBUG;
	ft_term_linetoetat(*n, buff, tline);
	if (tline->flags > 0)
	{
		DEBUG;
		ft__tl_bcl_nread__flagnl(buff, tline);
		*n = -1;
		if (!*buff)
			return (0);
	}
	else
	{
		DEBUG;
		ft__tl_bcl_nread__notflagnl(*n, buff, tline);
		return (-1);
	}
	return (1);
}

/*
** si retour -1 alors fin de ligne
*/

int
	ft__tl_bcl_nread(wchar_t **buff, t_term_line *tline)
{
	int		n;
	int		ret;
	wchar_t	*c;

	DEBUG;
	n = 0;
	while ((c = buff[0] + n) && *c)
	{
		DEBUG;
		if (*c == L'\n')
			if ((ret = ft__tl_bcl_nread__if(&n, buff, tline)) != 1)
				return (ret);
		DEBUG;
		++tline->len;
		++n;
	}
	DEBUGNBR(n);
	ft__tl_bcl_nread_join(n, buff, tline);
	tline->tcaps.pos_x += n;
	return (n);
}
