/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_linetoetat.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/23 02:50:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/23 02:50:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../etat/etat.h"
#include "../../inc/pr_wchar_t.h"
#include "../../inc/sh_term.h"
#include "../../inc/t_sh.h"

void
	ft_term_linetoetat(int n, wchar_t **buff, t_term_line *tline)
{
	char	*line;

	DEBUG;
	ft_termcaps__ctrl_e(&tline->tcaps);
	tline->flags = 0;
	DEBUGWCS(*buff);
	ft__tl_bcl_nread_join(n, buff, tline);
	DEBUGWCS(*buff);
	DEBUGWCS(tline->line);
	if (!tline->line)
		return ;
	DEBUG;
	line = ft_wcstombs_alloc(tline->line);
	ft_memchk_exit(line);
	DEBUGSTR(line);
	tline->flags = ft_etat_editionline(line, ft__static_sh(0));
	DEBUGSTR(line);
	ft_mbstowcs(tline->line, line, tline->len + 1);
	free(line);
	ft_memchk_exit(tline->line);
	DEBUGWCS(tline->line);
	DEBUGNBRUL(tline->flags);
}
