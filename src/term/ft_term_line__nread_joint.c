/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line__nread_joint.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/23 05:58:13 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/23 05:58:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "pr_wchar_t.h"

static void
	ft____nread_join_inser(int n, wchar_t **buff, t_term_line *tline)
{
	int		i;
	wchar_t	*tmp;
	wchar_t	*tmpsub;

	DEBUGNBR(tline->len);
	DEBUGWCS(*buff);
	DEBUGWCS(tline->line);
	tmpsub = ft_wcssub(*buff, 0, n);
	ft_memchk_exit(tmpsub);
	DEBUGWCS(tmpsub);
	if (tline->line)
	{
		DEBUGNBR(tline->tcaps.pos_x);
		i = tline->tcaps.pos_x + tline->nquote;
		DEBUGNBR(i);
		tmp = ft_wcsjoin_inser(tline->line, tmpsub, i);
		DEBUGWCS(tmp);
		free(tline->line);
		free(tmpsub);
		ft_memchk_exit(tmp);
		tline->line = tmp;
	}
	else
		tline->line = tmpsub;
	DEBUGWCS(tline->line);
}

void
	ft__tl_bcl_nread_join(int n, wchar_t **buff, t_term_line *tline)
{
	wchar_t	*tmpsub;

	DEBUGWCS(tline->line);
	DEBUGNBR(n);
	if (n)
		ft____nread_join_inser(n, buff, tline);
	tmpsub = NULL;
	if (buff[0][n])
		ft_memchk_exit(tmpsub = ft_wcsdup(*buff + n));
	DEBUGWCS(tmpsub);
	free(*buff);
	*buff = tmpsub;
	DEBUGWCS(*buff);
	DEBUGWCS(tline->line);
}
