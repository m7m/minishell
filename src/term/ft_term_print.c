/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_print.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:27:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:27:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "libft.h"
#include "l_sh.h"
#include "pr_wchar_t.h"

static int
	ft_tplmod(struct s_term_print t)
{
	return (((t.s - t.nl) + 1 + t.prompt_len) % t.co);
}

static void
	ft_term_print__rest(struct s_term_print t,
						t_scap cap, int y_max)
{
	if (t.nl != t.s)
	{
		if (y_max >= 0)
		{
			t.ny = ((t.s - t.nl) + 1 + t.prompt_len) / t.co;
			DEBUGNBR(y_max);
			DEBUGNBR(t.ny);
			if (y_max - t.ny <= 0)
			{
				DEBUGNBR(t.prompt_len);
				t.s -= ft_tplmod(t);
				DEBUGWCS(t.s);
				t.s -= t.co * ((t.ny - y_max));
				DEBUGWCS(t.s);
			}
			DEBUGWCS(t.nl);
			DEBUGWCS(t.s);
			DEBUGNBR(*t.s);
		}
		ft_putnwcs(t.nl, t.s - t.nl);
		ft_term_line_align(ft_tplmod(t), cap);
	}
}

static void
	ft_term_print__put(struct s_term_print *t, t_scap cap, int tab)
{
	ft_putnwcs(t->nl, (t->s - t->nl));
	if (tab)
	{
		ft_term_line_align(ft_tplmod(*t), cap);
		t->prompt_len = 0;
		FT_TPUTS(cap.sf, 2);
	}
	t->nl = t->s + 1;
}

static void
	ft_term_print__tab(struct s_term_print *t, t_scap cap)
{
	ft_term_print__put(t, cap, 0);
	ft_putstr("    ");
}

/*
** Affiche 's' sur stdout
*/

void
	ft_term_print(wchar_t *str, t_scap cap, int prompt, int y_max)
{
	struct s_term_print	t;

	t.co = ft_termcaps_co();
	t.prompt_len = prompt;
	t.nl = str;
	t.s = str;
	DEBUGNBR(y_max);
	while (*t.s)
	{
		if (*t.s == '\n')
		{
			if (y_max > 0)
			{
				t.ny = ((t.s - t.nl) + 1 + t.prompt_len) / t.co + 1;
				if (y_max - t.ny <= 0)
					break ;
				y_max -= t.ny;
			}
			ft_term_print__put(&t, cap, 1);
		}
		else if (*t.s == '\t')
			ft_term_print__tab(&t, cap);
		++t.s;
	}
	ft_term_print__rest(t, cap, y_max);
}
