/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_rel_.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:45:32 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:45:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "pr_wchar_t.h"
#include <stddef.h>

static int
	ft_termcaps__realsize(wchar_t *s, int len_max)
{
	int	t;

	t = ft_wcsncount(s, '\t', len_max);
	DEBUGNBR(t);
	t = t * TAB_SIZE;
	return (t + len_max);
}

/*
** Retourne la valeur relative de y max par rapport à la ligne
*/

int
	ft_termcaps_rel_y_max(t_termcaps *tcaps)
{
	int		y;
	wchar_t	*nl;
	wchar_t	*prev_nl;

	if (*tcaps->line)
	{
		nl = *tcaps->line + *tcaps->nquote;
		prev_nl = ft_wcschrnull(nl, L'\n');
		y = ft_termcaps__realsize(nl, prev_nl - nl) + tcaps->ppos_x;
		DEBUGNBR(y);
		y /= tcaps->co;
		DEBUGNBR(y);
		while (*prev_nl && (nl = ft_wcschrnull(prev_nl + 1, L'\n')))
		{
			++prev_nl;
			y += ft_termcaps__realsize(prev_nl, nl - prev_nl) / tcaps->co + 1;
			prev_nl = nl;
			DEBUGNBR(y);
		}
	}
	else
		y = tcaps->ppos_x / tcaps->co;
	DEBUGNBR(y);
	return (y);
}

static wchar_t
	*ft_termcaps_rel_y3__w(int co, int mv_x, wchar_t *prev_nl, int *y)
{
	wchar_t	*nl;

	while (mv_x > 0 && (nl = ft_wcsnwcs(prev_nl + 1, L"\n", mv_x)))
	{
		*y += ft_termcaps__realsize(prev_nl + 1, nl - (prev_nl + 1)) / co + 1;
		mv_x -= (nl - prev_nl);
		prev_nl = nl;
	}
	return (prev_nl);
}

/*
** mv_x: représente la longeur restante jusqu'au curseur non inclus
*/

int
	ft_termcaps_rel_y3(int prompt_len, int len_x, int co, wchar_t *line)
{
	int		y;
	int		x;
	int		mv_x;
	wchar_t	*prev_nl;

	y = 0;
	if (!line || !len_x || !(prev_nl = ft_wcsnwcs(line, L"\n", len_x)))
	{
		if (line && len_x > 0)
			x = ft_termcaps__realsize(line, len_x) + prompt_len;
		else
			x = prompt_len + len_x;
	}
	else
	{
		x = ft_termcaps__realsize(line, prev_nl - line) + prompt_len;
		y = x / co + 1;
		mv_x = len_x - ((prev_nl - line) + 1);
		prev_nl = ft_termcaps_rel_y3__w(co, mv_x, prev_nl, &y);
		x = ft_termcaps__realsize(prev_nl + 1, (line + len_x) - (prev_nl + 1));
	}
	y += x / co;
	DEBUGNBR(y);
	return (y);
}

int
	ft_termcaps_rel_y4(t_termcaps *tcaps)
{
	DEBUG;
	return (ft_termcaps_rel_y3(tcaps->ppos_x, tcaps->pos_x, tcaps->co,
							*tcaps->line + *tcaps->nquote));
}
