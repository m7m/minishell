/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line__read.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:11:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:11:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "minishell.h"
#include "libft.h"
#include "sh_term.h"
#include "const_term.h"

static char
	*ft__tl_bcl_read_concatbuff(int n, char *buff_read, char *buff)
{
	char	*tmp;

	DEBUG;
	if (buff)
	{
		tmp = buff;
		buff = ft_strjoin(buff, buff_read);
		free(tmp);
		ft_memchk_exit(buff);
	}
	else
		ft_memchk_exit(buff = ft_strdup(buff_read));
	if (n == BTERM_SIZE)
		ft_sleep(3500000);
	return (buff);
}

/*
** Retourne le flux reçus sur read ou le buff si non null
*/

char
	*ft__tl_bcl_read(int fd)
{
	int			n;
	int			buff_len;
	char		*buff;
	char		buff_read[BTERM_SIZE + 2];

	n = BTERM_SIZE;
	buff = NULL;
	buff_len = 0;
	while (n == BTERM_SIZE)
	{
		ft_memset(buff_read, 0, BTERM_SIZE + 1);
		ft_term_reset();
		if (buff_len >= INT_MAX - BTERM_SIZE
			|| (n = read(fd, buff_read, BTERM_SIZE)) < 0)
		{
			n = 0;
			if (ft_signal_trap_sigwinch(2) == 1)
				continue ;
			free(buff);
			return (NULL);
		}
		buff = ft__tl_bcl_read_concatbuff(n, buff_read, buff);
		buff_len += n;
	}
	return (buff);
}
