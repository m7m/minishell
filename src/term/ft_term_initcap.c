/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_initcap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:33:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:33:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

void
	ft_term_initcap__(const char *keycap, char *cap)
{
	DEBUGSTR(keycap);
	DEBUGSTR(cap);
	ft_memchk_exit(cap);
	ft_vtexit(cap);
	(void)keycap;
}

char
	*ft_term_initcap__unix(const char *keycap, char *cap)
{
	DEBUGSTR(keycap);
	DEBUGSTR(cap);
	if (!cap)
		ft_exit(EXIT_FAILURE, "Error: termcap not load\n");
	cap = ft_strdup(cap);
	ft_memchk_exit(cap);
	ft_vtexit(cap);
	(void)keycap;
	return (cap);
}

/*
** DEBUGSTR(tgetstr("sf", NULL));// down
** DEBUGSTR(tgetstr("sr", NULL));// up
** ft_putstr(tgetstr("bw", NULL));
** ft_putstr(tgetstr("up", NULL)); // deplace d'une ligne vers le haut
** ft_putstr(tgetstr("cr", NULL));// debut de ligne
** ft_putstr(tgetstr("al", NULL));//suprrime la ligne
** ft_putstr(tgetstr("dl", NULL));//suprrime la ligne ??
** DEBUGSTR(tgetstr("cs", NULL));// scroll region
** DEBUGSTR(tgoto("cs", 5, 10));// scroll region
** DEBUGSTR(tgoto(tgetstr("cs", NULL), 5, 10));
** DEBUGNBR(tgetnum("lm"));// scroll region
** DEBUGSTR(tgetstr("so", NULL));// enter standout mode
** DEBUGSTR(tgetstr("sg", NULL));// exit standout mode
** DEBUGSTR(tgetstr("cr", NULL));// debut de ligne
** DEBUGSTR(tgetstr("nw", NULL));// debut de la ligne suivante
** DEBUGSTR(tgetstr("CM", NULL));// positionne curseur
** DEBUGSTR(tgetstr("ch", NULL));// positionne curseur sur la ligne
** DEBUGSTR(tgetstr("bc", NULL));// backspace
** DEBUGSTR(tgetstr("cd", NULL));// clear end of screen
** DEBUGSTR(tgetstr("RX", NULL));// off flow control XON/XOFF
** DEBUGSTR(tgetstr("SX", NULL));// on flow control XON/XOFF
** DEBUGSTR(tgetstr("pc", NULL));
** DEBUGSTR(tgetstr("rP", NULL));
** DEBUGSTR(tgetstr("SA", NULL));// am mode
** DEBUGSTR(tgetstr("RA", NULL));// off am mode
** DEBUGSTR(tgetstr("Zk", NULL));
** DEBUGSTR(tgetstr("Zl", NULL));
** DEBUGSTR(tgetstr("Ml", NULL));
** DEBUGSTR(tgetstr("Zm", NULL));
** DEBUGSTR(tgetstr("MR", NULL));
** DEBUGSTR(tgetstr("Zn", NULL));
** DEBUGSTR(tgetstr("Zo", NULL));
** DEBUGSTR(tgetstr("Zp", NULL));
** DEBUGSTR(tgetstr("MT", NULL));
** DEBUGNBR(tgetflag("nx"));
** DEBUGNBR(tgetflag("xo"));
** //	DEBUGSTR(tgetstr("el", NULL));
** // mode
** DEBUGSTR(tgetstr("so", NULL));
** DEBUGSTR(tgetstr("se", NULL));
** //	DEBUGSTR(tgetstr("mb", NULL));
** DEBUGSTR(tgetstr("md", NULL));
** /\* DEBUGSTR(tgetstr("mh", NULL)); *\/
** /\* DEBUGSTR(tgetstr("mk", NULL)); *\/
** /\* DEBUGSTR(tgetstr("mp", NULL)); *\/
** DEBUGSTR(tgetstr("mr", NULL));
** DEBUGSTR(tgetstr("me", NULL));
** DEBUGSTR(tgetstr("as", NULL));
** DEBUGSTR(tgetstr("ae", NULL));
** /\* DEBUGSTR(tgetstr("sa", NULL)); *\/
** FT_TPUTS(tgetstr("se", NULL), 1);
*/

void
	ft_term_initcap__tget(t_scap *tc)
{
	INIT_BUFF;
	INIT_PBUFF;
	DEBUG;
	VAL_BUFF;
	VAL_PBUFF;
	FT_INITCAP(tc->sf, tgetstr("sf", BCAP), "sf");
	FT_INITCAP(tc->sr, tgetstr("sr", BCAP), "sr");
	FT_INITCAP(tc->sc, tgetstr("sc", BCAP), "sc");
	FT_INITCAP(tc->rc, tgetstr("rc", BCAP), "rc");
	FT_INITCAP(tc->nd, tgetstr("nd", BCAP), "nd");
	FT_INITCAP(tc->al, tgetstr("al", BCAP), "al");
	FT_INITCAP(tc->dl, tgetstr("dl", BCAP), "dl");
	FT_INITCAP(tc->cd, tgetstr("cd", BCAP), "cd");
	DEBUGSTR(tc->sf);
	DEBUGSTR(tc->sr);
	DEBUGSTR(tc->sc);
	DEBUGSTR(tc->rc);
	DEBUGSTR(tc->nd);
	DEBUGSTR(tc->al);
	DEBUGSTR(tc->dl);
	DEBUGSTR(tc->cd);
	if (BC)
		ft_vtexit(BC);
	BC = NULL;
}

static void
	ft_term_initcap__default(t_scap *tc)
{
	tc->sf = "\n";
	tc->sr = "\EM";
	tc->sc = "\E7";
	tc->rc = "\E8";
	tc->nd = "\E[C";
	tc->al = "\E[L";
	tc->dl = "\EM";
	tc->cd = "\E[J";
}

void
	ft_term_initcap(t_scap *tc)
{
	char				*term;
	char				buff[4096];

	DEBUG;
	if ((term = getenv("TERM")))
	{
		if (tgetent(buff, term))
		{
			ft_term_initcap__tget(tc);
			return ;
		}
		else
		{
			DEBUGSTR("Error: TERM unknown\n");
		}
	}
	else
	{
		DEBUGSTR("Error: TERM not set\n");
	}
	ft_term_initcap__default(tc);
}
