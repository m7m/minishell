/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_strpos.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/28 04:03:26 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/28 04:03:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Remplis la chaine 's' pour positionner le curseur a la position:
** y(line);x(row)
*/

void
	ft_termcaps_strpos(int x, int y, char *s)
{
	*s = 27;
	*(++s) = '[';
	if (y > 0)
		s += ft_itoa_s(y, s + 1);
	else
		s += ft_itoa_s(1, s + 1);
	*(++s) = ';';
	if (x > 0)
		s += ft_itoa_s(x, s + 1);
	else
		s += ft_itoa_s(1, s + 1);
	*(++s) = 'H';
	*(++s) = 0;
}
