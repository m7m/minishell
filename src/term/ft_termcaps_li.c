/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_li.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:29:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:29:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Retourne nombre de ligne
*/

int
	ft_termcaps_li(void)
{
	struct winsize	argp;

	if (!ioctl(STDOUT_FILENO, TIOCGWINSZ, &argp))
	{
		DEBUGNBR(argp.ws_row);
		return (argp.ws_row);
	}
	return (tgetnum("li"));
}
