/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_setclipboard.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:42:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:42:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "const_term.h"
#include "pr_wchar_t.h"

static char
	*ft__append_next(char *addclipboard, int addlen)
{
	int			oldlen;
	char		*s;

	DEBUG;
	s = NULL;
	oldlen = ft_getlocal(CONST_CLIPBOARD, &s);
	if (oldlen)
	{
		DEBUGSTR(s);
		s = ft_strdup_resize(s, oldlen, addlen);
		ft_memchk_exit(s);
		ft_memcpy(s + oldlen, addclipboard, addlen + 1);
	}
	return (s);
}

static char
	*ft__append_prev(char *prevclipboard, int prevlen)
{
	int			oldlen;
	char		*oldclipboard;
	char		*s;

	DEBUG;
	s = NULL;
	oldlen = ft_getlocal(CONST_CLIPBOARD, &oldclipboard);
	if (oldlen)
	{
		DEBUGSTR(oldclipboard);
		s = ft_strdup_resize(prevclipboard, prevlen, ++oldlen);
		ft_memchk_exit(s);
		DEBUGSTR(oldclipboard);
		DEBUGNBR(oldlen);
		ft_memcpy(s + prevlen, oldclipboard, oldlen);
	}
	return (s);
}

static int
	ft__append(char *c, int start, int end, int old_x[])
{
	int	boolpb;

	if (old_x[0] == end && old_x[1] == start)
		ft_exit(EXIT_FAILURE, "Error: set _clipboard\n");
	DEBUG;
	boolpb = old_x[0] == end
		&& (c = ft__append_prev(c, end - start));
	boolpb += old_x[1] == start
		&& (c = ft__append_next(c, end - start));
	if (boolpb)
	{
		ft_setlocal(CONST_CLIPBOARD, c);
		DEBUGSTR(c);
		free(c);
		return (0);
	}
	return (1);
}

static void
	ft_termcaps_setclipboard_setold(int old_x[3],
									int start, int end, int code_func)
{
	old_x[0] = start;
	old_x[1] = end;
	old_x[2] = code_func;
}

/*
** Enregistre un morceau de line dans la variable local _clipboard
** old_x[0] = old start
** old_x[1] = old end
** old_x[3] = code fonc kill
*/

void
	ft_termcaps_setclipboard(int code_func, int start, int end, wchar_t *line)
{
	static int	old_x[3] = {-1, -1, 1};
	wchar_t		*tmp;
	char		*clipboard;

	DEBUG;
	if (end - start > 0)
	{
		tmp = ft_wcssub(line, start, end - start);
		ft_memchk_exit(tmp);
		clipboard = ft_wcstombs_alloc(tmp);
		ft_memchk_exit(clipboard);
		free(tmp);
		DEBUGNBR(old_x[0]);
		DEBUGNBR(old_x[1]);
		DEBUGNBR(start);
		DEBUGNBR(end);
		DEBUGSTR(clipboard);
		if (old_x[2] != code_func || ft__append(clipboard, start, end, old_x))
			ft_setlocal(CONST_CLIPBOARD, clipboard);
		free(clipboard);
		ft_termcaps_setclipboard_setold(old_x, start, end, code_func);
	}
	else if (code_func != old_x[2])
		ft_termcaps_setclipboard_setold(old_x, -1, -1, code_func);
}
