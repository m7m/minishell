/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line_calc_ppos_x.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:17:32 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:17:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

/*
** détermine la dernière ligne du prompte
**  sinon il faut calculer la hauteur d'affichage du prompt ex: path\n>
** else if = size_t en int
*/

void
	ft_term_line_calc_ppos_x(int co, int *len, char **pprompt)
{
	int		prompt_len;
	char	*prompt;
	char	*tmp;

	if (!len)
		prompt_len = 0;
	else
		prompt_len = *len;
	if (!*pprompt)
		return ;
	prompt = *pprompt;
	if (prompt && (tmp = ft_strrchr(prompt, '\n')))
	{
		++tmp;
		prompt_len = ft_strlen(tmp);
		prompt = tmp;
	}
	else if (prompt_len < 0)
		prompt_len = 0;
	*len = prompt_len;
	if (!(prompt_len %= co))
		prompt_len = co;
	*pprompt = prompt + (*len - prompt_len);
	*len = prompt_len;
	DEBUGNBR(*len);
}

int
	ft_term_line_calc_nquote(wchar_t *line)
{
	int		len;
	wchar_t	*tmp;

	DEBUG;
	len = 0;
	if (line && (tmp = ft_wcsrchr(line, '\n')))
		len = tmp - (line + 1);
	return (len);
}
