/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_istermcaps.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:00:11 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:00:11 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "pr_wchar_t.h"
#include "flags_bindkeys.h"

int
	ft_istermcap_test(int enable_fonc, t_termcaps *tcaps)
{
	int			ret;

	if (ft_termcaps_pos(STDIN_FILENO, &tcaps->abs_x, &tcaps->abs_y))
		return (-1);
	DEBUGNBR(tcaps->pos_x);
	DEBUGSTR(tcaps->c);
	if (*tcaps->c == '\n')
		return (0);
	if ((*tcaps->c && (unsigned char)*tcaps->c > 0x1F)
		&& *tcaps->c != 127)
		return (0);
	ret = ft_termcaps(enable_fonc, tcaps);
	tcaps->old_bindkeys = ret;
	DEBUGNBR(ret);
	return (ret);
}

static int
	ft_termcaps__spec(int n, char **buff, t_term_line *tline)
{
	int	ret;

	tline->tcaps.c = *buff + n;
	if ((ret = ft_istermcap_test(1, &tline->tcaps)) < 0
		|| (ret == FL_CTRL_D && !tline->line) || ret == FL_CTRL_C)
	{
		DEBUG;
		free(tline->line);
		tline->line = NULL;
		ft_strdel(buff);
	}
	return (ret);
}

int
	ft_istermcap(char **buff, t_term_line *tline)
{
	int	ret;
	int	n;

	DEBUG;
	n = 0;
	DEBUGPTR(*buff);
	while (*buff && buff[0][n])
	{
		DEBUGNBR(buff[0][n]);
		if (tline->tcaps.old_bindkeys == FL_CTRL_V)
		{
			tline->tcaps.old_bindkeys = 0;
			return (0);
		}
		if ((ret = ft_termcaps__spec(n, buff, tline)) < 0
			|| ret == FL_CTRL_D || ret == FL_CTRL_C)
			return (ret);
		else if (!ret)
			++n;
	}
	tline->len = ft_wcslen(tline->line);
	DEBUGSTR(*buff);
	if (!**buff)
		ft_strdel(buff);
	return (0);
}
