/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 14:18:00 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/23 14:18:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "minishell.h"
#include "sh_term.h"
#include "t_struct.h"
#include "libft.h"
#include "pr_wchar_t.h"
#include "flags_bindkeys.h"

static int
	ft__tl_bcl__buff(wchar_t **buff, t_term_line *tline)
{
	int				ret;
	char			*tmp;

	if (!(tmp = ft__tl_bcl_read(tline->fd)))
		return (-1);
	ft_strreplace(tmp, '\r', '\n');
	tline->tcaps.co = ft_termcaps_co();
	if ((ret = ft_istermcap(&tmp, tline)))
	{
		free(tmp);
		if (ret == FL_CTRL_D)
			return (-3);
		free(*buff);
		*buff = 0;
		return (ret);
	}
	if (tmp)
	{
		DEBUGSTR(tmp);
		*buff = ft_mbstowcs_alloc(tmp);
		free(tmp);
		ft_memchk_exit(*buff);
		DEBUGWCS(*buff);
	}
	return (0);
}

static int
	ft__tl_bcl(t_term_line *tline, t_sh *sh)
{
	int				ret;
	static wchar_t	*buff = 0;

	DEBUG;
	ret = 0;
	sh->sf->term_line_buff = &buff;
	while (1)
	{
		DEBUG;
		if (!buff)
			if ((ret = ft__tl_bcl__buff(&buff, tline)))
				return (ret);
		DEBUGWCS(buff);
		if (buff)
		{
			if ((ret = ft__tl_bcl_nread(&buff, tline)) == -1)
				return (tline->len);
			tline->tcaps.old_bindkeys = 0;
		}
		ft_term_line_refresh(ret, &tline->tcaps, tline->line + tline->nquote);
	}
}

static int
	ft_term_line_init2(char **line, t_term_line *tline, t_sh *sh)
{
	int		ret;
	char	*tmp;

	if (ft_termcaps_pos(tline->fd, &tline->tcaps.abs_x, &tline->tcaps.abs_y))
		return (-1);
	if (tline->tcaps.abs_x > 1)
		ft_putstr("%\n");
	tline->tcaps.abs_x = 1;
	tmp = 0;
	if ((tline->tcaps.ppos_x = ft_getlocal("PS1", &tmp)))
	{
		write(STDOUT_FILENO, tmp, tline->tcaps.ppos_x);
		ft_term_line_calc_ppos_x(tline->tcaps.co, &tline->tcaps.ppos_x, &tmp);
	}
	tline->tcaps.home = tmp;
	DEBUGSTR(tline->tcaps.home);
	ret = ft__tl_bcl(tline, sh);
	DEBUGNBR(ret);
	DEBUGWCS(tline->line);
	if (tline->line)
	{
		ft_memchk_exit(*line = ft_wcstombs_alloc(tline->line));
		free(tline->line);
	}
	return (ret);
}

/*
** valeur de retour:
** -3 = fin de lecture, EOF
** -2 = sigint
** -1 = erreur non défini
*/

int
	ft_term_line(int fd, t_scap tc, char **line, t_sh *sh)
{
	int			ret;
	t_term_line tline;

	DEBUG;
	if (fd < 0 || !line)
		return (-1);
	tline.fd = fd;
	tline.flags = 0;
	tline.len = 0;
	tline.line = 0;
	tline.nquote = 0;
	tline.tcaps.c = 0;
	tline.tcaps.cap = tc;
	tline.tcaps.co = ft_termcaps_co();
	tline.tcaps.line = &tline.line;
	tline.tcaps.old_bindkeys = 0;
	tline.tcaps.pos_x = 0;
	tline.tcaps.ppos_x = 0;
	tline.tcaps.nquote = &tline.nquote;
	ret = ft_term_line_init2(line, &tline, sh);
	return (ret);
}
