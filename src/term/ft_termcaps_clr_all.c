/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_clr_all.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 23:49:44 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/29 23:49:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Efface toute la ligne d'edition
** et se positionne en en haut a gauche de celle ci
*/

void
	ft_termcaps_clr_all(t_termcaps *tcaps)
{
	int	y_max;

	DEBUG;
	y_max = ft_termcaps_rel_y_max(tcaps);
	tcaps->abs_y -= ft_termcaps_rel_y4(tcaps);
	DEBUGNBR(y_max);
	while (y_max >= 0)
	{
		ft_termcaps_clr_line(tcaps->abs_y, y_max, 0);
		--y_max;
	}
}
