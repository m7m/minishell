/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_init.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 04:03:16 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/06 04:03:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Dev sys sous linux p994
** sterm.c_oflag &= ~(OPOST);
** ISIG Lorsqu'un signal INTR, QUIT, SUSP, ou DSUSP arrivent
** engendre le signal correspondant.
** tcsetattr TCSAFLUSH
*/

#ifndef _XOPEN_SOURCE
# define _XOPEN_SOURCE
#endif

void
	ft_term_init(struct termios sterm, int fd)
{
	sterm.c_iflag &= ~(BRKINT | IGNBRK | ICRNL | IGNCR
					| PARMRK | INLCR | ISTRIP | IXON);
	sterm.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
	sterm.c_oflag |= (TABDLY | TAB0 | TAB1 | TAB2 | TAB3);
	sterm.c_cflag &= ~(CSIZE | PARENB);
	sterm.c_cflag |= CS8;
	sterm.c_cc[VMIN] = 1;
	sterm.c_cc[VTIME] = 100;
	DEBUG;
	if (tcsetattr(fd, TCSANOW, &sterm) < 0)
		ft_exit(EXIT_FAILURE, "Error: Not setattr tty\n");
	DEBUG;
}
