/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_clr_line.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:28:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:28:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Suprrimer la ligne correspondante a : abs_y + del_y
*/

void
	ft_termcaps_clr_line(int abs_y, int del_y, char *cap_al)
{
	char	s[4096];

	(void)cap_al;
	DEBUGNBR(del_y);
	ft_termcaps_setpos(1, abs_y + del_y, del_y);
	ft_termcaps_clr_n(1, s);
	FT_TPUTS(s, del_y);
}
