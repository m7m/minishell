/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:12:11 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/23 16:12:11 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"

static int
	ft_termcaps__wstfound(struct s_wsttermcaps twstt, t_termcaps *tcaps)
{
	int	ret;
	int	len;

	ret = twstt.f(tcaps);
	len = ft_strlen(tcaps->c);
	DEBUGNBR(len);
	if (len != twstt.c_len)
		ft_strcpy(tcaps->c, tcaps->c + twstt.c_len);
	else
		*tcaps->c = 0;
	return (ret);
}

/*
** else if (twstt[i].ctput)
** {nécessite un memcpy après coup voir ci-dessus
** FT_TPUTS(twstt[i].ctput, 1);}
*/

static int
	ft_termcaps__wst(t_termcaps *tcaps)
{
	struct s_wsttermcaps	twstt[FL_END_FL];
	int						ret;
	int						i;

	DEBUG;
	i = 0;
	ft_termcaps__init_twstt(twstt);
	while (twstt[i].c && ft_memcmp(twstt[i].c, tcaps->c, twstt[i].c_len))
	{
		++i;
		if (i > 254)
			return (0);
	}
	DEBUGSTR(twstt[i].c);
	DEBUGNBR(twstt[i].c_len);
	DEBUGPTR(twstt[i].f);
	DEBUGNBR(tcaps->old_bindkeys);
	if (twstt[i].f)
		ret = ft_termcaps__wstfound(twstt[i], tcaps);
	else
		ret = 0;
	return (ret);
}

static int
	ft_termcaps__wst_nofonc(t_termcaps *tcaps)
{
	struct s_wsttermcaps_nof	twstt[FL_END_FL];
	int							ret;
	int							i;

	DEBUG;
	i = 0;
	ft_termcaps__init_twstt_nof(twstt);
	while (twstt[i].c && ft_memcmp(twstt[i].c, tcaps->c, twstt[i].c_len))
	{
		++i;
		if (i > 254)
			return (0);
	}
	DEBUGSTR(twstt[i].c);
	DEBUGNBR(twstt[i].c_len);
	DEBUGNBR(twstt[i].f);
	ret = 0;
	if (twstt[i].f)
	{
		ret = twstt[i].f;
		ft_strcpy(tcaps->c, tcaps->c + twstt[i].c_len);
	}
	return (ret);
}

static void
	ft_test(char *c)
{
	if (c[1])
	{
		DEBUGC(c[2]);
		if (c[2])
		{
			DEBUGC(c[3]);
			if (c[3])
			{
				DEBUGC(c[4]);
				if (c[4])
				{
					DEBUGC(c[5]);
					if (c[5])
					{
						DEBUGC(c[6]);
					}
				}
			}
		}
	}
}

int
	ft_termcaps(int enable_fonc, t_termcaps *tcaps)
{
	int	ret;

	DEBUGSTR(tcaps->c);
	DEBUGNBRUC(tcaps->c[0]);
	DEBUGC(tcaps->c[1]);
	DEBUGNBRUC(tcaps->c[1]);
	;
	ft_test(tcaps->c);
	;
	DEBUGNBR(enable_fonc);
	if ((enable_fonc && !(ret = ft_termcaps__wst(tcaps)))
		|| (!enable_fonc && !(ret = ft_termcaps__wst_nofonc(tcaps))))
	{
		DEBUGSTR(" termcap disable ");
		*tcaps->c = 0;
	}
	else
		ft_termcaps_setclipboard(ret, 0, 0, 0);
	return (ret);
}
