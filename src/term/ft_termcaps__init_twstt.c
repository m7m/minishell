/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__init_twstt.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:41:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:41:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"

static void
	ft__twstt(struct s_wsttermcaps *twstt,
			char *c, char *ctput, int (*f)(t_termcaps *))
{
	twstt->c = c;
	DEBUGSTR(c);
	twstt->c_len = ft_strlen(c);
	twstt->ctput = ctput;
	twstt->f = f;
}

static struct s_wsttermcaps
	*ft___init_twstt_arrow(struct s_wsttermcaps twstt[])
{
	ft__twstt(++twstt, ARROW_U, 0, ft_termcaps__arrow_u);
	ft__twstt(++twstt, ARROW_D, 0, ft_termcaps__arrow_d);
	ft__twstt(++twstt, ARROW_R, 0, ft_termcaps__arrow_r);
	ft__twstt(++twstt, ARROW_L, 0, ft_termcaps__arrow_l);
	ft__twstt(++twstt, M_ARROW_R, 0, ft_termcaps__arrow_cr);
	ft__twstt(++twstt, M_ARROW_L, 0, ft_termcaps__arrow_cl);
	ft__twstt(++twstt, C_ARROW_U, 0, ft_termcaps__ctrl_arrow_u);
	ft__twstt(++twstt, C_ARROW_D, 0, ft_termcaps__ctrl_arrow_d);
	ft__twstt(++twstt, C_ARROW_R, 0, ft_termcaps__arrow_cr);
	ft__twstt(++twstt, C_ARROW_L, 0, ft_termcaps__arrow_cl);
	return (twstt);
}

void
	ft_termcaps__init_twstt(struct s_wsttermcaps twstt[])
{
	ft__twstt(twstt, BACKSPACE, 0, ft_termcaps__backspace);
	ft__twstt(++twstt, DEL, 0, ft_termcaps__del);
	ft__twstt(++twstt, HOME, 0, ft_termcaps__ctrl_a);
	ft__twstt(++twstt, END, 0, ft_termcaps__ctrl_e);
	ft__twstt(++twstt, HORIZ_TAB, 0, ft_termcaps__horiz_tab);
	ft__twstt(++twstt, CTRL_A, 0, ft_termcaps__ctrl_a);
	ft__twstt(++twstt, CTRL_C, 0, ft_termcaps__ctrl_c);
	ft__twstt(++twstt, CTRL_D, 0, ft_termcaps__ctrl_d);
	ft__twstt(++twstt, CTRL_E, 0, ft_termcaps__ctrl_e);
	ft__twstt(++twstt, CTRL_K, 0, ft_termcaps__ctrl_k);
	ft__twstt(++twstt, CTRL_L, 0, ft_termcaps__ctrl_l);
	ft__twstt(++twstt, CTRL_R, 0, ft_termcaps__ctrl_r);
	ft__twstt(++twstt, CTRL_U, 0, ft_termcaps__ctrl_u);
	ft__twstt(++twstt, CTRL_V, 0, ft_termcaps__ctrl_v);
	ft__twstt(++twstt, CTRL_W, 0, ft_termcaps__ctrl_w);
	ft__twstt(++twstt, CTRL_Y, 0, ft_termcaps__ctrl_y);
	twstt = ft___init_twstt_arrow(twstt);
	ft__twstt(++twstt, META_B, 0, ft_termcaps__arrow_cl);
	ft__twstt(++twstt, META_F, 0, ft_termcaps__arrow_cr);
	ft__twstt(++twstt, ALT_BACKSP, 0, ft_termcaps__alt_backspace);
	ft__twstt(++twstt, ALT_D, 0, ft_termcaps__alt_d);
	ft__twstt(++twstt, ALT_L, 0, ft_termcaps__alt_l);
	ft__twstt(++twstt, ALT_U, 0, ft_termcaps__alt_u);
	ft__twstt(++twstt, 0, 0, 0);
}
