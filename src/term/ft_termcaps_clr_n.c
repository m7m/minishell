/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_clr_n.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:28:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:28:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Supprime du cursuer jusqu'en fin de ligne
*/

void
	ft_termcaps_clr_n(int abs_x, char s[])
{
	int		i;

	s[0] = 27;
	s[1] = '[';
	i = ft_itoa_s((ft_termcaps_co() - abs_x) + 1, s + 2);
	s[i + 2] = 'X';
	s[i + 3] = 0;
	DEBUGSTR(s);
}
