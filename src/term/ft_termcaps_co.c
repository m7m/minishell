/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_co.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:29:44 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:29:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Retourne nombre de colonne
*/

int
	ft_termcaps_co(void)
{
	struct winsize	argp;

	if (!ioctl(STDOUT_FILENO, TIOCGWINSZ, &argp))
	{
		DEBUGNBR(argp.ws_col);
		return (argp.ws_col);
	}
	return (tgetnum("co"));
}
