/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line_align.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:11:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:11:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Repositionne le curseur sur la derniere colonne
** en le posisionnant sur la ligne suivante
** soit le curseur est en-dehors du terminal
** ou il est sur le dernier caractere
** au lieux d'etre sur la ligne suivante
*/

/*
** Gnu/Linux
** Premier if xterm depassement, le curseur est hors terminal
** OS X
** Second if recule, le curseur est decale de -1
*/

void
	ft_term_line_align(int x, t_scap caps)
{
	int	co;
	int	abs_x;

	if (ft_termcaps_pos(STDIN_FILENO, &abs_x, NULL))
		return ;
	co = ft_termcaps_co();
	DEBUGNBR(co);
	DEBUGNBR(abs_x);
	DEBUGNBR(x);
	if (abs_x == co + 1 && x == 1)
	{
		DEBUG;
		FT_TPUTS(caps.sf, 1);
	}
	else if (abs_x == co && x == 1)
	{
		DEBUG;
		FT_TPUTS(caps.sf, 1);
	}
}

void
	ft_term_line_align_refre(t_termcaps *tcaps)
{
	int	x;

	x = ft_termcaps_rel_x(tcaps);
	ft_term_line_align(x, tcaps->cap);
}
