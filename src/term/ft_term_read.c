/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_read.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 13:53:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/23 13:53:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "sh_term.h"
#include "pr_history.h"

static int
	ft_term_read__ctrl_d(t_sh *sh, char *line)
{
	int	len;

	DEBUG;
	if (line)
		return (ft_strlen(line));
	else if (!(len = ft_jobs_count(sh->tljobs.tjobs)))
	{
		free(line);
		ft_exit(EXIT_SUCCESS, "bye :(\n");
	}
	MSG_ERR3INT(len, "jobs is background");
	return (0);
}

void
	ft_term_read(t_sh *sh, t_scap tc)
{
	int			len;
	char		*line;

	DEBUG;
	line = NULL;
	DEBUGPTR(sh->tljobs.tjobs);
	ft_jobs_stat(&sh->tljobs);
	len = ft_term_line(sh->fd, tc, &line, sh);
	DEBUGNBR(len);
	DEBUGSTR(line);
	if (len == -3)
		len = ft_term_read__ctrl_d(sh, line);
	else if (len == -1 || len < -2)
		ft_exit(EXIT_SUCCESS, "Error: read unknown\n");
	ft_history_reset_line();
	if (len > 0 && !ft_history_parser(&line))
	{
		sh->nb_line = 0;
		ft_prompt_parse(sh, line, len);
		ft_history_limit(&sh->hhistory);
	}
	else
		free(line);
}
