/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_line_refresh.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:19:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:19:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "sh_term.h"
#include "pr_wchar_t.h"

/*
** repositionne le curseur
** que si le curseur n'était pas en fin de ligne
*/

static void
	ft__rest_cursor(t_term_line_re ttlre, t_termcaps *tcaps)
{
	int	abs_y;

	DEBUG;
	tcaps->pos_x = ttlre.x;
	if (ft_termcaps_pos(STDIN_FILENO, NULL, &abs_y))
		return ;
	DEBUGNBR(abs_y);
	abs_y -= ft_termcaps_rel_y_max(tcaps) - ft_termcaps_rel_y4(tcaps);
	DEBUGNBR(abs_y);
	DEBUGNBR(ttlre.li);
	DEBUGNBR(ttlre.prompt_len);
	ttlre.x = ft_termcaps_rel_x(tcaps);
	DEBUGNBR(ttlre.x);
	ft_termcaps_setpos(ttlre.x, abs_y, ttlre.li - abs_y);
	DEBUG;
}

/*
** efface la ligne + le prompt
** puis
** affiche le prompt + la ligne
** ft_term_print(ttlre.prompt, tcaps->cap);
*/

static void
	ft__clr_line(t_term_line_re ttlre, t_termcaps *tcaps, wchar_t *line)
{
	int		y_max;
	char	s[4096];

	DEBUGWCS(line);
	y_max = ft_termcaps_rel_y3(ttlre.prompt_len,
							ttlre.line_len - ttlre.n, ttlre.co_old, line);
	DEBUGNBR(y_max);
	while (y_max >= 0)
	{
		DEBUGNBR(ttlre.abs_y);
		ft_termcaps_setpos(1, ttlre.abs_y + y_max, y_max);
		ft_termcaps_clr_n(1, s);
		FT_TPUTS(s, y_max);
		--y_max;
	}
	tcaps->pos_x = ttlre.x;
	ft_term_print_size(1, ttlre.li, *tcaps);
	ft_term_line_align_refre(tcaps);
	ft__rest_cursor(ttlre, tcaps);
}

/*
** se positionne en debut de prompt
*/

static void
	ft__pos_home(t_term_line_re ttlre, t_termcaps *tcaps, wchar_t *line)
{
	int	abs_y;

	DEBUG;
	if (ft_termcaps_pos(STDIN_FILENO, NULL, &abs_y))
		return ;
	DEBUGNBR(abs_y);
	ttlre.abs_y = abs_y - ttlre.y;
	DEBUGNBR(ttlre.abs_y);
	DEBUGNBR(ttlre.y);
	ft_termcaps_setpos(1, ttlre.abs_y, ttlre.li - abs_y);
	ft__clr_line(ttlre, tcaps, line);
}

/*
** rétention du curseur
** ttlre.x = pos_x avant modif
** ttlre.y = avec prompt
*/

static void
	ft__start(t_term_line_re ttlre, t_termcaps *tcaps, wchar_t *line)
{
	DEBUG;
	ttlre.x = tcaps->pos_x;
	DEBUGNBR(ttlre.prompt_len);
	ft_term_line_calc_ppos_x(tcaps->co, &ttlre.prompt_len, &ttlre.prompt);
	DEBUGSTR(ttlre.prompt);
	DEBUGNBR(ttlre.prompt_len);
	DEBUGNBR(ttlre.x);
	DEBUGNBR(ttlre.n);
	DEBUGNBR(ttlre.co_old);
	ttlre.y = ft_termcaps_rel_y3(ttlre.prompt_len, ttlre.x - ttlre.n,
								ttlre.co_old, line);
	DEBUGNBR(ttlre.y);
	ttlre.line_len = ft_wcslen(line);
	DEBUGNBR(ttlre.line_len);
	ft__pos_home(ttlre, tcaps, line);
}

/*
** Affiche/Rafraîchis la ligne, dernière ligne du prompt inclus
** co_old lors d'un redimenssionnement l'ancienne valeur est nécessaire
** nb_x_add: représente le nombre de caractère ajouté,
**  dans le tampon, depuis le dernière appel de ft_term_line_refresh
** 1 rétention du curseur
** 2 se positionne en debut de prompt
** 3 efface la ligne + le prompt
** 4 affiche le prompt + la ligne
** 5 repositionne le curseur
*/

void
	ft_term_line_refresh(int nb_x_add, t_termcaps *tcaps, wchar_t *line)
{
	static int		co_old = -1;
	static int		li_old = -1;
	t_term_line_re	ttlre;

	DEBUGNBR(co_old);
	if (co_old == -1)
		co_old = ft_termcaps_co();
	if (li_old == -1)
		li_old = ft_termcaps_li();
	if (co_old > 1)
	{
		ttlre.prompt_len = tcaps->ppos_x;
		ttlre.prompt = tcaps->home;
		if (ft_signal_trap_sigwinch(0))
			ttlre.co_old = tcaps->co;
		else
			ttlre.co_old = co_old;
		ttlre.n = nb_x_add;
		DEBUGNBR(tcaps->pos_x);
		ttlre.li = ft_termcaps_li();
		ft__start(ttlre, tcaps, line);
		DEBUGNBR(tcaps->pos_x);
		co_old = ft_termcaps_co();
	}
}
