/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_setpos.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:33:31 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:33:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"

/*
** Positionne le curseur
*/

void
	ft_termcaps_setpos(int x, int y, int abs_y)
{
	char	s[4096];

	ft_termcaps_strpos(x, y, s);
	DEBUGSTR(s);
	FT_TPUTS(s, ft_termcaps_li() - abs_y);
}
