/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_term_print_size.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 03:29:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/01 03:29:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "libft.h"
#include "l_sh.h"
#include "pr_wchar_t.h"

/*
** Affiche la ligne en fonction de la taille du terminal
** si li < 1 alors initialise via ft_termcap_li()
** si terminal plus petit que la ligne
**  alors affiche au max li avec la ligne du curseur le plus en haut
*/

static void
	ft_term_print_size__pad(int li, int y, int y_max, t_termcaps *tcaps)
{
	DEBUGNBR(y);
	if (y > (y_max - li) + 1)
		y = (y_max - li) + 1;
	DEBUGNBR(y_max);
	DEBUGNBR(y);
	DEBUGWCS(*tcaps->line);
	while (ft_termcaps_rel_y4(tcaps) < y && (*tcaps->line)[tcaps->pos_x])
		++tcaps->pos_x;
	DEBUGNBR(tcaps->pos_x);
	if (y)
		tcaps->ppos_x = 0;
}

void
	ft_term_print_size(int print_prompt, int li, t_termcaps tcaps)
{
	int	y_max;
	int	y;
	int	p;

	DEBUG;
	if (li < 1)
		li = ft_termcaps_li();
	y_max = ft_termcaps_rel_y_max(&tcaps);
	p = 0;
	if (y_max >= li)
	{
		y = ft_termcaps_rel_y4(&tcaps);
		p = y;
	}
	if (tcaps.home && !p && print_prompt)
		ft_putstr(tcaps.home);
	if (*tcaps.line)
	{
		tcaps.pos_x = 0;
		if (p)
			ft_term_print_size__pad(li, y, y_max, &tcaps);
		ft_term_print(*tcaps.line + *tcaps.nquote + tcaps.pos_x,
					tcaps.cap, tcaps.ppos_x, li);
	}
}
