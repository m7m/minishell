/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__backspace.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/26 06:08:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/26 06:08:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"

int
	ft_termcaps__backspace(t_termcaps *tcaps)
{
	int		li;

	DEBUG;
	if (tcaps->pos_x)
	{
		DEBUGNBR(tcaps->abs_y);
		DEBUGNBR(tcaps->abs_x);
		li = ft_termcaps_li();
		if (--tcaps->abs_x)
			FT_TPUTS("\x1B[1D", li - tcaps->abs_y);
		else
		{
			FT_TPUTS(tcaps->cap.dl, li - tcaps->abs_y);
			ft_termcaps_setpos(tcaps->co, tcaps->abs_y - 1, li - tcaps->abs_y);
			--tcaps->abs_y;
			tcaps->abs_x = tcaps->co;
		}
		DEBUGNBR(tcaps->abs_y);
		DEBUGNBR(tcaps->abs_x);
		--tcaps->pos_x;
		ft_termcaps__del(tcaps);
		DEBUGNBR(tcaps->pos_x);
	}
	return (FL_BACKSPACE);
}
