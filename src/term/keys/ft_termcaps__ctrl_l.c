/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_l.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 00:01:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/01 00:01:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"

int
	ft_termcaps__ctrl_l(t_termcaps *tcaps)
{
	ft_termcaps_setpos(1, 1, 0);
	FT_TPUTS(tcaps->cap.cd, ft_termcaps_li());
	return (FL_CTRL_L);
}
