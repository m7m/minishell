/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__alt_u.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:59:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:59:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

/*
** Met en majuscule
*/

int
	ft_termcaps__alt_u(t_termcaps *tcaps)
{
	int		x;
	int		xx;
	wchar_t	*s;

	DEBUG;
	if ((*tcaps->line))
	{
		x = tcaps->pos_x + *tcaps->nquote;
		s = (*tcaps->line);
		ft_termcaps_rigth_wcs(tcaps, ft_wcsword);
		xx = tcaps->pos_x + *tcaps->nquote;
		while (x < xx)
		{
			if (ft_islower(s[x]))
				s[x] -= 32;
			++x;
		}
	}
	return (FL_ALT_U);
}
