/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__horiz_tab__.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 12:44:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/17 12:44:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "l_sh.h"
#include "libft.h"

char
	*ft_termcaps__horiz_tab__antsp(int b, char *motif, char *tabmotif)
{
	char	*startmotif;

	DEBUGSTR(tabmotif);
	DEBUGSTR(motif);
	startmotif = ft_strstr(tabmotif, motif);
	DEBUGNBR(b);
	DEBUGSTR(startmotif);
	startmotif += ft_strlen(motif);
	DEBUGSTR(startmotif);
	startmotif = ft_strnreplaces(startmotif,
								" ", "\\ ",
								ft_strlen(startmotif) - b);
	ft_memchk_exit(startmotif);
	DEBUGSTR(startmotif);
	return (startmotif);
}
