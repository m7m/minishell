/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__keypad_osx.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:09:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:09:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_struct.h"

int
	ft_termcaps__keypad_osx(t_termcaps *tcaps)
{
	DEBUG;
	(void)tcaps;
	return (0);
}
