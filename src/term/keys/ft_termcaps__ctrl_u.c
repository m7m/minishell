/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_u.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:05:39 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:05:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

int
	ft_termcaps__ctrl_u(t_termcaps *tcaps)
{
	int	x;

	DEBUG;
	if (tcaps->pos_x && (*tcaps->line))
	{
		DEBUG;
		x = tcaps->pos_x + *tcaps->nquote;
		ft_termcaps_setclipboard(FL_CTRL_U, 0, x,
								*tcaps->line + *tcaps->nquote);
		ft_termcaps__ctrl_a(tcaps);
		ft_termcaps__ctrl_k_del(tcaps, tcaps->ppos_x + 1, tcaps->abs_y);
		ft_memcpy(*tcaps->line + *tcaps->nquote, (*tcaps->line) + x,
				SW(ft_wcslen((*tcaps->line) + x) + 1));
	}
	return (FL_CTRL_U);
}
