/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_v.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 12:21:09 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/15 12:21:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"

int
	ft_termcaps__ctrl_v(t_termcaps *tcaps)
{
	DEBUG;
	(void)tcaps;
	return (FL_CTRL_V);
}
