/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__arrow_updown.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:03:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:03:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "minishell.h"
#include "pr_history.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

static void
	ft_termcaps__arrow_ud_quot(t_termcaps *tcaps, wchar_t *old_line)
{
	wchar_t	*line;

	if (*tcaps->nquote)
	{
		line = ft_memdup_resize(old_line, SW(*tcaps->nquote),
								SW(tcaps->pos_x + 1));
		free(old_line);
		ft_memchk_exit(line);
		old_line = *tcaps->line;
		if (*tcaps->line)
			ft_wcscpy(line + *tcaps->nquote, *tcaps->line);
		else
			line[*tcaps->nquote] = 0;
		*tcaps->line = line;
		DEBUGWCS(*tcaps->line);
	}
	free(old_line);
}

static void
	ft_termcaps__arrow_ud(t_termcaps *tcaps, size_t (*fgethist)(wchar_t **))
{
	wchar_t	*old_line;

	DEBUG;
	ft_termcaps__ctrl_a(tcaps);
	if (*tcaps->line)
		ft_termcaps__ctrl_k_del(tcaps, tcaps->ppos_x + 1, tcaps->abs_y);
	ft_history_save_current_ligne((*tcaps->line));
	;
	old_line = (*tcaps->line);
	(*tcaps->line) = NULL;
	tcaps->pos_x = fgethist(tcaps->line);
	ft_termcaps__arrow_ud_quot(tcaps, old_line);
	;
	DEBUG;
	if (*tcaps->line)
	{
		ft_term_print_size(0, -1, *tcaps);
		ft_term_line_align_refre(tcaps);
	}
	DEBUGWCS((*tcaps->line));
	DEBUGNBR(tcaps->pos_x);
}

int
	ft_termcaps__arrow_u(t_termcaps *tcaps)
{
	DEBUG;
	ft_termcaps__arrow_ud(tcaps, ft_gethistory_prev);
	DEBUGWCS((*tcaps->line));
	return (FL_ARROW_U);
}

int
	ft_termcaps__arrow_d(t_termcaps *tcaps)
{
	DEBUG;
	ft_termcaps__arrow_ud(tcaps, ft_gethistory_next);
	DEBUGWCS((*tcaps->line));
	return (FL_ARROW_D);
}
