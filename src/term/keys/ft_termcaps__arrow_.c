/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__arrow_.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:05:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:05:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "minishell.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

int
	ft_termcaps__arrow_r(t_termcaps *tcaps)
{
	int	relat_x;
	int	y;
	int	ynew;
	int	x;

	DEBUG;
	relat_x = tcaps->pos_x + *tcaps->nquote;
	if ((*tcaps->line) && (*tcaps->line)[relat_x])
	{
		y = ft_termcaps_rel_y4(tcaps);
		++tcaps->pos_x;
		ynew = ft_termcaps_rel_y4(tcaps);
		x = ft_termcaps_rel_x(tcaps);
		if (tcaps->abs_x + 1 > tcaps->co
			|| y < ynew)
		{
			DEBUGSTR(" next line ");
			++tcaps->abs_y;
			ft_termcaps_setpos(1, tcaps->abs_y, tcaps->abs_y);
		}
		else
			ft_termcaps_setpos(x, tcaps->abs_y, tcaps->abs_y);
	}
	return (FL_ARROW_R);
}

int
	ft_termcaps__arrow_l(t_termcaps *tcaps)
{
	int	x;
	int	y;

	DEBUG;
	if (tcaps->pos_x)
	{
		y = ft_termcaps_rel_y4(tcaps);
		--tcaps->pos_x;
		x = ft_termcaps_rel_x(tcaps);
		if (tcaps->abs_x - 1 == 0
			|| y > ft_termcaps_rel_y4(tcaps))
		{
			DEBUGSTR(" previous line ");
			--tcaps->abs_y;
			ft_termcaps_setpos(x, tcaps->abs_y, ft_termcaps_li() - 1);
		}
		else
			ft_termcaps_setpos(x, tcaps->abs_y, ft_termcaps_li() - 1);
	}
	return (FL_ARROW_L);
}

int
	ft_termcaps__arrow_cr(t_termcaps *tcaps)
{
	DEBUG;
	if ((*tcaps->line)
		&& (*tcaps->line)[tcaps->pos_x + *tcaps->nquote])
	{
		ft_termcaps_rigth_wcs(tcaps, ft_wcsword);
	}
	return (FL_C_ARROW_R);
}

int
	ft_termcaps__arrow_cl(t_termcaps *tcaps)
{
	DEBUG;
	if (tcaps->pos_x)
		ft_termcaps_left_wcs(tcaps, ft_wcsrword);
	return (FL_C_ARROW_L);
}
