/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_y.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:09:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:09:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "const_term.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

static void
	ft_termcaps__ctrl_y__pos(int len, t_termcaps *tcaps)
{
	int	y;
	int	old_y;
	int	li;

	DEBUG;
	old_y = ft_termcaps_rel_y4(tcaps);
	tcaps->pos_x += len;
	y = ft_termcaps_rel_y4(tcaps);
	if (old_y != y)
	{
		li = ft_termcaps_li();
		tcaps->abs_y += y - old_y;
		while (tcaps->abs_y > li)
		{
			FT_TPUTS(tcaps->cap.sf, 1);
			--tcaps->abs_y;
		}
	}
	tcaps->abs_x += len;
	tcaps->abs_x = (tcaps->abs_x - 1) % tcaps->co + 1;
	ft_termcaps_setpos(tcaps->abs_x, tcaps->abs_y, 0);
}

int
	ft_termcaps__ctrl_y(t_termcaps *tcaps)
{
	size_t	len;
	char	*clipboard;
	wchar_t	*wcsclipboard;
	wchar_t	*tmp;

	DEBUG;
	if ((len = ft_getlocal(CONST_CLIPBOARD, &clipboard)))
	{
		DEBUGSTR(clipboard);
		ft_memchk_exit(wcsclipboard = ft_mbstowcs_alloc(clipboard));
		if ((*tcaps->line))
		{
			tmp = ft_wcsjoin_inser((*tcaps->line), wcsclipboard,
								tcaps->pos_x + *tcaps->nquote);
			free(wcsclipboard);
			ft_memchk_exit(tmp);
		}
		else
			tmp = wcsclipboard;
		ft_termcaps__ctrl_k_del(tcaps, tcaps->abs_x, tcaps->abs_y);
		free((*tcaps->line));
		(*tcaps->line) = tmp;
		ft_termcaps__ctrl_y__pos(len, tcaps);
	}
	return (FL_CTRL_Y);
}
