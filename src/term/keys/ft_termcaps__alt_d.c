/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__alt_d.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:00:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:00:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

/*
** Retourne un pointeur sur la fin du premier mot trouvé
** un mot est tout caractere imprimable sauf " \t"
*/

int
	ft_termcaps__alt_d(t_termcaps *tcaps)
{
	int	x;
	int	relat_x;

	DEBUG;
	x = tcaps->pos_x + *tcaps->nquote;
	if ((*tcaps->line) && (*tcaps->line)[x])
	{
		ft_termcaps_rigth_wcs(tcaps, ft_wcsword);
		relat_x = tcaps->pos_x + *tcaps->nquote;
		if (x != relat_x)
		{
			DEBUGNBR(x);
			DEBUGNBR(relat_x);
			DEBUGWCS((*tcaps->line) + x);
			DEBUGWCS((*tcaps->line) + relat_x);
			ft_termcaps__ctrl_k_del(tcaps, tcaps->abs_x, tcaps->abs_y);
			ft_termcaps_setclipboard(FL_ALT_D, x, relat_x, (*tcaps->line));
			ft_wcscpy((*tcaps->line) + x, (*tcaps->line) + relat_x);
			tcaps->pos_x = x;
			DEBUGNBR(tcaps->pos_x);
		}
	}
	return (FL_ALT_D);
}
