/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_k.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/28 13:48:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/28 13:48:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "minishell.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

/*
** Par de la ligne y maximum puis remonte tout en suppriment n caractere
*/

static int
	ft_termcaps__ctrl_k__w(t_termcaps *tcaps, int abs_y)
{
	int	y_max;
	int	y_rel;

	DEBUG;
	y_max = ft_termcaps_rel_y_max(tcaps) - ft_termcaps_rel_y4(tcaps);
	y_rel = 0;
	DEBUGNBR(y_max);
	DEBUGNBR(y_rel);
	DEBUGNBR(ft_wcslen((*tcaps->line)));
	DEBUGNBR(tcaps->ppos_x);
	if (y_rel < y_max)
	{
		while (y_rel < y_max)
		{
			DEBUGNBR(y_max);
			ft_termcaps_clr_line(abs_y, y_max, tcaps->cap.al);
			--y_max;
		}
		return (1);
	}
	return (0);
}

void
	ft_termcaps__ctrl_k_del(t_termcaps *tcaps, int abs_x, int abs_y)
{
	char	s[2048 + 1];

	if (ft_termcaps__ctrl_k__w(tcaps, abs_y))
	{
		ft_termcaps_setpos(abs_x, abs_y, 1);
	}
	ft_termcaps_clr_n(abs_x, s);
	DEBUGSTR(s);
	FT_TPUTS(s, 1);
}

/*
** Si le curseur n'est pas en fin ligne
** si il y a des subligne
** retore position cuseur apres subline
** supprime n caractere
*/

int
	ft_termcaps__ctrl_k(t_termcaps *tcaps)
{
	int	relat_x;

	DEBUG;
	relat_x = tcaps->pos_x + *tcaps->nquote;
	if ((*tcaps->line) && (*tcaps->line)[relat_x])
	{
		DEBUGNBR(tcaps->abs_y);
		ft_termcaps__ctrl_k_del(tcaps, tcaps->abs_x, tcaps->abs_y);
		ft_termcaps_setclipboard(FL_CTRL_K,
								relat_x, ft_wcslen((*tcaps->line)),
								(*tcaps->line));
		(*tcaps->line)[relat_x] = 0;
	}
	return (FL_CTRL_K);
}
