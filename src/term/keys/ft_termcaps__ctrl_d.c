/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_d.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:03:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:03:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "pr_wchar_t.h"
#include "flags_bindkeys.h"

/*
** !! reasembler les frees au meme endroit
*/

static void
	ft_termcaps__ctrl_d__norm(t_termcaps *tcaps)
{
	if (*tcaps->line)
	{
		free(*tcaps->line);
		*tcaps->line = 0;
	}
}

int
	ft_termcaps__ctrl_d(t_termcaps *tcaps)
{
	wchar_t	*s;

	if (*tcaps->line && **tcaps->line)
	{
		s = *tcaps->line + *tcaps->nquote + tcaps->pos_x;
		DEBUGNBR(*tcaps->nquote);
		DEBUGNBR(*s);
		if (*s)
		{
			ft_termcaps__del(tcaps);
			return (FL_NONE);
		}
		DEBUGNBR(tcaps->pos_x);
		if (!*tcaps->nquote || tcaps->pos_x)
			return (FL_NONE);
		ft_termcaps__ctrl_e(tcaps);
	}
	else
		ft_termcaps__ctrl_d__norm(tcaps);
	FT_TPUTS(tcaps->cap.sf, 1);
	return (FL_CTRL_D);
}
