/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_e.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/26 06:10:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/26 06:10:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

/*
** Positionne le curseur a la fin de la ligne d'edition
*/

/*
** Voir pour scroller lors de abs_y negatif
** idem que pour ctrl+a
*/

int
	ft_termcaps__ctrl_e(t_termcaps *tcaps)
{
	int	len;
	int	x;
	int	y_max;

	DEBUG;
	len = (int)ft_wcslen((*tcaps->line) + *tcaps->nquote);
	if ((*tcaps->line) && tcaps->pos_x < len)
	{
		y_max = ft_termcaps_rel_y_max(tcaps);
		tcaps->abs_y += y_max - ft_termcaps_rel_y4(tcaps);
		tcaps->pos_x = len;
		x = ft_termcaps_rel_x(tcaps);
		DEBUGNBR(x);
		DEBUGNBR(tcaps->abs_y);
		ft_termcaps_setpos(x, tcaps->abs_y, y_max + 1);
	}
	return (FL_CTRL_E);
}
