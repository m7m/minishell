/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_c.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:04:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:04:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"

int
	ft_termcaps__ctrl_c(t_termcaps *tcaps)
{
	DEBUG;
	if (ft_termcaps_pos(STDIN_FILENO, NULL, &tcaps->abs_y))
		return (-1);
	ft_termcaps__ctrl_e(tcaps);
	FT_TPUTS(tcaps->cap.sf, 1);
	return (FL_CTRL_C);
}
