/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_arrow_updown.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/26 21:48:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/26 21:48:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

static int
	ft_termcaps__ctrl_arrow_updown__oldx(t_termcaps *tcaps)
{
	static int	oldx = 1;

	DEBUGNBR(oldx);
	if ((tcaps->old_bindkeys != FL_C_ARROW_D
		&& tcaps->old_bindkeys != FL_C_ARROW_U)
		|| !tcaps->pos_x)
		oldx = ft_termcaps_rel_x(tcaps);
	return (oldx);
}

/*
** x_new + (i - x) >= oldx) :
** defini si la distance est suffisante pour le recalage.
*/

static void
	ft_termcaps__ctrl_arrow_u_(int i, t_termcaps *tcaps)
{
	int		x_new;
	int		oldx;
	int		x;

	oldx = ft_termcaps__ctrl_arrow_updown__oldx(tcaps);
	x = ft_termcaps_rel_x(tcaps);
	tcaps->pos_x -= i;
	x_new = ft_termcaps_rel_x(tcaps);
	DEBUGNBR(oldx);
	DEBUGNBR(x);
	DEBUGNBR(x_new);
	DEBUGNBR(i);
	DEBUGNBR(tcaps->pos_x);
	if (((x_new < oldx && x_new == x)) && x_new + (i - x) >= oldx)
		tcaps->pos_x += oldx - x_new;
	else if (x_new > oldx)
	{
		if (tcaps->pos_x >= x_new - oldx)
			tcaps->pos_x -= x_new - oldx;
		else
			tcaps->pos_x = 0;
	}
	--tcaps->abs_y;
	DEBUGNBR(tcaps->pos_x);
	ft_termcaps_setpos(ft_termcaps_rel_x(tcaps), tcaps->abs_y, tcaps->abs_y);
}

int
	ft_termcaps__ctrl_arrow_u(t_termcaps *tcaps)
{
	int		i;
	wchar_t	*s;
	wchar_t	*send;

	DEBUG;
	if (ft_termcaps_rel_y4(tcaps))
	{
		s = *tcaps->line + *tcaps->nquote;
		send = s + tcaps->pos_x;
		i = 1;
		while (s < send - i && *(send - i) != L'\n' && i < tcaps->co)
			++i;
		DEBUGNBR(tcaps->co);
		DEBUGNBR(i);
		ft_termcaps__ctrl_arrow_u_(i, tcaps);
	}
	return (FL_C_ARROW_U);
}

static void
	ft_termcaps__ctrl_arrow_d_(int i, wchar_t *s, t_termcaps *tcaps)
{
	int		x_dist;
	int		oldx;

	oldx = ft_termcaps__ctrl_arrow_updown__oldx(tcaps);
	x_dist = oldx + i;
	DEBUGNBR(x_dist);
	if (s[i] == L'\n' && x_dist < tcaps->co && i <= x_dist)
	{
		DEBUG;
		++i;
		while (s[i] && s[i] != L'\n' && i < x_dist)
			++i;
	}
	DEBUGNBR(i);
	tcaps->pos_x += i;
	++tcaps->abs_y;
	DEBUGNBR(tcaps->pos_x);
	ft_termcaps_setpos(ft_termcaps_rel_x(tcaps), tcaps->abs_y, tcaps->abs_y);
}

int
	ft_termcaps__ctrl_arrow_d(t_termcaps *tcaps)
{
	int		i;
	wchar_t	*s;

	DEBUG;
	if (ft_termcaps_rel_y4(tcaps) < ft_termcaps_rel_y_max(tcaps))
	{
		s = *tcaps->line + *tcaps->nquote + tcaps->pos_x;
		i = 0;
		while (s[i] && s[i] != L'\n' && i < tcaps->co)
			++i;
		DEBUGNBR(tcaps->co);
		DEBUGNBR(i);
		DEBUGNBR(s[i]);
		ft_termcaps__ctrl_arrow_d_(i, s, tcaps);
	}
	return (FL_C_ARROW_D);
}
