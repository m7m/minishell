/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_r.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 15:51:25 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/29 15:51:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "pr_history.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"
#include "const_term.h"
#include "t_struct.h"

static void
	ft__ctrl_r_put(char *motif, wchar_t *line, t_scap cap)
{
	int	len;

	ft_putstr(REVERSE_I_SEARCH);
	if (motif)
		ft_putstr(motif);
	ft_putstr("': ");
	len = ft_strlen(REVERSE_I_SEARCH) + ft_strlen(motif) + 3;
	if (line)
		ft_term_print(line, cap, len, -1);
}

static int
	ft_termcaps__ctrl_r__ifret(int ret, t_termcaps *mod_tcaps)
{
	if (ret < 0 || ret == FL_CTRL_C || ret == FL_CTRL_D)
	{
		free(*mod_tcaps->line);
		*mod_tcaps->line = NULL;
		return (ret);
	}
	if (ret == FL_ARROW_R || ret == FL_ARROW_L
	|| ret == FL_ARROW_U || ret == FL_ARROW_D)
		return (1);
	return (0);
}

static void
	ft_termcaps__ctrl_r__ifret2(int ret,
								t_termcaps *modtcaps,
								struct s_term_ctrl_r *tctrl_r)
{
	DEBUG;
	if (!ret || ret == FL_CTRL_R)
	{
		if (!ret)
			ft_strcat(tctrl_r->motif, tctrl_r->buff);
		else if (ret == FL_CTRL_R)
		{
			if (!*tctrl_r->motif)
				ft_strcpy(tctrl_r->motif, tctrl_r->last_search);
			else if (tctrl_r->ichr)
				--tctrl_r->ichr;
		}
		if (!*tctrl_r->motif)
			return ;
		DEBUGSTR(tctrl_r->motif);
		tctrl_r->ichr = ft_gethistory_chrprev(tctrl_r->motif, tctrl_r->ichr);
		DEBUGNBR(tctrl_r->ichr);
		if (tctrl_r->ichr)
		{
			free(*modtcaps->line);
			*modtcaps->line = NULL;
			modtcaps->pos_x = ft_gethistory_wcs(tctrl_r->ichr, modtcaps->line);
		}
		DEBUGWCS(*modtcaps->line);
	}
}

static int
	ft_termcaps__ctrl_r__w(t_termcaps *mod_tcaps, struct s_term_ctrl_r *tctrl_r)
{
	int	ret;
	int	n;
	int	i;

	while ((n = read(STDIN_FILENO, tctrl_r->buff, 1024)) > 0
		&& n + ft_strlen(tctrl_r->motif) < 1024 && *tctrl_r->buff != 13)
	{
		DEBUGSTR(tctrl_r->buff);
		DEBUGWCS(*mod_tcaps->line);
		mod_tcaps->c = tctrl_r->buff;
		ret = ft_istermcap_test(0, mod_tcaps);
		mod_tcaps->ppos_x = tctrl_r->ppos_x + ft_strlen(tctrl_r->motif);
		DEBUGNBR(mod_tcaps->pos_x);
		ft_termcaps_clr_all(mod_tcaps);
		DEBUGNBR(ret);
		if (ft_termcaps__ctrl_r__ifret(ret, mod_tcaps))
			return (ret);
		if (ret == FL_BACKSPACE && (i = ft_strlen(tctrl_r->motif)))
			tctrl_r->motif[i - 1] = 0;
		else
			ft_termcaps__ctrl_r__ifret2(ret, mod_tcaps, tctrl_r);
		ft__ctrl_r_put(tctrl_r->motif, *mod_tcaps->line, mod_tcaps->cap);
		ft_memset(tctrl_r->buff, 0, 16);
	}
	return (-1);
}

int
	ft_termcaps__ctrl_r(t_termcaps *tcaps)
{
	int						ret;
	struct s_term_ctrl_r	tctrl_r;
	t_termcaps				mod_tcaps;

	DEBUG;
	ft_history_save_current_ligne((*tcaps->line));
	ft_termcaps_clr_all(tcaps);
	tctrl_r.ichr = 0;
	tcaps->pos_x = 0;
	*tcaps->nquote = 0;
	mod_tcaps = *tcaps;
	mod_tcaps.line = tcaps->line;
	tctrl_r.ppos_x = ft_strlen(REVERSE_I_SEARCH) + 3;
	mod_tcaps.home = REVERSE_I_SEARCH;
	ft_memset(tctrl_r.buff, 0, sizeof(tctrl_r.buff));
	ft_memset(tctrl_r.motif, 0, sizeof(tctrl_r.motif));
	ft__ctrl_r_put(0, *mod_tcaps.line, mod_tcaps.cap);
	if ((ret = ft_termcaps__ctrl_r__w(&mod_tcaps, &tctrl_r) < 0))
		return (ret);
	ft_strcpy(tctrl_r.last_search, tctrl_r.motif);
	DEBUG;
	return (FL_CTRL_R);
}
