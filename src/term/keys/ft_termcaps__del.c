/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__del.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/28 10:38:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/28 10:38:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "minishell.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

int
	ft_termcaps__del(t_termcaps *tcaps)
{
	size_t	len;
	int		relat_x;

	DEBUG;
	relat_x = tcaps->pos_x + *tcaps->nquote;
	len = ft_wcslen((*tcaps->line) + relat_x);
	if (len)
	{
		DEBUGWCS((*tcaps->line));
		FT_TPUTS(tcaps->cap.sc, ft_termcaps_li() - tcaps->abs_y);
		ft_termcaps__ctrl_k_del(tcaps, tcaps->abs_x, tcaps->abs_y);
		ft_memcpy((*tcaps->line) + relat_x,
				(*tcaps->line) + relat_x + 1, SW(len));
		DEBUGWCS((*tcaps->line));
		DEBUGWCS((*tcaps->line) + relat_x);
		FT_TPUTS(tcaps->cap.rc, ft_termcaps_li() - tcaps->abs_y);
	}
	return (FL_DEL);
}
