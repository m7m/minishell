/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__horiz_tab.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 09:01:09 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 09:01:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "sh_term.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"
#include "pr__static.h"
#include "pr_hash.h"

static int
	ft_termcaps__horiz_tab__sp_sl(char **tabmotif, int bool_m)
{
	size_t		len;
	struct stat	tmpstat;
	char		ci[2];
	char		*tmp;

	DEBUG;
	*ci = 0;
	ci[1] = 0;
	len = ft_strlen(*tabmotif);
	tmp = NULL;
	if (!bool_m && (ft_hchr(*ft__static_hexec(0), *tabmotif, len, (void **)&tmp)
					|| ft_hchr(*ft__static_hbuiltins(0), *tabmotif, len, NULL)))
	{
		if (tmp && !stat(tmp, &tmpstat) && S_ISDIR(tmpstat.st_mode))
			*ci = '/';
		else
			*ci = ' ';
		tmp = *tabmotif;
		*tabmotif = ft_strjoin(tmp, ci);
		free(tmp);
		ft_memchk_exit(*tabmotif);
		return (1);
	}
	return (0);
}

static int
	ft_termcaps__horiz_tab__inser(char *motif, char **tabmotif,
								t_termcaps *tcaps, int bool_m)
{
	int		len;
	char	*startmotif;
	char	*tmp;
	wchar_t	*add;
	wchar_t	*line;

	DEBUGSTR(*tabmotif);
	if ((tmp = ft_strrchr(motif, '/')))
		motif = tmp + 1;
	if (!ft_strcmp(*tabmotif, motif))
		return (1);
	len = ft_termcaps__horiz_tab__sp_sl(tabmotif, bool_m);
	startmotif = ft_termcaps__horiz_tab__antsp(len, motif, *tabmotif);
	ft_memchk_exit(add = ft_mbstowcs_alloc(startmotif));
	free(startmotif);
	len = ft_wcslen(add);
	line = ft_wcsjoin_inser((*tcaps->line), add, tcaps->pos_x + *tcaps->nquote);
	free(add);
	ft_memchk_exit(line);
	DEBUGWCS(line);
	free(*tcaps->line);
	*tcaps->line = line;
	tcaps->pos_x += len;
	ft_term_line_refresh(len, tcaps, line + *tcaps->nquote);
	return (0);
}

/*
** ajout du debut de tabmotif
*/

static int
	ft___horiz_tab__reas(char *motif, char **tabmotif, t_termcaps *tcaps,
						int bool_m)
{
	int		i;
	int		diff_len;
	int		diff_len_cmp;
	char	*psub;

	DEBUG;
	diff_len_cmp = 0;
	diff_len = ft_strpcmp(*tabmotif, tabmotif[1]);
	i = 1;
	while (tabmotif[i + 1] && diff_len > 0)
	{
		diff_len_cmp = ft_strpcmp(tabmotif[i], tabmotif[i + 1]);
		if (diff_len_cmp < diff_len)
			diff_len = diff_len_cmp;
		++i;
	}
	DEBUGNBR(diff_len);
	if (diff_len > 0)
	{
		ft_memchk_exit(psub = ft_strsub(tabmotif[--i], 0, diff_len));
		i = ft_termcaps__horiz_tab__inser(motif, &psub, tcaps, bool_m);
		free(psub);
		return (i);
	}
	return (1);
}

/*
** 1er if recherche $var
** 2eme recherche em mode: second argument
** 3eme recherche dans les binaires sinon identiaue au 2eme
*/

static void
	ft_termcaps__horiz_tab__search(int chrvar, int bool_m,
								char *motif, t_termcaps *tcaps)
{
	int		perm;
	int		itab;
	char	**tabmotif;

	DEBUGSTR(motif);
	DEBUGNBR(chrvar);
	perm = GET_PERM_RX | GET_DOTDIR | GET_DOTFILE;
	tabmotif = NULL;
	if (chrvar)
		itab = ft_search_in_h((const char *)++motif, &tabmotif,
							ft__static_hlocal(0), ft_hsearch_k);
	else if (*tcaps->nquote || bool_m)
		itab = ft_search(perm | GET_PERM_IGN, (const char *)motif, &tabmotif);
	else if (!(itab = ft_search_bin((const char *)motif, &tabmotif)))
		itab = ft_search(perm, (const char *)motif, &tabmotif);
	if (itab == 1)
		ft_termcaps__horiz_tab__inser(motif, tabmotif, tcaps, 0);
	if (itab > 1 && ft___horiz_tab__reas(motif, tabmotif, tcaps, 1)
		&& tcaps->old_bindkeys == FL_HORIZ_TAB)
		ft_term_print_pad(itab, tabmotif, tcaps->cap);
	if (itab)
		ft_tabfree(&tabmotif);
	ft_hdel_table_cont(ft__static_hexec(0));
}

int
	ft_termcaps__horiz_tab(t_termcaps *tcaps)
{
	char	*motif;
	int		notrplvar;
	wchar_t	*wcsmotif;
	wchar_t	*tmp;

	if (tcaps->line)
	{
		tmp = ft_wcssub(*tcaps->line, *tcaps->nquote, tcaps->pos_x);
		ft_memchk_exit(tmp);
		wcsmotif = ft_wcsnospace_leftv3(tmp);
		ft_wcsreplace(wcsmotif, L'\\', 0);
		if (wcsmotif > tmp && *wcsmotif == L'.' && wcsmotif[1] == 0)
			motif = ft_strdup("./.");
		else
			motif = ft_wcstombs_alloc(wcsmotif);
		ft_memchk_exit(motif);
		DEBUGSTR(motif);
		notrplvar = ft_isvar(motif);
		if (!notrplvar)
			ft_replace_var(&motif);
		ft_termcaps__horiz_tab__search(notrplvar, wcsmotif > tmp, motif, tcaps);
		free(tmp);
		free(motif);
	}
	return (FL_HORIZ_TAB);
}
