/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_a.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/26 06:15:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/26 06:15:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "flags_bindkeys.h"

/*
** Positionne le curseur au debut de la ligne d'edition
*/

/*
** Voir pour scroller lors de abs_y negatif
** while (abs_y < 1)
** {
** DEBUGSTR(tgetstr("sf", NULL));
** tputs(tgetstr("sf", NULL), 1, ft_putwchar_t);// down
** tputs(tgetstr("sr", NULL), 1, ft_putwchar_t);// up
** ++abs_y;
** }
*/

int
	ft_termcaps__ctrl_a(t_termcaps *tcaps)
{
	DEBUG;
	if (tcaps->pos_x)
	{
		DEBUGNBR(tcaps->abs_y);
		tcaps->abs_y -= ft_termcaps_rel_y4(tcaps);
		DEBUGNBR(tcaps->abs_y);
		DEBUGNBR(tcaps->ppos_x);
		ft_termcaps_setpos(tcaps->ppos_x + 1, tcaps->abs_y,
						ft_termcaps_rel_y_max(tcaps));
		tcaps->pos_x = 0;
	}
	return (FL_CTRL_A);
}
