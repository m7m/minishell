/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__alt_backspace.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:59:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:59:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

/*
** Kill from the cursor the start of the current word
*/

int
	ft_termcaps__alt_backspace(t_termcaps *tcaps)
{
	int	x;
	int	relat_x;

	DEBUG;
	x = tcaps->pos_x;
	if (x && ft_termcaps__arrow_cl(tcaps)
		&& x != tcaps->pos_x)
	{
		x += *tcaps->nquote;
		relat_x = tcaps->pos_x + *tcaps->nquote;
		ft_termcaps__ctrl_k_del(tcaps, tcaps->abs_x, tcaps->abs_y);
		ft_termcaps_setclipboard(FL_ALT_BACKSP, relat_x, x, (*tcaps->line));
		ft_memcpy((*tcaps->line) + relat_x,
				(*tcaps->line) + x,
				SW(ft_wcslen((*tcaps->line) + x) + 1));
		DEBUGNBR(relat_x);
		DEBUGNBR(x);
		DEBUGWCS((*tcaps->line) + relat_x);
	}
	return (FL_ALT_BACKSP);
}
