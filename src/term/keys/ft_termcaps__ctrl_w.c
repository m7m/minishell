/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps__ctrl_w.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:06:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:06:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "sh_term.h"
#include "def_bindkeys.h"
#include "flags_bindkeys.h"
#include "pr_wchar_t.h"

int
	ft_termcaps__ctrl_w(t_termcaps *tcaps)
{
	int	x;
	int	relat_x;

	DEBUG;
	x = tcaps->pos_x + *tcaps->nquote;
	if (x)
	{
		ft_termcaps_left_wcs(tcaps, ft_wcsnospace_left);
		relat_x = tcaps->pos_x + *tcaps->nquote;
		if (x != relat_x)
		{
			DEBUG;
			ft_termcaps__ctrl_k_del(tcaps, tcaps->abs_x, tcaps->abs_y);
			ft_termcaps_setclipboard(FL_CTRL_W, relat_x, x, (*tcaps->line));
			ft_memcpy((*tcaps->line) + relat_x,
					(*tcaps->line) + x,
					SW(ft_wcslen((*tcaps->line) + x) + 1));
			DEBUGNBR(relat_x);
			DEBUGNBR(x);
			DEBUGWCS((*tcaps->line) + relat_x);
		}
	}
	return (FL_CTRL_W);
}
