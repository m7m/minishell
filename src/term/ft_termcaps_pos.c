/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_pos.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/25 03:50:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/25 03:50:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh_term.h"
#include "minishell.h"
#include <limits.h>

/*
** Donne la position absolue du curseur
*/

#include <stdio.h>

static void
	ft_termcaps_pos__termtime0(void)
{
	struct termios	sterm;

	if ((tcgetattr(STDIN_FILENO, &sterm) < 0))
		ft_exit(EXIT_FAILURE, "Error: Not getattr tty\n");
	sterm.c_cc[VMIN] = 0;
	sterm.c_cc[VTIME] = 1;
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &sterm) < 0)
		ft_exit(EXIT_FAILURE, "Error: Not setattr tty\n");
}

static void
	ft_termcaps_pos__termtime1(void)
{
	struct termios	sterm;

	if ((tcgetattr(STDIN_FILENO, &sterm) < 0))
		ft_exit(EXIT_FAILURE, "Error: Not getattr tty\n");
	sterm.c_cc[VMIN] = 1;
	sterm.c_cc[VTIME] = 100;
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &sterm) < 0)
		ft_exit(EXIT_FAILURE, "Error: Not setattr tty\n");
}

static ssize_t
	ft_termcaps_pos__w(int fd, char cpos[])
{
	ssize_t	i;
	ssize_t	pass;
	ssize_t	n;
	char	*s;

	ft_memset(cpos, 0, 1024);
	pass = 0;
	i = 0;
	ft_termcaps_pos__termtime0();
	ft_putstr_fd("\x1B[6n", fd);
	while (!(s = ft_strchr(cpos, 'R')))
	{
		DEBUG;
		if ((n = read(fd, cpos + i, 254)) < 0
			|| i + n > 254 || pass == 2)
		{
			ft_termcaps_pos__termtime1();
			return (-1);
		}
		i += n;
		++pass;
	}
	*s = 0;
	ft_termcaps_pos__termtime1();
	return (0);
}

static int
	ft_termcaps_pos__if(int fd, int *pos_x, int *pos_y)
{
	char	cpos[1025];
	char	*chr;

	DEBUG;
	if (ft_termcaps_pos__w(fd, cpos))
		return (-1);
	DEBUGSTR(cpos);
	if (pos_y)
	{
		if (!(chr = ft_strchr(cpos, '[')))
			return (-1);
		*pos_y = ft_atoi(chr + 1);
		if (*pos_y < 1)
			*pos_y = 1;
	}
	if (pos_x)
	{
		if (!(chr = ft_strchr(cpos, ';')))
			return (-1);
		*pos_x = ft_atoi(chr + 1);
		if (*pos_x < 1)
			*pos_x = 1;
	}
	return (0);
}

int
	ft_termcaps_pos(int fd, int *pos_x, int *pos_y)
{
	if (ft_termcaps_pos__if(fd, pos_x, pos_y))
	{
		DEBUG;
		ft_sleep(INT_MAX / 1000);
		if (ft_termcaps_pos__if(fd, pos_x, pos_y))
		{
			DEBUG;
			ft_sleep(INT_MAX / 100);
			if (ft_termcaps_pos__if(fd, pos_x, pos_y))
			{
				MSG_ERR3INT(getpid(), "file descriptor not valide for read");
				ft_termcaps_pos__termtime1();
				return (-1);
			}
		}
	}
	return (0);
}
