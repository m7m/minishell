/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_free_notglobal.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 05:27:13 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 05:27:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_FREE_NOTGLOBAL_H
# define F_FREE_NOTGLOBAL_H

void			f_free_notglobal_cal(void);
struct s_free	*f_free_notglobal__static(void);

#endif
