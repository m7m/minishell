/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_updatekey_.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 23:49:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 23:49:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_updatekey_ret(t_htable *hlocal, int ret)
{
	t_hupdate	hupd;
	char		cc_itoa[128];

	DEBUGNBR(ret);
	hupd.key = "?";
	hupd.key_len = SC(1);
	hupd.data_len = ft_itoa_s(ret, cc_itoa) + 1;
	hupd.data = cc_itoa;
	if (ft_hupd(hlocal, &hupd))
		free(hupd.data);
}

void
	ft_updatekey__(t_htable *henvp, char *cmd)
{
	t_hupdate	hupd;

	DEBUG;
	hupd.key = "_";
	hupd.key_len = SC(1);
	if (cmd)
		hupd.data_len = ft_strlen(cmd) + 1;
	else
		hupd.data_len = 0;
	DEBUGNBR(hupd.data_len);
	hupd.data = (void *)cmd;
	if (ft_hupd(henvp, &hupd))
	{
		DEBUG;
		DEBUGPTR(hupd.data);
		free(hupd.data);
	}
	DEBUG;
}
