/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__init.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 15:39:50 by sdahan            #+#    #+#             */
/*   Updated: 2016/12/30 16:02:17 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void
	ft_add_opt_unary(t_opt_unary *tbt, char *opt,
	int (*f)(const char *))
{
	tbt->f = f;
	tbt->opt = opt;
}

static void
	ft_add_opt_binary(t_opt_binary *tbt, char *opt,
	int (*f)(int op, char **argv))
{
	tbt->f = f;
	tbt->opt = opt;
}

void
	ft_builtins_test__init_opt_unary(t_opt_unary *tbt)
{
	ft_add_opt_unary(tbt++, "b", ft_builtins_test__opt_unary_b);
	ft_add_opt_unary(tbt++, "c", ft_builtins_test__opt_unary_c);
	ft_add_opt_unary(tbt++, "d", ft_builtins_test__opt_unary_d);
	ft_add_opt_unary(tbt++, "e", ft_builtins_test__opt_unary_e);
	ft_add_opt_unary(tbt++, "f", ft_builtins_test__opt_unary_f);
	ft_add_opt_unary(tbt++, "g", ft_builtins_test__opt_unary_g);
	ft_add_opt_unary(tbt++, "h", ft_builtins_test__opt_unary_h);
	ft_add_opt_unary(tbt++, "k", ft_builtins_test__opt_unary_k);
	ft_add_opt_unary(tbt++, "n", ft_builtins_test__opt_unary_n);
	ft_add_opt_unary(tbt++, "p", ft_builtins_test__opt_unary_p);
	ft_add_opt_unary(tbt++, "r", ft_builtins_test__opt_unary_r);
	ft_add_opt_unary(tbt++, "s", ft_builtins_test__opt_unary_s);
	ft_add_opt_unary(tbt++, "t", ft_builtins_test__opt_unary_t);
	ft_add_opt_unary(tbt++, "u", ft_builtins_test__opt_unary_u);
	ft_add_opt_unary(tbt++, "w", ft_builtins_test__opt_unary_w);
	ft_add_opt_unary(tbt++, "x", ft_builtins_test__opt_unary_x);
	ft_add_opt_unary(tbt++, "z", ft_builtins_test__opt_unary_z);
	ft_add_opt_unary(tbt++, "L", ft_builtins_test__opt_unary_h);
	ft_add_opt_unary(tbt++, "O", ft_builtins_test__opt_unary_bigo);
	ft_add_opt_unary(tbt++, "G", ft_builtins_test__opt_unary_bigg);
}

void
	ft_builtins_test__init_opt_binary(t_opt_binary *tob)
{
	ft_add_opt_binary(tob++, "-nt", ft_builtins_test__opt_binary_nt);
	ft_add_opt_binary(tob++, "-ot", ft_builtins_test__opt_binary_ot);
	ft_add_opt_binary(tob++, "-ef", ft_builtins_test__opt_binary_ef);
	ft_add_opt_binary(tob++, "-eq", ft_builtins_test__opt_binary_eq);
	ft_add_opt_binary(tob++, "-ne", ft_builtins_test__opt_binary_ne);
	ft_add_opt_binary(tob++, "-gt", ft_builtins_test__opt_binary_gt);
	ft_add_opt_binary(tob++, "-ge", ft_builtins_test__opt_binary_ge);
	ft_add_opt_binary(tob++, "-lt", ft_builtins_test__opt_binary_lt);
	ft_add_opt_binary(tob++, "-le", ft_builtins_test__opt_binary_le);
	ft_add_opt_binary(tob++, "=", ft_builtins_test__opt_binary_seq);
	ft_add_opt_binary(tob++, "!=", ft_builtins_test__opt_binary_nseq);
}
