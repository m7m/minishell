/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gethistory_search.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 19:46:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/29 19:46:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_history.h"
#include "pr_history.h"
#include "pr_wchar_t.h"

static size_t
	ft_gethistory_cprev(char **data)
{
	int		key_len;
	size_t	data_len;
	char	skey[255];

	DEBUG;
	data_len = 0;
	if (g_thistory.id_key_h > 0)
	{
		key_len = ft_itoa_s(g_thistory.id_key_h, skey);
		*data = NULL;
		data_len = ft_hchr(*g_thistory.thhistory, skey, key_len, (void **)data);
		if (data_len > 0)
			--data_len;
	}
	return (data_len);
}

/*
** Retourne l'index si trouve sinon 0
*/

size_t
	ft_gethistory_chrprev(char *motif, int startindexsearch)
{
	size_t	data_len;
	char	*tmp;

	DEBUGNBR(startindexsearch);
	DEBUGSTR(motif);
	DEBUGNBR(g_thistory.thhistory->nb_elem);
	DEBUGNBR(g_thistory.id_key_h);
	if (!g_thistory.id_key_h || !startindexsearch)
		g_thistory.id_key_h = g_thistory.thhistory->nb_elem;
	else
		g_thistory.id_key_h = startindexsearch;
	DEBUGNBR(g_thistory.id_key_h);
	data_len = ft_gethistory_cprev(&tmp);
	if (!data_len)
		g_thistory.id_key_h = g_thistory.thhistory->nb_elem;
	DEBUGSTR(tmp);
	DEBUGSTR(motif);
	while (g_thistory.id_key_h && (!tmp || !ft_strstr(tmp, motif)))
	{
		--g_thistory.id_key_h;
		data_len = ft_gethistory_cprev(&tmp);
	}
	if (data_len)
		return (g_thistory.id_key_h);
	return (0);
}

size_t
	ft_gethistory_chrprev_cmp(char *motif, int startindexsearch)
{
	size_t	motif_len;
	size_t	data_len;
	char	*tmp;

	DEBUGNBR(startindexsearch);
	DEBUGSTR(motif);
	DEBUGNBR(g_thistory.thhistory->nb_elem);
	DEBUGNBR(g_thistory.id_key_h);
	if (!g_thistory.id_key_h || !startindexsearch)
		g_thistory.id_key_h = g_thistory.thhistory->nb_elem;
	else
		g_thistory.id_key_h = startindexsearch;
	DEBUGNBR(g_thistory.id_key_h);
	data_len = ft_gethistory_cprev(&tmp);
	if (!data_len)
		g_thistory.id_key_h = g_thistory.thhistory->nb_elem;
	motif_len = ft_strlen(motif);
	while (g_thistory.id_key_h && ft_strncmp(tmp, motif, motif_len))
	{
		--g_thistory.id_key_h;
		data_len = ft_gethistory_cprev(&tmp);
	}
	if (data_len)
		return (g_thistory.id_key_h);
	return (0);
}
