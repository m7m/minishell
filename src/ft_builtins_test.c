/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 10:36:00 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 10:50:32 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_checkopt_unary(const char *s)
{
	if (s[0] != '-')
		return (0);
	if (ft_strchr(OPT_UNARY, s[1]))
		return (1);
	return (0);
}

int
	ft_one_arg(int *pos, char **argv)
{
	return (!(argv[(*pos)++][0] != '\0'));
}

static int
	ft_two__arg(int *pos, char **argv)
{
	if (argv[*pos][0] == '!')
		return (argv[*pos - 1][0] != '!' ?
		!ft_one_arg(pos, argv) : ft_builtins_test_error("too many arguments"));
	else if (argv[*pos][0] == '-' &&
		argv[*pos][1] != '\0' &&
		argv[*pos][2] == '\0')
	{
		if (ft_checkopt_unary(argv[*pos]))
			return (ft_exec_unary(pos, argv));
		if (argv[*pos][1] == 'a' || argv[*pos][1] == 'o')
			return (BUILTIN_FALSE);
	}
	MSG_ERR4("test", argv[*pos], "unary operator expected");
	return (BUILTIN_FAILURE);
}

static int
	ft_three__arg(int *pos, char **argv)
{
	int		ret;

	if ((ret = ft_exec_binary(pos, argv)) != -1)
		return (ret);
	else if (!ft_strcmp(argv[*pos], "!"))
	{
		++(*pos);
		ret = ft_two__arg(pos, argv);
		ret = (ret != BUILTIN_FAILURE) ? !ret : ret;
		++(*pos);
		return (ret);
	}
	else if (!ft_strcmp(argv[*pos + 1], "-a") ||
		!ft_strcmp(argv[*pos + 1], "-o"))
	{
		if (argv[*pos + 1][1] == 'a')
			return (!(argv[*pos][0] && argv[*pos + 2][0]));
		if (argv[*pos + 1][1] == 'o')
			return (!(argv[*pos][0] || argv[*pos + 2][0]));
		return (-42);
	}
	MSG_ERR4("test", argv[*pos], "binary operator expected");
	return (BUILTIN_FAILURE);
}

void
	ft_builtins_test(t_sh *sh, t_argv *targv)
{
	int				ret;
	int				pos;

	pos = 1;
	ret = BUILTIN_FALSE;
	if (targv->argc - 1 == 1)
		ret = ft_one_arg(&pos, targv->argv);
	else if (targv->argc - 1 == 2)
		ret = ft_two__arg(&pos, targv->argv);
	else if (targv->argc - 1 == 3)
		ret = ft_three__arg(&pos, targv->argv);
	else if (targv->argc - 1 == 4 && !ft_strcmp(targv->argv[pos], "!"))
	{
		++pos;
		ret = ft_three__arg(&pos, targv->argv);
		ret = (ret != BUILTIN_FAILURE) ? !ret : ret;
	}
	else if (targv->argc - 1 >= 4)
	{
		ret = ft_builtins_test__parser(&pos, targv->argc, targv->argv);
		ret = (ret != BUILTIN_FAILURE) ? !ret : ret;
		if (ret != BUILTIN_FAILURE && pos < targv->argc)
			ret = ft_builtins_test_error("too many arguments");
	}
	ft_updatekey_ret(&sh->hlocal, ret);
}
