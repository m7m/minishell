/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:05:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:05:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char
	**ft_parse_arg(int narg, char **argv, int *retopt)
{
	int		i;
	int		j;
	char	**arg;

	i = 0;
	j = 0;
	arg = NULL;
	DEBUGNBR(narg);
	if (narg > 1)
	{
		if (!(arg = (char **)malloc(sizeof(char *) * (narg + 1))))
			ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
		arg[narg] = NULL;
		while (++i < narg)
		{
			if (ft_parse_opt(retopt, argv[i]))
			{
				arg[j] = argv[i];
				++j;
			}
		}
		arg[j] = NULL;
	}
	return (arg);
}
