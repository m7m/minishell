/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xetlocal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:46:22 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:46:22 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_hash.h"
#include "pr_wchar_t.h"
#include "pr__static_hlocal.h"

/*
** Chercher key dans hlocal
** Si *data = NULL = len = 0 = non trouvé
** La taille n'inclus pas le \0
*/

size_t
	ft_getlocal(char *key, char **data)
{
	size_t		len;
	t_htable	hlocal;

	DEBUG;
	hlocal = *ft__static_hlocal(NULL);
	len = ft_hchr(hlocal, key, ft_strlen(key), (void **)data);
	if (len)
		len = ft_mbslen((char *)*data);
	return (len);
}

size_t
	ft_getnlocal(size_t nkey, char *key, char **data)
{
	size_t		len;
	t_htable	hlocal;

	DEBUG;
	hlocal = *ft__static_hlocal(NULL);
	len = ft_hchr(hlocal, key, nkey, (void **)data);
	if (len)
		len = ft_mbslen((char *)*data);
	return (len);
}

int
	ft_getlocalint(char *key)
{
	int			value;
	size_t		len;
	char		*data;
	t_htable	hlocal;

	DEBUG;
	hlocal = *ft__static_hlocal(NULL);
	len = ft_hchr(hlocal, key, ft_strlen(key), (void **)&data);
	if (len)
		value = ft_atoi(data);
	else
		value = 0;
	DEBUGNBR(value);
	return (value);
}
