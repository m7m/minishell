/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_unary_rw.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 22:31:22 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:06:20 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test__opt_unary_r(const char *file)
{
	return (access(file, R_OK) == -1);
}

int
	ft_builtins_test__opt_unary_s(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && (st.st_size > 0)));
}

int
	ft_builtins_test__opt_unary_t(const char *str)
{
	long		fd;

	fd = ft_atol(str);
	return (!(0 <= fd && fd <= INT_MAX && isatty(fd)));
}

int
	ft_builtins_test__opt_unary_u(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && (st.st_mode & S_IFMT) == S_ISUID));
}

int
	ft_builtins_test__opt_unary_w(const char *file)
{
	return (access(file, W_OK) == -1);
}
