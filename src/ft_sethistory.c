/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sethistory.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 12:46:39 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/01 12:46:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "t_history.h"
#include "pr_history.h"
#include "const_term.h"
#include "pr_xetlocal.h"

/*
** met le thash en dernier et le retourne
*/

static t_hash
	*ft_history_swap(t_hash *thash, t_htable thhis)
{
	int		id;
	size_t	skey_len;
	char	skey[255];
	char	*data;
	t_hash	*thashn;

	DEBUGSTR((char *)thash->key);
	DEBUGSTR((char *)thash->data);
	if ((id = ft_atoi(thash->key) + 1) < 0)
		return (NULL);
	skey_len = ft_itoa_s(id, skey);
	data = (char *)thash->data;
	while ((thashn = ft_hchr_th(thhis, (void *)skey, skey_len)))
	{
		thash->data = thashn->data;
		thash->data_len = thashn->data_len;
		thash = thashn;
		if ((id = ft_atoi(thash->key) + 1) < 0)
			return (NULL);
		skey_len = ft_itoa_s(id, skey);
	}
	thash->data = (void *)data;
	thash->data_len = ft_strlen((char *)data) + 1;
	return (thash);
}

void
	ft_delhistory(char *key, t_htable *thhis)
{
	t_hash	*thash;

	DEBUG;
	if ((thash = ft_hchr_th(*thhis, (void *)key, ft_strlen(key))))
	{
		thash = ft_history_swap(thash, *thhis);
		DEBUGSTR((char *)thash->key);
		DEBUGSTR((char *)thash->data);
		ft_hdelkey_table(thhis, thash->key, thash->key_len);
	}
}

/*
** -1 : erreur
** 0 : historique rearanger
** > 0 : nouveau a ajouter
*/

int
	ft_history_isexisttolast(char *data, t_htable thhis)
{
	t_hash	*thash;

	DEBUG;
	if (!data
		|| !(thash = ft_hchr_d(thhis, (void *)data, ft_strlen(data) + 1))
		|| !(thash = ft_history_swap(thash, thhis)))
		return (thhis.nb_elem + 1);
	DEBUGSTR((char *)thash->key);
	DEBUGSTR((char *)thash->data);
	return (0);
}

void
	ft_delhistory_id(int id, t_htable *thhis)
{
	char	skey[255];

	ft_itoa_s(id, skey);
	ft_delhistory(skey, thhis);
}

void
	ft_history_limit(t_htable *thhis)
{
	int			limit;
	size_t		slimit;

	limit = ft_getlocalint(CONST_HISTSIZE);
	if (limit < 1 || limit > 90000)
		slimit = 500;
	else
		slimit = (size_t)limit;
	limit = (int)(thhis->nb_elem - slimit);
	if (limit > 0)
		limit += 2;
	while (limit > 0)
	{
		DEBUGNBR(limit);
		ft_delhistory_id(limit, thhis);
		--limit;
	}
}
