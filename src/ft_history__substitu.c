/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history__substitu.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 20:18:59 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 20:18:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/pr_history.h"
#include "../inc/l_sh.h"
#include <libft.h>

/*
** voir !:s
*/

int
	ft_history_parser__substitu(char **line)
{
	size_t	i;
	char	*first;
	char	*end;
	char	*tmp;
	char	*newline;

	first = *line + 1;
	if (!(end = ft_strchr(first, '^')) || end == first)
		return (-1);
	*end = 0;
	++end;
	if (!(i = ft_gethistory_chrprev(first, 0)))
		return (-1);
	i = ft_gethistory_id_nodup(i, line);
	newline = ft_strdup_resize(*line, i, ft_strlen(first));
	ft_memchk_exit(newline);
	;
	tmp = ft_strstr(newline, first);
	ft_strcpy(tmp, end);
	ft_strcpy(tmp + ft_strlen(end), *line + (tmp - newline) + ft_strlen(first));
	;
	free(first - 1);
	*line = newline;
	ft_history_add(*line, 1);
	return (0);
}
