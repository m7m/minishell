/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_unary_gp.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 18:49:28 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:01:19 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#ifndef S_ISVTX
# define S_ISVTX 0001000
#endif

int
	ft_builtins_test__opt_unary_g(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && (st.st_mode & S_IFMT) == S_ISGID));
}

int
	ft_builtins_test__opt_unary_h(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(lstat(file, &st) == 0 && S_ISLNK(st.st_mode)));
}

int
	ft_builtins_test__opt_unary_k(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && (st.st_mode & S_IFMT) == S_ISVTX));
}

int
	ft_builtins_test__opt_unary_n(const char *str)
{
	return (str[0] == '\0');
}

int
	ft_builtins_test__opt_unary_p(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && S_ISFIFO(st.st_mode)));
}
