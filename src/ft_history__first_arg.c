/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history__first_arg.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 18:11:30 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 18:11:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/t_history.h"
#include "../inc/pr_history.h"
#include "../inc/l_sh.h"
#include <libft.h>

static void
	ft_history_parser__first_arg__datalen(
		char **line, char **pline, char *data)
{
	size_t	data_len;
	char	*tmp;
	char	c;

	DEBUGSTR(*line);
	tmp = *line;
	data_len = 0;
	while (data[data_len] && !ft_strchr(" \n\t", data[data_len]))
	{
		if (data[data_len] == '\\')
			++data_len;
		++data_len;
	}
	DEBUGSTR(data);
	DEBUGSTR(pline - 1);
	c = data[data_len];
	data[data_len] = 0;
	*line = ft_strjoin_inser(*line, data, (*pline - 1) - *line);
	data[data_len] = c;
	free(tmp);
	ft_memchk_exit(*line);
	DEBUGSTR(*line);
	*pline = *line + (*pline - tmp) + (data_len - 1);
	DEBUGSTR(*pline);
}

void
	ft_history_parser__first_arg(
		char **line, char **pline)
{
	size_t	data_len;
	char	*data;

	DEBUGSTR(*line);
	data_len = ft_gethistory_id_nodup(g_thistory.thhistory->nb_elem, &data);
	ft_strcpy(*pline - 1, *pline + 1);
	if (data_len)
		ft_history_parser__first_arg__datalen(
			line, pline, data);
	else
		*pline = *pline - 1;
}
