/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft__static_hlocal.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 09:12:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/22 09:12:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_hash.h"

t_htable
	*ft__static_hlocal(t_htable *hlocal)
{
	static t_htable	*static_hlocal = 0;

	if (hlocal)
		static_hlocal = hlocal;
	return (static_hlocal);
}
