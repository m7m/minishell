/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 20:09:16 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/12 20:09:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/t_history.h"
#include "../inc/pr_history.h"
#include "../inc/l_sh.h"
#include "../inc/flags.h"
#include "../inc/minishell.h"
#include <libft.h>

static int
	ft_history_parser_while_if(char **line, char **pline)
{
	DEBUGSTR(*pline);
	if (**pline == '!')
		ft_history_parser__last_cmd(
			g_thistory.thhistory->nb_elem, 1, line, pline);
	else if (**pline == '$')
		ft_history_parser__last_arg(line, pline);
	else if (**pline == '^')
		ft_history_parser__first_arg(line, pline);
	else
	{
		if ((**pline == '-' && ft_isdigit(pline[0][1])
				&& !ft_history_parser__digit(1, line, pline))
			||
			(**pline == '?' && !ft_history_parser__chrword(
				1, line, pline, ft_gethistory_chrprev))
			||
			(**pline && (
					(ft_isdigit(**pline)
						&& !ft_history_parser__digit(0, line, pline))
					|| !ft_history_parser__chrword(
						0, line, pline, ft_gethistory_chrprev_cmp))))
			return (0);
		return (-1);
	}
	return (0);
}

static int
	ft_history_parser_while(char **line)
{
	long	flags;
	char	*pline;

	flags = 0;
	pline = *line;
	while (*pline)
	{
		if (*pline == '!'
			&& !(flags & ~(FL_SQUOT))
			&& !ft_strchr(" \t\n=(", pline[1]))
		{
			++pline;
			if (ft_history_parser_while_if(line, &pline))
				return (-1);
		}
		else
		{
			ft_linecomplete_flags_c(pline, &flags);
			++pline;
		}
	}
	if (**line != ' ')
		ft_history_add(*line, 1);
	return (0);
}

int
	ft_history_parser(char **line)
{
	char	*diff;

	diff = *line;
	if (**line == '^')
	{
		if (ft_history_parser__substitu(line))
		{
			MSG_ERR3("!^", "error substitution");
			return (-1);
		}
	}
	if (ft_history_parser_while(line) == -1)
	{
		MSG_ERR3("!", "error event");
		return (-1);
	}
	if (diff != *line)
		ft_putendl(*line);
	return (0);
}
