/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_env.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:55:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:55:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "targv.h"

static int
	ft_builtins_env__conf(char **argv, t_htable *thtabletmp, t_htable *thtable)
{
	int			itabstr;

	DEBUGPTR(argv);
	DEBUGSTR(*argv);
	itabstr = 0;
	if (**argv == '-' && argv[0][1] == 'i')
	{
		argv += 1;
		DEBUGSTR(argv[itabstr]);
		while (argv[itabstr] && ft_strchr(argv[itabstr], '='))
			++itabstr;
		ft_tabtohash_size(argv, itabstr, thtabletmp);
		++itabstr;
	}
	else
	{
		DEBUGSTR(argv[itabstr]);
		while (argv[itabstr] && ft_strchr(argv[itabstr], '='))
			++itabstr;
		ft_tabtohash_size(argv, itabstr, thtabletmp);
		ft_hdup_table(thtabletmp, thtable);
	}
	return (itabstr);
}

static void
	ft_builtins_env__exec(int itab, t_htable *thtable,
						t_sh *sh, t_argv *targv)
{
	t_argv	*next;

	DEBUGNBR(targv->argc);
	DEBUGNBR(itab);
	DEBUGSTR(targv->argv[itab]);
	if (targv->argv[itab])
	{
		next = NULL;
		ft_memchk_exit(ft_targv_newadd(&next));
		free(next->argv);
		ft_memchk_exit(next->argv = ft_tabdup(targv->argv + itab));
		next->argc = targv->argc - itab;
		DEBUGSTR(*next->argv);
		ft_isexec(sh, next);
		free(next->argv);
		free(next->envp);
		free(next);
	}
	else
	{
		DEBUG;
		ft_hashto_keyeqdata_write(*thtable);
	}
}

void
	ft_builtins_env__init(t_sh *sh, t_htable *thtable, t_argv *targv)
{
	size_t		num_table;
	size_t		nb_elem;
	t_htable	thtabletmp;
	int			itab;

	COM("initialise table");
	ft_hnew_table(thtable->hdb, &thtabletmp, 1);
	num_table = thtable->num_table;
	nb_elem = thtable->nb_elem;
	;
	itab = ft_builtins_env__conf(targv->argv + 1, &thtabletmp, thtable) + 1;
	COM("execute");
	thtable->num_table = thtabletmp.num_table;
	thtable->nb_elem = thtabletmp.nb_elem;
	ft_builtins_env__exec(itab, thtable, sh, targv);
	COM("restore");
	thtable->num_table = num_table;
	thtable->nb_elem = nb_elem;
	thtabletmp.num_table &= ~num_table;
	ft_hdel_table(&thtabletmp);
}

void
	ft_builtins_env(t_sh *sh, t_argv *targv)
{
	DEBUGPTR(targv->argv);
	ft_updatekey_ret(&sh->hlocal, 0);
	if (targv->argc == 1)
		ft_hashto_keyeqdata_write(sh->henvp);
	else
		ft_builtins_env__init(sh, &sh->henvp, targv);
}
