/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test_exec.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 14:13:29 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:15:23 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_exec_unary(int *pos, char **argv)
{
	int				i;
	int				ret;
	t_opt_unary		tbt[NB_OPT_UNARY + 1];

	i = -1;
	ft_bzero(tbt, sizeof(t_opt_unary) * (NB_OPT_UNARY + 1));
	ft_builtins_test__init_opt_unary(tbt);
	while (tbt[++i].opt)
	{
		if (!ft_strcmp(tbt[i].opt, &argv[*pos][1]))
		{
			ret = tbt[i].f(argv[++(*pos)]);
			return (ret);
		}
	}
	return (0);
}

int	ft_exec_binary(int *pos, char **argv)
{
	int				i;
	int				ret;
	t_opt_binary	tob[NB_OPT_BINARY + 1];

	i = -1;
	ft_bzero(tob, sizeof(t_opt_binary) * (NB_OPT_BINARY + 1));
	ft_builtins_test__init_opt_binary(tob);
	while (tob[++i].opt)
	{
		if (!ft_strcmp(tob[i].opt, argv[*pos + 1]))
		{
			ret = tob[i].f(*pos + 1, argv);
			*pos = (*pos) + 3;
			return (ret);
		}
	}
	return (-1);
}
