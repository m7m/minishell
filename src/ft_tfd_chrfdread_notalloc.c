/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tfd_chrfdread_notalloc.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/08 17:50:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/08 17:50:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tfd.h"

t_fd
	*ft_tfd_chrfdread_notalloc(t_fd *node, int fd)
{
	while (node && node->pipe[0] != fd)
		node = node->next;
	return (node);
}
