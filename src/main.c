/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 15:07:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 15:07:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <signal.h>
#include "f_free_notglobal.h"
#include "../inc/pr__static_tjobs.h"
#include "../inc/built_in_jobs_new.h"

static void
	f_istty(t_sh *sh)
{
	struct stat		stat_stdin;

	if (!fstat(STDIN_FILENO, &stat_stdin) && S_ISCHR(stat_stdin.st_mode))
	{
		DEBUG;
		sh->opt |= SH_TTY;
		ft_sigaction_init();
	}
}

int
	main(int arg, char **argv, char **envp)
{
	t_sh			sh[1];

	(void)arg;
	(void)argv;
	DEBUGSTR(*argv);
	ft__static_sh(sh);
	ft_memset(&sh->tljobs, 0, sizeof(t_builtins_jobs));
	sh->sf = f_free_notglobal__static();
	ft_memset(sh->sf, 0, sizeof(sh->sf));
	sh->tljobs.tjobs = ft_jobs_newalloc();
	sh->tljobs.sh_opt = &sh->opt;
	ft__static_tjobs(&sh->tljobs.tjobs);
	ft_atexit(ft_jobs_free_static);
	ft_atexit(ft_signal_reset_all);
	sh->opt = 0;
	sh->tet->c = 0;
	sh->targv = NULL;
	f_istty(sh);
	ft_init_hdb_tables(sh, envp);
	ft_prompt(sh);
	return (0);
}
