/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_history.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:31:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:31:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr_history.h"

static void
	ft_builtins_history_opt(t_sh *sh, t_argv *targv)
{
	if (!ft_strcmp(targv->argv[1], "-c"))
		ft_hdel_table_cont(&sh->hhistory);
	else if (!ft_strcmp(targv->argv[1], "-d") && targv->argv[2])
		ft_delhistory(targv->argv[2], &sh->hhistory);
	else if (!ft_strcmp(targv->argv[1], "-w"))
		ft_history_write();
	else if (!ft_strcmp(targv->argv[1], "-r"))
		ft_history_read();
}

void
	ft_builtins_history(t_sh *sh, t_argv *targv)
{
	size_t	i;
	size_t	key_len;
	size_t	pad;
	char	skey[255];
	char	*data;

	DEBUG;
	if (targv->argc == 1)
	{
		i = 1;
		skey[0] = '1';
		skey[1] = 0;
		key_len = 1;
		pad = ft_intlen(sh->hhistory.nb_elem) + 2;
		while (ft_gethistory(skey, key_len, &data))
		{
			ft_printf("%*s %s\n", pad, skey, data);
			free(data);
			key_len = ft_itoa_s(++i, skey);
		}
		ft_fflush(STDOUT_FILENO);
	}
	else if (targv->argv[1][0] == '-')
		ft_builtins_history_opt(sh, targv);
	ft_updatekey_ret(&sh->hlocal, 0);
}
