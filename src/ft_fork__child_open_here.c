/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork__child_open_here.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 16:01:49 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 16:01:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"
#include "etat/redirecting/etat_redirecting.h"

static int
	ft_fork__child_open_heretoetat(t_fd *t, t_sh *sh)
{
	int					ret;
	struct s_linetoargv	tlta;

	DEBUG;
	ft_memset(&tlta, 0, sizeof(struct s_linetoargv));
	if (t->opt & SFD_HERE_DOC_TAB)
		tlta.flags |= FL_HEREDT;
	tlta.line = t->str;
	tlta.pline = tlta.line;
	tlta.sh = sh;
	t->str = NULL;
	ret = ft_etat_redirecting(&tlta);
	if (ret)
	{
		free(tlta.line);
		return (-1);
	}
	t->str = tlta.line;
	return (0);
}

#ifdef __linux__

static void
	ft_fork__child_open_here__fork_child(int p[2], t_fd *t, t_argv *targv)
{
	size_t	len;
	char	s;

	DEBUGPTR(t);
	DEBUGSTR(t->str);
	close(p[0]);
	if (t->str)
	{
		len = ft_strlen(t->str);
		if (len && t->opt & SFD_HERE_LIN)
		{
			s = t->str[len - 1];
			ft_strreplace(t->str, '\n', ' ');
			if (s == '\n')
				t->str[len - 1] = '\n';
		}
		write(p[1], t->str, len);
		if (!len || t->str[len - 1] != '\n')
			write(p[1], "\n", sizeof(char));
	}
	close(p[1]);
	ft_fork__child_exit(EXIT_SUCCESS, 0, targv);
}

#else

static void
	ft_fork__child_open_here__fork_child(int p[2], t_fd *t, t_argv *targv)
{
	size_t	len;

	DEBUGPTR(t);
	DEBUGSTR(t->str);
	close(p[0]);
	if (t->str)
	{
		len = ft_strlen(t->str);
		write(p[1], t->str, len);
		if (!len || t->str[len - 1] != '\n')
			write(p[1], "\n", sizeof(char));
	}
	close(p[1]);
	ft_fork__child_exit(EXIT_SUCCESS, 0, targv);
}

#endif

static int
	ft_fork__child_open_here__fork(t_fd *t, t_argv *targv)
{
	int		p[2];
	pid_t	pc;

	pc = 0;
	if (pipe(p) || (pc = fork()) == -1)
		return (-1);
	else if (!pc)
		ft_fork__child_open_here__fork_child(p, t, targv);
	else
	{
		DEBUGNBR(t->pipe[0]);
		DEBUGNBR(t->pipe[1]);
		if (dup2(p[0], t->pipe[1]) == -1)
			return (ft_perror_dup(t->pipe[1], "dup2() file descriptor"));
		close(p[1]);
		t->pipe[0] = p[0];
	}
	return (0);
}

int
	ft_fork__child_open_here(t_fd *t, t_argv *targv, t_sh *sh)
{
	DEBUGSTR(t->str);
	if (t->str && ft_fork__child_open_heretoetat(t, sh))
		return (-1);
	DEBUGSTR(t->str);
	return (ft_fork__child_open_here__fork(t, targv));
}
