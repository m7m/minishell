/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tfd_newaddsort.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/08 08:03:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/08 08:03:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "tfd.h"

static int
	ft_tfd_sort_cmp(t_fd *t1, t_fd *t2)
{
	return (t1->pipe[0] < t2->pipe[0]);
}

t_fd
	*ft_tfd_newaddsort(t_fd **node)
{
	t_fd	*new;

	ft_memchk_exit(new = (t_fd *)ft_memalloc(sizeof(t_fd)));
	*node = ft_lstadd_sortf(*node, new, ft_tfd_sort_cmp);
	return (new);
}
