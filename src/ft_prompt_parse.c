/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prompt_parse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/22 00:56:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/22 00:56:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "etat/etat.h"

#include "parser/f_parser.h"

void
	ft_prompt_parse(t_sh *sh, char *line, size_t line_len)
{
	(void)line_len;
	if (IS_SHTTY(sh))
		tcsetattr(sh->fd, TCSANOW, &sh->sterm);
	DEBUG;
	f_parser_init(sh, line);
	free(line);
	if (IS_SHTTY(sh))
	{
		ft_term_init(sh->sterm, STDIN_FILENO);
		tcsetpgrp(STDIN_FILENO, getpid());
	}
}
