/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_bg.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 08:34:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/01 08:34:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr_fork.h"
#include "sh_signal.h"

static void
	ft_builtins_bg__print(int i, t_builtins_job tjobs)
{
	char	**argv;

	DEBUG;
	ft_printf("Job running: [%d] %d\tcmd: ", i, tjobs.pc);
	argv = tjobs.argv;
	DEBUG;
	while (*argv)
	{
		ft_printf(" %s", *argv);
		++argv;
	}
	ft_printf("\n");
	ft_fflush(STDOUT_FILENO);
}

static void
	ft_builtins_bg__bg(int index, t_sh *sh, t_argv *targv)
{
	pid_t	pgrp;

	if (sh->tljobs.tjobs[index].status == 1)
		MSG_ERR4("bg", targv->argv[1], "job is already background");
	else if (sh->tljobs.tjobs[index].status == 3)
		MSG_ERR4("bg", targv->argv[1], "job is termined");
	else
	{
		pgrp = getpgid(sh->tljobs.tjobs[index].pc);
		if (!(kill(-pgrp, SIGCONT)))
		{
			sh->tljobs.tjobs[index].status = 1;
			DEBUGSTR(sh->tljobs.tjobs[index].argv[0]);
			ft_builtins_bg__print(index, sh->tljobs.tjobs[index]);
		}
		else
			MSG_ERR3("Error internaly", "kill -SIGCONT");
	}
}

static int
	ft_builtins_bg__find_index(t_builtins_job *tjobs, t_argv *targv)
{
	int	index;
	int	indexend;

	index = -1;
	indexend = 0;
	if (targv->argv[1])
	{
		indexend = ft_atoi(targv->argv[1]);
		if ((size_t)ft_intlen(index) == ft_strlen(targv->argv[1]))
			index = indexend;
	}
	else
		while (tjobs[indexend].pc > -1)
		{
			if (tjobs[indexend].pc > 0)
				index = indexend;
			++indexend;
		}
	return (index);
}

void
	ft_builtins_bg(t_sh *sh, t_argv *targv)
{
	int	index;

	DEBUG;
	if (sh->tljobs.tjobs)
	{
		index = ft_builtins_bg__find_index(sh->tljobs.tjobs, targv);
		DEBUGNBR(index);
		if (index != -1 && sh->tljobs.tjobs[index].pc > 0)
		{
			ft_builtins_bg__bg(index, sh, targv);
			return ;
		}
	}
	if (targv->argv[1])
		MSG_ERR4("bg", targv->argv[1], "no such job");
	else
		MSG_ERR3("bg", "no such job");
}
