/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_linecomplete_flags.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 00:28:44 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/30 00:28:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_debug.h"
#include "flags.h"
#include "minishell.h"

static int
	ft_linecomplete_flags__antsl(char *c_pline, long *flags)
{
	if (*flags == FL_ANTSL
		|| (!(*flags & FL_ANT_C) && *flags & FL_ANTSL && *flags & FL_ALL))
		*flags |= FL_ANT_C;
	else if (*flags & FL_ANTSL)
	{
		*flags &= ~(FL_ANT_C | FL_ANTSL);
		return (ft_linecomplete_flags_c(c_pline, flags));
	}
	return (0);
}

static int
	ft_linecomplete_flags__open(char *c_pline, long *flags)
{
	if (*c_pline == '"')
		*flags = FL_DQUOT;
	else if (*c_pline == '\'')
		*flags = FL_SQUOT;
	else if (*c_pline == '`')
		*flags = FL_GRAVE;
	else if (*c_pline == '{')
		*flags = FL_ACCOL;
	else if (*c_pline == '(')
		*flags = FL_PAREN;
	else if (!ft_memcmp(c_pline, "<<", SC(2)))
		*flags = FL_HERED;
	return (!!(*flags));
}

int
	ft_linecomplete_flags_c(char *c_pline, long *flags)
{
	DEBUGC(*c_pline);
	DEBUGNBR(*flags);
	if (!(*flags & (FL_ANTSL | FL_SQUOT)) && *c_pline == '\\')
		*flags |= FL_ANTSL;
	else if (!*flags)
		return (ft_linecomplete_flags__open(c_pline, flags));
	else if ((*flags == FL_DQUOT && *c_pline == '"')
			|| (*flags == FL_SQUOT && *c_pline == '\'')
			|| (*flags == FL_ACCOL && *c_pline == '}')
			|| (*flags == FL_PAREN && *c_pline == ')')
			|| (*flags == FL_GRAVE && *c_pline == '`'))
	{
		*flags = 0;
		return (2);
	}
	else
		return (ft_linecomplete_flags__antsl(c_pline, flags));
	DEBUGNBR(*flags);
	return (0);
}
