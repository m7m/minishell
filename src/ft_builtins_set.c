/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_set.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 23:24:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 23:24:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int
	ft_builtins_set__err2(char *xarg)
{
	if (ft_isdigit(*xarg))
	{
		MSG_ERR4("set", xarg, "Variable name do not begin with a digit.");
		return (1);
	}
	while (*xarg)
		if (ft_isalnum(*xarg) || *xarg == '_')
			++xarg;
		else
		{
			MSG_ERR4("set", xarg,
					"Variable name must contain [a-zA-Z_] characters.");
			return (1);
		}
	return (0);
}

void
	ft_builtins_set(t_sh *sh, t_argv *targv)
{
	void	*olddata;

	DEBUGSTR(*targv->argv);
	DEBUGNBR(targv->argc);
	if (targv->argc == 1)
		ft_hashto_keyeqdata_write(sh->hlocal);
	else if (targv->argc == 3 || targv->argc == 2)
	{
		if (ft_builtins_set__err2(targv->argv[1]))
		{
			ft_updatekey_ret(&sh->hlocal, 1);
			return ;
		}
		DEBUGSTR(targv->argv[1]);
		DEBUGSTR(targv->argv[2]);
		olddata = ft_hupd_nosize(&sh->hlocal, targv->argv[1], targv->argv[2]);
		if (olddata)
			free(olddata);
	}
	ft_updatekey_ret(&sh->hlocal, 0);
}
