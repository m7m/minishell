/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnospace_left.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:43:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:43:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

/*
** Retourne un pointeur sur le debut du premier mot trouvé
** un mot est tout caractere imprimable sauf " "
*/

wchar_t
	*ft_wcsnospace_left(wchar_t *s)
{
	int	pass;
	int	pass_anc;
	int	len;

	pass = 0;
	pass_anc = 0;
	len = ft_wcslen(s);
	while (len)
	{
		--len;
		if (!(pass = (32 < s[len]
					&& (s[len] <= 126
						|| (s[len] > 0x80 && ft_isutf8(s[len])))))
			&& pass_anc)
			return (s + len + 1);
		else if (pass)
			pass_anc = pass;
	}
	DEBUG;
	return (s);
}

wchar_t
	*ft_wcsnospace_leftv2(wchar_t *s)
{
	int	i;

	i = ft_wcslen(s);
	while (i)
	{
		--i;
		if (!((32 < s[i] && (s[i] <= 126 || s[i] > 127))))
			return (s + i + 1);
	}
	DEBUG;
	return (s);
}

wchar_t
	*ft_wcsnospace_leftv3(wchar_t *s)
{
	int	i;

	i = ft_wcslen(s);
	while (i)
	{
		--i;
		if (!((32 < s[i] && (s[i] <= 126 || s[i] > 127)))
			&& (!i || s[i - 1] != '\\'))
			return (s + i + 1);
	}
	DEBUG;
	return (s);
}
