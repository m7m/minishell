/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_setenv.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:53:01 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:53:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int
	ft_builtins_setenv__err2(char *xarg)
{
	char	*pxarg;

	pxarg = xarg;
	while (*xarg)
		if (ft_isalnum(*xarg) || *xarg == '_')
			++xarg;
		else
		{
			MSG_ERR3(SETENV_NAME, SETENV_ERR_ALNUM);
			return (1);
		}
	if (xarg == pxarg)
	{
		MSG_ERR3(SETENV_NAME, SETENV_ERR_ALNUM);
		return (1);
	}
	return (0);
}

static int
	ft_builtins_setenv__err(t_sh *sh, t_argv *targv)
{
	DEBUGNBR(targv->argc);
	if (targv->argc == 1)
	{
		ft_builtins_env(sh, targv);
		return (1);
	}
	else if (targv->argc > 3)
	{
		MSG_ERR3(SETENV_NAME, SETENV_ERR_MANY);
		return (1);
	}
	else if (targv->argc > 1 && ft_isdigit(targv->argv[1][0]))
	{
		MSG_ERR3(SETENV_NAME, SETENV_ERR_DIGIT);
		return (1);
	}
	if (ft_builtins_setenv__err2(targv->argv[1]))
		return (1);
	return (0);
}

void
	ft_builtins_setenv(t_sh *sh, t_argv *targv)
{
	void	*old_d;

	DEBUGSTR(*targv->argv);
	DEBUGNBR(targv->argc);
	if (ft_builtins_setenv__err(sh, targv))
	{
		ft_updatekey_ret(&sh->hlocal, 1);
		return ;
	}
	if ((old_d = ft_hupd_nosize(&sh->henvp, targv->argv[1], targv->argv[2])))
		free(old_d);
	ft_correla_path(&sh->hexec, &sh->hcorrela, sh->henvp);
	ft_updatekey_ret(&sh->hlocal, 0);
}
