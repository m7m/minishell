/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 14:28:41 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/08 14:55:14 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int
	ft_updatekey_quit(t_sh *sh, int ret)
{
	ft_updatekey_ret(&sh->hlocal, ret);
	return (0);
}

void
	ft_init_read_buff(t_read_data *rd, int ac, char **av, int start)
{
	rd->ac = ac;
	rd->av = av;
	rd->start = start;
}

int
	ft_builtins_read(t_sh *sh, t_argv *targv)
{
	int			i;
	int			ret;
	t_read		tread;
	t_opt_read	opt[NB_OPT_READ + 1];

	i = 0;
	ft_bzero(opt, sizeof(t_opt_read) * (NB_OPT_READ + 1));
	ft_init_opt(opt);
	ft_init_tread(&tread);
	if ((ret = ft_parser_opt(opt, &tread, targv->argv, &i)) > BUILTIN_TRUE)
		return (ft_updatekey_quit(sh, ret));
	if (tread.time == 0)
		return (ft_updatekey_quit(sh, BUILTIN_FALSE));
	ft_init_sep(&tread);
	ret = BUILTIN_FAILURE;
	tread.reply = (i >= targv->argc) ? 1 : tread.reply;
	tread.prompt ? ft_putstr(tread.prompt) : ' ';
	ft_readinit_sig();
	ret = ft_entry_read(&tread, i, targv->argc, targv->argv);
	free(tread.sep);
	ft_readreset_sig();
	ft_updatekey_ret(&sh->hlocal, ret);
	return (0);
}
