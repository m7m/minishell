/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal_reset.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 21:44:13 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/11 21:44:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _POSIX_C_SOURCE
# define _POSIX_C_SOURCE 200809L
#endif

#include <signal.h>
#include <libft.h>

void
	ft_signal_reset(void)
{
	DEBUG;
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);
	signal(SIGCONT, SIG_DFL);
}

void
	ft_signal_reset_all(void)
{
	ft_signal_reset();
	signal(SIGCHLD, SIG_DFL);
}

void
	ft_signal_reset_job(void)
{
	DEBUG;
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
}
