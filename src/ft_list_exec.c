/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_exec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 14:44:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/18 14:44:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "const_file.h"

/*
** si different d'un dossier, . ou .. ajoute
*/

t_htable
	*ft__static_hexec(t_htable *thtable)
{
	static t_htable	*static_hinf = NULL;

	if (thtable)
		static_hinf = thtable;
	return (static_hinf);
}

static void
	ft_add_file_in_hash(char const *path, int path_len,
						char *file_name, int len_file)
{
	t_hupdate	hupd;
	t_hash		*thash;
	t_htable	*thtable;
	char		concatpar_dir[FILENAME_MAX + 1];

	thtable = ft__static_hexec(NULL);
	DEBUGSTR(file_name);
	hupd.key = file_name;
	hupd.key_len = len_file;
	ft_memcpy(concatpar_dir, path, path_len);
	concatpar_dir[path_len] = '/';
	ft_memcpy(concatpar_dir + path_len + 1, hupd.key, len_file + 1);
	hupd.data = concatpar_dir;
	hupd.data_len = path_len + hupd.key_len + 2;
	DEBUGSTR(hupd.data);
	if ((thash = ft_hadd(thtable, hupd.key, hupd.key_len)))
		ft_hadd_data(thtable, thash, hupd.data, hupd.data_len);
}

int
	ft_list_exec(int perm, char *path_henvp)
{
	char	*dir_end;
	char	c_tmp;

	DEBUGSTR(path_henvp);
	dir_end = path_henvp;
	while (path_henvp)
	{
		DEBUG;
		dir_end = ft_strchrnull(dir_end, ':');
		if (dir_end == path_henvp)
			return (1);
		c_tmp = *dir_end;
		*dir_end = 0;
		DEBUGSTR(path_henvp);
		ft_getopendir(path_henvp, perm,
					ft_add_file_in_hash);
		*dir_end = c_tmp;
		if (!c_tmp)
			return (1);
		++dir_end;
		path_henvp = dir_end;
	}
	return (0);
}
