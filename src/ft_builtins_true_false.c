/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_true_false.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 19:13:58 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/12 19:13:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/t_sh.h"
#include "../inc/ft_updatekey.h"

void
	ft_builtins_true(t_sh *sh, t_argv *targv)
{
	(void)targv;
	ft_updatekey_ret(&sh->hlocal, 0);
}

void
	ft_builtins_false(t_sh *sh, t_argv *targv)
{
	(void)targv;
	ft_updatekey_ret(&sh->hlocal, 1);
}
