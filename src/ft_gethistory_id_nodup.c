/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gethistory_id_nodup.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 21:04:35 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 21:04:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_history.h"
#include "pr_history.h"
#include "pr_hash.h"
#include <libft.h>

static size_t
	ft_gethistory_nodup(char *key, size_t key_len, char **data)
{
	size_t	data_len;

	data_len = ft_hchr(*g_thistory.thhistory, key, key_len, (void **)data);
	DEBUGNBR(data_len);
	DEBUGSTR(data);
	if (data_len > 1)
		--data_len;
	return (data_len);
}

size_t
	ft_gethistory_id_nodup(int key, char **data)
{
	int		key_len;
	char	skey[255];

	key_len = ft_itoa_s(key, skey);
	return (ft_gethistory_nodup(skey, key_len, data));
}
