/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_forksplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 11:03:17 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 11:03:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"
#include "sh_signal.h"
#include "redirecting.h"
#include "../inc/jobs.h"
#include "../inc/built_in_jobs.h"

void
	ft_fork_group(pid_t pc, t_sh *sh)
{
	DEBUG;
	setpgid(pc, pc);
	if (IS_SHTTY(sh))
		tcsetpgrp(STDIN_FILENO, pc);
	sh->tljobs.last_pc = pc;
}

static void
	ft_forksplit__parent(pid_t pc, t_sh *sh, t_argv *targv)
{
	DEBUGNBR(sh->tljobs.last_pc);
	if (!sh->tljobs.last_pc)
		ft_fork_group(pc, sh);
	else
	{
		DEBUG;
		setpgid(pc, sh->tljobs.last_pc);
	}
	DEBUGPTR(targv->argv);
	DEBUGPTR(*targv->argv);
	ft_builtins_jobs_add(JOB_PENDING, pc, &sh->tljobs, targv->argv);
}

#if DEBUG_LVL > 0
# define TEST_FORKSPLIT(t) ft_forksplit__test(t)

static void
	ft_forksplit__test(t_argv *targv)
{
	if (targv->argv[0])
	{
		DEBUGSTR(targv->argv[1]);
		if (targv->argv[1])
		{
			DEBUGSTR(targv->argv[2]);
			if (targv->argv[2])
			{
				DEBUGSTR(targv->argv[3]);
			}
		}
	}
}

#else
# define TEST_FORKSPLIT(t)
#endif

void
	ft_forksplit(t_sh *sh, t_argv *targv)
{
	pid_t	pc;

	DEBUGSTR(*targv->argv);
	TEST_FORKSPLIT(targv);
	pc = fork();
	if (pc == -1)
		MSG_ERR3("fork", "return -1");
	else if (!pc)
	{
		DEBUG;
		if (IS_SUBSH(sh))
			ft_signal_reset_job();
		else
			ft_signal_reset();
		ft_fork__child(sh, targv);
	}
	else
		ft_forksplit__parent(pc, sh, targv);
	DEBUG;
}
