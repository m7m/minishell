/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec_force_update.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 18:50:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/18 18:50:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Recherche de cmd dans PATH et met a jour la htable
** si trouve retourne l'entre de la htable
** sinon NULL
*/

static int
	ft_check_file__perm(char *tmp)
{
	int	ret;

	DEBUG;
	ret = ft_accesschk_path_x(tmp);
	if (ret != -1 && ret & X_OK)
		return (1);
	if (!ret)
		return (1);
	return (0);
}

static char
	*ft_check_file(char *path, size_t path_len, char *cmd)
{
	int		cmd_len;
	int		i;
	char	*tmp;
	char	*pend;

	DEBUGSTR(cmd);
	cmd_len = ft_strlen(cmd) + 1;
	tmp = ft_strnew(path_len + cmd_len + 2);
	ft_memchk_exit(tmp);
	pend = path + path_len;
	while (path < pend && *path)
	{
		DEBUGSTR(path);
		i = ft_strchrnull(path, ':') - path;
		ft_memcpy(tmp, path, i);
		tmp[i] = '/';
		++i;
		ft_memcpy(tmp + i, cmd, cmd_len);
		if (ft_check_file__perm(tmp))
			return (tmp);
		path += i;
	}
	free(tmp);
	return (NULL);
}

void
	*ft_exec_force_update(t_htable *hexec, t_htable henvp, char *cmd)
{
	void	*data;
	size_t	data_len;
	char	*bin;

	DEBUG;
	if (!(data_len = ft_hchr(henvp, "PATH", SC(4), &data)))
		return (NULL);
	data = ft_memdup_resize(data, data_len, SC(1));
	ft_memchk_exit(data);
	((char *)data)[data_len] = 0;
	bin = ft_check_file((char *)data, data_len, cmd);
	DEBUGSTR(bin);
	free(data);
	if (bin)
	{
		ft_hadd_n(hexec, cmd, ft_strlen(cmd), bin);
		free(bin);
	}
	data = NULL;
	if (!ft_hchr(*hexec, cmd, ft_strlen(cmd), &data))
		return (NULL);
	return (data);
}
