/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_unary_bf.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 16:21:03 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:04:38 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test__opt_unary_b(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && S_ISBLK(st.st_mode)));
}

int
	ft_builtins_test__opt_unary_c(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && S_ISCHR(st.st_mode)));
}

int
	ft_builtins_test__opt_unary_d(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && S_ISDIR(st.st_mode)));
}

int
	ft_builtins_test__opt_unary_e(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (stat(file, &st) != 0);
}

int
	ft_builtins_test__opt_unary_f(const char *file)
{
	struct stat st;

	ft_bzero(&st, sizeof(struct stat));
	return (!(stat(file, &st) == 0 && S_ISREG(st.st_mode)));
}
