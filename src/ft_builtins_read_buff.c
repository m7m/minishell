/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read_buff.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 17:00:11 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/08 15:00:28 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "get_next_line.h"

static char
	*ft_wipe_antislash(char *line, int len, int *ret, int sub)
{
	char	*tmp;
	int		count;

	count = 0;
	while (--len >= 0)
	{
		if (line[len] != '\\')
			break ;
		count++;
		if (len == 0 || line[len - 1] != '\\')
		{
			sub++;
			break ;
		}
		len--;
		sub++;
		count++;
	}
	(*ret = (count % 2 != 0)) ? ft_putstr("> ") : *ret;
	if (sub == 0)
		return (line);
	line[ft_strlen(line) - sub] = '\0';
	tmp = ft_strdup(line);
	ft_memdel((void**)&line);
	return (tmp);
}

static int
	ft_word_to_local(t_read_data *rd, int reply, int i, int *j)
{
	char	*extract;

	rd->tmp[i] = '\0';
	extract = &rd->tmp[*j];
	*j = i + 1;
	if (!rd->error)
	{
		if (ft_check_var(rd->start, rd->av, &rd->error, 1) > 0)
			return (BUILTIN_FALSE);
	}
	if (rd->error)
		return (BUILTIN_FALSE);
	if (reply || rd->start == rd->ac - 1)
	{
		rd->line = (rd->line) ? ft_strjoinf(&(rd->line), " ") : rd->line;
		rd->line = ft_strjoinf(&(rd->line), extract);
	}
	else
		ft_setlocal(rd->av[(rd->start)++], extract);
	return (0);
}

static void
	ft_treat_line(t_read *tread, t_read_data *rd, int *j)
{
	int		i;
	int		len;

	i = -1;
	len = ft_strlen(rd->tmp);
	while (++i < len)
	{
		if (ft_strchr(tread->sep, rd->tmp[i]))
			ft_word_to_local(rd, tread->reply, i++, j);
		while (rd->tmp[i] && ft_strchr(tread->sep, rd->tmp[i]))
			i++;
	}
}

static int
	ft_line_to_local(t_read *tread, t_read_data *rd)
{
	int		j;

	j = 0;
	ft_treat_line(tread, rd, &j);
	if (!rd->error)
		ft_check_var(rd->start, rd->av, &rd->error, 1);
	if (rd->error)
	{
		ft_memdel((void**)&rd->line);
		return (BUILTIN_FALSE);
	}
	if (rd->line)
	{
		rd->line = ft_strjoinf(&rd->line, " ");
		rd->line = ft_strjoinf(&rd->line, &(rd->tmp[j]));
		ft_buff_to_local(tread->reply, rd->av, rd->start, rd->line);
		ft_memdel((void**)&rd->line);
	}
	else
	{
		rd->line = &rd->tmp[j];
		ft_buff_to_local(tread->reply, rd->av, rd->start, &rd->tmp[j]);
	}
	return (BUILTIN_TRUE);
}

int
	ft_read_buff(t_read *tread, int start, int ac, char **av)
{
	int			ret;
	char		*res;
	t_read_data	rd;

	res = NULL;
	ft_bzero(&rd, sizeof(t_read_data));
	ft_init_read_buff(&rd, ac, av, start);
	rd.tmp = ft_strnew(1);
	while ((ret = get_next_line(tread->fd, &res)) > 0)
	{
		res = ft_wipe_antislash(res, ft_strlen(res), &ret, 0);
		rd.tmp = ft_strjoinf(&rd.tmp, res);
		ft_memdel((void**)&res);
		if (!ret)
			break ;
	}
	if (ret < 0)
	{
		ft_memdel((void**)&rd.tmp);
		return (130);
	}
	if (rd.tmp && rd.tmp[0])
		ret = ft_line_to_local(tread, &rd);
	ft_memdel((void**)&rd.tmp);
	return (ret);
}
