/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_opt.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 09:15:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/29 05:44:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_parse_opt(int *retopt, char *c_opt)
{
	int	opt;

	opt = *retopt;
	DEBUGSTR(c_opt);
	DEBUGNBR(opt);
	if (opt & FL_END)
		return (1);
	else if (*c_opt == '-' && *(c_opt + 1) == '-' && *(c_opt + 2) == 0)
	{
		*retopt |= FL_END;
		return (0);
	}
	if (*c_opt == '-' && *(c_opt + 1))
	{
		DEBUG;
		while (*(++c_opt))
			opt |= ft_opt_tree(opt, *c_opt);
		*retopt = opt;
		return (0);
	}
	return (1);
}
