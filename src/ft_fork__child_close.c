/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork__child_close.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/18 00:34:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/18 00:34:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "sh_term.h"
#include "redirecting.h"
#include "tfd.h"

void
	ft_fork__child_close(t_argv *targv)
{
	t_fd	*t;

	DEBUG;
	t = targv->tfd;
	while (t)
	{
		DEBUGNBR(t->opt & SFD_REDI_OUT);
		DEBUGNBR(t->opt & SFD_REDI_IN);
		DEBUGNBR(t->pipe[0]);
		DEBUGNBR(t->pipe[1]);
		close(t->pipe[0]);
		close(t->pipe[1]);
		t = t->next;
	}
}
