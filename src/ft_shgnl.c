/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shgnl.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/06 03:25:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/06 03:25:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "minishell.h"
#include "libft.h"
#include "get_next_line.h"

#include "etat/chkclose/ft_etat_chkclose.h"

static int
	ft_shgnl__chk(char *line, struct s_sh *sh)
{
	long	i;

	DEBUGSTR(line);
	i = ft_etat_chkclose_nointeractive(line, sh);
	if (!i)
		return (ft_strlen(line));
	if (i == -1)
	{
		ft_etat_chkclose(line, sh);
		return (-1);
	}
	return (0);
}

int
	ft_shgnl(int fd, char **line, struct s_sh *sh)
{
	int		ret;
	char	*tmp;

	*line = NULL;
	tmp = NULL;
	while ((ret = get_next_linenl(fd, line)) == 1)
	{
		DEBUGSTR(*line);
		if (tmp)
		{
			*line = ft_strjoin_free12(&tmp, line);
			ft_memchk_exit(line);
		}
		ret = ft_shgnl__chk(*line, sh);
		if (ret)
			return (ret);
		tmp = *line;
		*line = NULL;
	}
	*line = tmp;
	if (ret)
		return (-1);
	DEBUGSTR(*line);
	return (ft_strlen(*line));
}
