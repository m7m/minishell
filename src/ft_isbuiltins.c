/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isbuiltins.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 08:23:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/15 08:23:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "redirecting.h"

int
	ft_isbuiltins(t_sh *sh, t_argv **targv)
{
	void	*data;
	size_t	len;

	len = ft_strlen(*targv[0]->argv);
	DEBUGNBR(len);
	DEBUGNBR(targv[0]->argc);
	DEBUGSTR(*targv[0]->argv);
	if (!ft_hchr(sh->hbuiltins, *targv[0]->argv, len, &data))
		return (-1);
	DEBUG;
	targv[0]->builtins = (void(*)(t_sh *, t_argv *))data;
	return (0);
}
