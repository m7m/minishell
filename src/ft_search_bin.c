/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search_bin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:49:08 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:49:08 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr_xetenvp.h"
#include "pr_hash.h"
#include "const_file.h"
#include "pr__static.h"

/*
** si motif == \0 alors tout
*/

int
	ft_search(int perm, const char *motif, char ***resul)
{
	int			ret;
	char		*path;
	char		*tmp;
	t_htable	*thtable;

	DEBUGSTR(motif);
	*resul = NULL;
	ft_memchk_exit(path = ft_dirname((char *)motif));
	tmp = NULL;
	if (*path == '.' && *motif != '.')
	{
		ft_memchk_exit(tmp = ft_strjoin("./", motif));
		motif = (const char *)tmp;
	}
	DEBUGSTR(path);
	thtable = ft__static_hexec(0);
	ft_hdel_table_cont(thtable);
	ft_list_exec(perm | GET_DIR | GET_FILE, path);
	DEBUGSTR(motif);
	ret = ft_search_in_h(motif, resul, thtable, ft_hsearch_d);
	free(path);
	if (tmp)
		free(tmp);
	return (ret);
}

static int
	ft_search_bin_n(const char *motif, char ***resul)
{
	int		ret_builtins;
	int		ret;
	t_htab	*thtable;

	DEBUG;
	*resul = NULL;
	thtable = ft__static_hbuiltins(0);
	ret_builtins = ft_search_in_h(motif, resul, thtable, ft_hsearch_k);
	;
	DEBUG;
	thtable = ft__static_hexec(0);
	ret = ft_search_in_h(motif, resul, thtable, ft_hsearch_k);
	return (ret + ret_builtins);
}

/*
** Recherche des binaires dans le PATH
** si motif contient un '/' ou début par '.'
**  alors appel de ft_search avec les permissions dossier et fichier exécutable
*/

int
	ft_search_bin(const char *motif, char ***resul)
{
	int		perm;
	char	*path;

	DEBUGSTR(motif);
	path = NULL;
	if (*motif != '.' && *motif != '~'
		&& !ft_strchr((char *)motif, '/') && ft_getenvp("PATH", &path))
		ft_list_exec(GET_FILE | GET_DOTFILE | GET_PERM_X, path);
	else if (!path)
	{
		DEBUG;
		perm = GET_DIR | GET_DOTDIR | GET_FILE | GET_DOTFILE | GET_PERM_X;
		return (ft_search(perm, motif, resul));
	}
	else
		return (0);
	DEBUGSTR(path);
	return (ft_search_bin_n(motif, resul));
}
