/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_cd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 23:30:56 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/14 23:30:56 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void
	ft_builtins_cd___getpwd2(t_htable *hlocal, t_builtins_cd tcd)
{
	if (ft_accesschk_dir("cd", tcd.newpwd) || chdir(tcd.newpwd))
		return ;
	else if (tcd.opt & FL_BUI_CD_H)
	{
		write(STDOUT_FILENO, tcd.newpwd, ft_strlen(tcd.newpwd));
		ft_putchar('\n');
	}
	ft_builtins_cd____upd(hlocal, tcd);
}

static void
	ft_builtins_cd___getpwd(t_htable *hlocal, t_builtins_cd tcd, char *tmp)
{
	char	newpwd[MAXNAMLEN + 1];

	if (*tcd.newpwd == '/')
	{
		tcd.pwd_len = 1;
		tcd.pwd = "/";
	}
	else if (!(tcd.pwd_len = ft_hchr(*hlocal, "PWD", SC(3), (void **)&tcd.pwd)))
	{
		if (ft_perror_getcwd(&tcd.pwd))
			return ;
		tcd.pwd_len = ft_strlen(tcd.pwd);
		tmp = tcd.pwd;
	}
	ft_memcpy(newpwd, tcd.newpwd, tcd.newpwd_len);
	newpwd[tcd.newpwd_len] = 0;
	if ((tcd.newpwd = ft_parse_dotslash(
		tcd.opt & FL_BUI_CD_P, newpwd, tcd.pwd)))
	{
		tcd.newpwd_len = ft_strlen(tcd.newpwd) + 1;
		ft_builtins_cd___getpwd2(hlocal, tcd);
	}
	free(tcd.newpwd);
	free(tmp);
}

static void
	ft_builtins_cd___pwd2(t_htable *hlocal, t_builtins_cd tcd)
{
	char	*tmp;

	DEBUG;
	tmp = NULL;
	if (tcd.opt & FL_BUI_CD_P && *tcd.newpwd != '/')
	{
		DEBUG;
		if (ft_perror_getcwd(&tmp))
			return ;
		tcd.newpwd_len = ft_strlen(tmp) + 1;
		ft_memcpy(tmp + tcd.newpwd_len, tcd.newpwd,
				ft_strlen(tcd.newpwd) + sizeof(char));
		tmp[tcd.newpwd_len - 1] = '/';
		tcd.newpwd_len += ft_strlen(tcd.newpwd);
		tcd.newpwd = tmp;
	}
	else if (tcd.opt & FL_BUI_CD_P && *tcd.newpwd == '/')
	{
		tcd.pwd = tcd.newpwd;
		tcd.pwd_len = ft_strlen(tcd.newpwd);
	}
	ft_builtins_cd___getpwd(hlocal, tcd, 0);
	DEBUGPTR(tmp);
	free(tmp);
}

static int
	ft_builtins_cd__pwd(t_sh *sh, t_builtins_cd tcd)
{
	DEBUG;
	if (!tcd.newpwd && !(tcd.newpwd_len = ft_hchr(sh->henvp, "HOME", SC(4),
												(void **)&tcd.newpwd)))
		return (3);
	else if (tcd.newpwd_len == 1 && *tcd.newpwd == '-')
	{
		if (!(tcd.newpwd_len = ft_hchr(sh->hlocal, "OLDPWD", SC(6),
									(void **)&tcd.newpwd)))
			return (2);
		tcd.opt = FL_BUI_CD_H;
	}
	else
	{
		ft_builtins_cd___pwd2(&sh->hlocal, tcd);
		return (0);
	}
	ft_builtins_cd___getpwd(&sh->hlocal, tcd, 0);
	return (0);
}

void
	ft_builtins_cd(t_sh *sh, t_argv *targv)
{
	t_builtins_cd	tcd;
	int				errc;
	char			**parg;

	DEBUG;
	ft_memset(&tcd, 0, sizeof(t_builtins_cd));
	parg = ft_parse_argopt(targv->argc - 1, targv->argv + 1, &tcd.opt,
						ft_builtins_cd__opt_tree);
	DEBUGPTR(parg);
	if (parg)
	{
		if (*parg)
		{
			tcd.newpwd = *parg;
			ft_strdel_c(tcd.newpwd, '\\');
			tcd.newpwd_len = ft_strlen(tcd.newpwd);
		}
		free(parg);
	}
	errc = 1;
	if (tcd.opt & FL_BUI_CD_H || (errc = ft_builtins_cd__pwd(sh, tcd)))
		ft_builtins_cd__error(targv->argv[1], errc, &sh->hlocal);
	else
		ft_updatekey_ret(&sh->hlocal, 0);
}
