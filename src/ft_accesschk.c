/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_accesschk.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:33:39 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:33:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_accesschk(const char *path)
{
	int	ret;

	DEBUGSTR(path);
	ret = 0;
	if (!access(path, F_OK))
	{
		if (access(path, R_OK))
			ret |= R_OK;
		if (access(path, W_OK))
			ret |= W_OK;
		if (access(path, X_OK))
			ret |= X_OK;
	}
	else
		return (-1);
	return (ret);
}

/*
** Test si path est accessible en execution
** retourn 0
** sinon :
** -1 inexiste
** ou
** (flags access)
*/

int
	ft_accesschk_path_x(char *path)
{
	int		ret;
	char	*slash;

	DEBUG;
	slash = path + 1;
	DEBUGSTR(path);
	while (*(slash = ft_strchrnull(slash, '/')))
	{
		*slash = 0;
		DEBUGSTR(path);
		if ((ret = ft_accesschk(path)) & ~(W_OK | R_OK))
			return (ret);
		*slash = '/';
		++slash;
	}
	if ((ret = ft_accesschk(path)) & ~(W_OK | R_OK))
		return (ret);
	return (0);
}

int
	ft_accesschk_perror(char *path, int perm)
{
	int	ret;

	ret = ft_accesschk(path);
	if (ret == -1)
	{
		DEBUGSTR("No such file or directory");
		MSG_ERR3(path, "No such file or directory");
		ft_fflush(STDERR_FILENO);
		return (-1);
	}
	else if (ret & perm)
	{
		DEBUGSTR("Permission denied");
		MSG_ERR3(path, "Permission denied");
		ft_fflush(STDERR_FILENO);
		return (-1);
	}
	return (0);
}

static int
	ft_accesschk_perror__msg(char *cmd, char *path, int perm)
{
	int	ret;

	ret = ft_accesschk(path);
	if (ret == -1)
	{
		DEBUGSTR("No such file or directory");
		MSG_ERR4(cmd, path, "No such file or directory");
		ft_fflush(STDERR_FILENO);
		return (-1);
	}
	else if (ret & perm)
	{
		DEBUGSTR("Permission denied");
		MSG_ERR4(cmd, path, "Permission denied");
		ft_fflush(STDERR_FILENO);
		return (-1);
	}
	return (0);
}

int
	ft_accesschk_dir(char *cmd, char *path)
{
	char	*slash;

	DEBUG;
	slash = path + 1;
	DEBUGSTR(path);
	while (*(slash = ft_strchrnull(slash, '/')))
	{
		*slash = 0;
		DEBUGSTR(path);
		if (ft_accesschk_perror__msg(cmd, path, X_OK))
		{
			DEBUG;
			return (-1);
		}
		*slash = '/';
		++slash;
	}
	if (ft_accesschk_perror__msg(cmd, path, X_OK))
	{
		DEBUG;
		return (-1);
	}
	return (0);
}
