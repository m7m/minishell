/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tfd_chrfd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/11 12:57:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/11 12:57:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tfd.h"

t_fd
	*ft_tfd_chrfdread(t_fd **node, int fd)
{
	t_fd	*tfd;

	tfd = *node;
	while (tfd && tfd->pipe[0] != fd)
		tfd = tfd->next;
	DEBUGPTR(tfd);
	if (!tfd)
		if ((tfd = ft_tfd_newadd(node)))
			tfd->pipe[0] = fd;
	return (tfd);
}

t_fd
	*ft_tfd_chrfdwrite(t_fd **node, int fd)
{
	t_fd	*tfd;

	tfd = *node;
	while (tfd && tfd->pipe[1] != fd)
		tfd = tfd->next;
	DEBUGPTR(tfd);
	if (!tfd)
		if ((tfd = ft_tfd_newadd(node)))
			tfd->pipe[1] = fd;
	return (tfd);
}

t_fd
	*ft_tfd_chrfdwrite_notalloc(t_fd *node, int fd)
{
	while (node && node->pipe[1] != fd)
		node = node->next;
	return (node);
}
