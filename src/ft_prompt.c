/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prompt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:50:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:50:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_prompt_read(t_sh *sh)
{
	char	*line;
	int		len;

	DEBUG;
	len = ft_shgnl(STDIN_FILENO, &line, sh);
	if (len > 0)
	{
		DEBUGNBR(len);
		DEBUGSTR(line);
		line[len] = 0;
		ft_prompt_parse(sh, line, len);
	}
	else if (!len)
		ft_exit(ft_getlocalint("?"), "");
	else if (len < 0)
	{
		if (!line)
			MSG_ERR3("Error", "read STDIN_FILENO");
		free(line);
		ft_exit(EXIT_FAILURE, "");
	}
	else
		free(line);
}

void
	ft_prompt(t_sh *sh)
{
	t_scap		tc;

	DEBUG;
	sh->nb_line = 1;
	if (IS_SHTTY(sh))
	{
		ft_term(sh);
		ft_term_initcap(&tc);
		while (1)
			ft_term_read(sh, tc);
	}
	else
		while (1)
		{
			ft_jobs_stat(&sh->tljobs);
			ft_prompt_read(sh);
		}
}
