/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xethistory_readwrite.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:51:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:51:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "const_term.h"
#include "pr_xetlocal.h"
#include "pr_history.h"
#include <fcntl.h>
#include "minishell.h"

static void
	ft_history_write__antnl(int fd, char *line)
{
	char	*tmp;

	tmp = line;
	DEBUGSTR(line);
	while (*tmp && (tmp = ft_strchrnull(line, '\n')))
	{
		DEBUGSTR(tmp);
		ft_printf("%P%.*s", fd, tmp - line, line);
		if (*tmp == '\n')
			ft_printf("%P\\\n", fd);
		line = tmp + 1;
	}
	ft_printf("%P\n", fd);
}

static void
	ft_history_write__w(int fd)
{
	int		id;
	size_t	key_len;
	char	*line;
	char	skey[255];

	DEBUG;
	id = 1;
	ft_strcpy(skey, "1");
	key_len = 1;
	while (ft_gethistory(skey, key_len, &line))
	{
		ft_history_write__antnl(fd, line);
		DEBUG;
		free(line);
		line = NULL;
		++id;
		key_len = ft_itoa_s(id, skey);
	}
}

/*
** ft_fflush_all est obligatoire cause:
**  fd identique mais dans des lieux différent ????
*/

void
	ft_history_write(void)
{
	int		fd;
	int		flags;
	char	*file;

	DEBUG;
	ft_fflush_all();
	flags = O_WRONLY | O_CREAT | O_TRUNC;
	if (ft_getlocal(CONST_HISTFILE, &file)
		&& (fd = open(file, flags, S_IWUSR | S_IRUSR)) > -1)
	{
		ft_history_reset_line();
		ft_history_write__w(fd);
		ft_fflush(fd);
		close(fd);
	}
	else if (0)
		MSG_ERR4("HISTFILE", file, "file not writable");
}
