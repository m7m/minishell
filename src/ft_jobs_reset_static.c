/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_jobs_reset_static.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 10:49:36 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 10:49:38 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/built_in_jobs.h"
#include "../inc/pr__static_tjobs.h"
#include "../inc/pr__static_sh.h"

/*
** Attention avec atexit: revoir position, appeler aussi dans les fin de fork
*/

void
	ft_jobs_reset_static(void)
{
	int				i;
	t_builtins_job	*tjobs;

	DEBUG;
	tjobs = ft__static_tjobs(0);
	if (tjobs)
	{
		i = 0;
		while (i < JOBS_MAX)
		{
			if (tjobs[i].argv)
				ft_memdel((void **)&(tjobs[i].argv));
			++i;
		}
	}
	DEBUG;
}

void
	ft_jobs_free_static(void)
{
	t_builtins_job	*tjobs;

	DEBUG;
	tjobs = ft__static_tjobs(0);
	if (tjobs)
	{
		ft_jobs_reset_static();
		free(tjobs);
	}
	DEBUG;
}

void
	ft_jobs_reset(void)
{
	t_sh	*sh;

	sh = ft__static_sh(0);
	if (sh)
	{
		ft_jobs_reset_static();
		sh->tljobs.max_index = 0;
		sh->tljobs.last_index = 0;
		sh->tljobs.last_pc = 0;
	}
}
