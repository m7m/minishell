/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isexec.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/19 21:23:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/19 21:23:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"

void
	ft_isexec(t_sh *sh, t_argv *targv)
{
	DEBUGSTR(*targv->argv);
	targv->envp = ft_hashtotab(sh->henvp);
	if (*targv->argv
		&& ft_isbuiltins(sh, &targv)
		&& ft_isexec_bin(sh, targv))
		ft_updatekey_ret(&sh->hlocal, 127);
	else
	{
		DEBUG;
		if (!*targv->argv && !targv->tfd)
		{
			DEBUGSTR("f_tree_tolastparse: implementation en erreur\n");
			COM("voir  echo 'ls & ' |./42sh");
			return ;
		}
		DEBUGSTR(" shfork ");
		ft_shfork(sh, targv);
	}
}
