/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft__static_tjobs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 05:20:09 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/18 05:20:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

struct s_builtins_job
	*ft__static_tjobs(struct s_builtins_job **origtjobs)
{
	static struct s_builtins_job	**tjobs = 0x0;

	if (origtjobs)
		tjobs = origtjobs;
	if (tjobs)
		return (*tjobs);
	return (0x0);
}
