/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_echo.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 15:17:11 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/20 15:17:11 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int
	ft_builtins_echo__opt_tree(int opt, char c_opt)
{
	DEBUG;
	if (c_opt == 'n')
		opt |= FL_BUI_EC_NNEW;
	else if (c_opt == 'e')
	{
		opt &= ~FL_BUI_EC_NBAC;
		opt |= FL_BUI_EC_BAC;
	}
	else if (c_opt == 'E')
	{
		opt &= ~FL_BUI_EC_BAC;
		opt |= FL_BUI_EC_NBAC;
	}
	else
		return (-1);
	return (opt);
}

static void
	ft_builtins_echo__put(char c)
{
	if (c == 'a')
		ft_putchar('\a');
	else if (c == 'b')
		ft_putchar('\b');
	else if (c == 'f')
		ft_putchar('\f');
	else if (c == 'n')
		ft_putchar('\n');
	else if (c == 'r')
		ft_putchar('\r');
	else if (c == 't')
		ft_putchar('\t');
	else if (c == 'v')
		ft_putchar('\v');
	else if (c == '\\')
		ft_putchar('\\');
	else
	{
		ft_putchar('\\');
		if (c)
			ft_putchar(c);
	}
}

static char
	*ft_builtins_echo__putoctal(char *pstr)
{
	int		i;
	int		j;
	char	*wc;

	i = 0;
	j = 1;
	while (ft_isdigit(pstr[j]))
	{
		i = pstr[j] - 48 + i * 8;
		DEBUGNBR(i);
		++j;
	}
	--j;
	DEBUGNBR(i);
	wc = ft_wctoa((wchar_t)i);
	ft_memchk_exit(wc);
	if (*wc)
		ft_putstr(wc);
	else
		ft_putchar(0);
	DEBUGSTR(wc);
	free(wc);
	return (pstr + j);
}

static int
	ft_builtins_echo__backslash(char *pstr, int *opt)
{
	while (*pstr)
	{
		if (*pstr == '\\' && pstr[1])
		{
			++pstr;
			if (*pstr == 'c')
				return (*opt = -1);
			else if (*pstr == '0')
				pstr = ft_builtins_echo__putoctal(pstr);
			else
				ft_builtins_echo__put(*pstr);
		}
		else if (*pstr)
			ft_putchar(*pstr);
		++pstr;
	}
	return (0);
}

void
	ft_builtins_echo(t_sh *sh, t_argv *targv)
{
	int		opt;
	char	**echo_print;
	char	**pecho_print;

	opt = 0;
	pecho_print = ft_parse_argopt(targv->argc - 1, targv->argv + 1,
								&opt, ft_builtins_echo__opt_tree);
	echo_print = pecho_print;
	if (echo_print)
		while (*echo_print)
		{
			if (opt & FL_BUI_EC_BAC)
			{
				if (ft_builtins_echo__backslash(*echo_print, &opt))
					break ;
			}
			else
				ft_putstr(*echo_print);
			if (*(++echo_print))
				ft_putchar(' ');
		}
	if (!(opt & FL_BUI_EC_NNEW))
		ft_putchar('\n');
	free(pecho_print);
	ft_updatekey_ret(&sh->hlocal, 0);
}
