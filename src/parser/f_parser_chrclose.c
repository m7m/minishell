/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_chrclose.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 16:48:03 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/16 16:48:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_parser.h"
#include "../../inc/flags.h"
#include "iftoken/iftoken.h"

static int
	f_parser__chrcroisillon(t_parser *tp)
{
	char	*end;

	if (*tp->pline == '#'
	&& (tp->pline == tp->line
		|| ft_strchr("\t\n ;&|", *(tp->pline - 1))))
	{
		end = ft_strchrnull(tp->pline, '\n');
		if (*end)
			ft_strcpy(tp->pline, end + 1);
		else
			*tp->pline = 0;
		return (1);
	}
	return (0);
}

static void
	f_parser__chrclose_heredoc(t_parser *tp)
{
	long	flags;
	char	*line;

	flags = 0;
	line = tp->pline;
	while (*line && (*line == ' ' || *line == '\t'))
		++line;
	DEBUGSTR(line);
	while (*line && (*line != ' ' || *line != '\t' || flags))
	{
		if (ft_linecomplete_flags_c(line, &flags))
			ft_strcpy(line, line + 1);
		else
			++line;
	}
	DEBUGSTR(line);
}

int
	f_parser__chrclose_flags(t_parser *tp, long *flags)
{
	ft_linecomplete_flags_c(tp->pline, flags);
	if (*flags & FL_PAREN && *tp->pline == '(')
	{
		++tp->pline;
		while (*tp->pline && *tp->pline != ')')
		{
			DEBUGSTR(tp->pline);
			if (f_parser__chrclose(tp) && *tp->pline != ')')
			{
				DEBUG;
				return (-1);
			}
			DEBUGNBRUL(*flags);
			DEBUGSTR(tp->pline);
		}
		DEBUGSTR(tp->pline);
		if (*tp->pline)
			ft_linecomplete_flags_c(tp->pline, flags);
	}
	else if (!*flags && *tp->pline == ')')
		return (-1);
	return (0);
}

int
	f_parser__chrclose(t_parser *tp)
{
	long	flags;

	DEBUG;
	if (f_parser__chrcroisillon(tp))
		return (0);
	flags = 0;
	DEBUGSTR(tp->pline);
	f_parser__chrclose_flags(tp, &flags);
	++tp->pline;
	while (flags && *tp->pline)
	{
		if (f_parser__chrclose_flags(tp, &flags))
			return (-1);
		if (flags & FL_HERED)
		{
			flags = 0;
			f_parser__chrclose_heredoc(tp);
		}
		else
			++tp->pline;
	}
	DEBUGNBR(flags);
	if (flags)
		return (-1);
	return (0);
}
