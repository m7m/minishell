/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 20:22:17 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 20:22:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_TREE_H
# define F_TREE_H

# include "t_tree.h"

void	f_tree_freeparent(t_tree *t);
void	f_tree_free(t_tree *t);
void	f_tree_freeone(t_tree *t);
t_tree	*f_tree_newadd(int lr, t_tree *tparent);
void	f_tree_mvparent(
	struct s_tree **branch, struct s_tree *leaftoparent);

void	f_tree_exec_init(t_tree *t, t_sh *sh);

#endif
