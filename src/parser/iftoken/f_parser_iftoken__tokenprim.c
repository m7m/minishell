/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__tokenprim.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:19:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:19:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"

/*
** le noeud courant devient le residu
** le nouveau noeud est mis le plus en haut a droite
**  si token primaire
**  le right du nouveau est creer dans la recursion
*/

/*
** Si residu
**  met le residu sur le noeud courante
**  alloue un nouveau noeud -> tree_new
** sinon
**  supprime le noeud courant de l'arbre -> tree_new
** *
** Remplacement du noeud courant par 'tree_new'
** *
** si tree_parent est un token de lvl 1
**  va le plus à droite possible tant que:
**   tree_parent == lvl 1
**  placement de 'tree_new' en tant que enfant de 'tree_parent'
** sinon
**  Le 'tree_new' devient le parent absolu
*/

static t_tree
	*f_parser_iftoken__tokenprim__tree_new(struct s_token *st, t_parser *tp)
{
	char	*residu;
	t_tree	*tree_new;

	residu = f_parser_iftoken_residu(tp);
	*tp->pline = 0;
	tp->line = ++tp->pline;
	if (residu)
	{
		ft_memchk_exit(tree_new = (t_tree *)ft_memalloc(sizeof(t_tree)));
		tp->tree_cur->line = residu;
	}
	else
	{
		if (tp->tree_cur->parent)
		{
			if (tp->tree_cur->parent->left == tp->tree_cur)
				tp->tree_cur->parent->left = NULL;
			else
				tp->tree_cur->parent->right = NULL;
			tp->tree_cur->parent = NULL;
		}
		tree_new = tp->tree_cur;
	}
	tree_new->v = st->flags;
	return (tree_new);
}

static void
	f_parser_iftoken__tokenprim__tokenprim(
		t_tree *tree_parent, t_tree *tree_new)
{
	DEBUG;
	while (tree_parent->right && IS_TOKENPRIM(tree_parent->right->v))
		tree_parent = tree_parent->right;
	if (tree_parent != tree_new)
	{
		tree_new->left = tree_parent->right;
		tree_parent->right = tree_new;
		tree_new->parent = tree_parent;
		if (tree_new->left)
			tree_new->left->parent = tree_new;
	}
}

int
	f_parser_iftoken__tokenprim(struct s_token *st, t_parser *tp)
{
	t_tree	*tree_parent;
	t_tree	*tree_new;

	DEBUG;
	tree_new = f_parser_iftoken__tokenprim__tree_new(st, tp);
	tree_parent = tp->tree_parent;
	if (IS_TOKENPRIM(tree_parent->v))
		f_parser_iftoken__tokenprim__tokenprim(tree_parent, tree_new);
	else
	{
		DEBUG;
		tp->tree_parent = tree_new;
		tree_new->left = tree_parent;
		if (tree_parent)
			tree_parent->parent = tree_new;
	}
	tp->tree_cur = tree_new;
	return (f_parser_axiome(tp));
}
