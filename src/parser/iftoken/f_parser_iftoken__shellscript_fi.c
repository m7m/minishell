/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__shellscript_fi.c                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 16:53:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/27 16:53:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../f_tree.h"
#include "../../../inc/l_sh.h"
#include "../../../inc/sh_macro.h"

int
	f_parser_iftoken__sh_check(struct s_token *st, t_parser *tp)
{
	char	*prev;

	DEBUG;
	prev = ft_pass_space(tp->line);
	if (prev != tp->pline)
		return (-1);
	DEBUG;
	if (st->flags & (FL_DONE | FL_FI | FL_ESAC)
		&& tp->pline[st->len_c]
		&& !ft_strchr(" \t\n;<>&|#", tp->pline[st->len_c]))
		return (-1);
	DEBUGNBRUL(st->flags & (FL_IF | FL_ELIF | FL_ELSE));
	DEBUGSTR(tp->pline);
	DEBUGSTR(tp->pline + st->len_c);
	if (st->flags & (FL_IF | FL_ELIF | FL_ELSE | FL_WHILE | FL_DO)
		&& (!tp->pline[st->len_c]
			|| !ft_strchr(" \t\n;&<>#", tp->pline[st->len_c])))
		return (-1);
	*tp->pline = 0;
	tp->pline += st->len_c;
	tp->line = tp->pline;
	DEBUG;
	return (0);
}

/*
** Apres le retour de f_parser_iftoken_recursreturn
** si !-1 alors tout c'est bien passer
**  donc le pointeur 'pline' est defini sur le caractere suivant
*/

int
	f_parser_iftoken__sh_firts(
		long flag_start,
		long flag_end,
		struct s_token *st, t_parser *tp)
{
	int	ret;

	DEBUGSTR(tp->pline);
	if (f_parser_iftoken__sh_check(st, tp))
		return (0);
	DEBUGNBRUL(st->flags);
	if (tp->tree_cur->left)
	{
		DEBUGPTR(tp->tree_cur->left);
		DEBUGNBRUL(tp->tree_cur->left->v);
		DEBUGSTR(tp->tree_cur->left->line);
		MSG_ERRINTER("tp->tree_cur->left is nil");
		return (-1);
	}
	tp->tree_cur->v = st->flags;
	ret = f_parser_iftoken_recursreturn(flag_start, flag_end, tp);
	DEBUGNBR(ret);
	if (ret)
		return (ret);
	DEBUG;
	return (f_parser_axiome(tp));
}

int
	f_parser_iftoken__sh_if(struct s_token *st, t_parser *tp)
{
	return (f_parser_iftoken__sh_firts(FL_IF, FL_FI, st, tp));
}

int
	f_parser_iftoken__sh_while(struct s_token *st, t_parser *tp)
{
	return (f_parser_iftoken__sh_firts(FL_WHILE, FL_DONE, st, tp));
}

/*
** si fi: parent absolu devien fi
*/

int
	f_parser_iftoken__sh_end(struct s_token *st, t_parser *tp)
{
	DEBUGSTR(tp->pline);
	if (f_parser_iftoken__sh_check(st, tp))
		return (0);
	if (!tp->tree_cur->parent)
		ft_exit(EXIT_FAILURE, "fi not parent\n");
	if (tp->tree_cur->parent->left == tp->tree_cur)
		ft_exit(EXIT_FAILURE, "fi parent is left\n");
	if (tp->tree_cur->left || tp->tree_cur->right)
		ft_exit(EXIT_FAILURE, "fi what implement ??\n");
	f_tree_mvparent(&tp->tree_parent, tp->tree_cur);
	tp->tree_cur->v = st->flags;
	DEBUGNBRUL(st->flags);
	DEBUGSTR(tp->tree_cur->line);
	return (1);
}
