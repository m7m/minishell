/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__rede_out_append.c                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 07:11:15 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 07:11:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/redirecting.h"
#include "iftoken.h"
#include <fcntl.h>

int
	f_parser_iftoken__rede_out_append(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt |= SFD_OPEN;
	tp->tree_cur->tfd.file_flags |= O_APPEND | O_CREAT | O_WRONLY;
	return (f_parser_iftoken__rede_out__commun(st, tp));
}
