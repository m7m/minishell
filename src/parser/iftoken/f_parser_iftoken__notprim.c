/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__notprim.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:20:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:20:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../f_tree.h"

/*
** le noeud courant devient le token
** le nouveau noeud est mis a droite
**  ret le residu y est mis
** puis recursion
*/

int
	f_parser_iftoken__notprim(struct s_token *st,
							char *residu, t_parser *tp)
{
	t_tree	*tree_new;

	DEBUGSTR(tp->pline);
	DEBUGSTR(tp->line);
	tree_new = f_tree_newadd(0, tp->tree_cur);
	ft_memchk_exit(tree_new);
	tree_new->line = residu;
	DEBUGSTR(residu);
	tp->tree_cur->v = st->flags;
	return (f_parser_iftoken_place(st, tp));
}
