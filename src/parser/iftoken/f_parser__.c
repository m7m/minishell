/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser__.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 22:37:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/19 22:37:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"

int
	f_parser_iftoken__none(struct s_token *st, t_parser *tp)
{
	DEBUG;
	(void)st;
	(void)tp;
	return (-1);
}
