/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken_residu.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:34:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:34:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../../../inc/l_sh.h"

char
	*f_parser_iftoken_residu(t_parser *tp)
{
	int		len;
	char	*residu;

	DEBUGSTR(tp->line);
	len = tp->pline - tp->line;
	if (len)
	{
		residu = ft_strsub(tp->line, 0, len);
		ft_memchk_exit(residu);
	}
	else
		residu = NULL;
	DEBUGSTR(residu);
	return (residu);
}

char
	*f_parser_iftoken_residu2(struct s_token *st, t_parser *tp)
{
	char	*residu;

	DEBUGSTR(tp->line);
	residu = f_parser_iftoken_residu(tp);
	DEBUGSTR(residu);
	DEBUGSTR(tp->line);
	DEBUGSTR(tp->pline);
	*tp->pline = 0;
	tp->pline += st->len_c;
	tp->line = tp->pline;
	return (residu);
}
