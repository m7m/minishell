/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/15 14:54:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/15 14:54:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"

static struct s_token
	*f_parser_iftoken__init__red(struct s_token *pst)
{
	MC_TOKEN("&>>", REDE_OUT, f_parser_iftoken__rede_out_errappend);
	MC_TOKEN("&>", REDE_OUT, f_parser_iftoken__rede_out_err);
	MC_TOKEN(">&-", REDE_OUT, f_parser_iftoken__rede_out_close);
	MC_TOKEN(">&", REDE_OUT, f_parser_iftoken__rede_out_dup);
	MC_TOKEN(">>", REDE_OUT, f_parser_iftoken__rede_out_append);
	MC_TOKEN(">", REDE_OUT, f_parser_iftoken__rede_out);
	MC_TOKEN("<<<", HERE_LINE, f_parser_iftoken__rede_in_heredoc);
	MC_TOKEN("<<-", HERE_DOCTAB, f_parser_iftoken__rede_in_heredoc);
	MC_TOKEN("<<", HERE_DOC, f_parser_iftoken__rede_in_heredoc);
	MC_TOKEN("<&-", REDE_IN, f_parser_iftoken__rede_in_close);
	MC_TOKEN("<&", REDE_IN, f_parser_iftoken__rede_in_dup);
	MC_TOKEN("<", REDE_IN, f_parser_iftoken__rede_in);
	return (pst);
}

static struct s_token
	*f_parser_iftoken__init__shellscript(struct s_token *pst)
{
	DEBUGSTR("'");
	MC_TOKEN("if", FL_IF, f_parser_iftoken__sh_if);
	MC_TOKEN("then", FL_THEN, f_parser_iftoken__sh_if);
	MC_TOKEN("elif", FL_ELIF, f_parser_iftoken__sh_if);
	MC_TOKEN("else", FL_ELSE, f_parser_iftoken__sh_if);
	MC_TOKEN("fi", FL_FI, f_parser_iftoken__sh_end);
	/* MC_TOKEN("for", 0, f_parser_iftoken__shellscript_); */
	/* MC_TOKEN("in", 0, f_parser_iftoken__shellscript_); */
	MC_TOKEN("while", FL_WHILE, f_parser_iftoken__sh_while);
	MC_TOKEN("done", FL_DONE, f_parser_iftoken__sh_end);
	MC_TOKEN("do", FL_DO, f_parser_iftoken__sh_while);
	/* MC_TOKEN("case", 0, f_parser_iftoken__shellscript_); */
	/* MC_TOKEN("esac", 0, f_parser_iftoken__shellscript_); */
	/* MC_TOKEN(";;", 0, f_parser_iftoken__shellscript_); */
	return (pst);
}

static void
	f_parser_iftoken__init(struct s_token *pst)
{
	pst = f_parser_iftoken__init__red(pst);
	MC_TOKEN("&&", AND, f_parser_iftoken__and);
	MC_TOKEN("||", OR, f_parser_iftoken__and);
	MC_TOKEN("|&", PIPEERR, f_parser_iftoken__and);
	MC_TOKEN("|", PIPE, f_parser_iftoken__and);
	pst = f_parser_iftoken__init__shellscript(pst);
	MC_TOKEN("\n", NEWLINE, f_parser_iftoken__tokenprim);
	MC_TOKEN(";", SEMICO, f_parser_iftoken__tokenprim);
	MC_TOKEN("&", ESPE, f_parser_iftoken__tokenprim);
	MC_TOKEN(0, 0, f_parser_iftoken__none);
}

#if DEBUG_LVL > 0
# define F_TEST_TREE(t) f_test_tree(t)

static void
	f_test_tree(t_tree *t)
{
	if (!t->parent)
	{
		DEBUGNBRUL(REDE_OUT);
		DEBUGNBRUL(FL_PIPEERR);
		DEBUGNBRUL(FL_PIPE);
		DEBUGNBRUL(FL_IF);
		DEBUGNBRUL(FL_FI);
		DEBUGNBRUL(FL_ELIF);
		DEBUGNBRUL(FL_ELSE);
		DEBUGNBRUL(FL_THEN);
	}
	DEBUGSTR("\n");
	DEBUGPTR(t);
	DEBUGSTR(t->line);
	DEBUGNBRUL(t->v);
	DEBUGPTR(t->parent);
	DEBUGPTR(t->left);
	DEBUGPTR(t->right);
	DEBUGSTR(t->tfd.str);
	DEBUGNBR(t->tfd.pipe[0]);
	DEBUGNBR(t->tfd.pipe[1]);
	if (t->left)
		f_test_tree(t->left);
	if (t->right)
		f_test_tree(t->right);
}

#else
# define F_TEST_TREE(t)
#endif

int
	f_parser_iftoken(t_parser *tp)
{
	static struct s_token	st[255] = {{0, 0, 0, 0, 0}};
	int						i;
	char					*c;

	DEBUG;
	c = tp->pline;
	if (!st->c)
		f_parser_iftoken__init(st);
	;
	i = 0;
	DEBUG;
	while (st[i].c)
	{
		if (!ft_strncmp(st[i].c, c, st[i].len_c))
		{
			DEBUGPTR(tp->tree_cur);
			F_TEST_TREE(tp->tree_parent);
			return (st[i].f(st + i, tp));
		}
		++i;
	}
	return (0);
}
