/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__rede_out.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 13:20:35 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 13:20:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/redirecting.h"
#include "iftoken.h"
#include <fcntl.h>

int
	f_parser_iftoken__rede_out(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt |= SFD_OPEN;
	tp->tree_cur->tfd.file_flags |= O_TRUNC | O_CREAT | O_WRONLY;
	return (f_parser_iftoken__rede_out__commun(st, tp));
}

int
	f_parser_iftoken__rede_out_dup(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt |= SFD_DUP;
	return (f_parser_iftoken__rede_out__commun(st, tp));
}

int
	f_parser_iftoken__rede_out_close(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt = SFD_CLOSE;
	return (f_parser_iftoken__rede_out__commun(st, tp));
}

int
	f_parser_iftoken__rede_out_errappend(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.file_flags |= O_APPEND | O_CREAT | O_WRONLY;
	tp->tree_cur->tfd.opt |= SFD_ERRTOOUT;
	return (f_parser_iftoken__rede_out__commun(st, tp));
}

int
	f_parser_iftoken__rede_out_err(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.file_flags |= O_TRUNC | O_CREAT | O_WRONLY;
	tp->tree_cur->tfd.opt |= SFD_ERRTOOUT;
	return (f_parser_iftoken__rede_out__commun(st, tp));
}
