/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__and.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:21:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:21:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"

int
	f_parser_iftoken__and(struct s_token *st, t_parser *tp)
{
	char	*residu;

	DEBUG;
	residu = f_parser_iftoken_residu2(st, tp);
	return (f_parser_iftoken__notprim(st, residu, tp));
}
