/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_addtoken.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:29:37 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:29:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"

void
	f_addtoken(struct s_token *st, char *c, unsigned long fl,
			int (*f)(struct s_token *, t_parser *))
{
	DEBUGSTR(c);
	DEBUGNBRUL(fl);
	st->c = c;
	st->len_c = ft_strlen(c);
	st->flags = fl;
	st->lvl = f_parser_token_lvl(fl);
	st->f = f;
}
