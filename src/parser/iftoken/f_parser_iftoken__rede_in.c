/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__rede_in.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 13:17:57 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 13:17:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/redirecting.h"
#include "iftoken.h"
#include <fcntl.h>

int
	f_parser_iftoken__rede_in(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt |= SFD_OPEN;
	tp->tree_cur->tfd.file_flags |= O_RDONLY;
	return (f_parser_iftoken__rede_in__commun(st, tp));
}

int
	f_parser_iftoken__rede_in_dup(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt |= SFD_DUP;
	return (f_parser_iftoken__rede_in__commun(st, tp));
}

int
	f_parser_iftoken__rede_in_close(struct s_token *st, t_parser *tp)
{
	DEBUG;
	tp->tree_cur->tfd.opt |= SFD_CLOSE;
	return (f_parser_iftoken__rede_in__commun(st, tp));
}
