/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken_recursreturn.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:30:28 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:30:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../../../inc/sh_macro.h"

static int
	f_parser_iftoken_recursreturn__notflagend(
		unsigned long flag_end, struct s_parser *tp, struct s_parser *sstp)
{
	tp->tree_cur->right = sstp->tree_parent;
	DEBUGSTR("not flag_end");
	if (!tp->tree_cur->parent)
	{
		DEBUGSTR("error: tp->tree_cur->parent is nil");
		return (-1);
	}
	DEBUGPTR(sstp->tree_cur);
	DEBUGSTR(sstp->tree_cur->line);
	DEBUGNBRUL(sstp->tree_cur->v);
	DEBUGPTR(sstp->tree_cur->parent);
	tp->tree_cur->left = tp->tree_parent;
	tp->tree_cur->parent->right = NULL;
	tp->tree_parent->parent = tp->tree_cur;
	tp->tree_parent = tp->tree_cur;
	if (sstp->tree_parent->v == flag_end)
		tp->tree_cur = sstp->tree_parent;
	else
		tp->tree_cur = sstp->tree_cur;
	return (1);
}

static int
	f_parser_iftoken_recursreturn__continu(
		unsigned long flag_end, struct s_parser *tp, struct s_parser *sstp)
{
	DEBUGSTR(tp->line);
	DEBUGSTR(tp->pline);
	DEBUGNBRUL(tp->tree_cur->v);
	DEBUGNBRUL(sstp->tree_cur->v);
	DEBUGNBRUL(sstp->tree_parent->v);
	if (sstp->tree_cur->v != flag_end)
	{
		MSG_ERRINTER("sstp->tree_cur not flag_end");
		tp->tree_cur->left = sstp->tree_parent;
		return (-1);
	}
	tp->tree_cur->left = NULL;
	tp->tree_cur->right = sstp->tree_parent;
	tp->tree_cur = sstp->tree_cur;
	return (0);
}

/*
** flag_start détermine la fin de la remonter
** flag_end permet lors de la remonter de vérifier que flag_start à son flag_end
** La récursion sauvegarde l'état actuel, flag_start,
** puis part sur un nouvelle arbre
**  une fonctione dans la recursion doit l'arrêter, la recursion,
**   cette fonction détermine le flag_end
**  lors de la remonter tant que nous ne sommes par revenu à flag_start,
**   nous remontons le flag_end pour le lier à son start
**  une fois revenue sur flag_start retourne 0
** Entre au moins une fois en recursion.
** Retourne:
**  -1 == erreur
**   0 == remonter fini
**   1 == entraint de remonter
**  tp->tree_cur == flag_end donc attention en level
** toute les remonté met sur la droite le sous arbre
*/

int
	f_parser_iftoken_recursreturn(
		unsigned long flag_start, unsigned long flag_end, struct s_parser *tp)
{
	t_parser	sstp;

	DEBUGSTR(tp->line);
	f_parser_init_struct(tp->line, tp->sh, &sstp);
	DEBUGSTR(tp->line);
	if (f_parser_axiome(&sstp) != -1)
	{
		DEBUGPTR(sstp.tree_parent);
		sstp.tree_parent->parent = tp->tree_cur;
		tp->line = sstp.line;
		tp->pline = sstp.pline;
		if (tp->tree_cur->v != flag_start)
			return (f_parser_iftoken_recursreturn__notflagend(
						flag_end, tp, &sstp));
			;
		DEBUGSTR("fin de la remonte de la recursion");
		return (f_parser_iftoken_recursreturn__continu(flag_end, tp, &sstp));
	}
	DEBUGSTR("f_parser_iftoken_recursreturn: echec recursion");
	return (-1);
}
