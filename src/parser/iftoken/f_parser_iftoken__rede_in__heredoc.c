/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__rede_in__heredoc.c               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:23:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:23:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../../../inc/sh_macro.h"
#include "../../../inc/redirecting.h"

static char
	*f_parser_iftoken__rede_in__heredoc__eof(int *len, t_parser *tp)
{
	char	*eof;

	eof = (char *)malloc(SC(*len + 3));
	ft_memchk_exit(eof);
	DEBUGSTR(tp->tree_cur->line);
	if (tp->tree_cur->tfd.str)
	{
		ft_strcpy(eof + 1, tp->tree_cur->tfd.str);
		free(tp->tree_cur->tfd.str);
	}
	else
		eof[1] = 0;
	eof[0] = '\n';
	eof[++(*len)] = 0;
	DEBUGSTR(eof);
	if (!eof[1])
		ft_strcpy(eof + 1, "\n");
	return (eof);
}

static void
	f_parser_iftoken__rede_in__heredoc__msg(
		char *eof, char *eof_start, char *eof_end, t_parser *tp)
{
	if (!eof_end)
	{
		MSG_ERR3("warning", "end of line not terminated");
		tp->tree_cur->tfd.str = ft_strdup(eof_start + 1);
		*eof_start = 0;
	}
	else
	{
		eof_end[1] = 0;
		tp->tree_cur->tfd.str = ft_strdup(eof_start + 1);
		ft_strcpy(eof_start, eof_end + ft_strlen(eof));
	}
	ft_memchk_exit(tp->tree_cur->tfd.str);
	DEBUGSTR(tp->tree_cur->tfd.str);
}

void
	f_parser_iftoken__rede_in__heredoc(t_parser *tp)
{
	int		len;
	char	*eof;
	char	*eof_start;
	char	*eof_end;
	char	*tmp;

	DEBUGSTR(tp->line);
	DEBUGSTR(tp->pline);
	DEBUGSTR(tp->tree_cur->line);
	DEBUGSTR(tp->tree_cur->tfd.str);
	eof = tp->tree_cur->tfd.str;
	if ((eof_start = ft_strchr(tp->pline, '\n')))
	{
		len = ft_strlen(eof);
		eof = f_parser_iftoken__rede_in__heredoc__eof(&len, tp);
		tmp = eof_start;
		while ((eof_end = ft_strstr(tmp, eof))
			&& eof_end[len] != 0 && eof_end[len] != '\n')
			tmp = eof_end + 1;
		f_parser_iftoken__rede_in__heredoc__msg(eof, eof_start, eof_end, tp);
	}
	else
		ft_memchk_exit(tp->tree_cur->tfd.str = ft_strdup(""));
	free(eof);
	tp->pline = tp->line;
}
