/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken_place.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/31 15:41:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/31 15:41:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../f_tree.h"

static void
	f_parser_iftoken_place__tparent(t_tree *tparent, t_parser *tp)
{
	DEBUG;
	if (tp->tree_cur == tp->tree_cur->parent->left)
		tp->tree_cur->parent->left = tp->tree_cur->left;
	else
		tp->tree_cur->parent->right = tp->tree_cur->left;
	DEBUGPTR(tp->tree_cur);
	if (tparent->left)
	{
		DEBUG;
		if (tparent->right)
			tp->tree_cur->left = tparent->right;
		tparent->right = tp->tree_cur;
	}
	else
	{
		DEBUG;
		tp->tree_cur->left = tparent->right;
		tparent->right = tp->tree_cur;
	}
	tp->tree_cur->parent = tparent;
}

static void
	f_parser_iftoken_place__curparent(t_parser *tp)
{
	DEBUGPTR(tp->tree_cur);
	DEBUGPTR(tp->tree_cur->left);
	DEBUGPTR(tp->tree_cur->parent->left);
	DEBUGPTR(tp->tree_cur->parent->right);
	if (tp->tree_cur == tp->tree_cur->parent->left)
	{
		DEBUGSTR(tp->tree_cur->parent->left->line);
		tp->tree_cur->parent->left = tp->tree_cur->left;
		DEBUGSTR(tp->tree_cur->parent->left->line);
	}
	else
	{
		DEBUGPTR(tp->tree_cur->parent->right);
		tp->tree_cur->parent->right = tp->tree_cur->left;
		DEBUGPTR(tp->tree_cur->parent->right);
		DEBUGNBR(tp->tree_cur->parent->v);
	}
	tp->tree_cur->left = tp->tree_parent;
	tp->tree_parent->parent = tp->tree_cur;
	tp->tree_parent = tp->tree_cur;
	tp->tree_cur->parent = NULL;
}

static t_tree
	*f_parser_iftoken_place__chr(struct s_token *st, t_parser *tp)
{
	t_tree	*tparent;

	tparent = tp->tree_cur->parent;
	DEBUGNBR(st->lvl);
	if (st->lvl == 2)
		while (tparent && st->lvl <= f_parser_token_lvl(tparent->v))
			tparent = tparent->parent;
	else
		while (tparent && st->lvl < f_parser_token_lvl(tparent->v))
			tparent = tparent->parent;
	DEBUGPTR(tparent);
	DEBUGPTR(tp->tree_cur);
	DEBUGPTR(tp->tree_cur->parent);
	return (tparent);
}

/*
** Place le noeud en fonction du niveau du token
*/

int
	f_parser_iftoken_place(struct s_token *st, t_parser *tp)
{
	t_tree	*tparent;

	DEBUGPTR(tp->tree_cur);
	tparent = f_parser_iftoken_place__chr(st, tp);
	DEBUGPTR(tparent);
	if (tparent)
		f_parser_iftoken_place__tparent(tparent, tp);
	else if (tp->tree_cur->parent)
		f_parser_iftoken_place__curparent(tp);
	{
		DEBUGSTR("\n");
		DEBUGPTR(tp->tree_cur);
		DEBUGSTR(tp->tree_cur->line);
		DEBUGNBR(tp->tree_cur->v);
		DEBUGPTR(tp->tree_cur->parent);
		DEBUGPTR(tp->tree_cur->left);
		DEBUGPTR(tp->tree_cur->right);
		DEBUGSTR(tp->tree_cur->tfd.str);
	}
	return (f_parser_axiome(tp));
}
