/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iftoken.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:29:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:29:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IFTOKEN_H
# define IFTOKEN_H

# include "../f_parser.h"

# define MC_TOKEN(c, fl, f) f_addtoken(pst++, c, fl, f)

char	*f_parser_iftoken_residu(t_parser *tp);
char	*f_parser_iftoken_residu2(struct s_token *st, t_parser *tp);
int		f_parser_iftoken_recursreturn(
	unsigned long flag_start, unsigned long flag_end, struct s_parser *tp);

void	f_parser_iftoken__rede_in__heredoc(t_parser *tp);
int		f_parser_iftoken__rede_in_heredoc(struct s_token *st, t_parser *tp);

int		f_parser_iftoken__tokenprim(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__pipe(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__none(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__pipeerr(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__or(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__and(struct s_token *st, t_parser *tp);

int		f_parser_iftoken__rede_out_errappend(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_out_err(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_out_close(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_out_dup(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_out_append(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_out(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_out__commun(struct s_token *st, t_parser *tp);

int		f_parser_iftoken__rede_in_close(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_in_dup(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_in(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__rede_in__commun(struct s_token *st, t_parser *tp);

int		f_parser_iftoken__notprim(struct s_token *st,
								char *residu, t_parser *tp);
int		f_parser_iftoken_place(struct s_token *st, t_parser *tp);

int		f_parser_iftoken__sh_if(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__sh_end(struct s_token *st, t_parser *tp);
int		f_parser_iftoken__sh_while(struct s_token *st, t_parser *tp);

void	f_addtoken(
	struct s_token *st, char *c, unsigned long fl,
	int (*f)(struct s_token *, t_parser *));
int		f_parser_iftoken__dol_parenth_dquot(
	struct s_token *st, t_parser *tp);

void	f_parser_axiome__alloc_tree_cur(t_parser *tp);
int		f_parser_iftoken__subsh_recurs(
	struct s_token *st, t_parser *tp);

#endif
