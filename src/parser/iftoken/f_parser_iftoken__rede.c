/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_iftoken__rede.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/25 10:27:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/25 10:27:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iftoken.h"
#include "../../../inc/l_sh.h"
#include "../../etat/etat.h"
#include "../../../inc/redirecting.h"
#include "../f_tree.h"

static void
	f_parser_iftoken__rede___left(int fd_read, int fd_default, t_parser *tp)
{
	char	*c_fd;
	char	*residu;
	char	*residu_end;

	DEBUG;
	residu = tp->line;
	residu_end = tp->pline;
	while (residu_end > residu && *residu_end != ' ' && *residu_end != '\t')
		--residu_end;
	if (*residu_end == ' ' || *residu_end == '\t')
		++residu_end;
	c_fd = residu_end;
	residu = residu_end;
	residu_end = tp->pline;
	while (residu < residu_end && ft_isdigit(*residu))
		++residu;
	if (residu == residu_end && c_fd != residu)
	{
		tp->tree_cur->tfd.pipe[fd_read] = ft_atoi(c_fd);
		ft_strcpy(c_fd, tp->pline);
		tp->pline = c_fd;
	}
	else
		tp->tree_cur->tfd.pipe[fd_read] = fd_default;
	DEBUGNBR(tp->tree_cur->tfd.pipe[fd_read]);
}

static int
	f_parser_iftoken__rede___right(struct s_token *st, t_parser *tp)
{
	char	*line;
	char	*line_modifer;

	DEBUGSTR(tp->pline);
	if (tp->tree_cur->tfd.opt & SFD_CLOSE)
	{
		ft_strcpy(tp->pline, tp->pline + st->len_c);
		return (0);
	}
	line = tp->pline + st->len_c;
	if (!(line_modifer = ft_etat_word(&line, tp->sh)))
		return (-1);
	DEBUGSTR(line);
	DEBUGSTR(line_modifer);
	tp->tree_cur->line = line;
	;
	ft_strcpy(tp->pline, line_modifer);
	free(line_modifer);
	;
	DEBUGSTR(tp->pline);
	tp->tree_cur->tfd.str = tp->tree_cur->line;
	tp->tree_cur->line = NULL;
	return (0);
}

int
	f_parser_iftoken__rede_out__commun(struct s_token *st, t_parser *tp)
{
	DEBUGSTR(tp->line);
	DEBUGSTR(tp->pline);
	f_parser_iftoken__rede___left(0, STDOUT_FILENO, tp);
	DEBUGSTR(tp->pline);
	DEBUGSTR(tp->line);
	DEBUGNBR(SFD_CLOSE & tp->tree_cur->tfd.opt);
	if (f_parser_iftoken__rede___right(st, tp))
		return (-1);
	tp->tree_cur->v = st->flags;
	tp->tree_cur->tfd.opt |= SFD_REDI_OUT;
	return (f_parser_iftoken_place(st, tp));
}

int
	f_parser_iftoken__rede_in__commun(struct s_token *st, t_parser *tp)
{
	DEBUGSTR(tp->line);
	DEBUGSTR(tp->pline);
	f_parser_iftoken__rede___left(1, STDIN_FILENO, tp);
	DEBUGSTR(tp->pline);
	DEBUGSTR(tp->line);
	DEBUGNBR(st->flags);
	if (f_parser_iftoken__rede___right(st, tp))
	{
		DEBUGSTR("ERREUR");
		return (-1);
	}
	tp->tree_cur->v = st->flags;
	tp->tree_cur->tfd.opt |= SFD_REDI_IN;
	DEBUGNBR(tp->tree_cur->tfd.pipe[0]);
	DEBUGNBR(tp->tree_cur->tfd.pipe[1]);
	return (f_parser_iftoken_place(st, tp));
}

int
	f_parser_iftoken__rede_in_heredoc(struct s_token *st, t_parser *tp)
{
	DEBUGSTR(tp->line);
	DEBUGSTR(tp->pline);
	f_parser_iftoken__rede___left(1, STDIN_FILENO, tp);
	DEBUGSTR(tp->pline);
	DEBUGSTR(tp->line);
	DEBUGNBR(st->flags);
	if (f_parser_iftoken__rede___right(st, tp))
	{
		DEBUGSTR("ERREUR");
		return (-1);
	}
	if (st->flags == HERE_LINE)
		tp->tree_cur->tfd.opt |= SFD_HERE_LIN;
	else
	{
		f_parser_iftoken__rede_in__heredoc(tp);
		if (st->flags == HERE_DOCTAB)
			tp->tree_cur->tfd.opt |= SFD_HERE_DOC_TAB;
		else
			tp->tree_cur->tfd.opt |= SFD_HERE_DOC;
	}
	tp->tree_cur->v = st->flags;
	return (f_parser_iftoken_place(st, tp));
}
