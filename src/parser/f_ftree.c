/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/01 14:42:29 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/01 14:42:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree.h"
#include "f_flags_tree.h"
#include "tree/f_tree_exec.h"

#include "f_ftreetoargv.h"

#define SIZEOF_LONG ((unsigned long)sizeof(long) * 8)
#define FLAG_TO_INDEX(flag) f_ftree_calc_index((unsigned long)flag)
#define FTREE_ADD(flag, func) f[ FLAG_TO_INDEX(flag) ] = func;

static int
	f_ftree_calc_index(unsigned long flag)
{
	unsigned long	i;

	i = 0;
	if (flag)
		while (i < SIZEOF_LONG)
		{
			if ((unsigned long)flag &
				(unsigned long)((unsigned long)1 << (unsigned long)i))
				return (i);
			++i;
		}
	return (i);
}

static void
	f_ftree_init(void (*f[SIZEOF_LONG + 1])(t_tree *, t_sh *))
{
	DEBUG;
	FTREE_ADD(NEWLINE, f_ftree__newline);
	FTREE_ADD(ESPE, f_ftree__espe);
	FTREE_ADD(SEMICO, f_ftree__semico);
	FTREE_ADD(OR, f_ftree__or);
	FTREE_ADD(AND, f_ftree__and);
	FTREE_ADD(PIPEERR, f_ftree__pipeerr);
	FTREE_ADD(PIPE, f_ftree__pipe);
	FTREE_ADD(REDE_IN, f_ftree__redirecting);
	FTREE_ADD(REDE_OUT, f_ftree__redirecting);
	FTREE_ADD(HERE_DOC, f_ftree__redirecting);
	FTREE_ADD(HERE_DOCTAB, f_ftree__redirecting);
	FTREE_ADD(HERE_LINE, f_ftree__redirecting);
	FTREE_ADD(FL_IF, f_ftree__shellscript_if);
	FTREE_ADD(FL_THEN, f_ftree__shellscript_if);
	FTREE_ADD(FL_ELIF, f_ftree__shellscript_if);
	FTREE_ADD(FL_ELSE, f_ftree__shellscript_if);
	FTREE_ADD(FL_FI, f_ftree__shellscript_if);
	FTREE_ADD(FL_WHILE, f_ftree__shellscript_while);
	FTREE_ADD(FL_DO, f_ftree__shellscript_while);
	FTREE_ADD(FL_DONE, f_ftree__shellscript_while);
}

void
	(*f_ftree(unsigned long flag))(t_tree *, t_sh *)
{
	static void (*f[SIZEOF_LONG + 1])(t_tree *, t_sh *) = {NULL};

	DEBUGNBRUL(flag);
	if (flag)
	{
		if (!*f)
			f_ftree_init(f);
		DEBUGNBR(FLAG_TO_INDEX(flag));
		DEBUGPTR(f[FLAG_TO_INDEX(flag)]);
		if (flag && !f[FLAG_TO_INDEX(flag)])
			ft_exit(EXIT_FAILURE, "f_ftree: erreur: fonction lie non trouve\n");
		return (f[FLAG_TO_INDEX(flag)]);
	}
	return (NULL);
}

/*
** Permet de coinvertir l'arbre en (char **)
** voir le job control
*/

static void
	f_ftreetoargv_init(void (*f[SIZEOF_LONG + 1])(char ***, t_tree *))
{
	DEBUG;
	FTREE_ADD(NEWLINE, f_ftreetoargv__semico);
	FTREE_ADD(ESPE, f_ftreetoargv__espe);
	FTREE_ADD(SEMICO, f_ftreetoargv__semico);
	FTREE_ADD(OR, f_ftreetoargv__or);
	FTREE_ADD(AND, f_ftreetoargv__and);
	FTREE_ADD(PIPEERR, f_ftreetoargv__pipeerr);
	FTREE_ADD(PIPE, f_ftreetoargv__pipe);
	FTREE_ADD(REDE_IN, f_ftreetoargv__redirecting);
	FTREE_ADD(REDE_OUT, f_ftreetoargv__redirecting);
	FTREE_ADD(HERE_DOC, f_ftreetoargv__redirecting);
	FTREE_ADD(HERE_DOCTAB, f_ftreetoargv__redirecting);
	FTREE_ADD(HERE_LINE, f_ftreetoargv__redirecting);
	FTREE_ADD(FL_IF, f_ftreetoargv__shellscript_if);
	FTREE_ADD(FL_THEN, f_ftreetoargv__shellscript_if);
	FTREE_ADD(FL_ELIF, f_ftreetoargv__shellscript_if);
	FTREE_ADD(FL_ELSE, f_ftreetoargv__shellscript_if);
	FTREE_ADD(FL_FI, f_ftreetoargv__shellscript_if);
	FTREE_ADD(FL_PAREN, f_ftreetoargv__parenth);
	FTREE_ADD(FL_DOL_PARENTH, f_ftreetoargv__dol_parenth);
	FTREE_ADD(FL_PAREN_CLOSE, f_ftreetoargv__parenth_close);
	FTREE_ADD(FL_GRAVE, f_ftreetoargv__magicquot);
	FTREE_ADD(FL_WHILE, f_ftreetoargv__shellscript_while);
	FTREE_ADD(FL_DO, f_ftreetoargv__shellscript_while);
	FTREE_ADD(FL_DONE, f_ftreetoargv__shellscript_while);
}

void
	(*f_ftreetoargv(unsigned long flag))(char ***, t_tree *)
{
	static void (*f[SIZEOF_LONG + 1])(char ***, t_tree *) = {NULL};

	DEBUGNBRUL(flag);
	if (flag)
	{
		if (!*f)
			f_ftreetoargv_init(f);
		DEBUGNBR(FLAG_TO_INDEX(flag));
		DEBUGPTR(f[FLAG_TO_INDEX(flag)]);
		if (flag && !f[FLAG_TO_INDEX(flag)])
			ft_exit(EXIT_FAILURE,
				"f_ftreetoargv: erreur: fonction lie non trouve\n");
		return (f[FLAG_TO_INDEX(flag)]);
	}
	return (NULL);
}
