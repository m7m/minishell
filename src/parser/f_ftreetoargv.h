/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftreetoargv.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 06:54:31 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 06:54:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_FTREETOARGV_H
# define F_FTREETOARGV_H

void	f_ftreetoargv__shellscript_while(char ***argv, t_tree *t);
void	f_ftreetoargv__shellscript_if(char ***argv, t_tree *t);
void	f_ftreetoargv__redirecting(char ***argv, t_tree *t);
void	f_ftreetoargv__pipe(char ***argv, t_tree *t);
void	f_ftreetoargv__pipeerr(char ***argv, t_tree *t);
void	f_ftreetoargv__and(char ***argv, t_tree *t);
void	f_ftreetoargv__or(char ***argv, t_tree *t);
void	f_ftreetoargv__semico(char ***argv, t_tree *t);
void	f_ftreetoargv__espe(char ***argv, t_tree *t);
void	f_ftreetoargv__parenth_close(char ***argv, t_tree *t);
void	f_ftreetoargv__dol_parenth(char ***argv, t_tree *t);
void	f_ftreetoargv__parenth(char ***argv, t_tree *t);
void	f_ftreetoargv__magicquot(char ***argv, t_tree *t);

#endif
