/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_token_lvl.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 22:19:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/22 22:19:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_parser.h"

int
	f_parser_token_lvl(int flag)
{
	if (IS_TOKEN_LVL1(flag))
		return (1);
	else if (IS_TOKEN_LVL2(flag))
		return (2);
	else if (IS_TOKEN_LVL3(flag))
		return (3);
	else if (IS_TOKEN_LVL4(flag))
		return (4);
	return (NB_FLAGS + 1);
}
