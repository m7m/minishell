/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_flags_tree.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/15 12:05:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/15 12:05:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_FLAGS_TREE_H
# define F_FLAGS_TREE_H

# include "../../inc/flags.h"

/*
** lvl 1
*/
# define NEWLINE		FL_NEWLINE_2
# define ESPE			FL_ESPELUE
# define SEMICO			FL_SEMICOL

/*
** lvl 2
*/
# define OR				FL_OR
# define AND			FL_AND

/*
** lvl 3
*/
# define PIPEERR		FL_PIPEERR
# define PIPE			FL_PIPE

# define MASQ_SHELSCRIPT ((long)0x3FF << 27)

/*
** lvl 4
*/
# define REDE_IN		FL_REDIREC_IN
# define REDE_OUT		FL_REDIREC_OUT
# define HERE_DOC		FL_HERED
# define HERE_DOCTAB	FL_HEREDT
# define HERE_LINE		FL_HEREL

/*
** lvl 5
*/
# define GRAVE			FL_GRAVE
# define DOL_PARENTH	FL_DOL_PARENTH
# define DOL_ACCOL		FL_DOL_ACCOL
# define PARENTH		FL_PAREN
# define ACCOL			FL_ACCOL

# define SQUOT			FL_SQUOT
# define DQUOT			FL_DQUOT
# define CROCH			FL_CROCH
# define ACCOL			FL_ACCOL
# define PAREN			FL_PAREN

# define DOLLA			FL_DOLLA
# define EQUAL			FL_EQUAL
# define ANTSL			FL_ANTSL

/*
** Hors level
*/

# define NB_FLAGS		38

# define TOKEN_LVL1 (NEWLINE | ESPE | SEMICO)
# define TOKEN_LVL2 (OR | AND)
# define TOKEN_LVL3 (PIPEERR | PIPE)
# define TOKEN_LVL4 (REDE_IN | REDE_OUT | HERE_DOC | HERE_DOCTAB | HERE_LINE)
# define TOKEN_LVL5 ~(TOKEN_LVL1| TOKEN_LVL2 | TOKEN_LVL3 | TOKEN_LVL4)

# define IS_TOKEN_LVL1(flag) (flag & TOKEN_LVL1)
# define IS_TOKEN_LVL2(flag) (flag & TOKEN_LVL2)
# define IS_TOKEN_LVL3(flag) (flag & TOKEN_LVL3)
# define IS_TOKEN_LVL4(flag) (flag & TOKEN_LVL4)
# define IS_TOKEN_LVL5(flag) (flag & TOKEN_LVL5)

# define IS_TOKEN_LVL45(flag) (flag & (TOKEN_LVL4 | TOKEN_LVL5))
# define IS_TOKEN_LVL15(flag) (flag & ~0)
# define IS_TOKEN_LVL13(flag) (flag & (TOKEN_LVL1 | TOKEN_LVL2 | TOKEN_LVL3))

# define IS_TOKEN_REDIR(flag) (flag & (TOKEN_LVL3 | TOKEN_LVL4))
# define IS_TOKEN_SHELLSCRIPT(flag) (flag & MASQ_SHELSCRIPT)

#endif
