/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 00:31:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/14 00:31:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_PARSER_H
# define F_PARSER_H

# include "t_parser.h"
# include "unistd.h"
# include <libft.h>
# include "f_flags_tree.h"
# include "../etat/chkclose/ft_etat_chkclose.h"

/*
** & && | || |& ; \n
** if then elif fi while do done for case ;; until esac in !
** pas de geston de ;;
*/

# define LEX "&;\n|"

# define TOKEN_PRIM "&;\n"
# define TOKEN TOKEN_PRIM "|()<>"
# define SEPARAT " '\"`~${}#*[]?\t"

# define IS_TOKENPRIM(x) (x & TOKEN_LVL1)

void	f_parser_init(t_sh *sh, char *line);
int		f_parser_axiome(t_parser *tp);

int		f_parser_iftoken(t_parser *tp);
int		f_parser_token_lvl(int flag);

int		f_parser__chrclose(t_parser *tp);
int		f_parser_error(t_parser *tp, const char *msg);
int		f_parser_axiome_check(t_parser *tp);

int		ft_linecomplete_flags_c(char *c_pline, long *flags);

void	f_parser_init_struct(char *line, t_sh *sh, t_parser *tp);

char	*f_parser_iftoken_residu(t_parser *tp);

#endif
