/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 11:23:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 11:23:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_parser.h"
#include "../../inc/minishell.h"

/*
** manque free
*/

int
	f_parser_error(t_parser *tp, const char *msg)
{
	MSG_ERR3(tp->pline, msg);
	return (-1);
}
