/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_treetoargv__2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 07:04:39 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 07:04:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../t_tree.h"
#include "../../../inc/l_sh.h"

void	f_ftreetoargv__or(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "||");
}

void	f_ftreetoargv__semico(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, ";");
}

void	f_ftreetoargv__espe(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "&");
}

void	f_ftreetoargv__parenth(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "(");
}

void	f_ftreetoargv__dol_parenth(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "$(");
}
