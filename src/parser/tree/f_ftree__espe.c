/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__espe.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:35:28 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:35:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/jobs.h"
#include "../../../inc/flags.h"

void
	f_ftree__espe(t_tree *t, t_sh *sh)
{
	int	old_opt;

	old_opt = sh->opt;
	sh->opt &= ~(SH_JOBS | SH_TTY);
	sh->tljobs.last_pc = 0;
	f_ftree_subshell(JOB_RUNNING, NULL, t->left, sh);
	sh->opt = old_opt;
	f_tree_exec(t->right, sh);
}
