/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_pipe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 16:04:48 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/20 16:04:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/l_sh.h"
#include "../../../inc/t_builtins.h"
#include "../../../inc/built_in_jobs.h"
#include "../../../inc/jobs.h"
#include "../../../inc/sh_macro.h"
#include "../../../inc/ft_updatekey.h"

#include "../f_tree.h"
#include "f_ftree_sub.h"

#include "f_tree_exec.h"
#include "../../../inc/redirecting.h"
#include <fcntl.h>

#include "../inc/pr__static_tjobs.h"
#include "../inc/built_in_jobs_new.h"

static int
	ft_etat_subshell_tree__commun(int *p, t_tree *t, t_sh *sh)
{
	int				ret;

	sh->opt |= SH_REPARSE | SH_SUBSH;
	ret = f_ftree_subshell(JOB_PENDING, p, t, sh);
	sh->opt &= ~(SH_REPARSE | SH_SUBSH);
	;
	f_tree_freeone(t);
	return (ret);
}

int
	ft_etat_subshell_tree2(
		char *start,
		char *end,
		t_linetoargv *tlta)
{
	t_tree	*t;

	if (!(end - start))
		return (0);
	ft_memchk_exit(t = f_tree_newadd(0, 0));
	t->line = ft_strsub(start, 0, end - start);
	DEBUGSTR(t->line);
	ft_memchk_exit(t->line);
	if (ft_etat_subshell_tree__commun(0, t, tlta->sh))
		return (-1);
	f_tree_wait(tlta->sh);
	return (0);
}

static void
	ft_etat_subshell_tree__end__join(
		int len,
		char *line,
		char *start,
		t_linetoargv *tlta)
{
	char	*tmp;

	len = start - tlta->line;
	tmp = ft_strjoin_inser(tlta->line, line, len);
	free(line);
	ft_memchk_exit(tmp);
	free(tlta->line);
	tlta->pline = tmp + len;
	tlta->line = tmp;
}

static int
	ft_etat_subshell_tree__end(
		int *p,
		char *start,
		char *end,
		t_linetoargv *tlta)
{
	int		len;
	char	*line;

	line = NULL;
	len = f_ftree__sub_read(p[0], &line, &tlta->sh->tljobs);
	close(p[0]);
	close(p[1]);
	if (len == -1)
	{
		free(line);
		MSG_ERRINTER("f_ftree__subpipe return -1");
		return (-1);
	}
	DEBUGSTR(line);
	f_ftree__sub_endline(len, line);
	DEBUGSTR(line);
	ft_strcpy(start, end);
	if (line)
		ft_etat_subshell_tree__end__join(len, line, start, tlta);
	return (0);
}

int
	ft_etat_subshell_tree(
		char *start,
		char *end,
		t_linetoargv *tlta)
{
	int		ret;
	int		p[2];
	t_tree	*t;
	t_argv	*targv;

	if (pipe(p))
	{
		ft_updatekey_ret(&tlta->sh->hlocal, 2);
		MSG_ERR3("pipe()", "internaly error");
		return (-1);
	}
	if (!(end - start))
		return (0);
	ft_memchk_exit(t = f_tree_newadd(0, 0));
	t->line = ft_strsub(start, 0, end - start);
	DEBUGSTR(t->line);
	ft_memchk_exit(t->line);
	targv = tlta->sh->targv;
	tlta->sh->targv = NULL;
	f_ftree__sub_tfd(p, tlta->sh);
	ret = ft_etat_subshell_tree__commun(p, t, tlta->sh);
	tlta->sh->targv = targv;
	if (ret == -1)
		return (-1);
	return (ft_etat_subshell_tree__end(p, start, end, tlta));
}
