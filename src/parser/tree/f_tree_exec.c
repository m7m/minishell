/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_exec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:36:03 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:36:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/sh_macro.h"
#include "../../../inc/ft_updatekey.h"

void
	f_tree_exec(t_tree *t, t_sh *sh)
{
	void	(*f)(t_tree *, t_sh *);

	DEBUGPTR(t);
	if (!t)
		return ;
	DEBUGNBRUL(t->v);
	DEBUGSTR(t->line);
	if (!t->v && !t->line)
		return ;
	f = f_ftree(t->v);
	DEBUGPTR(f);
	if (f)
		f(t, sh);
	else
		f_ftree__lastparse(t, sh);
	DEBUG;
}

void
	f_tree_execone(t_tree *t, t_sh *sh)
{
	t_tree	*tr;
	t_tree	*tl;

	DEBUGPTR(t);
	tr = t->right;
	tl = t->left;
	t->right = NULL;
	t->left = NULL;
	t->v = 0;
	DEBUGSTR(t->line);
	f_tree_exec(t, sh);
	t->right = tr;
	t->left = tl;
}
