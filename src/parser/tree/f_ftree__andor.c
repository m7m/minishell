/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__andor.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 13:25:58 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 13:25:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/ft_updatekey.h"
#include "../../../inc/pr_xetlocal.h"

void
	f_ftree__and(t_tree *t, t_sh *sh)
{
	DEBUG;
	f_tree_exec(t->left, sh);
	DEBUG;
	f_tree_wait(sh);
	DEBUG;
	if (!ft_getlocalint("?"))
		f_tree_exec(t->right, sh);
}

void
	f_ftree__or(t_tree *t, t_sh *sh)
{
	DEBUG;
	f_tree_exec(t->left, sh);
	DEBUG;
	f_tree_wait(sh);
	DEBUG;
	if (ft_getlocalint("?"))
		f_tree_exec(t->right, sh);
}
