/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_add_tfd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 08:46:21 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 08:46:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

void
	f_add_tfd(
		struct s_argv **targv,
		int fd_read, int fd_write, int opt)
{
	struct s_fd	tfd;

	ft_memset(&tfd, 0, sizeof(struct s_fd));
	tfd.pipe[0] = fd_read;
	tfd.pipe[1] = fd_write;
	tfd.opt = opt;
	f_ftree__redirecting_add(&tfd, targv);
}
