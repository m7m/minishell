/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_newlinesemico.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:48:47 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:48:47 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

static void
	f_ftree__newline_count(int count_line, t_tree *t, t_sh *sh)
{
	DEBUGPTR(t);
	DEBUGPTR(t->left);
	f_tree_exec(t->left, sh);
	if (count_line)
		++sh->nb_line;
	DEBUGPTR(t);
	f_tree_wait(sh);
	DEBUGPTR(t->right);
	if (t->right)
		f_tree_exec(t->right, sh);
	DEBUG;
}

void
	f_ftree__newline(t_tree *t, t_sh *sh)
{
	f_ftree__newline_count(1, t, sh);
}

void
	f_ftree__semico(t_tree *t, t_sh *sh)
{
	DEBUG;
	f_ftree__newline_count(0, t, sh);
	DEBUG;
}
