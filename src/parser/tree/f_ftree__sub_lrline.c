/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__sub_lrline.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 14:06:52 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 14:06:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/l_sh.h"
#include "../../../inc/sh_macro.h"

static void
	f_ftree__sub_lrline__right(t_tree *t_left, t_tree *t_right)
{
	DEBUGNBR(t_right->v);
	DEBUGSTR(t_right->line);
	if (t_right->line)
	{
		if (t_left->line)
		{
			t_left->line = ft_strjoin_free12(
				&t_left->line, &t_right->line);
			ft_memchk_exit(t_left->line);
		}
		else
		{
			t_left->line = t_right->line;
			t_right->line = NULL;
		}
		DEBUGSTR(t_left->line);
	}
}

void
	f_ftree__sub_lrline(t_tree *t_left, char *line, t_tree *t_right)
{
	DEBUG;
	if (!t_left)
	{
		MSG_ERRINTER("t_left is nil");
		return ;
	}
	DEBUGSTR(t_left->line);
	DEBUGSTR(line);
	if (!t_left->line)
		t_left->line = line;
	else if (line)
	{
		t_left->line = ft_strjoin_free12(&t_left->line, &line);
		ft_memchk_exit(t_left->line);
		DEBUGSTR(t_left->line);
	}
	DEBUGPTR(t_right);
	if (t_right)
	{
		DEBUGSTR(t_right->line);
		f_ftree__sub_lrline__right(t_left, t_right);
	}
	DEBUGSTR(t_left->line);
}
