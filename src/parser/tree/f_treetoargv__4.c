/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_treetoargv__4.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 19:29:40 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/12 19:29:40 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

void	f_ftreetoargv__shellscript_while(char ***argv, struct s_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "while");
}
