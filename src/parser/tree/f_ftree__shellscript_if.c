/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__shellscript_if.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:50:17 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:50:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/flags.h"
#include "../../../inc/pr_xetlocal.h"
#include "../../../inc/built_in_jobs.h"
#include "../../../inc/ft_updatekey.h"
#include "../f_flags_tree.h"

static void
	f_ftree__shellscript_if__then(t_tree *t, t_sh *sh)
{
	DEBUG;
	f_tree_exec(t->left, sh);
	DEBUG;
	if (ft_getlocalint("?"))
	{
		DEBUGPTR(t->right);
		ft_updatekey_ret(&sh->hlocal, 0);
		f_tree_exec(t->right, sh);
		DEBUG;
	}
	else
	{
		DEBUGPTR(t->right->left);
		f_tree_exec(t->right->left, sh);
		DEBUG;
	}
	DEBUG;
}

void
	f_ftree__shellscript_if(t_tree *t, t_sh *sh)
{
	DEBUGSTR("is shellscript");
	if (t->v & FL_IF)
	{
		if (t->parent && IS_TOKEN_REDIR(t->parent->v))
			sh->tljobs.last_pc =
				f_ftree_subshell(JOB_PENDING, NULL, t->right, sh);
		else
			f_tree_exec(t->right, sh);
	}
	else if (t->v & FL_ELIF)
		f_tree_exec(t->right, sh);
	else if (t->v & FL_ELSE)
		f_tree_exec(t->right->left, sh);
	else if (t->v & FL_THEN)
		f_ftree__shellscript_if__then(t, sh);
	if (!ft_jobs_count_status(JOB_PENDING, &sh->tljobs))
		ft_jobs_stat(&sh->tljobs);
}
