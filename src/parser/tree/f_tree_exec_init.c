/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_exec_init.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:47:50 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:47:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

void
	f_tree_exec_init(t_tree *t, t_sh *sh)
{
	DEBUG;
	if (t->v)
	{
		f_tree_exec(t, sh);
		if (sh->targv)
			f_ftree__lastparse(t, sh);
	}
	else
		f_ftree__lastparse(t, sh);
	DEBUG;
	f_tree_wait(sh);
}
