/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_treetoargv__3.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 07:05:14 by mmichel           #+#    #+#             */
/*   Updated: 2017/02/01 07:05:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../t_tree.h"
#include "../../../inc/l_sh.h"

void	f_ftreetoargv__parenth_close(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, ")");
}

void	f_ftreetoargv__magicquot(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "`");
}
