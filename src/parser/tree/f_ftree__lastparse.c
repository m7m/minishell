/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__lastparse.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 13:26:15 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 13:26:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

#include "../../../inc/targv.h"
#include "../../../inc/ft_updatekey.h"
#include "../../../inc/built_in.h"

#include "../../../inc/minishell.h"

static void
	f_ftree__lastparse__ignspnl(t_tree *t, t_linetoargv *tlta)
{
	char	*s;

	if (t->line)
	{
		s = ft_pass_spacenewline(t->line);
		if (*s)
		{
			tlta->line = ft_strdup(t->line);
			DEBUGSTR(tlta->line);
			ft_memchk_exit(tlta->line);
			DEBUG;
			tlta->pline = tlta->line;
		}
	}
}

void
	f_ftree__lastparse(t_tree *t, t_sh *sh)
{
	t_linetoargv	tlta;

	DEBUGNBRUL(t->v);
	ft_memset(&tlta, 0, sizeof(tlta));
	DEBUG;
	tlta.sh = sh;
	DEBUGSTR(t->line);
	f_ftree__lastparse__ignspnl(t, &tlta);
	DEBUGSTR(tlta.line);
	if (!sh->targv)
		ft_memchk_exit(ft_targv_newadd(&sh->targv));
	sh->sf->line_lastparse = tlta.line;
	if (!tlta.line || !ft_etat_lastparse(&tlta))
		ft_isexec(sh, sh->targv);
	else
		ft_updatekey_ret(&sh->hlocal, 1);
	DEBUG;
	ft_targv_freeone(&sh->targv);
	free(tlta.line);
	sh->sf->line_lastparse = NULL;
	DEBUG;
}
