/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_wait.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:51:26 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:51:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/pr_fork.h"
#include <sys/wait.h>
#include <signal.h>

static void
	f_tree_wait__w_pc(pid_t pc, int status)
{
	int		sig;

	if (pc > 0)
	{
		DEBUGNBR(status);
		DEBUGNBR(WIFSIGNALED(status));
		DEBUGNBR(WIFEXITED(status));
		DEBUGNBR(WIFSTOPPED(status));
		if (WIFSIGNALED(status))
			sig = WTERMSIG(status);
		else if (WIFSTOPPED(status))
			sig = WSTOPSIG(status);
		else
			sig = 0;
		if (WIFEXITED(status))
			status = WEXITSTATUS(status);
		else
			status = 0;
		DEBUGNBR(status);
		DEBUGNBR(sig);
		ft_jobs_stat_actu(pc, status, sig);
	}
}

static int
	f_tree_wait__w(int i, pid_t pc, t_sh *sh)
{
	int		status;
	pid_t	pgrp;

	status = 0;
	pgrp = getpgid(pc);
	if (pgrp != -1)
		pc = waitpid(-pgrp, &status, WUNTRACED);
	else
		pc = waitpid(pc, &status, WUNTRACED);
	DEBUGNBR(pc);
	DEBUGNBR(status);
	if (pc < 1 || status == -1)
		++(i);
	else
	{
		i = 0;
		if (WIFSTOPPED(status))
		{
			ft_builtins_jobs_mod(JOB_STOPPED, pc, &sh->tljobs);
			return (i);
		}
	}
	f_tree_wait__w_pc(pc, status);
	DEBUGNBR(i);
	return (i);
}

static void
	f_tree_wait__upd(t_sh *sh)
{
	int		status;

	if (ft_jobs_count_status(JOB_PENDING, &sh->tljobs))
	{
		DEBUG;
		MSG_ERRINTER("ft_jobs_count_status != 0");
	}
	if (sh->tljobs.tjobs[sh->tljobs.max_index].status == JOB_PENDING_DELETE)
	{
		status = sh->tljobs.tjobs[sh->tljobs.max_index].sig_status;
		DEBUGNBR(sh->tljobs.tjobs[sh->tljobs.max_index].pc);
		DEBUGNBR(sh->tljobs.tjobs[sh->tljobs.max_index].status);
		DEBUGNBR(status);
		ft_updatekey_ret(&sh->hlocal, status);
		ft_jobs_stat(&sh->tljobs);
	}
	else if (sh->tljobs.tjobs[sh->tljobs.max_index].status == JOB_STOPPED)
	{
		status = sh->tljobs.tjobs[sh->tljobs.max_index].sig_status;
		DEBUGNBR(status);
		ft_updatekey_ret(&sh->hlocal, status);
		ft_jobs_stat(&sh->tljobs);
	}
	sh->tljobs.last_pc = 0;
}

void
	f_tree_wait(t_sh *sh)
{
	int		i;
	pid_t	pc;

	DEBUG;
	if (!(pc = ft_jobs_count_status(JOB_PENDING, &sh->tljobs)))
		return ;
	i = 0;
	while (i < 3 && pc)
	{
		if (kill(pc, 0))
			ft_builtins_jobs_mod(JOB_PENDING_DELETE, pc, &sh->tljobs);
		else
			i = f_tree_wait__w(i, pc, sh);
		if (i < 3)
			pc = ft_jobs_count_status(JOB_PENDING, &sh->tljobs);
	}
	DEBUGNBR(i);
	f_tree_wait__upd(sh);
}
