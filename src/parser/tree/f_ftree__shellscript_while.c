/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__shellscript_while.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 18:50:24 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/12 18:50:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../../../inc/flags.h"
#include "../../../inc/pr_xetlocal.h"
#include "../../../inc/built_in_jobs.h"
#include "../../../inc/ft_updatekey.h"
#include "../f_flags_tree.h"
#include <signal.h>

static int
	f_ftree__shellscript_while__ctrl_c(int i)
{
	static int	ii = 0;
	int			iii;

	iii = ii;
	ii = i;
	return (iii);
}

static void
	f_ftree__shellscript_while__do(t_tree *t, t_sh *sh)
{
	struct sigaction act;
	struct sigaction orig;

	DEBUG;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	act.sa_handler = (void (*)(int))(f_ftree__shellscript_while__ctrl_c);
	act.sa_flags = SA_RESTART;
	DEBUG;
	f_tree_exec(t->left, sh);
	DEBUG;
	sigaction(SIGINT, &act, &orig);
	while (!ft_getlocalint("?") && !f_ftree__shellscript_while__ctrl_c(0))
	{
		DEBUGPTR(t->right->left);
		f_tree_exec(t->right->left, sh);
		DEBUG;
		if (!ft_jobs_count_status(JOB_PENDING, &sh->tljobs))
			ft_jobs_stat(&sh->tljobs);
		f_tree_exec(t->left, sh);
	}
	sigaction(SIGINT, &orig, 0);
	DEBUG;
}

void
	f_ftree__shellscript_while(t_tree *t, t_sh *sh)
{
	DEBUGSTR("is shellscript");
	if (t->v & FL_WHILE)
	{
		if (t->parent && IS_TOKEN_REDIR(t->parent->v))
			sh->tljobs.last_pc =
				f_ftree_subshell(JOB_PENDING, NULL, t->right, sh);
		else
			f_tree_exec(t->right, sh);
	}
	else if (t->v & FL_DO)
		f_ftree__shellscript_while__do(t, sh);
}
