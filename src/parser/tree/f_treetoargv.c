/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_treetoargv.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:54:25 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:54:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../t_tree.h"
#include "../../../inc/l_sh.h"
#include <libft.h>

void
	f_ftreetoargv__commun(char ***argv, char *ins)
{
	char	**tmp;

	DEBUGSTR(ins);
	tmp = *argv;
	ins = ft_strdup(ins);
	ft_memchk_exit(ins);
	tmp = ft_taballocins(tmp, ins);
	ft_memchk_exit(tmp);
	free(*argv);
	*argv = tmp;
}

void
	f_treetoargv(t_tree *t, char ***argv)
{
	void	(*f)(char ***, t_tree *);

	DEBUGPTR(t->right);
	if (t->right)
		f_treetoargv(t->right, argv);
	DEBUG;
	{
		DEBUGNBR(t->v);
		DEBUGSTR(t->line);
		f = f_ftreetoargv(t->v);
		if (f)
			f(argv, t);
		else if (t->line)
			f_ftreetoargv__commun(argv, t->line);
	}
	DEBUGPTR(t->left);
	if (t->left)
		f_treetoargv(t->left, argv);
	DEBUG;
}

char
	**f_treetoargv_init(t_tree *t)
{
	char	**argv;

	DEBUG;
	argv = (char **)malloc(sizeof(char *));
	ft_memchk_exit(argv);
	*argv = NULL;
	f_treetoargv(t, &argv);
	return (argv);
}
