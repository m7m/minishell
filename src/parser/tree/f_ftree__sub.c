/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__sub.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 13:12:19 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 13:12:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/l_sh.h"
#include "../../../inc/t_builtins.h"
#include "../../../inc/built_in_jobs.h"
#include "../../../inc/jobs.h"
#include "../../../inc/sh_macro.h"

#include "f_tree_exec.h"
#include "../../../inc/redirecting.h"
#include <fcntl.h>

static char
	*f_ftree__sub_read__join(int *len, char *old_line, char **line)
{
	(void)len;
	old_line = ft_strjoin_free12(&old_line, line);
	ft_memchk_exit(old_line);
	return (old_line);
}

int
	f_ftree__sub_read(
		int fd_read, char **line, struct s_builtins_jobs *tljobs)
{
	int		len;
	int		n;
	char	*old_line;

	DEBUG;
	*line = NULL;
	old_line = NULL;
	n = 0;
	while (ft_jobs_count_status(JOB_PENDING, tljobs) || !old_line)
	{
		len = ft_readblock(fd_read, line);
		if (len < 1)
			break ;
		if (old_line)
			old_line = f_ftree__sub_read__join(&len, old_line, line);
		else
			old_line = *line;
		*line = NULL;
		n += len;
	}
	ft_strdel(line);
	*line = old_line;
	DEBUGSTR(*line);
	DEBUGNBR(n);
	return (n);
}

void
	f_ftree__sub_endline(int len, char *line)
{
	DEBUGSTR(line);
	DEBUGNBR(len);
	if (line && len)
	{
		DEBUGC(line[len - 1]);
		if (line[len - 1] == '\n')
			line[len - 1] = 0;
		DEBUGSTR(line);
	}
}

void
	f_ftree__sub_tfd(int p[2], struct s_sh *sh)
{
	DEBUGNBR(p[0]);
	DEBUGNBR(p[1]);
	COM("'");
	/* if (fcntl(p[1], F_SETFD, FD_CLOEXEC) == -1) */
	/* 	MSG_ERR3INT(p[1], "fcntl(fd, F_SETFD, FD_CLOEXEC)"); */
	DEBUG;
	f_add_tfd(&sh->targv, STDOUT_FILENO, p[1], SFD_DUP | SFD_REDI_OUT);
}
