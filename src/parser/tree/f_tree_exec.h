/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_exec.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:39:17 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:39:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_TREE_EXEC_H
# define F_TREE_EXEC_H

# include "../t_tree.h"
# include "../../etat/lastparse/ft_etat_lastparse.h"

void	f_add_tfd(
	struct s_argv **targv,
	int fd_read, int fd_write, int opt);

# define F_FTREE void	(*f_ftree(unsigned long flag)) (t_tree *, t_sh *)
# define F_FTREETO void (*f_ftreetoargv(unsigned long flag))(char ***, t_tree *)

/*
** la norm vois une global ???
*/

F_FTREE;
F_FTREETO;

void	f_ftreetoargv__commun(char ***argv, char *ins);
char	**f_treetoargv_init(t_tree *t);

void	f_tree_exec_init(t_tree *t, t_sh *sh);
void	f_tree_exec(t_tree *t, t_sh *sh);
void	f_tree_execone(t_tree *t, t_sh *sh);

void	f_tree_wait(t_sh *sh);
pid_t	f_ftree_subshell(
	int job_status, int pipe[2], t_tree *t_child, t_sh *sh);

void	f_ftree__semico(t_tree *t, t_sh *sh);
void	f_ftree__newline(t_tree *t, t_sh *sh);
void	f_ftree__espe(t_tree *t, t_sh *sh);

void	f_ftree__and(t_tree *t, t_sh *sh);
void	f_ftree__or(t_tree *t, t_sh *sh);

void	f_ftree__shellscript_if(t_tree *t, t_sh *sh);
void	f_ftree__shellscript_while(t_tree *t, t_sh *sh);

void	f_ftree__lastparse(t_tree *t, t_sh *sh);

void	f_ftree__redirecting_add(
	struct s_fd *tfd, struct s_argv **targv);

void	f_ftree__redirecting(t_tree *t, t_sh *sh);
void	f_ftree__pipe(t_tree *t, t_sh *sh);
void	f_ftree__pipeerr(t_tree *t, t_sh *sh);

#endif
