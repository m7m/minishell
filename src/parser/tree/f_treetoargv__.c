/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_treetoargv__.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:52:24 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 14:52:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../t_tree.h"
#include "../../../inc/l_sh.h"

void	f_ftreetoargv__shellscript_if(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "if");
}

void	f_ftreetoargv__redirecting(char ***argv, t_tree *t)
{
	char	si[255];

	DEBUG;
	ft_itoa_s(t->tfd.pipe[0], si);
	f_ftreetoargv__commun(argv, si);
	f_ftreetoargv__commun(argv, " <?> ");
	ft_itoa_s(t->tfd.pipe[1], si);
	f_ftreetoargv__commun(argv, si);
	if (t->tfd.str)
		f_ftreetoargv__commun(argv, t->tfd.str);
}

void	f_ftreetoargv__pipe(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "|");
}

void	f_ftreetoargv__pipeerr(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "|&");
}

void	f_ftreetoargv__and(char ***argv, t_tree *t)
{
	DEBUG;
	(void)t;
	f_ftreetoargv__commun(argv, "&&");
}
