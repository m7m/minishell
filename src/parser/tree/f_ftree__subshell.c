/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__subshell.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 11:24:24 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 11:24:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"
#include "../f_parser.h"
#include "../../../inc/sh_macro.h"
#include "../../../inc/pr_signal.h"
#include "../../../inc/flags.h"
#include "../../../inc/pr_xetlocal.h"
#include "../../../inc/built_in_jobs.h"
#include "../f_tree.h"
#include "pr_fork.h"
#include "../../f_free_notglobal.h"

static void
	f_ftree_subshell__child_end(
		t_argv *targv, t_tree *t_child, t_sh *sh)
{
	DEBUG;
	ft_jobs_stat(&sh->tljobs);
	if (t_child)
	{
		f_tree_freeparent(t_child);
		sh->sf->t = NULL;
	}
	if (targv)
	{
		ft_fork__child_close(targv);
		ft_targv_freeone(&targv);
	}
	close(STDOUT_FILENO);
	close(STDIN_FILENO);
	DEBUGSTR("'");
	/* f_free_notglobal_cal(); */
	DEBUGSTR("'");
	ft_exit(ft_getlocalint("?"), NULL);
}

static void
	f_ftree_subshell__child_reparse(
		t_tree *t_child, t_sh *sh)
{
	char	*line;

	DEBUGPTR(t_child);
	DEBUGSTR(t_child->line);
	line = t_child->line;
	t_child->line = NULL;
	DEBUGSTR("'");
	/* f_free_notglobal_cal(); */
	sh->opt &= ~SH_REPARSE;
	f_parser_init(sh, line);
	free(line);
	sh->sf->line = NULL;
}

static void
	f_ftree_subshell__child(
		int job_status, int pipe[2], t_tree *t_child, t_sh *sh)
{
	t_argv	*targv;

	ft_jobs_reset();
	targv = sh->targv;
	sh->targv = NULL;
	if (pipe)
		close(pipe[0]);
	(void)job_status;
	sh->tljobs.last_pc = getpid();
	if (targv)
		ft_fork__child_open(targv->tfd, targv, sh);
	if (IS_SUBSH(sh))
		ft_signal_reset_job();
	else
		ft_signal_reset();
	sh->opt &= ~(SH_TTY | SH_JOBS);
	DEBUGNBR(sh->opt);
	if (sh->opt & SH_REPARSE)
		f_ftree_subshell__child_reparse(t_child, sh);
	else
		f_tree_exec_init(t_child, sh);
	f_ftree_subshell__child_end(targv, t_child, sh);
}

static void
	f_ftree_subshell__parent(
		pid_t pc, int *pipe, t_sh *sh)
{
	if (pipe)
		close(pipe[1]);
	ft_sigaction_init();
	if (IS_SHTTY(sh))
	{
		tcsetpgrp(STDIN_FILENO, sh->tljobs.last_pc);
		setpgid(sh->tljobs.last_pc, sh->tljobs.last_pc);
	}
	if (!sh->tljobs.last_pc)
		sh->tljobs.last_pc = pc;
}

pid_t
	f_ftree_subshell(
		int job_status, int pipe[2], t_tree *t_child, t_sh *sh)
{
	pid_t	pc;
	char	**argv;

	pc = fork();
	if (pc == -1)
		MSG_ERR3("fork", "return -1");
	else if (!pc)
		f_ftree_subshell__child(job_status, pipe, t_child, sh);
	else
	{
		f_ftree_subshell__parent(pc, pipe, sh);
		argv = f_treetoargv_init(t_child);
		ft_memchk_exit(argv);
		ft_builtins_jobs_add(job_status, pc, &sh->tljobs, argv);
		ft_tabfree(&argv);
	}
	ft_targv_freeone(&sh->targv);
	return (pc);
}
