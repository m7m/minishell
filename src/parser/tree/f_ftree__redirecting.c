/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__redirecting.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 13:30:14 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 13:30:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

#include "../../../inc/targv.h"
#include "../../../inc/tfd.h"

void
	f_ftree__redirecting_add(
		struct s_fd *tfd, struct s_argv **targv)
{
	t_fd	*tfd_new;

	DEBUGPTR(targv);
	if (!*targv)
		ft_memchk_exit(ft_targv_newadd(targv));
	tfd_new = ft_tfd_newadd(&(*targv)->tfd);
	tfd_new->pipe[0] = tfd->pipe[0];
	tfd_new->pipe[1] = tfd->pipe[1];
	tfd_new->opt = tfd->opt;
	tfd_new->str = tfd->str;
	tfd->str = NULL;
	tfd_new->file_flags = tfd->file_flags;
	DEBUG;
}

void
	f_ftree__redirecting(t_tree *t, t_sh *sh)
{
	DEBUG;
	;
	if (SFD_DUP & t->tfd.opt)
	{
		if (!ft_strisdigit(t->tfd.str))
			t->tfd.opt = (t->tfd.opt & ~SFD_DUP) | SFD_OPEN;
		else
			t->tfd.pipe[!!(SFD_REDI_OUT & t->tfd.opt)] = ft_atoi(t->tfd.str);
		DEBUGNBR(t->tfd.pipe[0]);
		DEBUGNBR(t->tfd.pipe[1]);
	}
	f_ftree__redirecting_add(&t->tfd, &sh->targv);
	DEBUGPTR(t->left);
	DEBUGPTR(t->right);
	if (t->left)
		f_tree_exec(t->left, sh);
	else if (t->right)
		f_tree_exec(t->right, sh);
}
