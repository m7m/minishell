/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree__pipe.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 13:30:02 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/25 13:30:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_tree_exec.h"

#include "../../../inc/targv.h"
#include "../../../inc/tfd.h"
#include "../../../inc/sh_macro.h"
#include "../../../inc/ft_updatekey.h"
#include <fcntl.h>

static void
	f_ftree__pipe__tfd(int enable_stderr, int *p,
					t_tree *t, t_sh *sh)
{
	int	opt;

	DEBUG;
	if (fcntl(p[1], F_SETFD, FD_CLOEXEC) == -1)
		MSG_ERR3INT(p[1], "fcntl(pipe[1], F_SETFD, FD_CLOEXEC)");
	if (fcntl(p[0], F_SETFD, FD_CLOEXEC) == -1)
		MSG_ERR3INT(p[1], "fcntl(pipe[1], F_SETFD, FD_CLOEXEC)");
	opt = SFD_DUP | SFD_REDI_OUT;
	;
	if (enable_stderr)
		f_add_tfd(&sh->targv, STDERR_FILENO, p[1], opt);
	f_add_tfd(&sh->targv, STDOUT_FILENO, p[1], opt);
	;
	DEBUGPTR(sh->targv->tfd);
	ft_fdtexit(p[0]);
	f_tree_exec(t->left, sh);
	ft_fdtexit_del(p[0]);
	close(p[1]);
	DEBUG;
	f_add_tfd(&sh->targv, STDIN_FILENO, p[0], opt);
	DEBUG;
	f_tree_exec(t->right, sh);
	DEBUG;
	close(p[0]);
}

static void
	f_ftree__pipe__commun(int enable_stderr,
						t_tree *t, t_sh *sh)
{
	int	p[2];

	DEBUG;
	if (pipe(p))
	{
		ft_updatekey_ret(&sh->hlocal, 2);
		MSG_ERR3("pipe()", "internaly error");
	}
	else
	{
		DEBUGNBR(p[0]);
		DEBUGNBR(p[1]);
		f_ftree__pipe__tfd(enable_stderr, p, t, sh);
	}
	DEBUG;
}

void
	f_ftree__pipe(t_tree *t, t_sh *sh)
{
	DEBUG;
	f_ftree__pipe__commun(0, t, sh);
	DEBUG;
}

void
	f_ftree__pipeerr(t_tree *t, t_sh *sh)
{
	DEBUG;
	f_ftree__pipe__commun(1, t, sh);
	DEBUG;
}
