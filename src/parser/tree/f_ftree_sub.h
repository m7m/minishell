/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_ftree_sub.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 13:20:04 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 13:20:04 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef F_FTREE_SUB_H
# define F_FTREE_SUB_H

# include "../../../inc/t_builtins.h"
# include "../t_tree.h"

int		f_ftree__sub_read(
	int fd_read, char **line, struct s_builtins_jobs *tljobs);
void	f_ftree__sub_lrline(
	struct s_tree *t_left, char *line, struct s_tree *t_right);
void	f_ftree__sub_endline(int len, char *line);

void	f_ftree__sub_tfd(int p[2], struct s_sh *sh);

#endif
