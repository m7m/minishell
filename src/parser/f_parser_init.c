/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 11:21:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 11:21:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_parser.h"
#include "f_tree.h"
#include "../../inc/minishell.h"

#if DEBUG_LVL > 0
# define F_TEST_TREE(t) f_test_tree(t)

static void
	f_test_tree(t_tree *t)
{
	if (!t->parent)
	{
		DEBUGNBRUL(REDE_OUT);
		DEBUGNBRUL(FL_PIPEERR);
		DEBUGNBRUL(FL_PIPE);
		DEBUGNBRUL(FL_IF);
		DEBUGNBRUL(FL_FI);
		DEBUGNBRUL(FL_ELIF);
		DEBUGNBRUL(FL_ELSE);
		DEBUGNBRUL(FL_THEN);
	}
	DEBUGSTR("\n");
	DEBUGPTR(t);
	DEBUGSTR(t->line);
	DEBUGNBRUL(t->v);
	DEBUGPTR(t->parent);
	DEBUGPTR(t->left);
	DEBUGPTR(t->right);
	DEBUGSTR(t->tfd.str);
	DEBUGNBR(t->tfd.pipe[0]);
	DEBUGNBR(t->tfd.pipe[1]);
	if (t->left)
		f_test_tree(t->left);
	if (t->right)
		f_test_tree(t->right);
}

#else
# define F_TEST_TREE(t)
#endif

/*
** retourne la racine du noeud
*/

t_tree
	*f_parser(t_parser *tp)
{
	if (f_parser_axiome(tp) != -1)
		return (tp->tree_parent);
	f_tree_free(tp->tree_parent);
	return (NULL);
}

void
	f_parser_init_struct(char *line, t_sh *sh, t_parser *tp)
{
	tp->line = line;
	tp->pline = tp->line;
	tp->sh = sh;
	tp->tree_parent = NULL;
	tp->tree_cur = NULL;
}

void
	f_parser_init(t_sh *sh, char *line)
{
	t_parser	tp;

	DEBUGSTR(line);
	sh->sf->line = line;
	if (ft_etat_chkclose(line, sh)
		|| !*ft_pass_spacenewline(line))
		return ;
	f_parser_init_struct(line, sh, &tp);
	if (f_parser_axiome(&tp) != -1)
	{
		DEBUG;
		F_TEST_TREE(tp.tree_parent);
		f_tree_exec_init(tp.tree_parent, sh);
	}
	else
	{
		MSG_ERRNBLINE(sh->nb_line, tp.pline, "error: syntax");
		DEBUGSTR("error f_parser_axiome");
		DEBUGPTR(tp.tree_parent);
		F_TEST_TREE(tp.tree_parent);
	}
	DEBUG;
	f_tree_free(tp.tree_parent);
	sh->sf->t = NULL;
	DEBUG;
}
