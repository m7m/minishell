/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_axiome.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 11:21:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 11:21:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_parser.h"

/*
** else == seulemenet a l'initiation de la recursion
*/

void
	f_parser_axiome__alloc_tree_cur(t_parser *tp)
{
	t_tree	*t;

	t = (t_tree *)ft_memalloc(sizeof(t_tree));
	ft_memchk_exit(t);
	if (tp->tree_cur)
	{
		DEBUGPTR(tp->tree_cur->right);
		DEBUGSTR(tp->tree_cur->line);
		DEBUGNBRUL(tp->tree_cur->v);
		tp->tree_cur->right = t;
		tp->tree_cur->right->parent = tp->tree_cur;
	}
	else
	{
		DEBUG;
		if (!tp->tree_parent)
			tp->tree_parent = t;
		tp->sh->sf->t = t;
	}
	tp->tree_cur = t;
}

int
	f_parser_axiome__w(t_parser *tp)
{
	int		flags;

	while (*tp->pline)
	{
		if (*tp->pline == '\\')
		{
			if (!tp->pline[1])
				ft_exit(EXIT_FAILURE,
				"f_parser_axiome: c'est quoi cette implementation de merde\n");
			tp->pline += 2;
		}
		else if ((flags = f_parser_iftoken(tp)))
			return (flags);
		else if (ft_strchr("`\"'({#", *tp->pline))
		{
			if (f_parser__chrclose(tp))
				return (-1);
		}
		else
			++tp->pline;
	}
	return (0);
}

/*
** C'est moche
*/

int
	f_parser_axiome(t_parser *tp)
{
	int		flags;

	DEBUG;
	if (f_parser_axiome_check(tp))
		return (f_parser_error(tp, "syntax error: axiome."));
	f_parser_axiome__alloc_tree_cur(tp);
	if ((flags = f_parser_axiome__w(tp)))
		return (flags);
	if (*tp->pline)
		return (-1);
	tp->tree_cur->line = f_parser_iftoken_residu(tp);
	DEBUG;
	return (1);
}
