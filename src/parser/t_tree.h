/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_tree.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 17:45:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 17:45:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_TREE_H
# define T_TREE_H

# include "../../inc/t_argv.h"
# include "../../inc/t_tfd.h"

typedef struct	s_tree
{
	unsigned long			v;
	char					*line;

	struct s_fd				tfd;

	struct s_tree			*parent;
	struct s_tree			*left;
	struct s_tree			*right;
}				t_tree;

#endif
