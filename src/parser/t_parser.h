/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_parser.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 12:08:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 12:08:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_PARSER_H
# define T_PARSER_H

# include "../../inc/t_sh.h"
# include "../../inc/t_argv.h"
# include "t_tree.h"

typedef struct	s_parser
{
	char				*line;
	char				*pline;

	t_tree				*tree_cur;
	t_tree				*tree_parent;

	t_sh				*sh;
	t_tree				*block_shellscript;
}				t_parser;

struct			s_token
{
	char			*c;
	int				len_c;
	int				lvl;
	unsigned long	flags;
	int				(*f)(struct s_token *, struct s_parser *);
};

#endif
