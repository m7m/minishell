/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_parser_axiome_check.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 12:01:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 12:01:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "f_parser.h"

/*
** Verifie si 'ls && || ee' ou 'ls ||' ou '|| ls'
*/

int
	f_parser_axiome_check(t_parser *tp)
{
	char	*s;

	DEBUGSTR(tp->pline);
	s = ft_pass_spacenewline(tp->pline);
	DEBUGSTR(s);
	DEBUGPTR(tp->tree_parent);
	if (tp->tree_cur
		&& (tp->tree_cur->v == FL_PAREN_CLOSE || tp->tree_cur->v == FL_GRAVE))
		return (0);
	DEBUG;
	if (*s
		&& (!tp->tree_cur
			|| (!IS_TOKEN_SHELLSCRIPT(tp->tree_cur->v)
				&& !IS_TOKEN_LVL4(tp->tree_cur->v)))
		&& ft_strchr(LEX, *s))
		return (-1);
	DEBUGPTR(tp->tree_cur);
	if (!*s && tp->tree_cur
		&& !IS_TOKENPRIM(tp->tree_cur->v)
		&& !IS_TOKEN_REDIR(tp->tree_cur->v)
		&& !IS_TOKEN_SHELLSCRIPT(tp->tree_cur->v))
		return (-1);
	DEBUG;
	return (0);
}
