/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 18:51:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/19 18:51:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_tree.h"
#include <libft.h>

/*
** lr : left/right, 0,1
*/

t_tree
	*f_tree_newadd(int lr, t_tree *tparent)
{
	t_tree	*new;

	new = ft_memalloc(sizeof(t_tree));
	if (new)
		new->parent = tparent;
	if (tparent)
	{
		if (lr)
			tparent->right = new;
		else
			tparent->left = new;
	}
	return (new);
}

/*
** branch devient enfant gauche de leaftoparent
*/

void
	f_tree_mvparent(
		struct s_tree **branch,
		struct s_tree *leaftoparent)
{
	DEBUG;
	leaftoparent->parent->right = leaftoparent->left;
	leaftoparent->left = (*branch);
	(*branch)->parent = leaftoparent;
	leaftoparent->parent = NULL;
	(*branch) = leaftoparent;
}
