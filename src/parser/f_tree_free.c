/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_tree_free.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 20:15:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/17 20:15:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_tree.h"

void
	f_tree_freeone(t_tree *t)
{
	DEBUGNBRUL(t->v);
	DEBUGSTR(t->line);
	DEBUGSTR(t->tfd.str);
	if (t->tfd.str)
		free(t->tfd.str);
	DEBUGSTR(t->line);
	if (t->line)
		free(t->line);
	free(t);
}

void
	f_tree_free(t_tree *t)
{
	if (t)
	{
		if (t->left)
			f_tree_free(t->left);
		if (t->right)
			f_tree_free(t->right);
		f_tree_freeone(t);
	}
}

void
	f_tree_freeparent(t_tree *t)
{
	while (t->parent)
	{
		DEBUGPTR(t);
		t = t->parent;
	}
	f_tree_free(t);
}
