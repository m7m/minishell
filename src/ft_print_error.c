/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 08:53:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/01 08:53:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_perr(const char *str, size_t str_len)
{
	write(STDERR_FILENO, str, str_len);
}

int
	ft_print_error(const char *cmd, int cmd_len,
				const char *msg_err, int msg_err_len)
{
	DEBUGSTR(cmd);
	DEBUGSTR(msg_err);
	ft_perr(NAME_PROG_ERR, LEN_NAME_PROG + 2);
	if (cmd_len < 0)
		ft_putstr_fd(cmd, STDERR_FILENO);
	else
		write(STDERR_FILENO, cmd, cmd_len);
	if (msg_err_len < 0)
		ft_putstr_fd(msg_err, STDERR_FILENO);
	else
		write(STDERR_FILENO, msg_err, msg_err_len);
	return (1);
}

void
	ft_print_error_n(const char *cmd, const char *msg_err)
{
	ft_print_error(cmd, ft_strlen(cmd), msg_err, ft_strlen(msg_err));
}

int
	ft_perror_getcwd(char **pbuff)
{
	char	*buff;

	DEBUG;
	ft_memchk_exit(buff = malloc(SC(BUFF_SIZE)));
	if (!getcwd(buff, BUFF_SIZE - 1))
	{
		MSG_ERR_GETCWD;
		free(buff);
		return (-1);
	}
	DEBUG;
	*pbuff = buff;
	DEBUG;
	return (0);
}

void
	ft_print_error_not_close(int flag)
{
	char	*err;

	DEBUG;
	if (flag == FL_ACCOL)
		err = "{}";
	else if (flag == FL_CROCH)
		err = "[]";
	else if (flag == FL_PAREN)
		err = "()";
	else if (flag == FL_DQUOT)
		err = "\"";
	else if (flag == FL_SQUOT)
		err = "'";
	else if (flag == FL_GRAVE)
		err = "`";
	else
		err = "undefined";
	MSG_ERR3(err, "bad opening / closing");
}
