/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history__last_arg.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 18:16:49 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 18:16:49 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/t_history.h"
#include "../inc/pr_history.h"
#include "../inc/l_sh.h"
#include <libft.h>

static void
	ft_history_parser__last_arg__datalen(
		char **line, char **pline, char *data, size_t data_len)
{
	char	*tmp;

	DEBUGSTR(*line);
	tmp = *line;
	--data_len;
	while (data_len && !ft_strchr(" \n\t", data[data_len - 1]))
	{
		while (data_len && !ft_strchr(" \n\t", data[data_len]))
			--data_len;
		if (data_len && data[data_len - 1] == '\\')
			--data_len;
		else if (ft_strchr(" \n\t", data[data_len]))
			++data_len;
	}
	DEBUGSTR(data);
	DEBUGSTR(*pline - 1);
	*line = ft_strjoin_inser(*line, data + data_len, (*pline - 1) - *line);
	free(tmp);
	ft_memchk_exit(*line);
	DEBUGSTR(*line);
	*pline = *line + (*pline - tmp) + (ft_strlen(data + data_len) - 1);
	DEBUGSTR(*pline);
}

void
	ft_history_parser__last_arg(
		char **line, char **pline)
{
	size_t	data_len;
	char	*data;

	DEBUGSTR(*line);
	data_len = ft_gethistory_id_nodup(g_thistory.thhistory->nb_elem, &data);
	ft_strcpy(*pline - 1, *pline + 1);
	if (data_len)
		ft_history_parser__last_arg__datalen(
			line, pline, data, data_len);
	else
		*pline = *pline - 1;
}
