/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork__child_exit.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/19 22:13:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/19 22:13:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"
#include "f_free_notglobal.h"

static void
	ft_fork__child_exit_comm(t_argv *targv)
{
	DEBUGPTR(targv);
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	ft_fork__child_close(targv);
	ft_targv_free(targv);
}

void
	ft_fork__child_exit(int status, char *msg, t_argv *targv)
{
	DEBUG;
	ft_fork__child_exit_comm(targv);
	if (msg)
		MSG_ERR2(msg);
	COM("'");
	/* f_free_notglobal_cal(); */
	ft_exit(status, NULL);
}

void
	ft_fork__child_exit_fd(int status, int fderr, char *msg, t_argv *targv)
{
	DEBUG;
	ft_fork__child_exit_comm(targv);
	MSG_ERR3INT(fderr, msg);
	ft_exit(status, NULL);
}
