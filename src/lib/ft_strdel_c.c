/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel_c.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/22 07:24:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/22 07:24:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void
	ft_strdel_c(char *str, char c)
{
	size_t	len;

	len = ft_strlen(str);
	while (*str)
	{
		if (*str == c)
			ft_memcpy(str, str + 1, len);
		++str;
		--len;
	}
}
