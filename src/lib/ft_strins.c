/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strins.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/28 01:37:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/28 01:37:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Insere ins dans dst a la position pos de dst
*/

void
	ft_strins(char *dst, char *ins, size_t pos)
{
	size_t	lenins;
	size_t	lendst;

	DEBUGSTR(dst + pos);
	DEBUGSTR(ins);
	DEBUGNBR(pos);
	lendst = ft_strlen(dst + pos) + 1;
	lenins = ft_strlen(ins);
	ft_memcpy(dst + pos + lenins, dst + pos, lendst);
	ft_memcpy(dst + pos, ins, lenins);
	DEBUGSTR(dst + (pos - 1));
}
