/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcount.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 12:46:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/15 12:46:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Compte le nombre d'ocurance de motif dans s
*/

int
	ft_strcount(const char *s, const char motif)
{
	int	n;

	n = 0;
	if (motif)
		while (*s)
		{
			if (*s == motif)
				++n;
			++s;
		}
	return (n);
}

int
	ft_strncount(const char *s, const char motif, int max)
{
	int	n;
	int	i;

	i = 0;
	n = 0;
	if (motif)
		while (s[i] && i < max)
		{
			if (s[i] == motif)
				++n;
			++i;
		}
	return (n);
}
