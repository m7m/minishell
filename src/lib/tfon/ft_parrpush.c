/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrpush.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 12:05:16 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:55 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"
#include "libft_internal.h"

#include <stdlib.h>

void	ft_parrpush(void ***tab, void *elem)
{
	void			**n_tab;
	unsigned int	arr_len;

	arr_len = ft_parrlen(*tab);
	n_tab = (arr_len % ALLOC_PARR) ? *tab :
	(void**)ft_m(sizeof(void*) * (arr_len + ALLOC_PARR + 1));
	n_tab[arr_len] = elem;
	n_tab[arr_len + 1] = (void*)0;
	if (n_tab == *tab)
		return ;
	while (arr_len--)
		n_tab[arr_len] = (*tab)[arr_len];
	free(*tab);
	*tab = n_tab;
}
