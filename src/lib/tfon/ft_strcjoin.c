/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 12:59:51 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:31:01 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <stdlib.h>

char	*ft_strcjoin(const char *s1, const char *s2, char c)
{
	char	*str;
	char	*beg;

	str = (char*)ft_m(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 2));
	beg = str;
	while (*s1)
		*str++ = *s1++;
	*str++ = c;
	while (*s2)
		*str++ = *s2++;
	*str ^= *str;
	return (beg);
}
