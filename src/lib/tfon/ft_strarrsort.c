/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarrsort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 16:52:16 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:59 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

static void		strarrsort_insert(char **j, char **i)
{
	char	*s;

	s = *i;
	while (i != j)
	{
		*i = i[-1];
		--i;
	}
	*i = s;
}

void			ft_strarrsort(char **arr)
{
	char	**i;
	char	**j;
	char	*s1;
	char	*s2;

	i = arr;
	while (*i)
	{
		j = arr;
		while (j != i)
		{
			s1 = *j;
			s2 = *i;
			while (*s1 == *s2 && *s1)
			{
				++s1;
				++s2;
			}
			if (*s2 < *s1)
				strarrsort_insert(j, i);
			++j;
		}
		++i;
	}
}
