/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gdc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 15:49:25 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:23 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <dirent.h>

char	**ft_gdc(char *path)
{
	char			**rc;
	DIR				*dir;
	struct dirent	*dirent;

	if ((dir = opendir(path)))
	{
		rc = (char**)ft_parrnew();
		readdir(dir);
		readdir(dir);
		while ((dirent = readdir(dir)))
			ft_parrpush((void***)&rc, ft_strdup(dirent->d_name));
		closedir(dir);
		return (rc);
	}
	return ((char**)0);
}
