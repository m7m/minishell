/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:37:29 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:51 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

void	**ft_parrnew(void)
{
	void	**arr;

	arr = (void**)ft_m(sizeof(void*));
	*arr = (void*)0;
	return (arr);
}
