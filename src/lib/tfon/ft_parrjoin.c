/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 17:56:34 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:45 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

void	**ft_parrjoin(void **a1, void **a2)
{
	void	**na;

	na = ft_parrnew();
	while (*a1)
		ft_parrpush(&na, *a1++);
	while (*a2)
		ft_parrpush(&na, *a2++);
	return (na);
}
