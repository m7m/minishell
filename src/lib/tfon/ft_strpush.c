/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strpush.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 12:44:08 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:31:04 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"
#include "libft_internal.h"

#include <stdlib.h>

void	ft_strpush(char **str, char c)
{
	char			*n_str;
	unsigned int	len;

	n_str =
		((len = ft_strlen(*str)) % ALLOC_STR_CHAR) ?
		*str : (char*)ft_m(sizeof(char) * (len + ALLOC_STR_CHAR + 1));
	n_str[len] = c;
	n_str[len + 1] = '\0';
	if (n_str == *str)
		return ;
	while (len--)
		n_str[len] = (*str)[len];
	free(*str);
	*str = n_str;
}
