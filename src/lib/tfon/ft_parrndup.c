/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrndup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tberthie <tberthie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:14:59 by tberthie          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:49 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

void	**ft_parrndup(void **array, unsigned int n)
{
	void			**new_array;
	unsigned int	i;

	new_array = (void**)ft_m(sizeof(void*) * (n + 1));
	i = 0;
	while (array[i] && n--)
	{
		new_array[i] = array[i];
		++i;
	}
	while (n--)
		new_array[i] = (void*)0;
	new_array[i] = (void*)0;
	return (new_array);
}
