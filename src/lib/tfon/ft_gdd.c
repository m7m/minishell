/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gdd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 14:22:27 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:26 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

static unsigned char	check_link_is_dir(char *path)
{
	struct stat		s;

	return (!stat(path, &s) && S_ISDIR(s.st_mode)) ? 1 : 0;
}

char					**ft_gdd(char *path)
{
	char			**dd;
	DIR				*dir;
	struct dirent	*dirent;

	if ((dir = opendir(path)))
	{
		dd = (char**)ft_parrnew();
		readdir(dir);
		readdir(dir);
		while ((dirent = readdir(dir)))
			if (dirent->d_type == DT_DIR ||
			(dirent->d_type == DT_LNK && check_link_is_dir(path)))
				ft_parrpush((void***)&dd, ft_strdup(dirent->d_name));
		closedir(dir);
		return (dd);
	}
	return ((char**)0);
}
