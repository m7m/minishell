/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 12:09:04 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:36 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"
#include "libft_internal.h"

void	**ft_parrdup(void **array)
{
	void	**n_arr;

	n_arr = ft_parrnew();
	while (*array)
		ft_parrpush(&n_arr, *array++);
	return (n_arr);
}
