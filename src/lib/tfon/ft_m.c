/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_m.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/04 16:43:36 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:30:31 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <stdlib.h>

void	*ft_m(unsigned int byte_nb)
{
	void	*ptr;

	if ((ptr = malloc((size_t)byte_nb)))
		return (ptr);
	exit(-1);
}
