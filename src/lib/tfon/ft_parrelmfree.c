/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrelmfree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/24 16:11:04 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:31:15 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <stdlib.h>

void	ft_parrelmfree(void **array)
{
	while (*array)
		free(**(void***)array++);
}
