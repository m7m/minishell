/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freeswap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/04 15:04:22 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:29:31 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <stdlib.h>

void	*ft_freeswap(void **ptr, void *swap)
{
	free(*ptr);
	*ptr = swap;
	return (swap);
}
