/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrdc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 19:53:36 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:31:07 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

void	ft_strrdc(char *str)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (str[i])
	{
		j = 0;
		while (j != i && str[j] != str[i])
			++j;
		if (j != i)
			ft_strrem(str + i);
		else
			++i;
	}
}
