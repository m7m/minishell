/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parrfree.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfontani <tfontani@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:16:57 by tfontani          #+#    #+#             */
/*   Updated: 2017/05/04 11:31:54 by tfontani         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftfon.h"

#include <stdlib.h>

void	ft_parrfree(void **array)
{
	void	**beg;

	beg = array;
	while (*array)
		free(*array++);
	free(beg);
}
