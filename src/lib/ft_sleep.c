/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sleep.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/07 08:09:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/07 08:09:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void
	ft_sleep(int time)
{
	void	*tmp;

	while (time > -1)
	{
		time--;
		if (!(tmp = malloc(sizeof(char) * 100)))
			return ;
		free(tmp);
	}
}
