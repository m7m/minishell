/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strword.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:55:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:55:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Cherche la fin du prochain mot
** retourne le caractere suivant le mot
** ou la fin de chaine
*/

char
	*ft_strword(char *s)
{
	int	pass;
	int	pass_anc;

	pass = 0;
	pass_anc = 0;
	while (*s)
	{
		if (!(pass = ft_isalnum(*s)) && pass_anc)
			return (s);
		else if (pass)
			pass_anc = pass;
		++s;
	}
	return (s);
}
