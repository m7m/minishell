/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_refont.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:06:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:06:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Prend en parametre le tableau contenant une liste de mot
** + la longuer de se tableau
** + le nouveau mot a ajouter
** retour: un tableau contenant la liste des mots + le nouveau
*/

static char
	**ft_strsplit_add__in(char **tabstr, size_t len, char *addmot)
{
	char	**tmptabstr;
	size_t	i;

	i = 0;
	DEBUGNBR(len);
	if (!(tmptabstr = (char **)ft_memalloc(sizeof(char *) * (len + 2))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	tmptabstr[len] = addmot;
	tmptabstr[len + 1] = NULL;
	while (i < len)
	{
		DEBUGSTR(tabstr[i]);
		tmptabstr[i] = tabstr[i];
		++i;
	}
	DEBUGSTR(tmptabstr[i]);
	free(tabstr);
	return (tmptabstr);
}

/*
** Substitut dans chaine 's' de longeur 'len_sub' - cette longueur sur s
** et ajoute le substitut au tableau de taille 'nbmots'
** le tableau doit etre pres alloue
** une reallocation est faite sur le tableau
*/

char
	**ft_strsub_add(char **tabstr, int len_tab, char const *s, int len_sub)
{
	char	*ssub;

	DEBUGSTR(s);
	DEBUGNBR(len_sub);
	if (len_sub > INT_MAX - 100)
	{
		ft_putstr_fd("Error: max arg\n", STDERR_FILENO);
		return (tabstr);
	}
	if (!(ssub = ft_strsub((char *)s - len_sub, 0, len_sub)))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	tabstr = ft_strsplit_add__in(tabstr, len_tab, ssub);
	return (tabstr);
}
