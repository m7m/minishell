/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrword.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:30:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:30:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Cherche le debut du précédent mot
** retourne le caractere precedent le mot
** un le debut de la chaine
*/

char
	*ft_strrword(char *s)
{
	int	pass;
	int	pass_anc;
	int	len;

	pass = 0;
	pass_anc = 0;
	len = ft_strlen(s);
	while (len)
	{
		--len;
		if (!(pass = ft_isalnum(s[len])) && pass_anc)
			return (s + len + 1);
		else if (pass)
			pass_anc = pass;
	}
	DEBUG;
	return (s);
}
