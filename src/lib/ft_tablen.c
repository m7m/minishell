/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tablen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 01:51:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 01:51:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

size_t	ft_tablen(char **tab)
{
	char	**ptab;

	if (!tab)
		return (0);
	ptab = tab;
	while (*ptab)
		++ptab;
	return (ptab - tab);
}
