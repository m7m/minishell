/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hadd_data.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 00:52:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/29 00:52:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** data_len = size data + \0
** Duplique data
*/

void
	ft_hadd_data(t_htable *thtable, t_hash *thash,
					void *data, size_t data_len)
{
	DEBUGSTR(data);
	DEBUGNBR(data_len);
	if (data_len)
		ft_memchk_exit(thash->data = ft_memdup(data, data_len));
	else
		thash->data = NULL;
	thash->data_len = data_len;
	if (thtable->data_lenmax < thash->data_len)
	{
		thtable->data_lenmax = thash->data_len;
		ft_htab_lie_upd(thtable, thash->data_len);
	}
	DEBUGPTR(thash->data);
	DEBUGNBR(thash->data_len);
}
