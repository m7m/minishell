/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hchr_noeud.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:17:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:17:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Recherche key dans un noeud
*/

t_hash
	*ft_hchr_noeudkey(t_hash *noeud, void *key, size_t key_len, int ntable)
{
	t_hash	*collision;

	collision = noeud;
	while (ft_hchk_keqk(collision, key, key_len, ntable))
		collision = collision->next;
	return (collision);
}

/*
** Retourne le noeud associe a la key
*/

t_hash
	**ft_hchr_noeud(t_htable thtable, void *key, size_t key_len)
{
	size_t	h;

	h = ft_hash_calc(thtable.hdb->size, (char *)key, key_len);
	return (thtable.hdb->thash + h);
}
