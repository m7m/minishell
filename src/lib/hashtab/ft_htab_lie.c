/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htab_lie.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 21:52:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/09 21:52:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

static void
	ft_htab_lie__size(t_htable *thtable_lie, size_t key_len, size_t data_len)
{
	if (thtable_lie->key_lenmax < key_len)
		thtable_lie->key_lenmax = key_len;
	if (thtable_lie->data_lenmax < data_len)
		thtable_lie->data_lenmax = data_len;
}

void
	ft_htab_lie_upd(t_htable *thtable, size_t data_len)
{
	int			num_table;
	t_htable	*thtable_lie;

	DEBUG;
	num_table = thtable->num_table;
	thtable_lie = thtable->thtable_lie;
	while (thtable_lie && thtable != thtable_lie)
	{
		if (BANDEQ(thtable_lie->num_table, num_table))
			if (thtable_lie->data_lenmax < data_len)
			{
				DEBUG;
				thtable_lie->data_lenmax = data_len;
			}
		thtable_lie = thtable_lie->thtable_lie;
	}
}

/*
** adddel = 1: add, 0: del
*/

void
	ft_htab_lie(t_htable *thtable, t_hash *thash, int adddel)
{
	t_htable	*thtable_lie;

	DEBUG;
	thtable_lie = thtable->thtable_lie;
	while (thtable_lie && thtable != thtable_lie)
	{
		if (BANDEQ(thtable_lie->num_table, thash->num_table))
		{
			if (adddel)
			{
				DEBUGNBR(thtable_lie->num_table);
				DEBUGSTR(thash->key);
				DEBUGSTR(thash->data);
				ft_htab_lie__size(thtable_lie, thash->key_len, thash->data_len);
				++thtable_lie->nb_elem;
			}
			else if (thtable_lie->nb_elem)
				--thtable_lie->nb_elem;
		}
		thtable_lie = thtable_lie->thtable_lie;
	}
}
