/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabtohash.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 14:00:32 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 14:00:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

void
	ft_tabtohash_size(char *tabstr[], int itabstr, t_htable *thtable)
{
	char	*str;
	char	*ptabstr;

	DEBUGNBR(itabstr);
	while (itabstr)
	{
		--itabstr;
		ptabstr = tabstr[itabstr];
		DEBUGSTR(ptabstr);
		str = ft_strchrnull(ptabstr, '=');
		ft_hupd_n(thtable, ptabstr,
				str - ptabstr,
				ptabstr + (str - ptabstr) + 1);
	}
	DEBUG;
}

void
	ft_tabtohash(char *tabstr[], t_htable *thtable)
{
	int		len;
	char	**ptabstr;

	DEBUG;
	len = 0;
	ptabstr = tabstr;
	while (*ptabstr)
	{
		DEBUGSTR(*ptabstr);
		++ptabstr;
		++len;
	}
	DEBUGNBR(len);
	if (len)
		ft_tabtohash_size(tabstr, len, thtable);
}
