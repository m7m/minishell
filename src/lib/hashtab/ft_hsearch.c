/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hsearch.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:49:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:49:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"
#include "l_sh.h"
#include "t_hash.h"

/*
** Fonction de recherche dans une htable
** si motif == \0 alors tout
*/

static t_ht
	**ft_hsearch__alloc(t_htable thtable)
{
	t_ht	**tabh;

	tabh = (t_ht **)ft_malloc_array(sizeof(t_ht *), thtable.nb_elem + 1,
									sizeof(t_ht), thtable.nb_elem + 1);
	ft_memchk_exit(tabh);
	return (tabh);
}

static t_ht
	**ft_hsearch__cmpk(void *key, size_t key_len, t_hash *next, t_ht **ptabh)
{
	if (!key_len || !ft_memcmp(next->key, key, key_len))
	{
		DEBUG;
		(*ptabh)->key = next->key;
		(*ptabh)->key_len = next->key_len;
		(*ptabh)->data = next->data;
		DEBUGSTR((*ptabh)->data);
		(*ptabh)->data_len = next->data_len;
		++ptabh;
	}
	return (ptabh);
}

static t_ht
	**ft_hsearch__cmpd(void *data, size_t data_len, t_hash *next, t_ht **ptabh)
{
	if (!data_len || !ft_memcmp(next->data, data, data_len))
	{
		DEBUG;
		(*ptabh)->key = next->data;
		(*ptabh)->key_len = next->data_len;
		(*ptabh)->data = next->key;
		DEBUGSTR((*ptabh)->data);
		(*ptabh)->data_len = next->key_len;
		++ptabh;
	}
	return (ptabh);
}

size_t
	ft_hsearch_k(t_htable thtable, void *key, size_t key_len, t_ht ***tabh)
{
	t_ht	**ptabh;
	t_hash	**thash;
	t_hash	*next;

	DEBUG;
	*tabh = ft_hsearch__alloc(thtable);
	ptabh = *tabh;
	thash = thtable.hdb->thash;
	while (thtable.nb_elem)
	{
		if ((next = *thash))
			while (next)
			{
				if (next->num_table & thtable.num_table)
				{
					if (next->key_len >= key_len)
						ptabh = ft_hsearch__cmpk(key, key_len, next, ptabh);
					--thtable.nb_elem;
				}
				next = next->next;
			}
		++thash;
	}
	*ptabh = 0;
	return (ptabh - *tabh);
}

size_t
	ft_hsearch_d(t_htable thtable, void *data, size_t data_len, t_ht ***tabh)
{
	t_ht	**ptabh;
	t_hash	**thash;
	t_hash	*next;

	DEBUG;
	*tabh = ft_hsearch__alloc(thtable);
	ptabh = *tabh;
	thash = thtable.hdb->thash;
	while (thtable.nb_elem)
	{
		if ((next = *thash))
			while (next)
			{
				if (next->num_table & thtable.num_table)
				{
					if (next->data_len >= data_len)
						ptabh = ft_hsearch__cmpd(data, data_len, next, ptabh);
					--thtable.nb_elem;
				}
				next = next->next;
			}
		++thash;
	}
	*ptabh = 0;
	return (ptabh - *tabh);
}
