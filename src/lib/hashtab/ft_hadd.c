/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hadd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:31:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:31:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

static void
	ft_hadd__rpl(t_hash *thash, void *key, size_t key_len)
{
	ft_memchk_exit(thash->key = ft_memdup(key, key_len + SC(1)));
	*((char *)(thash->key + key_len)) = 0;
	thash->key_len = key_len;
	thash->data = NULL;
	thash->data_len = 0;
}

static void
	ft_hadd__inc(t_hdb *hdb, t_htable *thtable, t_hash *thash)
{
	if (thtable->key_lenmax < thash->key_len)
		thtable->key_lenmax = thash->key_len;
	++hdb->nb_elem;
	++thtable->nb_elem;
	ft_htab_lie(thtable, thash, 1);
}

/*
** Duplique key
** Retourne thash lie a la key
** Retourne NULL si key existe
*/

t_hash
	*ft_hadd(t_htable *thtable, void *key, size_t key_len)
{
	size_t	h;
	t_hash	*collision;
	t_hdb	*hdb;

	DEBUGSTR(key);
	DEBUGNBR(key_len);
	DEBUGNBR(thtable->num_table);
	hdb = thtable->hdb;
	h = ft_hash_calc(hdb->size, key, key_len);
	if ((collision = hdb->thash[h]))
	{
		if (ft_hchr_noeudkey(collision, key, key_len, thtable->num_table))
		{
			DEBUGSTR("Doublon");
			return (NULL);
		}
		DEBUGSTR("Collision");
		++hdb->nb_collission;
	}
	ft_memchk_exit((hdb->thash[h] = (t_hash *)malloc(sizeof(t_hash))));
	hdb->thash[h]->next = collision;
	hdb->thash[h]->num_table = thtable->num_table;
	ft_hadd__rpl(hdb->thash[h], key, key_len);
	ft_hadd__inc(hdb, thtable, hdb->thash[h]);
	return (hdb->thash[h]);
}

/*
** Aucune duplication.
** Si existe ne fait rien et return NULL
** Sinon ajout 'nodup' a la table de hashage
*/

t_hash
	*ft_hadd_nodup(t_htable *thtable, t_hash *nodup)
{
	size_t	h;
	t_hash	*collision;
	t_hdb	*hdb;

	DEBUGSTR(nodup->key);
	DEBUGNBR(nodup->key_len);
	hdb = thtable->hdb;
	h = ft_hash_calc(hdb->size, nodup->key, nodup->key_len);
	if ((collision = hdb->thash[h]))
	{
		if (ft_hchr_noeudkey(collision, nodup->key, nodup->key_len,
							thtable->num_table))
		{
			DEBUGSTR("Doublon");
			return (NULL);
		}
		DEBUGSTR("Collision");
		++hdb->nb_collission;
	}
	hdb->thash[h] = nodup;
	nodup->next = collision;
	ft_hadd__inc(hdb, thtable, nodup);
	if (thtable->data_lenmax < nodup->data_len)
		thtable->data_lenmax = nodup->data_len;
	return (hdb->thash[h]);
}

/*
** Ajout le nouveau th si inexiste sinon retourne data
** la taille de data est calculer jusqu'au \0
*/

void
	*ft_hadd_n(t_htable *thtable, void *key, size_t key_len, char *data)
{
	t_hash	*thash;

	if ((thash = ft_hadd(thtable, key, key_len)))
		ft_hadd_data(thtable, thash, (void *)data, ft_strlen(data) + 1);
	else
		return (data);
	return (NULL);
}
