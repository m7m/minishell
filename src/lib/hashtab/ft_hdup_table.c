/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hdup_table.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 22:34:21 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 22:34:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

static void
	ft_hdup_table__thcp__dup(t_htable *thtable, t_hash *thashold)
{
	t_hash	*thashnew;

	thashnew = ft_hdup_th(thashold);
	if (!thashnew)
		ft_exit(EXIT_FAILURE, "Error: duplication thash\n");
	thashnew->num_table = thtable->num_table;
	DEBUGNBR(thashnew->num_table);
	if (!ft_hadd_nodup(thtable, thashnew))
		ft_hdel_th(thashnew, 1);
}

static void
	ft_hdup_table__thcp(t_hdb *hdb, int num_tableold, t_htable *thtablenew)
{
	t_hash	**thash;
	t_hash	**thashend;
	t_hash	*tmp;

	DEBUG;
	thash = hdb->thash;
	thashend = thash + hdb->size + 1;
	while (thash < thashend)
	{
		if (*thash)
		{
			tmp = *thash;
			while (tmp)
			{
				if (tmp->num_table & num_tableold)
					ft_hdup_table__thcp__dup(thtablenew, tmp);
				tmp = tmp->next;
			}
		}
		++thash;
	}
}

void
	ft_hdup_table(t_htable *thtablenew, t_htable *thtableold)
{
	DEBUGNBR(thtablenew->num_table);
	DEBUGNBR(thtableold->num_table);
	thtablenew->key_lenmax = thtableold->key_lenmax;
	thtablenew->data_lenmax = thtableold->data_lenmax;
	ft_hdup_table__thcp(thtableold->hdb, thtableold->num_table, thtablenew);
	DEBUGNBR(thtablenew->nb_elem);
	DEBUGNBR(thtableold->nb_elem);
}
