/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hchr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:30:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:30:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Check si hk1->key correspond a key
** Retourne 1 si different
*/

int
	ft_hchk_keqk(t_hash *hk1, void *key, size_t key_len, int ntable)
{
	return (hk1
			&& (!((ntable & hk1->num_table) == ntable)
				|| hk1->key_len != key_len
				|| ft_memcmp(hk1->key, key, hk1->key_len)));
}

/*
** Check si hd1->data correspond a data
** Retourne 1 si different
*/

int
	ft_hchk_deqd(t_hash *hd1, void *data, size_t data_len, int ntable)
{
	if (!data_len && hd1 && !hd1->data_len)
		return (1);
	return (hd1
			&& (!((ntable & hd1->num_table) == ntable)
				|| hd1->data_len != data_len
				|| ft_memcmp(hd1->data, data, hd1->data_len)));
}

/*
** Retourne la taille de data
** Fait pointer data sur thash->data si data n'est pas NULL
*/

size_t
	ft_hchr(t_htable thtable, void *key, size_t key_len, void **data)
{
	t_hash	*collision;

	DEBUGSTR(key);
	DEBUGNBR(key_len);
	collision = *ft_hchr_noeud(thtable, key, key_len);
	collision = ft_hchr_noeudkey(collision, key, key_len, thtable.num_table);
	if (!collision)
	{
		DEBUGSTR("not found");
		return (0);
	}
	if (data)
		*data = collision->data;
	DEBUGSTR(collision->data);
	DEBUGNBR(collision->data_len);
	return (collision->data_len);
}

/*
** Retoune thash associe a la cle sinon renvoie NULL
** La valeur retouner ne doit pas etre 'free' !!!
*/

t_hash
	*ft_hchr_th(t_htable thtable, void *key, size_t key_len)
{
	t_hash	*collision;

	DEBUG;
	collision = *ft_hchr_noeud(thtable, key, key_len);
	collision = ft_hchr_noeudkey(collision, key, key_len, thtable.num_table);
	if (!collision)
	{
		DEBUGSTR("not found");
		return (NULL);
	}
	DEBUGSTR(collision->key);
	DEBUGNBR(collision->key_len);
	DEBUGPTR(collision->data);
	DEBUGNBR(collision->data_len);
	return (collision);
}
