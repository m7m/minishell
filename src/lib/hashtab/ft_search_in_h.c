/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search_in_h.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 11:14:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/22 11:14:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "libft.h"
#include "l_sh.h"

/*
** Retourne le nombre d'occurence trouvé
** motif peut représenter un fichier/dossier ou son chemin/ plus un motif
** qui est rechercher
** le motif est découpé en path + motif
** le path représente ou sera recherche le motif.
** ;
** f stoque dans un char[][]
** si resula trop grand pose la question
*/

/*
** Retire le path aux resultats de recherche
*/

static size_t
	ft_search__totab(size_t it, const char *motif, char ***resul, t_ht **t)
{
	size_t		isubstitut;
	char		*tmp;
	const char	*substitut;

	DEBUG;
	substitut = (const char *)ft_strrchr((char *)motif, '/');
	if (substitut)
		isubstitut = (substitut - motif) + 1;
	else
		isubstitut = 0;
	while (*t)
	{
		tmp = (char *)ft_memdup_resize((*t)->key + SC(isubstitut),
									(*t)->key_len - SC(isubstitut),
									SC(1));
		ft_memchk_exit(tmp);
		tmp[(*t)->key_len - isubstitut] = 0;
		if (!ft_tabsort_add_igndbl((*resul), tmp))
		{
			free(tmp);
			--it;
		}
		++t;
	}
	return (it);
}

static size_t
	ft_search__realloc(size_t it, const char *motif, char ***resul, t_ht **t)
{
	char	**tmp;

	if (!*resul)
	{
		DEBUGSTR("new resul");
		ft_memchk_exit(*resul = (char **)malloc(sizeof(char *) * (it + 2)));
		**resul = 0;
	}
	else
	{
		tmp = *resul;
		ft_memchk_exit(*resul = ft_tabresize(*resul, it + 2));
		free(tmp);
	}
	if (!(it = ft_search__totab(it, motif, resul, t)) && !**resul)
	{
		free(*resul);
		*resul = NULL;
	}
	return (it);
}

/*
** Appel la fonction de recherche pour retourner un tableau de structure 't_ht'
** transmet le tableau de structure à la fonction ci-dessus.
*/

int
	ft_search_in_h(const char *motif, char ***resul, t_htable *thtable,
				size_t (*fhsearch)(t_htable, void *, size_t, t_ht ***))
{
	t_ht	**t;
	t_ht	**pt;
	size_t	it;

	DEBUGSTR(motif);
	it = fhsearch(*thtable, (void *)motif, ft_strlen(motif), &t);
	pt = t;
	DEBUGNBR(it);
	if (it)
		it = ft_search__realloc(it, motif, resul, t);
	free(pt);
	return (it);
}
