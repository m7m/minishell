/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hdel_db.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/26 22:04:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/26 22:04:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "stddef.h"

void
	ft_hreset_db(t_hdb *hdb)
{
	DEBUG;
	hdb->nb_elem = 0;
	hdb->nb_collission = 0;
	hdb->nb_resize = 0;
	hdb->nums_table = 0;
}

/*
** Vide seulement le contenu du noeud
** Ne touche pas au table lie.
*/

static void
	ft_hdel_db_table_cont(t_hash **thash, size_t *nb_elem,
						int num_table, int dbdata_static)
{
	t_hash	*prev;
	t_hash	*tmp;
	t_hash	*next;

	prev = NULL;
	tmp = *thash;
	while (tmp)
	{
		next = tmp->next;
		if (tmp->num_table & num_table)
		{
			if (prev)
				prev->next = next;
			else
				*thash = next;
			ft_hdel_th(tmp, tmp->num_table & dbdata_static);
			--(*nb_elem);
		}
		else
			prev = tmp;
		tmp = next;
	}
}

void
	ft_hdel_db_table(t_hdb *hdb, int nums_table)
{
	t_hash	**thash;
	t_hash	**thashend;

	DEBUGNBR(hdb->nb_elem);
	DEBUGNBR(hdb->data_static & nums_table);
	thash = hdb->thash;
	thashend = hdb->size + thash + 1;
	while (thash < thashend)
	{
		if (*thash)
		{
			ft_hdel_db_table_cont(thash, &hdb->nb_elem,
								nums_table, hdb->data_static);
		}
		++thash;
	}
	hdb->nums_table &= ~nums_table;
}

void
	ft_hdel_db(t_hdb *hdb)
{
	DEBUG;
	if (hdb->thash)
	{
		ft_hdel_db_table(hdb, ~0);
		free(hdb->thash);
		hdb->thash = NULL;
	}
	hdb->size = 0;
	ft_hreset_db(hdb);
	free(hdb);
}
