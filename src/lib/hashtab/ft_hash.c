/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hash.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:06:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:06:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

size_t
	ft_hash(char *s, size_t len)
{
	size_t	h;
	size_t	i;
	size_t	j;

	if (len > 55)
		len = 55;
	i = (sizeof(size_t) - sizeof(char)) * 8 - 1;
	j = 0;
	h = 0;
	while (j < len)
	{
		h |= *s << i;
		--i;
		++j;
		++s;
		h *= len;
	}
	return (h);
}

size_t
	ft_hash_calc(size_t hdb_size, char *key, size_t key_len)
{
	size_t	h;

	if (!key_len)
		key_len = ft_strlen((char *)key);
	h = ft_hash((char *)key, key_len) % hdb_size;
	return (h);
}
