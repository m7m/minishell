/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hdel.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/26 20:33:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/26 20:33:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"
#include "stddef.h"

void
	ft_hdel_th(t_hash *thash, int data_static)
{
	if (data_static)
		free(thash->data);
	free(thash->key);
	free(thash);
}

void
	ft_hdel_table(t_htable *thtable)
{
	DEBUG;
	ft_hdel_db_table(thtable->hdb, thtable->num_table);
	thtable->nb_elem = 0;
}

/*
** Free le thash correspond a la cle
*/

int
	ft_hdelkey_table(t_htable *thtable, void *key, size_t key_len)
{
	t_hash	**hnoeud;
	t_hash	*parent;
	t_hash	*collision;

	if (!ft_hchr_th(*thtable, key, key_len))
		return (-1);
	hnoeud = ft_hchr_noeud(*thtable, key, key_len);
	parent = ft_hchr_keyparent(*hnoeud, key, key_len, thtable->num_table);
	if (!parent)
	{
		collision = (*hnoeud);
		(*hnoeud) = collision->next;
	}
	else
	{
		collision = parent->next;
		parent->next = collision->next;
	}
	ft_htab_lie(thtable, collision, 0);
	ft_hdel_th(collision, thtable->data_static);
	--thtable->nb_elem;
	--thtable->hdb->nb_elem;
	return (0);
}

static void
	ft_hdel_table_cont__w(t_hash **thash, size_t *nb_elem, t_htable *thtable)
{
	t_hash	*prev;
	t_hash	*tmp;
	t_hash	*next;

	prev = NULL;
	tmp = *thash;
	while (tmp)
	{
		next = tmp->next;
		if (thtable->num_table & tmp->num_table)
		{
			if (prev)
				prev->next = next;
			else
				*thash = next;
			ft_htab_lie(thtable, tmp, 0);
			ft_hdel_th(tmp, tmp->num_table & thtable->hdb->data_static);
			--(*nb_elem);
		}
		else
			prev = tmp;
		tmp = next;
	}
}

void
	ft_hdel_table_cont(t_htable *thtable)
{
	size_t	nb_elem;
	t_hash	**thash;

	DEBUG;
	nb_elem = thtable->nb_elem;
	thash = thtable->hdb->thash;
	while (nb_elem)
	{
		if (*thash)
			ft_hdel_table_cont__w(thash, &nb_elem, thtable);
		++thash;
	}
	DEBUGNBR(thtable->nb_elem);
	thtable->hdb->nb_elem -= thtable->nb_elem;
	thtable->nb_elem = 0;
}
