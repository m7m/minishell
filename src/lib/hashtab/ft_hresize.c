/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hresize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:41:08 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:41:08 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Redimenssionne un **thash en fonction du nombre d'element
** Retourne 1 si plus grand
*/

static void
	ft_hadd_th(t_hash **thash, size_t thash_size, t_hash *add)
{
	size_t	h;
	t_hash	*collision;

	h = ft_hash_calc(thash_size, add->key, add->key_len);
	collision = thash[h];
	thash[h] = add;
	add->next = collision;
}

static void
	ft_hresize__thmv(t_hash **old_th, size_t old_th_size,
					t_hash **new_th, size_t new_th_size)
{
	t_hash	**poldth;
	t_hash	**oldth_end;
	t_hash	*next;
	t_hash	*tmp;

	DEBUG;
	poldth = old_th;
	oldth_end = old_th + old_th_size;
	while (poldth < oldth_end)
	{
		if (poldth)
		{
			tmp = *poldth;
			while (tmp)
			{
				next = tmp->next;
				tmp->next = NULL;
				ft_hadd_th(new_th, new_th_size, tmp);
				tmp = next;
			}
		}
		++poldth;
	}
}

int
	ft_hresize_th(t_hdb *hdb)
{
	size_t		len;
	t_hash		**new_th;

	DEBUG;
	len = ft_hash_len(hdb->nb_elem);
	if (len < hdb->size)
		return (0);
	new_th = ft_hnew_thash(len);
	ft_hresize__thmv(hdb->thash, hdb->size, new_th, len);
	free(hdb->thash);
	hdb->thash = new_th;
	hdb->size = len;
	return (1);
}
