/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hupd_no.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 04:37:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 04:37:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

void
	*ft_hupd_nosize(t_htable *thtable, char *key, char *data)
{
	t_hupdate	hupd;

	DEBUG;
	hupd.key = (void *)key;
	hupd.key_len = ft_strlen(key);
	hupd.data = (void *)data;
	if (data)
		hupd.data_len = ft_strlen(data) + 1;
	else
		hupd.data_len = 0;
	ft_hupd(thtable, &hupd);
	DEBUG;
	return (hupd.data);
}

static void
	ft_hupd_nodup__cp(int num_table, t_hash *thash, t_hupdate *hupd)
{
	thash->key = hupd->key;
	thash->key_len = hupd->key_len;
	thash->data = hupd->data;
	thash->data_len = hupd->data_len;
	thash->num_table = num_table;
	thash->next = NULL;
	hupd->key = NULL;
	hupd->data = NULL;
}

static void
	ft_hupd_nodup__cp2(size_t data_len, t_htable *thtable,
					t_hash *thash, t_hupdate *hupd)
{
	thtable->data_lenmax -= data_len;
	thash->data = hupd->data;
	thash->data_len = hupd->data_len;
	if (thtable->data_lenmax < hupd->data_len)
		thtable->data_lenmax = hupd->data_len;
}

void
	*ft_hupd_nodup(t_htable *thtable, t_hupdate *hupd)
{
	size_t	data_len;
	t_hash	*thash;
	void	*data;

	DEBUGSTR(hupd->key);
	DEBUGSTR(hupd->data);
	data = NULL;
	data_len = 0;
	if (!(thash = ft_hchr_th(*thtable, hupd->key, hupd->key_len)))
	{
		ft_memchk_exit(thash = (t_hash *)ft_memalloc(sizeof(t_hash)));
		ft_hupd_nodup__cp(thtable->num_table, thash, hupd);
		ft_hadd_nodup(thtable, thash);
	}
	else if (ft_hchk_deqd(thash, hupd->data, hupd->data_len,
						thtable->num_table))
	{
		DEBUGSTR("update");
		data = thash->data;
		data_len = thash->data_len;
		ft_hupd_nodup__cp2(data_len, thtable, thash, hupd);
	}
	hupd->data = data;
	hupd->data_len = data_len;
	return (data);
}
