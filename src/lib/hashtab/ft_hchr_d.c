/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hchr_d.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/29 14:47:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/29 14:47:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Recherche la 'key' associe a 'data'
*/

t_hash
	*ft_hchr_d(t_htable thtable, void *data, size_t data_len)
{
	t_hash	*collision;
	t_hash	**thash;

	DEBUGNBR(thtable.nb_elem);
	thash = thtable.hdb->thash;
	while (thtable.nb_elem)
	{
		if (*thash)
		{
			collision = *thash;
			while (ft_hchk_deqd(collision, data, data_len, thtable.num_table))
			{
				if (thtable.num_table & collision->num_table)
					--thtable.nb_elem;
				collision = collision->next;
			}
			if (collision)
			{
				DEBUGSTR((char *)collision->data);
				return (collision);
			}
		}
		++thash;
	}
	return (NULL);
}
