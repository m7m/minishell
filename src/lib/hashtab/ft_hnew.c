/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hnew.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 01:01:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 01:01:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Definit une taille convenable pour le thash
*/

size_t
	ft_hash_len(size_t len)
{
	len /= 8;
	DEBUGNBR(len);
	if (len)
		len = 0xf * len;
	else
		len = 0xf;
	if (!(len % 2))
		++len;
	DEBUGNBR(len);
	return (len);
}

t_hash
	**ft_hnew_thash(size_t len)
{
	t_hash	**thash;

	if (!(thash = (t_hash **)ft_memalloc(sizeof(t_hash) * (len + 1))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	return (thash);
}

t_hdb
	*ft_hnew_db(size_t nb_elem_base)
{
	size_t		len;
	t_sttexit	*sttexit;
	t_hdb		*thdb;

	DEBUG;
	if (!(thdb = (t_hdb *)ft_memalloc(sizeof(t_hdb))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	len = ft_hash_len(nb_elem_base);
	thdb->thash = ft_hnew_thash(len);
	thdb->size = len;
	thdb->data_static &= ~0;
	ft_memchk_exit(sttexit = (t_sttexit *)malloc(sizeof(t_sttexit)));
	sttexit->data = thdb;
	sttexit->f = (void *)ft_hdel_db;
	ft_sttexit(sttexit);
	DEBUGNBR(thdb->size);
	return (thdb);
}

static int
	ft_hnew_table__numtabe(int nums_table)
{
	size_t	i;

	i = 0;
	while (i < sizeof(int) * 8 && nums_table & B(i))
		++i;
	if (i > sizeof(int) * 8)
		ft_exit(EXIT_FAILURE, "Error: maximum number of table reached");
	return (i);
}

/*
** Alloc une table dans db
** data_static: si 0 indique dans db que les donnees sont statique
*/

void
	ft_hnew_table(t_hdb *hdb, t_htable *thtable, int data_static)
{
	DEBUG;
	thtable->key_lenmax = 0;
	thtable->data_lenmax = 0;
	thtable->nb_elem = 0;
	thtable->hdb = hdb;
	thtable->num_table = B(ft_hnew_table__numtabe(hdb->nums_table));
	thtable->thtable_lie = 0;
	hdb->nums_table |= thtable->num_table;
	if (!data_static)
	{
		thtable->data_static = 0;
		hdb->data_static &= ~thtable->num_table;
	}
	else
	{
		thtable->data_static = 1;
		hdb->data_static |= thtable->num_table;
	}
	DEBUGNBR(thtable->num_table);
}
