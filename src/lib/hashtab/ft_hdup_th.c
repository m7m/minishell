/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hdup_th.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 22:34:16 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 22:34:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

t_hash
	*ft_hdup_th(t_hash *thash)
{
	t_hash	*new;

	DEBUG;
	if (!thash->key_len)
	{
		DEBUGSTR("key_len is null ");
		return (NULL);
	}
	ft_memchk_exit(new = (t_hash *)malloc(sizeof(t_hash)));
	new->key = ft_memdup(thash->key, thash->key_len);
	ft_memchk_exit(new->key);
	new->key_len = thash->key_len;
	if (thash->data_len)
	{
		new->data = ft_memdup(thash->data, thash->data_len);
		ft_memchk_exit(new->data);
	}
	else
		new->data = NULL;
	new->data_len = thash->data_len;
	new->num_table = thash->num_table;
	new->next = NULL;
	return (new);
}
