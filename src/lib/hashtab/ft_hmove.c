/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hmove.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 05:54:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/12 05:54:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Deplace 'thash' dans dst
*/

void
	ft_hmove_th(t_htable *dst, t_htable *src, t_hash *thash)
{
	DEBUG;
	--src->nb_elem;
	ft_htab_lie(src, thash, 0);
	thash->num_table = dst->num_table;
	++dst->nb_elem;
	ft_htab_lie(dst, thash, 1);
}

/*
** Si key non trouve dans src retourne -1
** Si existe dans dst et est different de celui de src
**  alors supprime celui de dst
**  puis change la table associe et retourne 0
*/

int
	ft_hmove_key(t_htable *dst, t_htable *src, void *key, size_t key_len)
{
	t_hash	*thash;
	t_hash	*dstthash;

	DEBUGSTR(key);
	if ((thash = ft_hchr_th(*src, key, key_len)))
	{
		if ((dstthash = ft_hchr_th(*dst, key, key_len)) != thash)
			ft_hdelkey_table(dst, key, key_len);
		ft_hmove_th(dst, src, thash);
		return (0);
	}
	return (-1);
}

int
	ft_hmove_key_nosize(t_htable *dst, t_htable *src, char *key)
{
	DEBUGSTR(key);
	if (ft_hmove_key(dst, src, (void *)key, ft_strlen(key)))
		return (-1);
	return (0);
}

/*
** Si n'existe pas creer l'element dans 'dst'.
** Si existe dans dst lors de la creation:
**  met a jour, free l'ancien element et retourne -1.
*/

int
	ft_hmoveadd_key_nosize(t_htable *dst, t_htable *src, char *key, char *data)
{
	void	*olddata;

	DEBUGSTR(key);
	DEBUGSTR(data);
	ft_hmove_key(dst, src, (void *)key, ft_strlen(key));
	{
		DEBUGSTR(data);
		if ((olddata = ft_hupd_nosize(dst, key, data)))
		{
			DEBUGSTR(olddata);
			free(olddata);
			return (-1);
		}
	}
	return (0);
}
