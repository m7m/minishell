/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashtotab.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 15:23:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 15:23:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Retourne un tab depuis un tableau de hachage
*/

static void
	ft_hashtotab__conv(char **ptabh, t_hash *next, size_t *nb_elem)
{
	DEBUGSTR(next->key);
	DEBUGNBR(next->key_len);
	DEBUGSTR(next->data);
	DEBUGNBR(next->data_len);
	ft_memcpy(*ptabh, next->key, next->key_len);
	(*ptabh)[next->key_len] = '=';
	ft_memcpy(*ptabh + next->key_len + 1, next->data, next->data_len);
	(*ptabh)[next->key_len + next->data_len + 1] = 0;
	--(*nb_elem);
	DEBUGSTR(*ptabh);
}

static char
	**ft_hashtotab__alloc(t_htable thtable)
{
	size_t	nb_elem;
	size_t	cell;
	char	**tabh;

	nb_elem = thtable.nb_elem + 1;
	cell = thtable.data_lenmax + thtable.key_lenmax + 3;
	tabh = (char **)ft_memalloc_array(sizeof(char), nb_elem, cell);
	ft_memchk_exit(tabh);
	tabh[thtable.nb_elem] = NULL;
	return (tabh);
}

char
	**ft_hashtotab(t_htable thtable)
{
	char	**tabh;
	char	**ptabh;
	t_hash	**thash;
	t_hash	*next;

	DEBUGNBR(thtable.nb_elem);
	DEBUGNBR(thtable.num_table);
	tabh = ft_hashtotab__alloc(thtable);
	ptabh = tabh;
	thash = thtable.hdb->thash;
	while (thtable.nb_elem)
	{
		if ((next = *thash))
			while (next)
			{
				if (BANDEQ(thtable.num_table, next->num_table))
				{
					ft_hashtotab__conv(ptabh, next, &thtable.nb_elem);
					++ptabh;
				}
				next = next->next;
			}
		++thash;
	}
	return (tabh);
}
