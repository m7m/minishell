/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashto_keyeqdata.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 23:29:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 23:29:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void
	ft_hashto_keyeqdata__rpl(void **buff, void *kd, size_t len, char c_end)
{
	ft_memcpy(*buff, kd, len);
	((char *)(*buff))[len] = c_end;
	*buff += len + 1;
}

/*
** data est un char donc data_len contient le \0
*/

static void
	ft_hashto_keyeqdata__r(void **buff, t_hash *e)
{
	size_t	len;

	ft_hashto_keyeqdata__rpl(buff, e->key, e->key_len, '=');
	len = e->data_len;
	if (len && !((char *)e->data)[len - 1])
		--len;
	ft_hashto_keyeqdata__rpl(buff, e->data, len, '\n');
}

/*
** Retourne la fin de buff
*/

static char
	*ft_hashto_keyeqdata__w(t_hash **thash, size_t nb_elem,
							void *buff, int num_table)
{
	t_hash	*e;
	size_t	i;

	DEBUGNBR(nb_elem);
	DEBUGNBR(num_table);
	i = 0;
	while (i < nb_elem)
	{
		if (*thash)
		{
			e = *thash;
			while (e)
			{
				if (BANDEQ(num_table, e->num_table))
				{
					ft_hashto_keyeqdata__r(&buff, e);
					++i;
				}
				e = e->next;
			}
		}
		++thash;
	}
	return (buff);
}

/*
** Convertie la table en :
** key=data\n
*/

size_t
	ft_hashto_keyeqdata(t_htable thtable, void **buff)
{
	void	*vbuff;
	void	*pbuff;
	size_t	ibuff;

	DEBUGNBR(thtable.data_lenmax);
	DEBUGNBR(thtable.key_lenmax);
	DEBUGNBR(thtable.nb_elem);
	ibuff = (thtable.data_lenmax + thtable.key_lenmax + 2);
	ibuff *= thtable.nb_elem;
	DEBUGNBR(ibuff);
	ft_memchk_exit(vbuff = malloc(SC((ibuff + 1 + 6))));
	pbuff = ft_hashto_keyeqdata__w(thtable.hdb->thash, thtable.nb_elem, vbuff,
							thtable.num_table);
	DEBUG;
	*buff = vbuff;
	return (pbuff - vbuff);
}

void
	ft_hashto_keyeqdata_write(t_htable thtable)
{
	void		*buff;
	size_t		ibuff;

	if ((ibuff = ft_hashto_keyeqdata(thtable, &buff)))
		write(STDOUT_FILENO, buff, ibuff);
	DEBUG;
	free(buff);
	DEBUG;
}
