/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hupd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:32:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:32:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_hash.h"

/*
** Retourne l'ancienne valeur si changement il y a
** Remplace les valeurs de data dans hupd apres mise a jour
** Duplique key si nouvelle
** Duplique data.
*/

static void
	ft_hupd__upd(t_htable *thtable, t_hupdate *hupd, t_hash *thash)
{
	DEBUGNBR(hupd->data_len);
	if (hupd->data_len)
	{
		thash->data = ft_memdup(hupd->data, hupd->data_len);
		ft_memchk_exit(thash->data);
	}
	else
		thash->data = NULL;
	DEBUGPTR(thash->data);
	thash->data_len = hupd->data_len;
	if (thtable->data_lenmax < hupd->data_len)
	{
		DEBUGSTR((char *)hupd->data);
		thtable->data_lenmax = hupd->data_len;
		ft_htab_lie_upd(thtable, hupd->data_len);
	}
}

void
	*ft_hupd(t_htable *thtable, t_hupdate *hupd)
{
	size_t	data_len;
	t_hash	*thash;
	void	*data;

	DEBUGSTR(hupd->key);
	DEBUGSTR(hupd->data);
	data = NULL;
	data_len = 0;
	if (!(thash = ft_hchr_th(*thtable, hupd->key, hupd->key_len)))
	{
		thash = ft_hadd(thtable, hupd->key, hupd->key_len);
		ft_hadd_data(thtable, thash, hupd->data, hupd->data_len);
	}
	else if (ft_hchk_deqd(thash, hupd->data, hupd->data_len,
						thtable->num_table))
	{
		DEBUGNBR(thash->data_len);
		DEBUGPTR(thash->data);
		data = thash->data;
		data_len = thash->data_len;
		ft_hupd__upd(thtable, hupd, thash);
	}
	hupd->data = data;
	hupd->data_len = data_len;
	return (data);
}

/*
** Met a jour la donnee et free l'ancienne
*/

void
	ft_hupd_n(t_htable *thtable, void *key, size_t key_len, char *data)
{
	t_hupdate	hupd;

	hupd.key = key;
	hupd.key_len = key_len;
	hupd.data = (void *)data;
	if (data)
		hupd.data_len = ft_strlen(data) + 1;
	else
		hupd.data_len = 0;
	DEBUGSTR(hupd.key);
	DEBUGNBR(hupd.key_len);
	DEBUGSTR(hupd.data);
	DEBUGNBR(hupd.data_len);
	if (ft_hupd(thtable, &hupd))
		free(hupd.data);
}

/*
** Met a jour si existe
*/

void
	*ft_hupd_isexist(t_htable *thtable, t_hupdate *hupd)
{
	DEBUG;
	if (ft_hchr(*thtable, hupd->key, hupd->key_len, NULL))
		return (ft_hupd(thtable, hupd));
	DEBUGSTR("inexiste");
	return (NULL);
}
