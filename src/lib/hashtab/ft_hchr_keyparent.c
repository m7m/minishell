/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hchr_keyparent.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 00:17:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/23 00:17:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Retourne le parent de key
** key doit existe
** si key represente la premiere maille alors retourne NULL
** ne prend pas en compte le numero de table
*/

t_hash
	*ft_hchr_keyparent(t_hash *hnoeud, void *key, size_t key_len, int ntable)
{
	t_hash	*tmp;

	DEBUG;
	tmp = ft_hchr_noeudkey(hnoeud, key, key_len, ntable);
	if (hnoeud == tmp)
		return (NULL);
	while (hnoeud->next && hnoeud->next != tmp)
		hnoeud = hnoeud->next;
	if (hnoeud->next)
		return (hnoeud);
	return (NULL);
}
