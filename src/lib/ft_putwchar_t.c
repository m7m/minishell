/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar_t.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/28 08:35:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/28 08:35:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int
	ft_putwchar_t(int c)
{
	size_t	i;

	i = 0;
	while (i < sizeof(int) && ((char *)&c)[i])
		++i;
	if (i)
		i = write(STDOUT_FILENO, &c, i);
	return (i);
}
