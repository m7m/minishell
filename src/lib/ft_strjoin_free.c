/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_free.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 11:55:26 by mmichel           #+#    #+#             */
/*   Updated: 2017/01/27 11:55:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strjoin_free1(char **s1, const char *s2)
{
	char	*tmp;

	tmp = ft_strjoin(*s1, s2);
	ft_strdel(s1);
	return (tmp);
}

char	*ft_strjoin_free2(const char *s1, char **s2)
{
	char	*tmp;

	tmp = ft_strjoin(s1, *s2);
	ft_strdel(s2);
	return (tmp);
}

char	*ft_strjoin_free12(char **s1, char **s2)
{
	char	*tmp;

	tmp = ft_strjoin(*s1, *s2);
	ft_strdel(s1);
	ft_strdel(s2);
	return (tmp);
}
