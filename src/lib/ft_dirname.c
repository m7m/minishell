/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dirname.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:51:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:51:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Retourne le parent de 'path'
** si path finit par un '/' retourne path
*/

static char
	*ft_dirname__alloc(int ipath, char *path, char *tmp)
{
	char	*dir;

	if ((dir = ft_strnew(ipath)))
	{
		if (!tmp)
			*dir = '.';
		else
		{
			ft_memcpy(dir, path, ipath);
		}
		dir[ipath] = 0;
	}
	DEBUGSTR(dir);
	return (dir);
}

char
	*ft_dirname(char *path)
{
	int		ipath;
	char	*tmp;

	DEBUGSTR(path);
	if ((tmp = ft_strrchr(path, '/')))
	{
		DEBUGSTR(tmp);
		if (tmp != path)
			ipath = (tmp - path);
		else
		{
			tmp = "/";
			ipath = 1;
		}
	}
	else
		ipath = 2;
	return (ft_dirname__alloc(ipath, path, tmp));
}
