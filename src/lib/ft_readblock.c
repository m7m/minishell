/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readblock.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 21:54:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/30 21:54:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int
	ft_readline__wif(char **line, char tmp[])
{
	char	*tmp2;

	if (*line)
	{
		if (!(tmp2 = ft_strjoin(*line, tmp)))
			return (-1);
		free(*line);
	}
	else if (!(tmp2 = ft_strdup(tmp)))
		return (-1);
	*line = tmp2;
	return (0);
}

static int
	ft_readline__w(int fd, char **line)
{
	int		n;
	int		i;
	char	tmp[BUFF_SIZE + 2];

	ft_memset(tmp, 0, BUFF_SIZE + 1);
	*line = NULL;
	n = 1;
	i = 0;
	while (n && (n = read(fd, tmp, BUFF_SIZE)) > 0)
	{
		DEBUGNBR(n);
		i += n;
		if (ft_readline__wif(line, tmp))
			return (-1);
		if (n != BUFF_SIZE)
			n = 0;
		ft_memset(tmp, 0, BUFF_SIZE + 1);
	}
	DEBUGNBR(n);
	if (n == -1)
		return (-1);
	return (i);
}

int
	ft_readblock(int fd, char **line)
{
	int	ret;

	DEBUG;
	if (fd < 0 || !line)
		return (-1);
	DEBUG;
	*line = NULL;
	ret = ft_readline__w(fd, line);
	DEBUGNBR(ret);
	if (ret < 0)
	{
		free(*line);
		*line = NULL;
		return (-1);
	}
	return (ret);
}
