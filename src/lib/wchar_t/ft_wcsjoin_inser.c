/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsjoin_inser.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:55:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:55:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

wchar_t	*ft_wcsjoin_inser(wchar_t const *s1, wchar_t const *s2, int ins)
{
	int		lens1;
	int		lens2;
	wchar_t	*tmp;
	wchar_t	*s1s2;

	lens1 = ft_wcslen(s1);
	lens2 = ft_wcslen(s2);
	DEBUGNBR(lens1);
	DEBUGNBR(ins);
	s1s2 = NULL;
	if (lens1 >= ins
		&& (s1s2 = (wchar_t *)malloc(sizeof(wchar_t) * (lens1 + lens2 + 1))))
	{
		ft_memcpy(s1s2, s1, SW(ins));
		tmp = s1s2 + ins;
		ft_memcpy(tmp, s2, SW(lens2 + 1));
		tmp += lens2;
		ft_memcpy(tmp, s1 + ins, SW((lens1 - ins) + 1));
		s1s2[lens1 + lens2] = 0;
	}
	return (s1s2);
}
