/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 07:54:44 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/17 23:34:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_wchar_t.h"
#include <stdlib.h>

wchar_t	*ft_wcsjoin(wchar_t const *s1, wchar_t const *s2)
{
	int		lens1;
	int		lens2;
	wchar_t	*tmp;
	wchar_t	*s1s2;

	lens1 = ft_wcslen(s1);
	lens2 = ft_wcslen(s2);
	if (!(s1s2 = (wchar_t *)malloc(sizeof(wchar_t) * (lens1 + lens2 + 1))))
		return (NULL);
	if (s1s2)
	{
		ft_wcscpy(s1s2, s1);
		tmp = s1s2 + lens1;
		ft_wcscpy(tmp, s2);
		s1s2[lens1 + lens2] = 0;
	}
	return (s1s2);
}
