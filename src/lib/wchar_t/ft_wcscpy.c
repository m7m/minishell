/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcscpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:51:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:51:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_wchar_t.h"
#include "libft.h"

wchar_t	*ft_wcscpy(wchar_t *dst, const wchar_t *src)
{
	size_t	ls;

	ls = ft_wcslen(src);
	ft_memcpy(dst, src, SW(ls + 1));
	return (dst);
}
