/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mbslen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:52:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:52:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

/*
** Retourne la taille
** en prennant en compte les caractères larges convertie en char
*/

static size_t
	ft_mbslen__w(size_t i, wchar_t *wc, unsigned char *s)
{
	DEBUG;
	while ((s[i] & 0xC0) == 0x80 && i < sizeof(wchar_t))
	{
		*wc |= (unsigned)s[i] << (i * sizeof(char) * 8);
		++i;
	}
	return (i);
}

size_t
	ft_mbslen(char *str)
{
	size_t			len;
	size_t			i;
	wchar_t			wc;
	unsigned char	*s;

	s = (unsigned char *)str;
	len = 0;
	if (s)
		while (*s)
		{
			if (*s > 0x7F)
			{
				wc = *s;
				i = ft_mbslen__w(1, &wc, s);
				if (ft_isutf8(wc))
					s += i - 1;
			}
			++s;
			++len;
		}
	DEBUGNBR(len);
	return (len);
}
