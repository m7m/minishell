/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mbsnlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:53:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:53:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

/*
** Retourne un pointeur sur la position n
** en fonction de la taille des cactères
** retourne la position sur le 'n'ieme caractere.
*/

static size_t
	ft_mbsnlen__w(size_t i, wchar_t *wc, unsigned char *s)
{
	while ((s[i] & 0xC0) == 0x80 && i < sizeof(wchar_t))
	{
		*wc |= (unsigned)s[i] << (i * sizeof(char) * 8);
		++i;
	}
	return (i);
}

char
	*ft_mbsnlen(char *str, size_t n)
{
	size_t			len;
	size_t			i;
	wchar_t			wc;
	unsigned char	*s;

	DEBUGSTR(str);
	s = (unsigned char *)str;
	len = 0;
	if (s)
		while (*s && len < n)
		{
			if (*s > 0x7F)
			{
				wc = *s;
				i = ft_mbsnlen__w(1, &wc, s);
				if (ft_isutf8(wc))
					s += i - 1;
			}
			++s;
			++len;
		}
	DEBUGNBR(len);
	return ((char *)s);
}
