/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:43:09 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/30 13:44:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Dupliquer une chaine
*/

#include "libft.h"
#include "pr_wchar_t.h"

wchar_t	*ft_wcsdup(const wchar_t *s1)
{
	int		len;
	wchar_t	*dus1;

	len = ft_wcslen(s1) + 1;
	dus1 = (wchar_t *)malloc(sizeof(wchar_t) * (len));
	if (dus1)
		ft_memcpy(dus1, s1, SW(len));
	return (dus1);
}
