/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcswcs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 18:02:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/29 18:02:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Renvoie un pointeur sur la première occurrence
**  du caractère c dans la chaîne s.
*/

wchar_t	*ft_wcswcs(const wchar_t *s1, const wchar_t *s2)
{
	int i;

	i = 0;
	if (!*s2)
		return ((wchar_t *)s1);
	while (*s1)
	{
		while (*s1 == *s2)
		{
			if (!*s1 || !*s2)
				return ((wchar_t *)s1 - i);
			++i;
			++s1;
			++s2;
		}
		if (!*s2 && i)
			return ((wchar_t *)s1 - i);
		s1 -= i - 1;
		s2 -= i;
		i = 0;
	}
	return (NULL);
}
