/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcschr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:50:25 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:50:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stddef.h>

wchar_t	*ft_wcschr(const wchar_t *s, int c)
{
	while (*s)
		if (*s == c)
			return ((wchar_t *)s);
		else
			++s;
	if (c)
		return (NULL);
	return ((wchar_t *)s);
}
