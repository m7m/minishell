/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsreplace.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 19:04:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/15 19:04:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <pr_wchar_t.h>

/*
** Recherche 'c' dans 's' puis remplace 'c' par 'replace'
** Si 'replace' vaut 0 supprime tout les occurrence de c dans s
*/

void
	ft_wcsreplace(wchar_t *s, wchar_t c, wchar_t replace)
{
	wchar_t	*ps;

	if (replace)
		while ((s = ft_wcschr(s, c)))
		{
			*s = replace;
			++s;
		}
	else
	{
		ps = s;
		while (*s)
		{
			if (*s == c)
				++s;
			else
			{
				*ps = *s;
				++s;
				++ps;
			}
		}
		*ps = *s;
	}
}

/*
** Duplique s puis recherche motif et le remplace par replace
*/

static wchar_t
	*ft_wcsreplaces__realloc(int len, int i, wchar_t *r)
{
	wchar_t	*tmp;

	tmp = r;
	if (r)
	{
		DEBUGWCS(r);
		r = ft_memdup_resize(r, SW(len), SW(i + 1));
		free(tmp);
	}
	else
		r = (wchar_t *)malloc(SW(len + i + 1));
	if (!r)
	{
		free(tmp);
		return (NULL);
	}
	r[len + i] = 0;
	DEBUG;
	return (r);
}

wchar_t
	*ft_wcsreplaces(const wchar_t *s,
					const wchar_t *motif,
					const wchar_t *replace)
{
	int		lm;
	int		lr;
	int		len;
	wchar_t	*r;
	wchar_t	*ps;

	DEBUG;
	lm = ft_wcslen(motif);
	lr = ft_wcslen(replace);
	r = 0;
	len = 0;
	while ((ps = ft_wcswcs(s, motif)))
	{
		if (!(r = ft_wcsreplaces__realloc(len, (ps - s) + lr, r)))
			return (NULL);
		ft_memcpy(r + len, s, SW(ps - s));
		len += (ps - s);
		ft_memcpy(r + len, replace, SW(lr));
		len += lr;
		s = ps + lm;
	}
	lm = ft_wcslen(s);
	if ((r = ft_wcsreplaces__realloc(len, lm, r)))
		ft_memcpy(r + len, s, SW(lm + 1));
	return (r);
}

wchar_t
	*ft_wcsnreplaces(const wchar_t *s,
					const wchar_t *motif,
					const wchar_t *replace,
					size_t n)
{
	int		lm;
	int		lr;
	int		len;
	wchar_t	*r;
	wchar_t	*ps;

	lm = ft_wcslen(motif);
	lr = ft_wcslen(replace);
	r = 0;
	len = 0;
	while (n && (ps = ft_wcsnwcs(s, motif, n)))
	{
		if (!(r = ft_wcsreplaces__realloc(len, (ps - s) + lr, r)))
			return (NULL);
		ft_memcpy(r + len, s, SW(ps - s));
		len += (ps - s);
		ft_memcpy(r + len, replace, SW(lr));
		len += lr;
		n -= (ps - s) + lm;
		s = ps + lm;
	}
	lm = ft_wcslen(s);
	if ((r = ft_wcsreplaces__realloc(len, lm, r)))
		ft_memcpy(r + len, s, SW(lm + 1));
	return (r);
}
