/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mbstowcs_alloc.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:56:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:56:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_debug.h"

#include <stdlib.h>
#include <stddef.h>
#include "pr_wchar_t.h"

/*
** Convertion char en wchar_t
*/

static unsigned char
	*ft_mbstowcs_alloc__isutf(wchar_t *wc, unsigned char *s)
{
	size_t	i;

	DEBUG;
	i = 1;
	while ((s[i] & 0xC0) == 0x80 && i < sizeof(wchar_t))
	{
		*wc |= (unsigned)s[i] << (i * sizeof(char) * 8);
		++i;
	}
	if (ft_isutf8(*wc))
		s += i - 1;
	return (s);
}

wchar_t
	*ft_mbstowcs_alloc(char *str)
{
	size_t			j;
	wchar_t			*wcs;
	wchar_t			wc;
	unsigned char	*s;

	DEBUG;
	wcs = NULL;
	j = 0;
	s = (unsigned char *)str;
	if (s && (wcs = (wchar_t *)malloc(sizeof(wchar_t) * (ft_mbslen(str) + 1))))
	{
		while (*s)
		{
			wc = (wchar_t)*s;
			if (wc > 0x7F)
				s = ft_mbstowcs_alloc__isutf(&wc, s);
			wcs[j] = wc;
			++j;
			++s;
		}
		wcs[j] = 0;
	}
	return (wcs);
}

/*
** Converti s dans wcs de max n
** fonction non testé et non évalué
*/

size_t
	ft_mbstowcs(wchar_t *wcs, char *src, size_t n)
{
	size_t			j;
	wchar_t			wc;
	unsigned char	*s;

	DEBUG;
	j = 0;
	s = (unsigned char *)src;
	if (s && wcs)
	{
		while (*s && j < n)
		{
			wc = (wchar_t)*s;
			if (wc > 0x7F)
				s = ft_mbstowcs_alloc__isutf(&wc, s);
			wcs[j] = wc;
			++j;
			++s;
		}
		wcs[j] = 0;
	}
	return (j);
}
