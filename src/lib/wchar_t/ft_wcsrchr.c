/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 00:12:26 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/04 22:09:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stddef.h>

/*
** Renvoie un pointeur sur la dernière occurrence
**  du caractère c dans la chaîne s.
*/

wchar_t	*ft_wcsrchr(const wchar_t *s, int c)
{
	wchar_t *ret;

	ret = NULL;
	while (*s)
	{
		if (*s == c)
			ret = (wchar_t *)s;
		++s;
	}
	if (c)
		return (ret);
	return ((wchar_t *)s);
}
