/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcstombs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 08:01:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 08:01:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

char	*ft_wcstombs_alloc(wchar_t *wcs)
{
	size_t	i;
	size_t	len;
	char	*str;
	char	*tmp;

	DEBUG;
	str = NULL;
	if (wcs)
	{
		len = ft_wcslen(wcs) * sizeof(wchar_t);
		if ((str = ft_strnew(sizeof(char) * (len + 1))))
		{
			tmp = (char *)wcs;
			i = -1;
			while ((size_t)((char *)tmp - (char *)wcs) < len)
			{
				if (*tmp)
					str[++i] = *tmp;
				++tmp;
			}
		}
	}
	return (str);
}

/*
** Retourne le nombre de caractére de 'src' écrit dans 'dest'
*/

size_t	ft_wcstombs(char *dest, wchar_t *src, size_t n)
{
	size_t	j;
	char	*tmp;
	char	*pdest;
	wchar_t	*psrc;

	DEBUG;
	psrc = src;
	if (src)
	{
		pdest = dest;
		while (*psrc && (size_t)(pdest - dest) < n)
		{
			tmp = (char *)psrc;
			j = 0;
			while (j < sizeof(wchar_t))
			{
				if (tmp[j])
					*(pdest++) = tmp[j];
				++j;
			}
			++psrc;
		}
		*pdest = 0;
	}
	return ((size_t)((psrc - src)));
}
