/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsnwcs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:56:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:56:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_wchar_t.h"

/*
** locate a substring in a string
** voir optimisation sur 'ms1'
*/

wchar_t	*ft_wcsnwcs(const wchar_t *s1, const wchar_t *s2, size_t n)
{
	size_t	i;
	size_t	ii;
	size_t	lens2;
	size_t	ms1;

	ms1 = 0;
	ii = 0;
	lens2 = ft_wcslen(s2);
	i = 0;
	while (s1[ii] && s2[i] && ii < n)
	{
		if (s1[ii] == s2[i])
			++i;
		else
		{
			ii = ms1++;
			i = 0;
		}
		++ii;
	}
	if (i == lens2)
		return ((wchar_t *)s1 + (ii - i));
	return (NULL);
}
