/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcssub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:57:44 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:57:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

/*
** Alloue (avec malloc(3)) et retourne la copie "fraiche" d'un troncon
** de la chaine de caracteres passee en parametre.
** Le troncon commence a index start et a pour longueur len.
** Si start et len ne designent pas un troncon de chaine valide,
** le comportement est indetermine. Si allocation echoue,
** la fonction renvoie NULL.
*/

/*
** Substitut dans 's' à partir de 'start' de taille 'len'
*/

wchar_t	*ft_wcssub(wchar_t const *s, unsigned int start, size_t len)
{
	wchar_t		*tronc;

	tronc = (wchar_t *)malloc(sizeof(wchar_t) * (len + 1));
	if (tronc)
	{
		ft_memcpy(tronc, s + start, SW(len));
		tronc[len] = 0;
	}
	return (tronc);
}
