/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsrword.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:58:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:58:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pr_wchar_t.h"

/*
** Cherche le debut du précédent mot
** retourne le caractere precedent le mot
** un le debut de la chaine
*/

wchar_t
	*ft_wcsrword(wchar_t *s)
{
	int	pass;
	int	pass_anc;
	int	len;

	pass = 0;
	pass_anc = 0;
	len = ft_wcslen(s);
	while (len)
	{
		--len;
		if (!(pass = ft_iswalnum(s[len])) && pass_anc)
			return (s + len + 1);
		else if (pass)
			pass_anc = pass;
	}
	DEBUG;
	return (s);
}
