/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcschrnull.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:50:47 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:50:47 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** La fonction strchrnul() est comme strchr()
** excepté que si c n'est pas trouvé dans s,
** elle renvoie un pointeur sur l'octet nul à la fin de s,
** plutôt que NULL.
*/

wchar_t	*ft_wcschrnull(const wchar_t *s, int c)
{
	while (*s)
		if (*s == c)
			return ((wchar_t *)s);
		else
			++s;
	return ((wchar_t *)s);
}
