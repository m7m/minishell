/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iswalnum.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:49:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:49:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_wchar_t.h"

int	ft_iswalnum(wint_t wc)
{
	if ((47 < wc && wc < 58) || (64 < wc && wc < 91) || (96 < wc && wc < 123))
		return (1);
	else if (wc > 0x80 && ft_isutf8(wc))
		return (1);
	return (0);
}
