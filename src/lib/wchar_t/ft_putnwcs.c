/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnwcs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 07:42:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/01 07:42:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "l_sh.h"
#include "pr_wchar_t.h"

void
	ft_putnwcs(wchar_t *wcs, int n)
{
	size_t	i;
	size_t	j;
	char	buff[514];

	DEBUGNBR(n);
	ft_memset(buff, 0, 513);
	i = 0;
	while (n >= 512)
	{
		DEBUGWCS(wcs + i);
		j = ft_wcstombs(buff, wcs + i, 512);
		write(STDOUT_FILENO, buff, ft_strlen(buff));
		i += j;
		n -= j;
	}
	j = 1;
	while (n && j)
	{
		j = ft_wcstombs(buff, wcs + i, n);
		DEBUGNBR(j);
		DEBUGSTR(buff);
		write(STDOUT_FILENO, buff, ft_strlen(buff));
		i += j;
		n -= j;
	}
}
