/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsword.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:57:17 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:57:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_wchar_t.h"

/*
** Cherche la fin du prochain mot
** retourne le caractere suivant le mot
** ou la fin de chaine
*/

wchar_t
	*ft_wcsword(wchar_t *s)
{
	int	pass;
	int	pass_anc;

	pass = 0;
	pass_anc = 0;
	while (*s)
	{
		if (!(pass = ft_iswalnum(*s)) && pass_anc)
			return (s);
		else if (pass)
			pass_anc = pass;
		++s;
	}
	return (s);
}
