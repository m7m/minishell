/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_array.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 11:36:06 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/18 11:36:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Alloc un tableau 1 dimension
** ligne : de ty_l (ex: sizeof(char *)) de nb_l (nombre de ligne)
** collonne : de ty_c (ex: sizeof(char)) de ta_col
**  (taille de la collonne (taille a l'interieure d'une ligne))
*/

void	**ft_malloc_array(size_t ty_l, size_t nb_l, size_t ty_c, size_t ta_col)
{
	void	**list;
	void	*col;
	size_t	i;

	list = (void **)malloc(ty_l * nb_l + ty_c * ta_col * nb_l);
	col = (void *)(list + (nb_l + 1));
	i = 0;
	while (i < nb_l)
	{
		list[i] = col + ta_col * i * ty_c;
		++i;
	}
	return (list);
}
