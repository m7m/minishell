/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_multi.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 21:44:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/24 21:44:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Prend en parametre le tableau contenant une liste de mot
** + la longuer de se tableau
** + le nouveau mot a ajouter
** retour: un tableau contenant la liste des mots + le nouveau
*/

static	char
	**ft_strsplit_multi_pp(char **listmots, size_t len, char *addmot)
{
	char	**tmplistmots;
	size_t	i;

	i = 0;
	if (!(tmplistmots = (char **)ft_memalloc(sizeof(char *) * (len + 2))))
		return (NULL);
	tmplistmots[len] = addmot;
	tmplistmots[len + 1] = 0;
	while (i < len)
	{
		tmplistmots[i] = listmots[i];
		++i;
	}
	free(listmots);
	return (tmplistmots);
}

static	char
	**ft_strsplit_multi_add(char **lmotssplit, char const *s,
							int im, int nbmots)
{
	char	*nmotssplit;

	if (!(nmotssplit = ft_strsub((char *)s - im, 0, im)))
		return (NULL);
	lmotssplit = ft_strsplit_multi_pp(lmotssplit,
								nbmots,
								nmotssplit);
	return (lmotssplit);
}

static char
	**ft_strsplit__alloc(char *s)
{
	char	**lmotssplit;

	ft_memchk_exit(lmotssplit = (char **)ft_memalloc(sizeof(char *) * 2));
	*lmotssplit = (char *)s;
	return (lmotssplit);
}

char
	**ft_strsplit_multi(char const *s, char *cmulti, int nmulti)
{
	char	**lmotssplit;
	int		im;
	int		nbmots;

	lmotssplit = ft_strsplit__alloc((char *)s);
	nbmots = 0;
	im = 0;
	while (*s && lmotssplit)
	{
		if (!ft_memchr(cmulti, *s, nmulti))
			++im;
		else if (im >= 1)
		{
			lmotssplit = ft_strsplit_multi_add(lmotssplit, s, im, nbmots++);
			im = 0;
		}
		++s;
	}
	if (im)
		lmotssplit = ft_strsplit_multi_add(lmotssplit, s, im, nbmots);
	else if (!nbmots && !im)
		*lmotssplit = 0;
	return (lmotssplit);
}

char
	**ft_strsplit_multiandend(char const *s, char *cmulti, int nmulti,
							char *cend)
{
	char	**lmotssplit;
	int		im;
	int		nbmots;
	int		len;

	lmotssplit = ft_strsplit__alloc((char *)s);
	len = ft_strlen(cend) + 1;
	nbmots = 0;
	im = 0;
	while (*s && !ft_memchr(cend, (int)*s, len) && lmotssplit)
	{
		if (!ft_memchr(cmulti, (int)*s, nmulti))
			++im;
		else if (im >= 1)
		{
			lmotssplit = ft_strsplit_multi_add(lmotssplit, s, im, nbmots++);
			im = 0;
		}
		++s;
	}
	if (im)
		lmotssplit = ft_strsplit_multi_add(lmotssplit, s, im, nbmots);
	else if (!nbmots && !im)
		*lmotssplit = 0;
	return (lmotssplit);
}
