/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pass_space.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/20 23:43:55 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/20 23:43:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char
	*ft_pass_space(char *s)
{
	while (*s && (*s == ' ' || *s == '\t'))
		++s;
	return (s);
}

char
	*ft_pass_space_rev(char *start, char *s)
{
	while (s > start && (*s == ' ' || *s == '\t'))
		--s;
	return (s);
}

char
	*ft_pass_spacenewline(char *s)
{
	while (*s && (*s == ' ' || *s == '\t' || *s == '\n'))
		++s;
	return (s);
}

char
	*ft_pass_spacenewline_rev(char *start, char *s)
{
	while (s > start && (*s == ' ' || *s == '\t' || *s == '\n'))
		--s;
	return (s);
}
