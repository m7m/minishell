/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopendir.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 17:21:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 17:21:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "const_file.h"

static int
	ft__filtre__dot(const char *file_name, mode_t st_mode, int filtre)
{
	if (S_ISDIR(st_mode) && *file_name == '.')
	{
		if (filtre & GET_DOTDOT
			|| (((*(file_name + 1) != 0 && *(file_name + 1) != '.')
					|| ((*(file_name + 1) == '.' && *(file_name + 2) != 0)))))
			return (1);
	}
	else
		return (1);
	return (0);
}

static int
	ft__filtre__file(const char *file_name, mode_t st_mode, int filtre)
{
	if (S_ISDIR(st_mode))
	{
		DEBUGSTR(file_name);
		if (filtre & GET_DIR && *file_name != '.')
			return (1);
		else if (filtre & GET_DOTDIR && *file_name == '.')
			return (1);
	}
	if (!S_ISDIR(st_mode))
	{
		DEBUGSTR(file_name);
		if (filtre & GET_FILE && *file_name != '.')
			return (1);
		else if (filtre & GET_DOTFILE && *file_name == '.')
			return (1);
	}
	return (0);
}

static int
	ft__filtre__perm(const char *path_file_name, int filtre)
{
	int	ret;

	DEBUGNBR(filtre);
	ret = 255;
	if (filtre & GET_PERM_IGN)
		ret = 0;
	else if (filtre & GET_PERM_RO)
		ret = access(path_file_name, R_OK);
	else if (filtre & GET_PERM_WO)
		ret = access(path_file_name, W_OK);
	else if (filtre & GET_PERM_XO)
		ret = access(path_file_name, X_OK);
	else
	{
		if (filtre & GET_PERM_R)
			ret = access(path_file_name, R_OK);
		if (ret && filtre & GET_PERM_W)
			ret = access(path_file_name, W_OK);
		if (ret && filtre & GET_PERM_X)
			ret = access(path_file_name, X_OK);
	}
	if (!ret)
		return (1);
	DEBUGNBR(ret);
	return (0);
}

static void
	ft_readdir_to_tab(DIR *dirp, const char *path, int opt,
					void (*f)(const char *, int, char *, int))
{
	struct dirent	*dp_cur;
	struct stat		stat_cur;
	int				len;
	char			path_file[FILENAME_MAX + 1];
	int				len_file;

	DEBUGSTR(path);
	len = ft_strlen(path);
	ft_memcpy(path_file, path, len + 1);
	if (len && path_file[len - 1] == '/')
		--len;
	path_file[len] = '/';
	while ((dp_cur = readdir(dirp)))
	{
		len_file = ft_strlen(dp_cur->d_name);
		DEBUGSTR(dp_cur->d_name);
		ft_memcpy(path_file + len + 1, dp_cur->d_name, len_file + 1);
		DEBUGSTR(path_file);
		if (!stat(path_file, &stat_cur)
			&& ft__filtre__dot(dp_cur->d_name, stat_cur.st_mode, opt)
			&& ft__filtre__file(dp_cur->d_name, stat_cur.st_mode, opt)
			&& ft__filtre__perm(path_file, opt))
			f(path, len, dp_cur->d_name, len_file);
	}
	closedir(dirp);
}

/*
** opt = flags pour filter les fichier/dossier et permission
**  valeur de opt voir: "const_file.h"
** f = path, path_len, file_name
** Retourne:
** -1 erreur sur opendir
** 3 si path n'est ni un fichier et ni un dossier
** 2 si erreur
** 1 si path est un fichier
** sinon 0
*/

int
	ft_getopendir(const char *path, int opt,
				void (*f)(const char *, int, char *, int))
{
	struct stat		path_stat;
	DIR				*dirp;

	DEBUG;
	if (!stat(path, &path_stat))
	{
		if (S_ISDIR(path_stat.st_mode))
		{
			if ((dirp = opendir(path)))
			{
				ft_readdir_to_tab(dirp, path, opt, f);
				return (0);
			}
			return (-1);
		}
		else if (S_ISREG(path_stat.st_mode))
			return (1);
		else
			return (3);
	}
	return (2);
}
