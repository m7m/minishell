/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readlink.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 18:33:53 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/11 18:33:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

ssize_t
	ft_readlink(const char *pathname, char **buff)
{
	ssize_t	i;
	ssize_t	len;

	len = 4095;
	ft_memchk_exit(*buff = ft_strnew(4096));
	while ((i = readlink(pathname, *buff, len)) == len)
	{
		free(*buff);
		len += 4095;
		ft_memchk_exit(*buff = ft_strnew(len));
	}
	return (i);
}
