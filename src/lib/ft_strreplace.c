/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strreplace.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:55:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:55:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Remplace chaque occurence de 'c' par 'replace' dans 's'
*/

#include "libft.h"

/*
** Recherche 'c' dans 's' puis remplace 'c' par 'replace'
** Si 'replace' vaut 0 supprime tout les occurrence de c dans s
*/

void
	ft_strreplace(char *s, char c, char replace)
{
	char	*ps;

	if (replace)
		while ((s = ft_strchr(s, c)))
		{
			*s = replace;
			++s;
		}
	else
	{
		ps = s;
		while (*s)
		{
			if (*s == c)
				++s;
			else
			{
				*ps = *s;
				++s;
				++ps;
			}
		}
		*ps = *s;
	}
}

/*
** Duplique s puis recherche motif et le remplace par replace
*/

static char
	*ft_strreplaces__realloc(int len, int i, char *r)
{
	char	*tmp;

	tmp = r;
	if (r)
	{
		DEBUGSTR(r);
		r = ft_memdup_resize(r, SC(len), SC(i + 1));
		free(tmp);
	}
	else
		r = (char *)malloc(SC(len + i + 1));
	if (!r)
	{
		free(tmp);
		return (NULL);
	}
	r[len + i] = 0;
	DEBUG;
	return (r);
}

char
	*ft_strreplaces(const char *s, const char *motif, const char *replace)
{
	int		lm;
	int		lr;
	int		len;
	char	*r;
	char	*ps;

	DEBUG;
	lm = ft_strlen(motif);
	lr = ft_strlen(replace);
	r = 0;
	len = 0;
	while ((ps = ft_strstr(s, motif)))
	{
		if (!(r = ft_strreplaces__realloc(len, (ps - s) + lr, r)))
			return (NULL);
		ft_memcpy(r + len, s, SC(ps - s));
		len += (ps - s);
		ft_memcpy(r + len, replace, SC(lr));
		len += lr;
		s = ps + lm;
	}
	lm = ft_strlen(s);
	if ((r = ft_strreplaces__realloc(len, lm, r)))
		ft_memcpy(r + len, s, SC(lm + 1));
	return (r);
}

char
	*ft_strnreplaces(const char *s, const char *motif, const char *replace,
					size_t n)
{
	size_t	lm;
	size_t	lr;
	size_t	len;
	char	*r;
	char	*ps;

	lm = ft_strlen(motif);
	lr = ft_strlen(replace);
	r = 0;
	len = 0;
	while (n && (ps = ft_strnstr(s, motif, n)))
	{
		if (!(r = ft_strreplaces__realloc(len, (ps - s) + lr, r)))
			return (NULL);
		ft_memcpy(r + len, s, SC(ps - s));
		len += (ps - s);
		ft_memcpy(r + len, replace, SC(lr));
		len += lr;
		n -= (ps - s) + lm;
		s = ps + lm;
	}
	lm = ft_strlen(s);
	if ((r = ft_strreplaces__realloc(len, lm, r)))
		ft_memcpy(r + len, s, SC(lm + 1));
	return (r);
}
