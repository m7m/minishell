/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_inser.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/26 00:25:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/26 00:25:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_inser(char const *s1, char const *s2, int ins)
{
	int		lens1;
	int		lens2;
	char	*tmp;
	char	*s1s2;

	lens1 = ft_strlen(s1);
	lens2 = ft_strlen(s2);
	if (!(s1s2 = (char *)malloc(sizeof(char) * (lens1 + lens2 + 1))))
		return (NULL);
	if (s1s2)
	{
		ft_strncpy(s1s2, s1, ins);
		tmp = s1s2 + ins;
		ft_strcpy(tmp, s2);
		tmp += lens2;
		ft_strcpy(tmp, s1 + ins);
		s1s2[lens1 + lens2] = 0;
	}
	return (s1s2);
}
