/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/21 01:33:32 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/21 01:33:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_tabsize(char **tab)
{
	size_t	i;
	char	**ptab;

	if (!tab)
		return (0);
	ptab = tab;
	i = 0;
	while (*ptab)
	{
		i += ft_strlen(*ptab) + 1;
		++ptab;
	}
	i = sizeof(char *) * (ptab - tab) + sizeof(char *) * i;
	return (i);
}
