/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mv.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 20:08:16 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/11 20:08:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void
	*ft_mv(void **dst, void **src)
{
	*dst = *src;
	*src = NULL;
	return (*dst);
}

char
	*ft_mvstr(char **dst, char **src)
{
	*dst = *src;
	*src = NULL;
	return (*dst);
}
