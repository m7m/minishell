/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strpcmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 15:07:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/27 15:07:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Retourne l'index des caractere different
** si aucun caractere diffrent alors -1
*/

int	ft_strpcmp(const char *s1, const char *s2)
{
	int	i;

	i = 0;
	while ((s1[i] || s2[i]) && s1[i] == s2[i])
		++i;
	if ((unsigned char)s1[i] - (unsigned char)s2[i])
		return (i);
	return (-1);
}
