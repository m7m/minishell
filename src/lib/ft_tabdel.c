/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 20:36:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/28 20:36:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Retire et free l'element de trouvant a l'"index"
** puis decale tout les elements se trouvant apres l'index a -1
*/

void
	ft_tabdel(char **tab, int index)
{
	int	i;

	i = index;
	while (tab[i])
	{
		if (i > index)
		{
			tab[i - 1] = tab[i];
		}
		else if (i == index)
			free(tab[i]);
		++i;
	}
	tab[i - 1] = tab[i];
}
