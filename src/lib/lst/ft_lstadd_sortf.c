/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_sortf.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/08 06:58:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/08 06:58:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_list.h"

t_list
	*ft_lstadd_sortf(t_list *lst, t_list *new, int (*cmp)(t_list *, t_list *))
{
	t_list	*plst;

	if (!lst)
		return (new);
	plst = lst;
	if (!cmp(lst, new))
	{
		new->next = lst;
		return (new);
	}
	while (lst->next)
	{
		if (!cmp(lst->next, new))
		{
			new->next = lst->next;
			lst->next = new;
			return (plst);
		}
		lst = lst->next;
	}
	lst->next = new;
	return (plst);
}
