/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 09:56:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/25 12:58:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_lst.h"

/*
** Ajout a la fin
*/

t_list		*ft_lstadd(t_list **plist, t_list *new)
{
	t_list	*tmp;

	DEBUG;
	if (*plist)
	{
		tmp = *plist;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	else
		*plist = new;
	DEBUG;
	return (new);
}
