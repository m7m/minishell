/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 11:04:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/24 14:08:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_lst.h"

t_list	*ft_lstnew(void)
{
	t_list	*mail;

	if (!(mail = (t_list *)ft_memalloc(sizeof(t_list))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	DEBUGPTR(mail);
	return (mail);
}
