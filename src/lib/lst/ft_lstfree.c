/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 10:48:20 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/01 10:48:20 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_lst.h"

/*
** free tout les maillons
*/

void
	ft_lstfree(t_list *lstlist)
{
	t_list	*tmp;

	while (lstlist)
	{
		tmp = lstlist;
		lstlist = lstlist->next;
		free(tmp);
	}
}
