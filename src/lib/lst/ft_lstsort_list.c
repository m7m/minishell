/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort_list.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 09:42:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/01 09:42:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_list.h"

static void
	ft_lstsort_list__norm(t_list **lst, int (*cmp)(t_list *, t_list *),
					t_list **plst, t_list **ret)
{
	t_list	*tmpprev;
	t_list	*tmpnext;

	tmpprev = *lst;
	tmpnext = (*lst)->next;
	while (tmpnext)
	{
		if (!cmp(*lst, tmpnext))
		{
			if (*ret == *lst)
			{
				*ret = tmpnext;
				*plst = tmpnext;
			}
			else
				(*plst)->next = tmpnext;
			tmpprev->next = tmpnext->next;
			tmpnext->next = *lst;
			*lst = tmpnext;
		}
		else
			tmpprev = tmpnext;
		tmpnext = tmpnext->next;
	}
}

t_list
	*ft_lstsort_list(t_list *lst, int (*cmp)(t_list *, t_list *))
{
	t_list	*plst[1];
	t_list	*ret[1];

	*plst = lst;
	*ret = *plst;
	while (lst)
	{
		ft_lstsort_list__norm(&lst, cmp, plst, ret);
		*plst = lst;
		lst = lst->next;
	}
	return (*ret);
}
