/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnewadd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 09:51:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/19 09:51:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_lst.h"

t_list		*ft_lstnewadd(t_list **plist)
{
	t_list	*new;
	t_list	*tmp;

	DEBUG;
	new = ft_lstnew();
	if (*plist)
	{
		tmp = *plist;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	else
		*plist = new;
	DEBUG;
	return (new);
}
