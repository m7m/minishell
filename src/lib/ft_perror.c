/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perror.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 07:16:12 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/29 07:16:12 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "signal.h"

void
	ft_perror_signal(int sign)
{
	if (SIGHUP == sign)
		ft_putstr("Child: Hangup: ");
	else if (SIGINT == sign)
		ft_putstr("Child: Exit: ");
	else if (SIGQUIT == sign)
		ft_putstr("Child: Quit: ");
	else if (SIGILL == sign)
		ft_putstr("Child: SIGILL: ");
	else if (SIGABRT == sign)
		ft_putstr("Child: Abort: ");
	else if (SIGKILL == sign)
		ft_putstr("Child: Killed: ");
	else if (SIGSEGV == sign)
		ft_putstr("Child: Segmentation fault: ");
	else if (SIGUSR1 == sign)
		ft_putstr("Child: SIGUSR1: ");
	else if (SIGUSR2 == sign)
		ft_putstr("Child: SIGUSR2: ");
	else if (SIGSTOP == sign)
		ft_putstr("Child: SIGSTOP: ");
	else
		ft_putstr("Child: Signal: ");
	ft_putnbr(sign);
	ft_putchar('\n');
}
