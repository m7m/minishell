/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_cd__.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/22 12:16:39 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/22 12:16:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_builtins_cd__error(char *path, int errcode, t_htable *hlocal)
{
	if (errcode == 1)
		MSG_ERR1(CD_NAME, CD_USAGES);
	else if (errcode == 2)
		MSG_ERR3(CD_NAME, CD_ERR_OLD);
	else if (errcode == 3)
		MSG_ERR3(CD_NAME, CD_ERR_HOM);
	else
		MSG_ERR4("cd", path, CD_ERR_NA);
	ft_updatekey_ret(hlocal, 1);
}

int
	ft_builtins_cd__opt_tree(int opt, char c_opt)
{
	DEBUG;
	if (!opt && c_opt == 'P')
		opt |= FL_BUI_CD_P;
	else if (!opt && c_opt == 'L')
		opt |= FL_BUI_CD_L;
	else
		opt |= FL_BUI_CD_H;
	return (opt);
}

void
	ft_builtins_cd____upd(t_htable *hlocal, t_builtins_cd tcd)
{
	char		*oldpwd;
	size_t		oldpwd_len;
	t_hupdate	hupd;
	char		*tmp;

	DEBUGSTR(tcd.newpwd);
	oldpwd = NULL;
	oldpwd_len = ft_hchr(*hlocal, "PWD", SC(3), (void **)&oldpwd);
	hupd.key = "PWD";
	hupd.key_len = SC(3);
	hupd.data = tcd.newpwd;
	hupd.data_len = tcd.newpwd_len;
	tmp = NULL;
	if (ft_hupd(hlocal, &hupd))
		tmp = hupd.data;
	hupd.key = "OLDPWD";
	hupd.key_len = SC(6);
	hupd.data = oldpwd;
	hupd.data_len = oldpwd_len;
	if (ft_hupd(hlocal, &hupd))
		free(hupd.data);
	DEBUGPTR(tmp);
	free(tmp);
}
