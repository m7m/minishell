/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_local_min.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 23:36:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/07/23 23:36:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void
	ft_local_min___itoa(t_htable *thtable, void *key, size_t key_len,
							int colines)
{
	t_hupdate		hupd;
	char			cc_itoa[128];

	hupd.key = key;
	hupd.key_len = key_len;
	hupd.data_len = ft_itoa_s(colines, cc_itoa);
	hupd.data = cc_itoa;
	DEBUGSTR(hupd.data);
	if (ft_hupd_isexist(thtable, &hupd))
		free(hupd.data);
}

static void
	ft_local_min__ps1(t_htable *thtable)
{
	size_t	i;
	char	*ps1;
	void	*data;

	DEBUG;
	data = NULL;
	if ((i = ft_hchr(*thtable, "USER", 4, &data)))
	{
		ft_memchk_exit(ps1 = (char *)malloc(SC(i + 6)));
		if (!((char *)data)[i - 1])
			--i;
		DEBUGSTR(data);
		ft_memcpy(ps1, data, i);
		if (ft_memcmp(ps1, "root", SC(5)))
			ft_memcpy(ps1 + i, "$> ", SC(4));
		else
			ft_memcpy(ps1 + i, "#> ", SC(4));
		data = ps1;
	}
	else
		ps1 = NAME_PROG "$> ";
	DEBUGSTR(ps1);
	ft_hadd_n(thtable, "PS1", SC(3), ps1);
	free(data);
}

static void
	ft_local_min__pwd(t_htable *thtable)
{
	t_hupdate	hupd;
	char		*dir;

	DEBUG;
	dir = NULL;
	if (ft_perror_getcwd(&dir))
		return ;
	else if (!ft_hchr(*thtable, "PWD", SC(3), NULL))
	{
		hupd.key = (void *)"PWD";
		hupd.key_len = SC(3);
		hupd.data = (void *)dir;
		if (dir)
			hupd.data_len = ft_strlen(dir) + 1;
		else
			hupd.data_len = 0;
		ft_hupd(thtable, &hupd);
	}
	free(dir);
}

void
	ft_local_min__colline(t_htable *hlocal)
{
	struct winsize	argp;

	DEBUG;
	if (!ioctl(STDOUT_FILENO, TIOCGWINSZ, &argp))
	{
		DEBUG;
		ft_local_min___itoa(hlocal, (void *)"LINES", SC(5), argp.ws_row);
		ft_local_min___itoa(hlocal, (void *)"COLUMNS", SC(7), argp.ws_col);
	}
}

void
	ft_local_min(t_htable *hlocal)
{
	DEBUG;
	ft_local_min__colline(hlocal);
	ft_local_min__ps1(hlocal);
	ft_local_min__pwd(hlocal);
	ft_local_min__ifs(hlocal);
}
