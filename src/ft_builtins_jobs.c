/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_jobs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/21 00:10:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/21 00:10:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include "t_builtins.h"
#include "t_sh.h"
#include "l_sh.h"
#include "libft.h"
#include "flags.h"
#include "../inc/jobs.h"
#include "../inc/built_in_jobs.h"

static int
	ft_builtins_jobs__opt_tree(int opt, char copt)
{
	if (copt == 'l')
		opt |= JOBS_L;
	else if (copt == 'n')
		opt |= JOBS_N;
	else if (copt == 'p')
		opt |= JOBS_P;
	else if (copt == 'r')
		opt |= JOBS_R;
	else if (copt == 's')
		opt |= JOBS_S;
	else if (copt == 'x')
		opt |= JOBS_X;
	else
		opt = -1;
	return (opt);
}

static void
	ft_builtins_jobs__pri(int opt, int i, t_builtins_job *tjobs)
{
	if (opt & JOBS_L)
		ft_printf("[%d] ", i);
	if (opt & JOBS_P || opt == JOBS_L)
		ft_printf("%d", tjobs[i].pc);
	if (opt & JOBS_L)
	{
		if (tjobs[i].status == JOB_RUNNING && opt & JOBS_R)
			ft_printf("\tstatus: running");
		else if (tjobs[i].status == JOB_TERMINATED)
			ft_printf("\tstatus: termined");
		else if (tjobs[i].status == JOB_STOPPED && opt & JOBS_S)
			ft_printf("\tstatus: stopped");
		else if (opt & JOBS_S)
			ft_printf("\tstatus: unknown");
	}
}

static void
	ft_builtins_jobs__l(int opt, t_builtins_job *tjobs)
{
	int		i;
	char	**argv;

	i = -1;
	DEBUG;
	while (++i < JOBS_MAX && tjobs[i].pc > -1)
		if (tjobs[i].pc)
			if ((tjobs[i].status != JOB_STOPPED && opt & JOBS_R)
				|| (tjobs[i].status != JOB_RUNNING && opt & JOBS_S)
				|| opt & JOBS_P)
			{
				ft_builtins_jobs__pri(opt, i, tjobs);
				if (opt & JOBS_L)
				{
					ft_printf("\t cmd:");
					argv = tjobs[i].argv;
					while (*argv)
					{
						ft_printf(" %s", *argv);
						++argv;
					}
				}
				ft_printf("\n");
			}
	ft_fflush(STDOUT_FILENO);
}

void
	ft_builtins_jobs(t_sh *sh, t_argv *targv)
{
	int				opt;
	char			**pargv;

	if (sh->tljobs.tjobs)
	{
		opt = 0;
		pargv = ft_parse_argopt(targv->argc - 1, targv->argv + 1, &opt,
								ft_builtins_jobs__opt_tree);
		if (!opt)
			opt = JOBS_L;
		DEBUGNBR(opt & JOBS_L);
		DEBUGNBR(opt & JOBS_N);
		DEBUGNBR(opt & JOBS_P);
		DEBUGNBR(opt & JOBS_R);
		DEBUGNBR(opt & JOBS_S);
		DEBUGNBR(opt & JOBS_X);
		ft_builtins_jobs__l(opt, sh->tljobs.tjobs);
		(void)pargv;
		free(pargv);
	}
}
