/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read_1b1.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 16:14:49 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/08 23:57:41 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_check_var(int start, char **av, int *error, int verbose)
{
	if (av[start] && !ft_check_valid_identifiers(av[start]))
	{
		if (verbose)
			MSG_ERR4("read", av[start], "not a valid identifier");
		if (error)
			*error = 1;
		return (BUILTIN_FALSE);
	}
	return (0);
}

void
	ft_buff_to_local(int reply, char **av, int i, char *tmp)
{
	if (reply)
		ft_setlocal("REPLY", tmp);
	else
	{
		ft_setlocal(av[i++], tmp);
		while (av[i])
		{
			ft_setlocal(av[i], "");
			i++;
		}
	}
}

static int
	ft_make_var(t_read *tread, t_read_data *rd, char **argv, int *i)
{
	if (ft_strchr(tread->sep, rd->buff[0]))
	{
		if (rd->escape)
			return (0);
		if (!rd->error)
		{
			if (ft_check_var(*i, argv, &rd->error, 0) > 0)
				return (BUILTIN_FALSE);
		}
		if (rd->error)
			return (BUILTIN_FALSE);
		if (tread->bign != -1 || tread->reply || *i == rd->ac)
			rd->tmp = ft_strjoinf(&(rd->tmp), " ");
		else
		{
			ft_setlocal(argv[*i], rd->tmp);
			ft_memdel((void**)&(rd->tmp));
			(*i)++;
		}
	}
	else
		rd->tmp = ft_strjoinf(&(rd->tmp), rd->buff);
	(rd->count)++;
	rd->prev = rd->buff[0];
	return (0);
}

static int
	ft_treat_end_buff(t_read_data *rd, int reply, int i, int ret)
{
	if (ret < 0)
	{
		ft_memdel((void**)&(rd->tmp));
		return (130);
	}
	if (rd->error)
	{
		ft_memdel((void**)&(rd->tmp));
		MSG_ERR4("read", rd->av[i], "not a valid identifier");
		return (BUILTIN_FALSE);
	}
	ft_buff_to_local(reply, rd->av, i, rd->tmp);
	ft_memdel((void**)&(rd->tmp));
	return (BUILTIN_TRUE);
}

int
	ft_read_1b1(t_read *tread, int i, int ac, char **av)
{
	t_read_data	rd;
	int			ret;

	ft_bzero(&rd, sizeof(t_read_data));
	rd.ac = ac - 1;
	while ((ret = read(tread->fd, &(rd.buff), B_READ_SIZE)) > 0)
	{
		if (rd.buff[0] == tread->delim && !rd.escape && tread->bign == -1)
			break ;
		if (!tread->r && rd.buff[0] == '\\' && !rd.escape)
		{
			rd.escape = 1;
			continue ;
		}
		ft_make_var(tread, &rd, av, &i);
		if (rd.count == tread->max_char || rd.count == tread->bign)
			break ;
		if (rd.buff[0] == '\n' && rd.escape)
			ft_putstr("> ");
		rd.escape = (rd.escape) ? 0 : rd.escape;
	}
	rd.av = av;
	return (ft_treat_end_buff(&rd, tread->reply, i, ret));
}
