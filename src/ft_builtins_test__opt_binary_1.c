/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_binary_1.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 14:38:36 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:01:44 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test__opt_binary_nt(int op, char **argv)
{
	struct stat		st;
	time_t			l_mtime;

	ft_bzero(&st, sizeof(struct stat));
	if (stat(argv[op - 1], &st) != 0)
		return (BUILTIN_FALSE);
	l_mtime = st.st_mtime;
	if (stat(argv[op + 1], &st) != 0)
		return (BUILTIN_FALSE);
	return (l_mtime <= st.st_mtime);
}

int
	ft_builtins_test__opt_binary_ot(int op, char **argv)
{
	struct stat		st;
	time_t			l_mtime;

	ft_bzero(&st, sizeof(struct stat));
	if (stat(argv[op - 1], &st) != 0)
		return (BUILTIN_FALSE);
	l_mtime = st.st_mtime;
	if (stat(argv[op + 1], &st) != 0)
		return (BUILTIN_FALSE);
	return (l_mtime >= st.st_mtime);
}

int
	ft_builtins_test__opt_binary_ef(int op, char **argv)
{
	struct stat		st_left;
	struct stat		st_right;

	ft_bzero(&st_left, sizeof(struct stat));
	ft_bzero(&st_right, sizeof(struct stat));
	return (!(stat(argv[op - 1], &st_left) == 0 &&
			stat(argv[op + 1], &st_right) == 0 &&
			st_left.st_dev == st_right.st_dev &&
			st_left.st_ino == st_right.st_ino));
}

int
	ft_builtins_test__opt_binary_eq(int op, char **argv)
{
	if (!ft_islong(argv[op - 1]))
	{
		MSG_ERR4("test", argv[op - 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	if (!ft_islong(argv[op + 1]))
	{
		MSG_ERR4("test", argv[op + 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	return (!(ft_atol(argv[op - 1]) == ft_atol(argv[op + 1])));
}
