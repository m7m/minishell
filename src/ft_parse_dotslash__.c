/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_dotslash__.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 18:38:25 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/11 18:38:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"
#include "libft.h"
#include "../inc/l_sh.h"

static void
	ft_parse_dotslash__cutdirslash__link_joint(
		char **pwd, char *linksymbolique)
{
	char	*tmp;

	DEBUGSTR(linksymbolique);
	if (*linksymbolique == '/')
	{
		free(*pwd);
		*pwd = linksymbolique;
	}
	else
	{
		if ((tmp = ft_strrchr(*pwd, '/')))
			tmp[1] = 0;
		*pwd = ft_strjoin_free12(pwd, &linksymbolique);
		ft_memchk_exit(*pwd);
	}
}

/*
** *pwd = "/var"
** dir = "../usr"
** *pwd_len = 4
*/

int
	ft_parse_dotslash__cutdirslash__link(
		char **pwd, size_t *pwd_len, char *dir)
{
	int			i;
	struct stat	stru_stat;
	char		*linksymbolique;
	char		*tmp;

	DEBUG;
	i = 0;
	while (!lstat(*pwd, &stru_stat) && S_ISLNK(stru_stat.st_mode)
	&& ft_readlink((const char *)*pwd, &linksymbolique) != -1)
	{
		if (++i > 7)
		{
			MSG_ERR4("cd", *pwd, "Too many levels of symbolic links");
			return (-1);
		}
		ft_parse_dotslash__cutdirslash__link_joint(pwd, linksymbolique);
		*pwd_len = ft_strlen(*pwd);
		tmp = *pwd;
		*pwd = ft_strdup_resize(
			*pwd, *pwd_len, ft_strlen(dir) + sizeof(char) * 2);
		free(tmp);
		ft_memchk_exit(*pwd);
		DEBUGSTR(*pwd);
	}
	return (0);
}
