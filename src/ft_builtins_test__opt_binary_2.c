/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_test__opt_binary_2.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:28:28 by sdahan            #+#    #+#             */
/*   Updated: 2017/01/01 11:03:34 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_builtins_test__opt_binary_ne(int op, char **argv)
{
	if (!ft_islong(argv[op - 1]))
	{
		MSG_ERR4("test", argv[op - 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	if (!ft_islong(argv[op + 1]))
	{
		MSG_ERR4("test", argv[op + 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	return (!(ft_atol(argv[op - 1]) != ft_atol(argv[op + 1])));
}

int
	ft_builtins_test__opt_binary_gt(int op, char **argv)
{
	if (!ft_islong(argv[op - 1]))
	{
		MSG_ERR4("test", argv[op - 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	if (!ft_islong(argv[op + 1]))
	{
		MSG_ERR4("test", argv[op + 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	return (!(ft_atol(argv[op - 1]) > ft_atol(argv[op + 1])));
}

int
	ft_builtins_test__opt_binary_ge(int op, char **argv)
{
	if (!ft_islong(argv[op - 1]))
	{
		MSG_ERR4("test", argv[op - 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	if (!ft_islong(argv[op + 1]))
	{
		MSG_ERR4("test", argv[op + 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	return (!(ft_atol(argv[op - 1]) >= ft_atol(argv[op + 1])));
}

int
	ft_builtins_test__opt_binary_lt(int op, char **argv)
{
	if (!ft_islong(argv[op - 1]))
	{
		MSG_ERR4("test", argv[op - 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	if (!ft_islong(argv[op + 1]))
	{
		MSG_ERR4("test", argv[op + 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	return (!(ft_atol(argv[op - 1]) < ft_atol(argv[op + 1])));
}

int
	ft_builtins_test__opt_binary_le(int op, char **argv)
{
	if (!ft_islong(argv[op - 1]))
	{
		MSG_ERR4("test", argv[op - 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	if (!ft_islong(argv[op + 1]))
	{
		MSG_ERR4("test", argv[op + 1], "integer expression expected");
		return (BUILTIN_FAILURE);
	}
	return (!(ft_atol(argv[op - 1]) >= ft_atol(argv[op + 1])));
}
