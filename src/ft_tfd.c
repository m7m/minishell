/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tfd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 11:48:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/03 11:48:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "tfd.h"

t_fd
	*ft_tfd_newadd(t_fd **node)
{
	t_fd	*tfd;

	tfd = *node;
	ft_memchk_exit(*node = (t_fd *)ft_memalloc(sizeof(t_fd)));
	(*node)->next = tfd;
	return (*node);
}

void
	ft_tfd_free(t_fd *node)
{
	t_fd	*tmp;

	while (node)
	{
		tmp = node->next;
		if (node->str)
			free((void *)node->str);
		free(node);
		node = tmp;
	}
}

static int
	ft_tfd_sort_cmp_opt(t_fd *t1, t_fd *t2)
{
	if (!(t1->opt & SFD_CREATE) && t2->opt & SFD_CREATE)
		return (0);
	if (!(t1->opt & (SFD_OPEN | SFD_CREATE)) && t2->opt & SFD_OPEN)
		return (0);
	return (1);
}

void
	ft_tfd_sort(t_fd **node)
{
	*node = ft_lstsort_list(*node, ft_tfd_sort_cmp_opt);
}
