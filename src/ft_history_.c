/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history_.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 18:09:37 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/05 18:09:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/l_sh.h"
#include "../inc/pr_history.h"
#include "../inc/t_history.h"
#include <libft.h>

void
	ft_history_parser__last_cmd(
		int index, size_t size, char **line, char **pline)
{
	size_t	data_len;
	char	*data;
	char	*tmp;

	DEBUGNBR(index);
	DEBUGNBR(size);
	DEBUGSTR(*line);
	data = NULL;
	data_len = ft_gethistory_id_nodup(index, &data);
	ft_strcpy(*pline - 1, *pline + size);
	if (data_len)
	{
		tmp = *line;
		DEBUGSTR(data);
		*line = ft_strjoin_inser(*line, data, (*pline - 1) - *line);
		free(tmp);
		ft_memchk_exit(*line);
		DEBUGSTR(*line);
		*pline = *line + ((*pline + data_len - 1) - tmp);
		DEBUGSTR(*pline);
	}
	else
		*pline = *pline - 1;
}

int
	ft_history_parser__digit(
		int rev, char **line, char **pline)
{
	int		key;
	size_t	data_len;
	char	*data;
	char	*tmp;

	key = ft_atoi(*pline + rev);
	if (rev)
		key = g_thistory.thhistory->nb_elem - (key - 1);
	DEBUGNBR(key);
	DEBUGSTR(*line);
	data_len = ft_gethistory_id_nodup(key, &data);
	tmp = *pline - 1;
	*pline += ft_intlen(key) + rev;
	ft_strcpy(tmp, *pline);
	if (!data_len)
		return (-1);
	key = tmp - *line;
	tmp = *line;
	DEBUGSTR(data);
	*line = ft_strjoin_inser(*line, data, key);
	free(tmp);
	ft_memchk_exit(*line);
	*pline = *line + data_len + key;
	return (0);
}

static char
	*ft_history_parser__chrword__end(
		int start, char **pline)
{
	char	*end;

	if (start)
	{
		end = *pline + 1;
		DEBUGSTR(end);
		while (*end && *end != '?')
		{
			if (*end == '\\' && end[1])
				++end;
			++end;
		}
		*end = 0;
	}
	else
	{
		end = *pline;
		while (*end && *end != ' ' && *end != '\t')
			++end;
	}
	return (end);
}

int
	ft_history_parser__chrword(
		int start,
		char **line, char **pline,
		size_t (*fhistchr)(char *, int))
{
	size_t	size;
	size_t	index;
	char	*end;

	DEBUG;
	end = ft_history_parser__chrword__end(start, pline);
	if ((index = fhistchr(*pline + start, 0)))
	{
		DEBUGSTR(*pline);
		size = end - (*pline - 1);
		ft_history_parser__last_cmd(index, size, line, pline);
		return (0);
	}
	return (-1);
}
