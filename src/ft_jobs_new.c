/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_jobs_new.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/09 10:08:48 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/09 10:08:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/built_in_jobs.h"

static t_builtins_job
	*ft_builtins__jobs_alloc(void)
{
	int				i;
	t_builtins_job	*ptjobs;

	DEBUGNBR(JOBS_MAX);
	i = JOBS_MAX;
	ptjobs = (t_builtins_job *)ft_memalloc(sizeof(t_builtins_job) * JOBS_MAX);
	ft_memchk_exit(ptjobs);
	DEBUGPTR(ptjobs);
	ptjobs[--i].pc = -1;
	return (ptjobs);
}

t_builtins_job
	*ft_jobs_newalloc(void)
{
	return (ft_builtins__jobs_alloc());
}
