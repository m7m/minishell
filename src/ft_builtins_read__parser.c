/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_read__parser.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <sdahan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 15:14:07 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/04 11:00:43 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_check_valid_identifiers(char *var)
{
	int		i;

	if (var[0] != '_' && !ft_isalpha(var[0]))
		return (0);
	i = 0;
	while (var[++i])
	{
		if (!ft_isalpha(var[i]) && var[i] != '_' && !ft_isdigit(var[i]))
		{
			return (0);
		}
	}
	return (1);
}

static int
	ft_display_error(void)
{
	MSG_ERR4("read", "-", "not a valid identifier");
	return (BUILTIN_FAILURE);
}

int
	ft_parser_opt(t_opt_read *opt, t_read *read, char **argv, int *i)
{
	int		j;
	int		k;
	int		ret;

	while (argv[++(*i)] && argv[*i][0] == '-')
	{
		k = 1;
		j = -1;
		if (!argv[*i][1])
			return (ft_display_error());
		while (opt[++j].opt)
		{
			if (argv[*i][k] == opt[j].opt)
			{
				if ((ret = opt[j].f(read, argv, i, k++)) > 0)
					return (ret);
				if (!ret)
					break ;
				j = -1;
			}
		}
		if (!opt[j].opt)
			return (ft_read_usage(argv[*i], "invalid option"));
	}
	return (0);
}
