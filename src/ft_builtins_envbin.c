/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_envbin.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 23:58:31 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/27 23:58:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_builtins_envbin(t_sh *sh, t_argv *targv)
{
	(void)targv;
	ft_hashto_keyeqdata_write(sh->hexec);
	ft_updatekey_ret(&sh->hlocal, 0);
}
