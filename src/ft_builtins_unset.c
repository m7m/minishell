/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_unset.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 22:10:31 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/08 22:10:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "targv.h"

static void
	ft_builtins_unset__arg(t_htable *thtable, t_argv *targv)
{
	int	i;

	i = 1;
	while (targv->argv[i])
	{
		DEBUGNBR(thtable->nb_elem);
		ft_hdelkey_table(thtable, targv->argv[i], ft_strlen(targv->argv[1]));
		DEBUGNBR(thtable->nb_elem);
		++i;
	}
}

static void
	ft_builtins_unset_(t_htable *thtable, t_argv *targv, char *name)
{
	if (targv->argc == 1)
	{
		MSG_ERR3(name, UNSETENV_ERR_FEW);
		return ;
	}
	if (!ft_strcmp(targv->argv[1], "-a"))
		ft_hdel_table_cont(thtable);
	else
		ft_builtins_unset__arg(thtable, targv);
}

void
	ft_builtins_unsetenv(t_sh *sh, t_argv *targv)
{
	DEBUGSTR(*targv->argv);
	DEBUGPTR(sh);
	ft_builtins_unset_(&sh->henvp, targv, "unsetenv");
	ft_correla_path(&sh->hexec, &sh->hcorrela, sh->henvp);
	ft_updatekey_ret(&sh->hlocal, 0);
}

void
	ft_builtins_unset(t_sh *sh, t_argv *targv)
{
	DEBUGSTR(*targv->argv);
	DEBUGPTR(sh);
	ft_builtins_unset_(&sh->hlocal, targv, "unset");
	ft_updatekey_ret(&sh->hlocal, 0);
}
