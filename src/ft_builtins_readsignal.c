/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins_readsignal.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdahan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 16:31:33 by sdahan            #+#    #+#             */
/*   Updated: 2017/05/11 10:21:07 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <sys/time.h>
#include "minishell.h"

static void
	ft_manager_read_sig(int sig, siginfo_t *siginfo, void *context)
{
	(void)sig;
	(void)context;
	(void)siginfo;
}

void
	ft_run_timeout(long sec)
{
	struct itimerval it;

	ft_bzero(&it, sizeof(struct itimerval));
	it.it_value.tv_sec = sec;
	it.it_value.tv_usec = 0;
	it.it_interval.tv_sec = 0;
	it.it_interval.tv_usec = 0;
	setitimer(ITIMER_REAL, &it, 0);
}

void
	ft_readreset_sig(void)
{
	struct sigaction action;

	ft_memset(&action, '\0', sizeof(action));
	action.sa_handler = SIG_DFL;
	sigaction(SIGALRM, &action, NULL);
	ft_sigaction_init();
}

void
	ft_readinit_sig(void)
{
	struct sigaction action;

	ft_memset(&action, '\0', sizeof(action));
	action.sa_flags = SA_SIGINFO;
	action.sa_sigaction = &ft_manager_read_sig;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGALRM, &action, NULL);
}
