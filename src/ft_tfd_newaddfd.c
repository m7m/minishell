/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tfd_newaddfd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 07:13:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/11 07:13:59 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "tfd.h"

static t_fd
	*ft_tfd_newaddfdcommun(t_fd **node, int fd_rw, int fd)
{
	t_fd	*tfd;

	if ((tfd = ft_tfd_newadd(node)))
		tfd->pipe[fd_rw] = fd;
	return (tfd);
}

t_fd
	*ft_tfd_newaddfdr(t_fd **node, int fd)
{
	return (ft_tfd_newaddfdcommun(node, 0, fd));
}

t_fd
	*ft_tfd_newaddfdw(t_fd **node, int fd)
{
	return (ft_tfd_newaddfdcommun(node, 1, fd));
}
