/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gen_builtins_hash.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 08:43:54 by mmichel           #+#    #+#             */
/*   Updated: 2017/04/06 11:52:07 by sdahan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void
	ft_gen_builtins_hash_(t_htable *thtable, t_hash *thash)
{
	(void)thtable;
	(void)thash;
	COM("'");
	/*thash = ft_hadd(thtable, "[", 1);*/
	/*thash->data = (void *)ft_builtins_hook;*/
	/*thash->data_len = 1;*/
	;
}

void
	ft_gen_builtins_hash(t_htable *thtable)
{
	t_hash	*thash;

	DEBUG;
	GEN_BUILTINS_H(cd, 2);
	GEN_BUILTINS_H(setenv, 6);
	GEN_BUILTINS_H(unset, 5);
	GEN_BUILTINS_H(unsetenv, 8);
	GEN_BUILTINS_H(env, 3);
	GEN_BUILTINS_H(exit, 4);
	GEN_BUILTINS_H(pwd, 3);
	GEN_BUILTINS_H(help, 4);
	GEN_BUILTINS_H(envbin, 6);
	GEN_BUILTINS_H(echo, 4);
	GEN_BUILTINS_H(set, 3);
	GEN_BUILTINS_H(history, 7);
	GEN_BUILTINS_H(fg, 2);
	GEN_BUILTINS_H(jobs, 4);
	GEN_BUILTINS_H(bg, 2);
	GEN_BUILTINS_H(export, 6);
	GEN_BUILTINS_H(test, 4);
	GEN_BUILTINS_H(read, 4);
	/*GEN_BUILTINS_H(true, 4);
	GEN_BUILTINS_H(false, 5);*/
	ft_gen_builtins_hash_(thtable, thash);
}
