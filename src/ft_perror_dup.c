/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perror_dup.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/18 06:47:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/18 06:47:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int
	ft_perror_dup(int fd, const char *msg)
{
	close(fd);
	MSG_ERR3INT(fd, msg);
	return (-1);
}
