/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_surrogate__.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 06:44:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 06:44:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "pr_xetenvp.h"

char
	*ft_surrogate__inser(int i, int ivar, char *var, char **argv)
{
	char	*tmp;

	DEBUGSTR(*argv);
	DEBUGSTR(var);
	if (ivar && var)
	{
		tmp = *argv;
		*argv = ft_strjoin_inser(tmp, var, i);
		free(tmp);
		ft_memchk_exit(*argv);
	}
	tmp = *argv + ivar + i;
	DEBUGSTR(*argv);
	return (tmp);
}

/*
** Substitu dans argv a la position arg
** si notnull = 1
**  alors ne pas toucher a la variable si elle vaut null ou non trouve
** retourne un pointeur sur la nouvelle chaine,
**  qui pointe sur la fin de la substitution
*/

char
	*ft_surrogate__dol(char **argv, char *arg, int notnull)
{
	size_t	ivar;
	size_t	i;
	char	*var;

	DEBUG;
	i = ft_lenvar(arg + 1);
	DEBUGNBR(i);
	if (i)
	{
		var = NULL;
		if (!(ivar = ft_getnenvp(i, arg + 1, &var)))
			ivar = ft_getnlocal(i, arg + 1, &var);
		DEBUGSTR(arg);
		DEBUGNBR(notnull);
		DEBUGNBR(ivar);
		if (!notnull || (notnull && ivar))
		{
			ft_strcpy(arg, arg + i + 1);
			arg = ft_surrogate__inser(arg - *argv, ivar, var, argv);
		}
	}
	else
		++arg;
	return (arg);
}

/*
** Remplace le tilde se trouvant en premiere position de **argv
** retourne un pointeur sur la fin du substitu
*/

char
	*ft_surrogate__tilde(char **argv)
{
	size_t	ivar;
	size_t	i;
	char	*var;
	char	*arg;

	DEBUG;
	arg = *argv;
	ivar = ft_getenvp("HOME", &var);
	DEBUGSTR(arg);
	ft_strcpy(arg, arg + 1);
	if (ivar)
	{
		DEBUG;
		i = (arg - *argv);
		arg = ft_surrogate__inser(i, ivar, var, argv);
	}
	return (arg);
}

char
	*ft_surrogate__tilde2(char *arg, char **argv)
{
	size_t	ivar;
	size_t	i;
	char	*var;

	DEBUG;
	ivar = ft_getenvp("HOME", &var);
	DEBUGSTR(arg);
	ft_strcpy(arg, arg + 1);
	if (ivar)
	{
		DEBUG;
		i = (arg - *argv);
		arg = ft_surrogate__inser(i, ivar, var, argv);
	}
	return (arg);
}
