/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork__child_open__.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 20:05:30 by mmichel           #+#    #+#             */
/*   Updated: 2017/03/11 20:05:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pr_fork.h"
#include "etat/lastparse/ft_etat_lastparse.h"

static int
	ft_fork__child_open_xto_open__lastparse_err(
		t_linetoargv *tlta, t_sh *sh, t_argv *targv_sav)
{
	char	*msg;

	if (!tlta->line || !sh->targv->argc)
		msg = "nil argument";
	else if (sh->targv->argc > 1)
		msg = "multi argument";
	else
		msg = *sh->targv->argv;
	MSG_ERR3(msg, "ambiguous redirect");
	free(tlta->line);
	ft_targv_freeone(&sh->targv);
	sh->targv = targv_sav;
	return (-1);
}

static int
	ft_fork__child_open_xto_open__lastparse(t_fd *t, t_sh *sh)
{
	t_linetoargv	tlta;
	t_argv			*targv_sav;

	ft_memset(&tlta, 0, sizeof(tlta));
	tlta.sh = sh;
	ft_mvstr(&tlta.line, &t->str);
	tlta.pline = tlta.line;
	ft_mv((void **)&targv_sav, (void **)&sh->targv);
	ft_memchk_exit(ft_targv_newadd(&sh->targv));
	if (!tlta.line
		|| ft_etat_lastparse(&tlta)
		|| sh->targv->argc > 1
		|| !sh->targv->argc)
		return (ft_fork__child_open_xto_open__lastparse_err(
					&tlta, sh, targv_sav));
		t->str = *sh->targv->argv;
	*sh->targv->argv = NULL;
	free(tlta.line);
	ft_targv_freeone(&sh->targv);
	sh->targv = targv_sav;
	return (0);
}

int
	ft_fork__child_open_xto_open(t_fd *t, int fd_read, int fd_write, t_sh *sh)
{
	DEBUGSTR(t->str);
	DEBUGNBR(t->file_flags);
	DEBUGNBR(t->file_flags & O_CREAT);
	DEBUGNBR(t->file_flags & O_WRONLY);
	DEBUGNBR(t->file_flags & O_RDONLY);
	DEBUGNBR(t->file_flags & O_APPEND);
	if (ft_fork__child_open_xto_open__lastparse(t, sh))
		return (-1);
	DEBUGSTR(t->str);
	t->pipe[fd_write] = open(t->str, t->file_flags, S_IWUSR | S_IRUSR);
	DEBUGNBR(t->pipe[fd_write]);
	if (t->pipe[fd_write] == -1)
	{
		MSG_ERR3(t->str, "Error open file");
		return (-1);
	}
	DEBUGNBR(t->pipe[fd_read]);
	if (dup2(t->pipe[fd_write], t->pipe[fd_read]) == -1)
		return (ft_perror_dup(t->pipe[fd_write], "dup2() file descriptor"));
	return (0);
}
